namespace CoinTradePros.DAL.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CourseInstance")]
    public partial class CourseInstance
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CourseInstance()
        {
            CourseEnrollments = new HashSet<CourseEnrollment>();
        }

        public Guid Id { get; set; }

        [Required]
        [StringLength(200)]
        public string Name { get; set; }

        [Column(TypeName = "text")]
        public string Description { get; set; }

        public Guid CourseOfferingId { get; set; }

        public Guid OfferingCoachId { get; set; }

        public Guid InstanceTypeId { get; set; }

        public int Periods { get; set; }

        public int MaxStudents { get; set; }

        [StringLength(150)]
        public string Location { get; set; }

        [Column(TypeName = "money")]
        public decimal TotalCost { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CourseEnrollment> CourseEnrollments { get; set; }

        public virtual CourseInstanceType CourseInstanceType { get; set; }

        public virtual CourseOffering CourseOffering { get; set; }

        public virtual ProTraderProfile ProTraderProfile { get; set; }
    }
}
