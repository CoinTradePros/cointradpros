namespace CoinTradePros.DAL.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("QuestionAnswer")]
    public partial class QuestionAnswer
    {
        public Guid Id { get; set; }

        public Guid ProTraderProfileId { get; set; }

        [Required]
        [StringLength(250)]
        public string Question { get; set; }

        [Required]
        [StringLength(500)]
        public string Answer { get; set; }

        public byte SortOrder { get; set; }

        public virtual ProTraderProfile ProTraderProfile { get; set; }
    }
}
