namespace CoinTradePros.DAL.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CourseChapterExam")]
    public partial class CourseChapterExam
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CourseChapterExam()
        {
            ChapterExamQuestions = new HashSet<ChapterExamQuestion>();
            ChapterExamScores = new HashSet<ChapterExamScore>();
        }

        public Guid Id { get; set; }

        public Guid CourseChapterId { get; set; }

        [Column(TypeName = "numeric")]
        public decimal MinScore { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ChapterExamQuestion> ChapterExamQuestions { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ChapterExamScore> ChapterExamScores { get; set; }

        public virtual CourseChapter CourseChapter { get; set; }
    }
}
