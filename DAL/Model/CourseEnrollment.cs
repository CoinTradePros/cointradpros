namespace CoinTradePros.DAL.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CourseEnrollment")]
    public partial class CourseEnrollment
    {
        public Guid Id { get; set; }

        public Guid CourseInstanceId { get; set; }

        public DateTime EnrollmentDate { get; set; }

        public Guid TraderId { get; set; }

        public Guid? CurrentPageId { get; set; }

        public DateTime? LastAccessDate { get; set; }

        public virtual CourseInstance CourseInstance { get; set; }

        public virtual CoursePage CoursePage { get; set; }

        public virtual TraderProfile TraderProfile { get; set; }
    }
}
