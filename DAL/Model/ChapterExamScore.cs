namespace CoinTradePros.DAL.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ChapterExamScore")]
    public partial class ChapterExamScore
    {
        public Guid Id { get; set; }

        public Guid CourseChapterExamId { get; set; }

        public Guid TraderId { get; set; }

        [Column(TypeName = "numeric")]
        public decimal Score { get; set; }

        public DateTime ScoreDate { get; set; }

        public virtual CourseChapterExam CourseChapterExam { get; set; }

        public virtual TraderProfile TraderProfile { get; set; }
    }
}
