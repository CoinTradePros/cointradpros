namespace CoinTradePros.DAL.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("UserSetting")]
    public partial class UserSetting
    {
        [Key]
        [Column(Order = 0)]
        public Guid Id { get; set; }

        [StringLength(128)]
        public string UserId { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(50)]
        public string Platform { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(50)]
        public string SettingKey { get; set; }

        [Key]
        [Column(Order = 3)]
        [StringLength(250)]
        public string SettingValue { get; set; }
    }
}
