namespace CoinTradePros.DAL.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Trade")]
    public partial class Trade
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Trade()
        {
            TradeReactions = new HashSet<TradeReaction>();
        }

        public Guid Id { get; set; }

        public Guid TraderId { get; set; }

        public Guid DenominationId { get; set; }

        public Guid? ExchangeId { get; set; }

        public Guid AssetId { get; set; }

        public Guid? ProTraderProfileId { get; set; }

        public DateTimeOffset EntryDate { get; set; }

        public DateTimeOffset? ExitDate { get; set; }

        public decimal EntryPrice { get; set; }

        public decimal? ExitPrice { get; set; }

        public decimal PositionSize { get; set; }

        public decimal? ProfitLossAmount { get; set; }

        public decimal? ProfitLossPercent { get; set; }

        [Column(TypeName = "text")]
        public string Comments { get; set; }

        public bool AllowSocialSharing { get; set; }

        [StringLength(250)]
        public string ChartImagePath { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] PostDate { get; set; }

        [StringLength(100)]
        public string ExchangeExternalIdentifier { get; set; }

        public virtual Asset Asset { get; set; }

        public virtual Asset Asset1 { get; set; }

        public virtual Exchange Exchange { get; set; }

        public virtual ProTraderProfile ProTraderProfile { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TradeReaction> TradeReactions { get; set; }

        public virtual TraderProfile TraderProfile { get; set; }
    }
}
