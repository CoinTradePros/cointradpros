namespace CoinTradePros.DAL.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ConnectionTable")]
    public partial class ConnectionTable
    {
        public Guid ID { get; set; }

        [Required]
        public string ConnectionID { get; set; }

        [StringLength(128)]
        public string Users { get; set; }

        public virtual AspNetUser AspNetUser { get; set; }
    }
}
