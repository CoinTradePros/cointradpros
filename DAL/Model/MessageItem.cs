namespace CoinTradePros.DAL.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("MessageItem")]
    public partial class MessageItem
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public MessageItem()
        {
            MessageItem1 = new HashSet<MessageItem>();
        }

        public Guid Id { get; set; }

        public Guid MessageId { get; set; }

        [Required]
        [StringLength(20)]
        public string Channel { get; set; }

        public Guid MessageTransmitterId { get; set; }

        [Required]
        [StringLength(50)]
        public string ChannelIdentifier { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] SendDate { get; set; }

        [Column(TypeName = "text")]
        public string Errors { get; set; }

        public virtual Message Message { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MessageItem> MessageItem1 { get; set; }

        public virtual MessageItem MessageItem2 { get; set; }
    }
}
