namespace CoinTradePros.DAL.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ExchangeAsset")]
    public partial class ExchangeAsset
    {
        public Guid Id { get; set; }

        public Guid ExchangeId { get; set; }

        public Guid AssetId { get; set; }

        [Required]
        [StringLength(10)]
        public string CurrencyPair { get; set; }

        public virtual Asset Asset { get; set; }

        public virtual Exchange Exchange { get; set; }
    }
}
