namespace CoinTradePros.DAL.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ProfileDetailType")]
    public partial class ProfileDetailType
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ProfileDetailType()
        {
            ProfileDetails = new HashSet<ProfileDetail>();
        }

        public Guid Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        [StringLength(250)]
        public string Description { get; set; }

        [StringLength(25)]
        public string IconName { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProfileDetail> ProfileDetails { get; set; }

        public virtual ProfileDetailType ProfileDetailType1 { get; set; }

        public virtual ProfileDetailType ProfileDetailType2 { get; set; }
    }
}
