namespace CoinTradePros.DAL.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CourseSectionExam")]
    public partial class CourseSectionExam
    {
        public Guid Id { get; set; }

        public Guid CourseSectionId { get; set; }

        [Column(TypeName = "numeric")]
        public decimal MinScore { get; set; }

        public virtual CourseSection CourseSection { get; set; }
    }
}
