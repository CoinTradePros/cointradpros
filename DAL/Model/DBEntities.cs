namespace CoinTradePros.DAL.Model
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class DBEntities : DbContext
    {
        public DBEntities()
            : base("name=DBEntities")
        {
        }

     
        public virtual DbSet<AspNetRole> AspNetRoles { get; set; }
        public virtual DbSet<AspNetUserClaim> AspNetUserClaims { get; set; }
        public virtual DbSet<AspNetUserLogin> AspNetUserLogins { get; set; }
        public virtual DbSet<AspNetUser> AspNetUsers { get; set; }
        public virtual DbSet<Asset> Assets { get; set; }
        public virtual DbSet<ChapterExamQuestion> ChapterExamQuestions { get; set; }
        public virtual DbSet<ChapterExamScore> ChapterExamScores { get; set; }
        public virtual DbSet<ChatRecord> ChatRecords { get; set; }
        public virtual DbSet<ConnectionTable> ConnectionTables { get; set; }
        public virtual DbSet<CourseChapter> CourseChapters { get; set; }
        public virtual DbSet<CourseChapterExam> CourseChapterExams { get; set; }
        public virtual DbSet<CourseEnrollment> CourseEnrollments { get; set; }
        public virtual DbSet<CourseInstance> CourseInstances { get; set; }
        public virtual DbSet<CourseInstanceType> CourseInstanceTypes { get; set; }
        public virtual DbSet<CourseOffering> CourseOfferings { get; set; }
        public virtual DbSet<CoursePage> CoursePages { get; set; }
        public virtual DbSet<CourseSection> CourseSections { get; set; }
        public virtual DbSet<CourseSectionExam> CourseSectionExams { get; set; }
        public virtual DbSet<CourseType> CourseTypes { get; set; }
        public virtual DbSet<Discount> Discounts { get; set; }
        public virtual DbSet<Exchange> Exchanges { get; set; }
        public virtual DbSet<ExchangeAsset> ExchangeAssets { get; set; }
        public virtual DbSet<Feature> Features { get; set; }
        public virtual DbSet<FeatureType> FeatureTypes { get; set; }
        public virtual DbSet<Message> Messages { get; set; }
        public virtual DbSet<MessageItem> MessageItems { get; set; }
        public virtual DbSet<MessageTransmitter> MessageTransmitters { get; set; }
        public virtual DbSet<Package> Packages { get; set; }
        public virtual DbSet<PackageDiscount> PackageDiscounts { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<ProductDiscount> ProductDiscounts { get; set; }
        public virtual DbSet<ProductPurchase> ProductPurchases { get; set; }
        public virtual DbSet<ProfileDetail> ProfileDetails { get; set; }
        public virtual DbSet<ProfileDetailType> ProfileDetailTypes { get; set; }
        public virtual DbSet<ProTraderProfile> ProTraderProfiles { get; set; }
        public virtual DbSet<QuestionAnswer> QuestionAnswers { get; set; }
        public virtual DbSet<SectionExamScore> SectionExamScores { get; set; }
        public virtual DbSet<Subscription> Subscriptions { get; set; }
        public virtual DbSet<sysdiagram> sysdiagrams { get; set; }
        public virtual DbSet<Trade> Trades { get; set; }
        public virtual DbSet<TradeReaction> TradeReactions { get; set; }
        public virtual DbSet<TradeReactionType> TradeReactionTypes { get; set; }
        public virtual DbSet<TraderProfile> TraderProfiles { get; set; }
        public virtual DbSet<CryptoInvoice> CryptoInvoices { get; set; }
        public virtual DbSet<UserSetting> UserSettings { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AspNetRole>()
                .HasMany(e => e.AspNetUsers)
                .WithMany(e => e.AspNetRoles)
                .Map(m => m.ToTable("AspNetUserRoles").MapLeftKey("RoleId").MapRightKey("UserId"));

            modelBuilder.Entity<AspNetUser>()
                .HasMany(e => e.AspNetUserClaims)
                .WithRequired(e => e.AspNetUser)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<AspNetUser>()
                .HasMany(e => e.AspNetUserLogins)
                .WithRequired(e => e.AspNetUser)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<AspNetUser>()
                .HasMany(e => e.ChatRecords)
                .WithOptional(e => e.AspNetUser)
                .HasForeignKey(e => e.ReceiverID);

            modelBuilder.Entity<AspNetUser>()
                .HasMany(e => e.ChatRecords1)
                .WithOptional(e => e.AspNetUser1)
                .HasForeignKey(e => e.SenderID);

            modelBuilder.Entity<AspNetUser>()
                .HasMany(e => e.ConnectionTables)
                .WithOptional(e => e.AspNetUser)
                .HasForeignKey(e => e.Users);

            modelBuilder.Entity<AspNetUser>()
                .HasMany(e => e.Messages)
                .WithRequired(e => e.AspNetUser)
                .HasForeignKey(e => e.SenderId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Asset>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Asset>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<Asset>()
                .Property(e => e.Symbol)
                .IsUnicode(false);

            modelBuilder.Entity<Asset>()
                .Property(e => e.Website)
                .IsUnicode(false);

            modelBuilder.Entity<Asset>()
                .HasMany(e => e.ExchangeAssets)
                .WithRequired(e => e.Asset)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Asset>()
                .HasMany(e => e.Trades)
                .WithRequired(e => e.Asset)
                .HasForeignKey(e => e.AssetId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Asset>()
                .HasMany(e => e.Trades1)
                .WithRequired(e => e.Asset1)
                .HasForeignKey(e => e.DenominationId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ChapterExamQuestion>()
                .Property(e => e.QuestionText)
                .IsUnicode(false);

            modelBuilder.Entity<ChapterExamQuestion>()
                .Property(e => e.OptionA)
                .IsUnicode(false);

            modelBuilder.Entity<ChapterExamQuestion>()
                .Property(e => e.OptionB)
                .IsUnicode(false);

            modelBuilder.Entity<ChapterExamQuestion>()
                .Property(e => e.OptionC)
                .IsUnicode(false);

            modelBuilder.Entity<ChapterExamQuestion>()
                .Property(e => e.OptionD)
                .IsUnicode(false);

            modelBuilder.Entity<ChapterExamQuestion>()
                .Property(e => e.OptionE)
                .IsUnicode(false);

            modelBuilder.Entity<ChapterExamQuestion>()
                .Property(e => e.CorrectOption)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ChapterExamScore>()
                .Property(e => e.Score)
                .HasPrecision(3, 2);

            modelBuilder.Entity<ChatRecord>()
                .Property(e => e.Message)
                .IsUnicode(false);

            modelBuilder.Entity<CourseChapter>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<CourseChapter>()
                .HasMany(e => e.CourseChapterExams)
                .WithRequired(e => e.CourseChapter)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CourseChapterExam>()
                .Property(e => e.MinScore)
                .HasPrecision(5, 2);

            modelBuilder.Entity<CourseChapterExam>()
                .HasMany(e => e.ChapterExamQuestions)
                .WithRequired(e => e.CourseChapterExam)
                .HasForeignKey(e => e.ChapterExamId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CourseChapterExam>()
                .HasMany(e => e.ChapterExamScores)
                .WithRequired(e => e.CourseChapterExam)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CourseInstance>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<CourseInstance>()
                .Property(e => e.TotalCost)
                .HasPrecision(19, 4);

            modelBuilder.Entity<CourseInstance>()
                .HasMany(e => e.CourseEnrollments)
                .WithRequired(e => e.CourseInstance)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CourseInstanceType>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<CourseInstanceType>()
                .HasMany(e => e.CourseInstances)
                .WithRequired(e => e.CourseInstanceType)
                .HasForeignKey(e => e.InstanceTypeId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CourseOffering>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<CourseOffering>()
                .Property(e => e.PerStudentFee)
                .HasPrecision(19, 4);

            modelBuilder.Entity<CourseOffering>()
                .Property(e => e.NoCapMonthlyFee)
                .HasPrecision(19, 4);

            modelBuilder.Entity<CourseOffering>()
                .HasMany(e => e.CourseInstances)
                .WithRequired(e => e.CourseOffering)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CourseOffering>()
                .HasMany(e => e.CourseSections)
                .WithRequired(e => e.CourseOffering)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CoursePage>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<CoursePage>()
                .HasMany(e => e.CourseEnrollments)
                .WithOptional(e => e.CoursePage)
                .HasForeignKey(e => e.CurrentPageId);

            modelBuilder.Entity<CourseSection>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<CourseSection>()
                .HasMany(e => e.CourseChapters)
                .WithRequired(e => e.CourseSection)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CourseSection>()
                .HasMany(e => e.CourseSectionExams)
                .WithRequired(e => e.CourseSection)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CourseSectionExam>()
                .Property(e => e.MinScore)
                .HasPrecision(3, 2);

            modelBuilder.Entity<CourseType>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<Discount>()
                .Property(e => e.AmountOff)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Discount>()
                .Property(e => e.PercentOff)
                .HasPrecision(18, 0);

            modelBuilder.Entity<Discount>()
                .Property(e => e.CouponCode)
                .IsUnicode(false);

            modelBuilder.Entity<Discount>()
                .HasMany(e => e.PackageDiscounts)
                .WithRequired(e => e.Discount)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Discount>()
                .HasMany(e => e.ProductDiscounts)
                .WithRequired(e => e.Discount)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Discount>()
                .HasMany(e => e.ProductPurchases)
                .WithRequired(e => e.Discount)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Exchange>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Exchange>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<Exchange>()
                .Property(e => e.ApiUrl)
                .IsUnicode(false);

            modelBuilder.Entity<Exchange>()
                .Property(e => e.ApiProcessor)
                .IsUnicode(false);

            modelBuilder.Entity<Exchange>()
                .HasMany(e => e.ExchangeAssets)
                .WithRequired(e => e.Exchange)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ExchangeAsset>()
                .Property(e => e.CurrencyPair)
                .IsUnicode(false);

            modelBuilder.Entity<Feature>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Feature>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<Feature>()
                .Property(e => e.IconPath)
                .IsUnicode(false);

            modelBuilder.Entity<FeatureType>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<FeatureType>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<FeatureType>()
                .Property(e => e.FeatureProcessor)
                .IsUnicode(false);

            modelBuilder.Entity<FeatureType>()
                .Property(e => e.DefaultIconClass)
                .IsUnicode(false);

            modelBuilder.Entity<FeatureType>()
                .HasMany(e => e.Features)
                .WithRequired(e => e.FeatureType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Message>()
                .Property(e => e.SubmittedText)
                .IsUnicode(false);

            modelBuilder.Entity<Message>()
                .HasMany(e => e.MessageItems)
                .WithRequired(e => e.Message)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MessageItem>()
                .Property(e => e.SendDate)
                .IsFixedLength();

            modelBuilder.Entity<MessageItem>()
                .Property(e => e.Errors)
                .IsUnicode(false);

            modelBuilder.Entity<MessageItem>()
                .HasMany(e => e.MessageItem1)
                .WithRequired(e => e.MessageItem2)
                .HasForeignKey(e => e.MessageTransmitterId);

            modelBuilder.Entity<Package>()
                .Property(e => e.Title)
                .IsUnicode(false);

            modelBuilder.Entity<Package>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<Package>()
                .Property(e => e.MonthlyPrice)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Package>()
                .Property(e => e.AnnualPrice)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Package>()
                .Property(e => e.LifetimePrice)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Package>()
                .Property(e => e.IconPath)
                .IsUnicode(false);

            modelBuilder.Entity<Package>()
                .HasMany(e => e.PackageDiscounts)
                .WithRequired(e => e.Package)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Package>()
                .HasMany(e => e.Subscriptions)
                .WithRequired(e => e.Package)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Package>()
                .HasMany(e => e.Features)
                .WithMany(e => e.Packages)
                .Map(m => m.ToTable("PackageFeature").MapLeftKey("PackageId").MapRightKey("FeatureId"));

            modelBuilder.Entity<Product>()
                .Property(e => e.Title)
                .IsUnicode(false);

            modelBuilder.Entity<Product>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<Product>()
                .Property(e => e.Price)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Product>()
                .Property(e => e.ImagePath)
                .IsUnicode(false);

            modelBuilder.Entity<Product>()
                .HasMany(e => e.ProductDiscounts)
                .WithRequired(e => e.Product)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Product>()
                .HasMany(e => e.ProductPurchases)
                .WithRequired(e => e.Product)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ProductPurchase>()
                .Property(e => e.PurchaseDate)
                .IsFixedLength();

            modelBuilder.Entity<ProductPurchase>()
                .Property(e => e.AmountPaid)
                .HasPrecision(19, 4);

            modelBuilder.Entity<ProfileDetailType>()
                .HasMany(e => e.ProfileDetails)
                .WithRequired(e => e.ProfileDetailType)
                .HasForeignKey(e => e.DetailTypeId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ProfileDetailType>()
                .HasOptional(e => e.ProfileDetailType1)
                .WithRequired(e => e.ProfileDetailType2);

            modelBuilder.Entity<ProTraderProfile>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<ProTraderProfile>()
                .Property(e => e.TagLine)
                .IsUnicode(false);

            modelBuilder.Entity<ProTraderProfile>()
                .Property(e => e.VideoEmbed)
                .IsUnicode(false);

            modelBuilder.Entity<ProTraderProfile>()
                .Property(e => e.PortraitPath)
                .IsUnicode(false);

            modelBuilder.Entity<ProTraderProfile>()
                .HasMany(e => e.CourseInstances)
                .WithRequired(e => e.ProTraderProfile)
                .HasForeignKey(e => e.OfferingCoachId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ProTraderProfile>()
                .HasMany(e => e.CourseOfferings)
                .WithRequired(e => e.ProTraderProfile)
                .HasForeignKey(e => e.CourseOwnerId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ProTraderProfile>()
                .HasMany(e => e.Packages)
                .WithRequired(e => e.ProTraderProfile)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ProTraderProfile>()
                .HasMany(e => e.QuestionAnswers)
                .WithRequired(e => e.ProTraderProfile)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<QuestionAnswer>()
                .Property(e => e.Question)
                .IsUnicode(false);

            modelBuilder.Entity<QuestionAnswer>()
                .Property(e => e.Answer)
                .IsUnicode(false);

            modelBuilder.Entity<SectionExamScore>()
                .Property(e => e.Score)
                .HasPrecision(3, 2);

            modelBuilder.Entity<Subscription>()
                .Property(e => e.PaymentAmount)
                .HasPrecision(18, 10);

            modelBuilder.Entity<Subscription>()
                .HasMany(e => e.ChatRecords)
                .WithRequired(e => e.Subscription)
                .HasForeignKey(e => e.SubscriberID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Subscription>()
                .HasMany(e => e.CryptoInvoices)
                .WithRequired(e => e.Subscription)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Trade>()
                .Property(e => e.EntryPrice)
                .HasPrecision(18, 10);

            modelBuilder.Entity<Trade>()
                .Property(e => e.ExitPrice)
                .HasPrecision(18, 10);

            modelBuilder.Entity<Trade>()
                .Property(e => e.PositionSize)
                .HasPrecision(18, 10);

            modelBuilder.Entity<Trade>()
                .Property(e => e.ProfitLossAmount)
                .HasPrecision(18, 10);

            modelBuilder.Entity<Trade>()
                .Property(e => e.ProfitLossPercent)
                .HasPrecision(18, 10);

            modelBuilder.Entity<Trade>()
                .Property(e => e.Comments)
                .IsUnicode(false);

            modelBuilder.Entity<Trade>()
                .Property(e => e.ChartImagePath)
                .IsUnicode(false);

            modelBuilder.Entity<Trade>()
                .Property(e => e.PostDate)
                .IsFixedLength();

            modelBuilder.Entity<Trade>()
                .HasMany(e => e.TradeReactions)
                .WithRequired(e => e.Trade)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TradeReaction>()
                .Property(e => e.SenderComment)
                .IsUnicode(false);

            modelBuilder.Entity<TradeReaction>()
                .Property(e => e.ShareUrl)
                .IsUnicode(false);

            modelBuilder.Entity<TradeReactionType>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<TradeReactionType>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<TradeReactionType>()
                .Property(e => e.ShareNetwork)
                .IsUnicode(false);

            modelBuilder.Entity<TradeReactionType>()
                .Property(e => e.IconUrl)
                .IsUnicode(false);

            modelBuilder.Entity<TradeReactionType>()
                .Property(e => e.ReactionProcessor)
                .IsUnicode(false);

            modelBuilder.Entity<TradeReactionType>()
                .HasMany(e => e.TradeReactions)
                .WithRequired(e => e.TradeReactionType)
                .HasForeignKey(e => e.TradeReactionTypeId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TradeReactionType>()
                .HasMany(e => e.TradeReactions1)
                .WithRequired(e => e.TradeReactionType1)
                .HasForeignKey(e => e.TradeReactionTypeId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TraderProfile>()
                .HasMany(e => e.ChapterExamScores)
                .WithRequired(e => e.TraderProfile)
                .HasForeignKey(e => e.TraderId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TraderProfile>()
                .HasMany(e => e.CourseEnrollments)
                .WithRequired(e => e.TraderProfile)
                .HasForeignKey(e => e.TraderId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TraderProfile>()
                .HasMany(e => e.ProfileDetails)
                .WithRequired(e => e.TraderProfile)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TraderProfile>()
                .HasMany(e => e.ProTraderProfiles)
                .WithRequired(e => e.TraderProfile)
                .HasForeignKey(e => e.TraderId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TraderProfile>()
                .HasMany(e => e.Trades)
                .WithRequired(e => e.TraderProfile)
                .HasForeignKey(e => e.TraderId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TraderProfile>()
                .HasMany(e => e.TradeReactions)
                .WithRequired(e => e.TraderProfile)
                .HasForeignKey(e => e.SenderId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CryptoInvoice>()
                .Property(e => e.Amount)
                .HasPrecision(18, 0);

            modelBuilder.Entity<CryptoInvoice>()
                .Property(e => e.PaymentAmount)
                .IsFixedLength();
        }
    }
}
