namespace CoinTradePros.DAL.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ChatRecord")]
    public partial class ChatRecord
    {
        public Guid ID { get; set; }

        public Guid SubscriberID { get; set; }

        [StringLength(128)]
        public string SenderID { get; set; }

        [StringLength(128)]
        public string ReceiverID { get; set; }

        [Column(TypeName = "text")]
        [Required]
        public string Message { get; set; }

        [Column(TypeName = "date")]
        public DateTime SenderDate { get; set; }

        public TimeSpan SenderTime { get; set; }

        public bool Status { get; set; }

        public bool? SenderIsDeleted { get; set; }

        public bool? ReceiverIsDeleted { get; set; }

        public virtual AspNetUser AspNetUser { get; set; }

        public virtual AspNetUser AspNetUser1 { get; set; }

        public virtual Subscription Subscription { get; set; }
    }
}
