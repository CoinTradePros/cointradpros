namespace CoinTradePros.DAL.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TradeReaction")]
    public partial class TradeReaction
    {
        public Guid Id { get; set; }

        public Guid TradeId { get; set; }

        public Guid SenderId { get; set; }

        public Guid TradeReactionTypeId { get; set; }

        [StringLength(500)]
        public string SenderComment { get; set; }

        [StringLength(200)]
        public string ShareUrl { get; set; }

        public virtual Trade Trade { get; set; }

        public virtual TradeReactionType TradeReactionType { get; set; }

        public virtual TraderProfile TraderProfile { get; set; }

        public virtual TradeReactionType TradeReactionType1 { get; set; }
    }
}
