namespace CoinTradePros.DAL.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PackageDiscount")]
    public partial class PackageDiscount
    {
        public Guid Id { get; set; }

        public Guid PackageId { get; set; }

        public Guid DiscountId { get; set; }

        public DateTimeOffset? StartDate { get; set; }

        public DateTimeOffset? EndDate { get; set; }

        public virtual Discount Discount { get; set; }

        public virtual Package Package { get; set; }
    }
}
