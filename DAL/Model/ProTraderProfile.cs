namespace CoinTradePros.DAL.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ProTraderProfile")]
    public partial class ProTraderProfile
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ProTraderProfile()
        {
            CourseInstances = new HashSet<CourseInstance>();
            CourseOfferings = new HashSet<CourseOffering>();
            Packages = new HashSet<Package>();
            Trades = new HashSet<Trade>();
            QuestionAnswers = new HashSet<QuestionAnswer>();
        }

        public Guid Id { get; set; }

        public Guid TraderId { get; set; }

        [Column(TypeName = "text")]
        public string Description { get; set; }

        [StringLength(150)]
        public string TagLine { get; set; }

        [Column(TypeName = "text")]
        public string VideoEmbed { get; set; }

        [StringLength(250)]
        public string PortraitPath { get; set; }

        [StringLength(50)]
        public string ChatHash { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CourseInstance> CourseInstances { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CourseOffering> CourseOfferings { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Package> Packages { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Trade> Trades { get; set; }

        public virtual TraderProfile TraderProfile { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<QuestionAnswer> QuestionAnswers { get; set; }
    }
}
