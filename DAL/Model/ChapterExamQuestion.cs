namespace CoinTradePros.DAL.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ChapterExamQuestion")]
    public partial class ChapterExamQuestion
    {
        public Guid Id { get; set; }

        public Guid ChapterExamId { get; set; }

        [Column(TypeName = "text")]
        [Required]
        public string QuestionText { get; set; }

        [Column(TypeName = "text")]
        public string OptionA { get; set; }

        [Column(TypeName = "text")]
        public string OptionB { get; set; }

        [Column(TypeName = "text")]
        public string OptionC { get; set; }

        [Column(TypeName = "text")]
        public string OptionD { get; set; }

        [Column(TypeName = "text")]
        public string OptionE { get; set; }

        [Required]
        [StringLength(1)]
        public string CorrectOption { get; set; }

        public virtual CourseChapterExam CourseChapterExam { get; set; }
    }
}
