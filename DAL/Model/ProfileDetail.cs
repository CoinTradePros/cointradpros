namespace CoinTradePros.DAL.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ProfileDetail")]
    public partial class ProfileDetail
    {
        public Guid Id { get; set; }

        public Guid TraderProfileId { get; set; }

        public Guid DetailTypeId { get; set; }

        [StringLength(25)]
        public string DisplayText { get; set; }

        [StringLength(150)]
        public string DetailUrl { get; set; }

        public bool ShowUrl { get; set; }

        public bool SalesPageDetail { get; set; }

        public int SortOrder { get; set; }

        public bool ShowIconOnly { get; set; }

        public bool HideIcon { get; set; }

        public virtual ProfileDetailType ProfileDetailType { get; set; }

        public virtual TraderProfile TraderProfile { get; set; }
    }
}
