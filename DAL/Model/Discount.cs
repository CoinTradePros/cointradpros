namespace CoinTradePros.DAL.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Discount")]
    public partial class Discount
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Discount()
        {
            PackageDiscounts = new HashSet<PackageDiscount>();
            ProductDiscounts = new HashSet<ProductDiscount>();
            ProductPurchases = new HashSet<ProductPurchase>();
            Subscriptions = new HashSet<Subscription>();
        }

        public Guid Id { get; set; }

        public Guid ProTraderProfileId { get; set; }

        [Column(TypeName = "money")]
        public decimal? AmountOff { get; set; }

        public decimal? PercentOff { get; set; }

        [StringLength(25)]
        public string CouponCode { get; set; }

        public bool SingleUse { get; set; }

        public int? DiscountDuration { get; set; }

        public bool OnlySubscribers { get; set; }

        public bool OnlyNewSubscribers { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PackageDiscount> PackageDiscounts { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProductDiscount> ProductDiscounts { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProductPurchase> ProductPurchases { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Subscription> Subscriptions { get; set; }
    }
}
