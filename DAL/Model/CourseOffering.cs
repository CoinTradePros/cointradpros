namespace CoinTradePros.DAL.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CourseOffering")]
    public partial class CourseOffering
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CourseOffering()
        {
            CourseInstances = new HashSet<CourseInstance>();
            CourseSections = new HashSet<CourseSection>();
        }

        public Guid Id { get; set; }

        [Required]
        [StringLength(299)]
        public string Name { get; set; }

        [Column(TypeName = "text")]
        [Required]
        public string Description { get; set; }

        public Guid? CourseTypeId { get; set; }

        public Guid CourseOwnerId { get; set; }

        public short UnlockedSectionCount { get; set; }

        public bool PublicOffering { get; set; }

        [Column(TypeName = "money")]
        public decimal PerStudentFee { get; set; }

        [Column(TypeName = "money")]
        public decimal NoCapMonthlyFee { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CourseInstance> CourseInstances { get; set; }

        public virtual CourseType CourseType { get; set; }

        public virtual ProTraderProfile ProTraderProfile { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CourseSection> CourseSections { get; set; }
    }
}
