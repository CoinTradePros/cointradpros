namespace CoinTradePros.DAL.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Message")]
    public partial class Message
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Message()
        {
            MessageItems = new HashSet<MessageItem>();
        }

        public Guid Id { get; set; }

        [Required]
        [StringLength(128)]
        public string SenderId { get; set; }

        public Guid? DataSourceId { get; set; }

        [Required]
        [StringLength(50)]
        public string MessageType { get; set; }

        [Column(TypeName = "text")]
        public string SubmittedText { get; set; }

        [StringLength(250)]
        public string SubmittedImagePath { get; set; }

        public DateTimeOffset SendDate { get; set; }

        public DateTimeOffset? CompletedDate { get; set; }

        public int? ErrorCount { get; set; }

        public virtual AspNetUser AspNetUser { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MessageItem> MessageItems { get; set; }
    }
}
