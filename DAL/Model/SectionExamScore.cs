namespace CoinTradePros.DAL.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SectionExamScore")]
    public partial class SectionExamScore
    {
        public Guid Id { get; set; }

        public Guid TraderId { get; set; }

        public Guid CourseExamId { get; set; }

        [Column(TypeName = "numeric")]
        public decimal Score { get; set; }

        public DateTime CompletionDate { get; set; }
    }
}
