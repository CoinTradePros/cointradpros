namespace CoinTradePros.DAL.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Package")]
    public partial class Package
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Package()
        {
            PackageDiscounts = new HashSet<PackageDiscount>();
            Subscriptions = new HashSet<Subscription>();
            Features = new HashSet<Feature>();
        }

        public Guid Id { get; set; }

        public Guid ProTraderProfileId { get; set; }

        [Required]
        [StringLength(100)]
        public string Title { get; set; }

        [Column(TypeName = "text")]
        public string Description { get; set; }

        [Column(TypeName = "money")]
        public decimal? MonthlyPrice { get; set; }

        [Column(TypeName = "money")]
        public decimal? AnnualPrice { get; set; }

        [Column(TypeName = "money")]
        public decimal? LifetimePrice { get; set; }

        [StringLength(250)]
        public string IconPath { get; set; }

        public int? SortOrder { get; set; }

        public virtual ProTraderProfile ProTraderProfile { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PackageDiscount> PackageDiscounts { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Subscription> Subscriptions { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Feature> Features { get; set; }
    }
}
