namespace CoinTradePros.DAL.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ProductPurchase")]
    public partial class ProductPurchase
    {
        public Guid Id { get; set; }

        public Guid ProductId { get; set; }

        public Guid DiscountId { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] PurchaseDate { get; set; }

        public DateTimeOffset? FulfillmentDate { get; set; }

        [Column(TypeName = "money")]
        public decimal AmountPaid { get; set; }

        public virtual Discount Discount { get; set; }

        public virtual Product Product { get; set; }
    }
}
