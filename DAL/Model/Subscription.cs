namespace CoinTradePros.DAL.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Subscription")]
    public partial class Subscription
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Subscription()
        {
            ChatRecords = new HashSet<ChatRecord>();
            CryptoInvoices = new HashSet<CryptoInvoice>();
        }

        public Guid Id { get; set; }

        public Guid? TraderProfileId { get; set; }

        public Guid PackageId { get; set; }

        public Guid? DiscountId { get; set; }

        public DateTimeOffset SubscriptionStart { get; set; }

        public DateTimeOffset? SubscriptionEnd { get; set; }

        public bool IsCancelled { get; set; }

        public bool PendingPayment { get; set; }

        public int PaymentFrequency { get; set; }

        public decimal PaymentAmount { get; set; }

        [Required]
        [StringLength(10)]
        public string PaymentCurrency { get; set; }

        [StringLength(50)]
        public string PaymentEmail { get; set; }

        [StringLength(100)]
        public string StripeSourceToken { get; set; }

        [StringLength(100)]
        public string StripeCustomerId { get; set; }

        [StringLength(100)]
        public string StripeSubscriptionId { get; set; }

        [StringLength(100)]
        public string StripeSinglePaymentId { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ChatRecord> ChatRecords { get; set; }

        public virtual Discount Discount { get; set; }

        public virtual Package Package { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CryptoInvoice> CryptoInvoices { get; set; }

        public virtual TraderProfile TraderProfile { get; set; }
    }
}
