namespace CoinTradePros.DAL.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CryptoInvoice")]
    public partial class CryptoInvoice
    {
        [Key]
        [Column(Order = 0)]
        public Guid Id { get; set; }

        [Key]
        [Column(Order = 1)]
        public Guid SubscriptionId { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NotificationCount { get; set; }

        [Key]
        [Column(Order = 3)]
        public DateTimeOffset DueDate { get; set; }

        [Key]
        [Column(Order = 4)]
        [StringLength(50)]
        public string Currency { get; set; }

        [Key]
        [Column(Order = 5)]
        public decimal Amount { get; set; }

        [StringLength(250)]
        public string QrCodeUrl { get; set; }

        public int? ConfirmsNeeded { get; set; }

        [Key]
        [Column(Order = 6)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ConfirmsProcessed { get; set; }

        [Key]
        [Column(Order = 7)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PaymentStatus { get; set; }

        public DateTimeOffset? Timeout { get; set; }

        [StringLength(200)]
        public string PaymentAddress { get; set; }

        [StringLength(10)]
        public string PaymentAmount { get; set; }

        [StringLength(250)]
        public string PaymentStatusUrl { get; set; }

        [StringLength(100)]
        public string SessionId { get; set; }

        [StringLength(150)]
        public string CoinPayTxId { get; set; }

        public virtual Subscription Subscription { get; set; }
    }
}
