﻿
using CoinTradePros.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
  public  class UnitOfWork:IUnitOfWork
    {
        private readonly DBEntities _context;
        public UnitOfWork(DBEntities context)
        {
            _context = context;

            //Persons = new PersonRepository(_context);
            //classes = new eClassRepository(_context);
        }

        //public IPersonRepository Persons { get; private set; }
        //public IeClassRepository classes { get; private set; }

        public int Complete()
        {
            return _context.SaveChanges();
        }
        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
