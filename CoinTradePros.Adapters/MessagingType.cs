﻿namespace CoinTradePros.Adapters
{
    public enum MessagingType
    {
        TradeAlert,
        ManualAlert,
        TwoFactorAuthCode,
        ChannelVerification
    }
}
