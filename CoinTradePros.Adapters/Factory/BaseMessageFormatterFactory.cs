﻿using System;

namespace CoinTradePros.Adapters.Factory
{
    public class BaseMessageFormatterFactory
    {
        public static ICreateMessageFormatters GetInstance(MessagingType messagingType)
        {
            switch (messagingType)
            {
                case MessagingType.TradeAlert:
                {
                    return new AlertFormatterFactory();
                }
                case MessagingType.ManualAlert:
                {
                    return new ManualAlertFormatterFactory();
                }
                default:
                {
                    throw new NotImplementedException($"Formatting for the {messagingType} messaging type has not been implemented yet.");
                }
            }
        }
    }
}
