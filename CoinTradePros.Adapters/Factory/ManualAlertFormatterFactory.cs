﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoinTradePros.Adapters.Messaging.Formatters.ManualAlert;
using CoinTradePros.Interfaces.Messaging;

namespace CoinTradePros.Adapters.Factory
{
    public class ManualAlertFormatterFactory : ICreateMessageFormatters
    {
        public IFormatMessages GetInstance(TransmitterType transmitterType)
        {
            switch (transmitterType)
            {
                case TransmitterType.Discord:
                case TransmitterType.Facebook:
                case TransmitterType.Skype:
                case TransmitterType.Email:
                case TransmitterType.Slack:
                case TransmitterType.Twilio:
                case TransmitterType.Telegram:
                case TransmitterType.Twitter:
                {
                    // TODO:  Keep it simple for now, but implement other 
                    return new SimpleManualAlertFormatter();
                }
                default:
                {
                    throw new NotImplementedException($"Messaging on the {transmitterType} network is not suppoerted just yet.");
                }
            }
        }
    }
}
