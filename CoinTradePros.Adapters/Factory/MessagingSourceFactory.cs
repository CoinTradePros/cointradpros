﻿using System;
using System.Collections.Generic;
using CoinTradePros.Adapters.Messaging;
using CoinTradePros.Interfaces.Messaging;

namespace CoinTradePros.Adapters.Factory
{
    public class MessagingSourceProviderFactory
    {
        public static IProvideMessagingSource GetInstance(
            ISupplyMessageData sourceData, 
            ICollection<TransmitterType> transmitterTypes, 
            MessagingType messagingType,
            ICollection<Guid> recipientSegmentIds)
        {
            switch (messagingType)
            {
                case MessagingType.TradeAlert:
                {
                    return new TradeAlertAdapter(sourceData, recipientSegmentIds, transmitterTypes);
                }
                case MessagingType.ManualAlert:
                {
                    return new ManualAlertAdapter(sourceData, recipientSegmentIds, transmitterTypes);
                }
            }
            throw new NotImplementedException($"The MessageType {messagingType} is currenly not supported.");
        }
    }
}
