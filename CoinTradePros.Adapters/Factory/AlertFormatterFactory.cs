﻿using System;
using CoinTradePros.Adapters.Messaging.Formatters;
using CoinTradePros.Adapters.Messaging.Formatters.TradeAlert;
using CoinTradePros.Interfaces.Messaging;

namespace CoinTradePros.Adapters.Factory
{
    public class AlertFormatterFactory : ICreateMessageFormatters
    {
        public IFormatMessages GetInstance(TransmitterType transmitterType)
        {
            switch (transmitterType)
            {
                case TransmitterType.Discord:
                case TransmitterType.Facebook:
                case TransmitterType.Skype:
                case TransmitterType.Email:
                case TransmitterType.Slack:
                case TransmitterType.Twilio:
                case TransmitterType.Telegram:
                case TransmitterType.Twitter:
                {
                    // TODO:  Keep it simple for now, but implement other 
                    return new SimpleTradeAlertFormatter();
                }
                default:
                {
                    throw new NotImplementedException($"Messaging on the {transmitterType} network is not suppoerted just yet.");
                }
            }
        }
    }
}
