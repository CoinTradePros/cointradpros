﻿using System;
using System.Collections.Generic;
using CoinTradePros.Adapters.Messaging;
using CoinTradePros.BusinessObjects;
using CoinTradePros.Interfaces.Messaging;

namespace CoinTradePros.Adapters.Factory
{
    public class ReceiverFactory
    {
        public static IProvideRecipient GetInstance(string userId, UserSettingCollection userSettings)
        {
            Guid id;
            if (Guid.TryParse(userId, out id))
            {
                return GetInstance(id, userSettings);
            }
            throw new ArgumentException($"The userId {userId} was not a parsabble Guid", nameof(userId));
        }

        public static IProvideRecipient GetInstance(Guid userId, UserSettingCollection userSettings)
        {
            return new TraderReceiverAdapter(userId, userSettings);
        }
    }
}
