﻿namespace CoinTradePros.Adapters.Factory
{
    public class MessageFormatterProviderFactory 
    {
        public static ICreateMessageFormatters GetInstance(MessagingType messageType)
        {
            switch (messageType)
            {
                case MessagingType.TradeAlert:
                {
                    return new AlertFormatterFactory();
                }
                case MessagingType.ManualAlert:
                {
                    return new ManualAlertFormatterFactory();
                }
            }
            return new AlertFormatterFactory();
        }
    }
}
