﻿using System.Collections.Generic;
using System.Configuration;
using CoinTradePros.BusinessObjects;
using CoinTradePros.Interfaces.Messaging;
using CoinTradePros.Messaging.Factory;

namespace CoinTradePros.Adapters.Messaging.Formatters.ManualAlert
{
    public class SimpleManualAlertFormatter : IFormatMessages
    {
        public IMessage GetMessage(object dataSource, IDictionary<string, string> messageBag)
        {
            Message data = (Message)dataSource;
            IMessage outgoing = MessageFactory.GetInstance();
            if (string.IsNullOrEmpty(data.SubmittedImagePath))
            {
                outgoing.MessageBody = data.SubmittedText;
            }
            else
            {
                outgoing.MessageBody =
                    $"{data.SubmittedText}\n{data.SubmittedImagePath}";
            }
            outgoing.MessageHeading = "An Alert from Your Coach at CoinTradePros.com!";
            return outgoing;
        }
    }
}
