﻿using System;
using System.Collections.Generic;
using System.Text;
using CoinTradePros.BusinessObjects;
using CoinTradePros.Interfaces.Messaging;
using CoinTradePros.Messaging.Factory;

namespace CoinTradePros.Adapters.Messaging.Formatters.TradeAlert
{
    /// <summary>
    /// Provides a simple format of a trade alert message.
    /// </summary>
    public class SimpleTradeAlertFormatter : IFormatMessages
    {
        public IMessage GetMessage(object dataSource, IDictionary<string, string> messageBag)
        {
            TradeView trade = dataSource as TradeView;
            if (trade == null)
            {
                throw new ArgumentException("The dataSource supplied was not a TradeView item.");
            }
            if (messageBag == null)
            {
                throw new ArgumentNullException(nameof(messageBag));
            }
            IMessage message = MessageFactory.GetInstance();
            string openStatus = trade.IsClosed() ? "closed" : "opened";
            string sourceLink = messageBag["MoreInfoUrl"];
            if (string.IsNullOrEmpty(sourceLink))
            {
                throw new InvalidOperationException("SimpleTradeAlertFormatter expects the TradeLink item in the messageBag to process messages.");
            }

            message.MessageHeading = $"{trade.TraderFirstName}  {trade.TraderLastName} ({trade.TraderUserName}) just {openStatus} a new {trade.AssetName} trade.  ";

            StringBuilder content = new StringBuilder();
            content.Append($"{trade.TraderFirstName}  {trade.TraderLastName} ({trade.TraderUserName}) just {openStatus} a new {trade.AssetName} trade on {trade.ExchangeName}.  ");
            content.Append($"To see the trade details, click here: {sourceLink}");
            message.MessageBody = content.ToString();

            return message;
        }
    }
}
