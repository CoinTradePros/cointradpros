﻿using System;
using System.Collections.Generic;
using System.Text;
using CoinTradePros.BusinessObjects;
using CoinTradePros.Interfaces.Messaging;
using CoinTradePros.Messaging.Factory;

namespace CoinTradePros.Adapters.Messaging.Formatters
{
    public class DiscordTradeAdapter : IFormatMessages
    {

        public IMessage GetMessage(object dataSource, IDictionary<string, string> messageBag)
        {
            /*
             * 
*italics*
__*underline italics*__
**bold**
__**underline bold**__
***bold italics***
__***underline bold italics***__
Underline	__underline__
Strikethrough	 ~~Strikethrough~~
             * 
             * 
             */
            TradeView tradeView = dataSource as TradeView;
            if (tradeView == null)
            {
                throw new ArgumentException("The TradeView item was null from the IMessageSource.GetDataItem method call.");
            }
            StringBuilder messageContent = new StringBuilder();
            messageContent.Append("");

            IMessage message = MessageFactory.GetInstance();
            message.MessageBody = messageContent.ToString();
            return message;
        }
    }
}
