﻿using CoinTradePros.Interfaces.Messaging;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using CoinTradePros.BusinessObjects;
using CoinTradePros.Interfaces.Messaging.Events;
using CoinTradePros.Messaging.Factory;

namespace CoinTradePros.Adapters.Messaging
{
    public class TraderReceiverAdapter : IProvideRecipient
    {
        private readonly UserSettingCollection _userSettings;

        private readonly Guid _userId;

        private readonly IReceiveMessages _messageReceiver;

        public TraderReceiverAdapter(Guid userId, UserSettingCollection userSettings)
        {
            _userSettings = userSettings;
            _userId = userId;
            _messageReceiver = RecipientFactory.GetInstance();
            _messageReceiver.NeedChannelIdentifier += SupplyChannelIdentifier;
            _messageReceiver.NeedRecipientSettings += SupplyRecipientSettings;
            _messageReceiver.ConfirmChannelSubscription += VerifyChannelSubscription;
        }

        private void VerifyChannelSubscription(object sender, ChannelSubscriberEventArgs e)
        {
            if (_userSettings != null)
            {
                UserSetting setting = _userSettings.GetUserSetting(_userId, $"{e.ChannelType}_Subscribed");
                if (setting == null)
                {
                    e.IsChannelSubscriber = false;
                }
                else
                {
                    bool isSubscribed;
                    bool.TryParse(setting.SettingValue, out isSubscribed);
                    e.IsChannelSubscriber = isSubscribed;
                }
            }
        }

        private void SupplyRecipientSettings(object sender, RecipientSettingsEventArgs e)
        {
            IDictionary<string, string> settings = new ConcurrentDictionary<string, string>();
            foreach (var userSetting in _userSettings)
            {
                if (userSetting.Platform.ToLower() == "messaging")
                {
                    settings.Add(userSetting.SettingKey, userSetting.SettingValue);
                } 
            }
            e.Settings = settings;
        }

        private void SupplyChannelIdentifier(object sender, ChannelIdentifierEventArgs e)
        {
            string settingKey = $"{e.ChannelType}_ChannelId";
            UserSetting setting = _userSettings.GetUserSetting(_userId, settingKey);

            if (setting == null)
            {
                // TODO: Add a way to capture when a user does not have a channel id when they are subscribed to a channel.
                throw new ApplicationException($"The user {_userId} is subscribed to the {e.ChannelType} channel, but no identifier was found in the UserSettings for SettingKey: {settingKey}");
            }
            e.ChannelIdentifier = setting.SettingValue;
        }

        public IReceiveMessages GetRecipient()
        {
            return _messageReceiver;
        }
    }
}
