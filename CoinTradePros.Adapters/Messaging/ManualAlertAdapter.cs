﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Deployment.Internal;
using CoinTradePros.Adapters.Factory;
using CoinTradePros.BusinessObjects;
using CoinTradePros.Interfaces.Messaging;
using CoinTradePros.Interfaces.Messaging.Events;
using CoinTradePros.Messaging.Factory;

namespace CoinTradePros.Adapters.Messaging
{
    public class ManualAlertAdapter : BaseMessageAdapter
    {
        private readonly Message _alertMessage;
        private readonly ICollection<Guid> _packageIds;
        private readonly ICollection<TransmitterType> _transmitterTypes;

        public ManualAlertAdapter(
            ISupplyMessageData messageData,
            ICollection<Guid> packageIds,
            ICollection<TransmitterType> transmitterTypes) : 
            this((Message)messageData, packageIds, transmitterTypes)
        {
            //  Nothing to do here.
        }

        public ManualAlertAdapter(
            Message message,
            ICollection<Guid> packageIds,
            ICollection<TransmitterType> transmitterTypes) :
            base(message, transmitterTypes)
        {
            _alertMessage = message;
            _packageIds = packageIds;
            _transmitterTypes = transmitterTypes;
        }

        protected override void SupplyMessageSettings(object sender, MessageSettingsEventArgs e)
        {
            e.MessageSettings = ConfigFactory.GetInstance(e.TransmitterType,
                new ConcurrentDictionary<Guid, IDictionary<string, string>>());
        }

        protected override void SupplyRecipients(object sender, RecipientsEventArgs e)
        {
            // Load up the subscribers who are subscribed to this ProTrader's trades.

            Guid id;
            if (!Guid.TryParse(_alertMessage.SenderId, out id))
            {
                return;
            }

            ProTraderProfile prof = ProTraderProfile.GetFromUserId(id);
            Guid proTraderId = prof?.Id ?? Guid.Empty;

            // Get the settings for all the recipients at one time and iterate over the list
            ICollection<IReceiveMessages> subscribers = new List<IReceiveMessages>();
            ICollection<Guid> ids = new List<Guid>();
            var subs = SubscriptionCollection.GetProTraderSubscribers(proTraderId);

            Guid subId;
            foreach (var sub in subs)
            {
                if (sub?.PackageId != null &&
                    _packageIds.Contains(sub.PackageId.Value) &&
                    Guid.TryParse(sub.SubscriberUserId, out subId))
                {
                    ids.Add(subId);
                }
            }
            foreach (var sub in subs)
            {
                if (!Guid.TryParse(sub.SubscriberUserId, out subId) || !ids.Contains(subId))
                {
                    continue;
                }
                UserSettingCollection userSettings =
                    UserSettingCollection.GetPlatformSettingGroup(subId, PlatformType.Messaging.ToString());
                IProvideRecipient rec = ReceiverFactory.GetInstance(sub.SubscriberUserId, userSettings);
                subscribers.Add(rec.GetRecipient());
            }
            e.Recipients = subscribers;
        }

        protected override void SupplyDataSourceUserId(object sender, DataSourceUserIdArgs e)
        {
            if (_alertMessage.DataSourceId == null)
            {
                Guid id;
                if (Guid.TryParse(_alertMessage.SenderId, out id))
                {
                    e.UserId = id;
                }
            }
            else
            {
                e.UserId = _alertMessage.DataSourceId;
            }
        }

        protected override void SupplyMessageFormatters(object sender, MessageFormatterEventArgs e)
        {
            ICreateMessageFormatters factory = BaseMessageFormatterFactory.GetInstance(MessagingType.ManualAlert);
            foreach (var tt in _transmitterTypes)
            {
                IFormatMessages formatter = factory.GetInstance(tt);
                e.MessageFormatters.Add(tt, formatter);
            }
        }

        //public ManualAlertAdapter(ISupplyMessageData sourceData, ICollection<TransmitterType> transmitterTypes) : base(sourceData, transmitterTypes)
        //{
        //}
    }
}
