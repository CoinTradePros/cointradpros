﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using CoinTradePros.BusinessObjects;
using CoinTradePros.Interfaces.Messaging;

namespace CoinTradePros.Adapters.Messaging.Config
{
    public class TwilioConfigurator : IConfigMessageTransmitters
    {
        private readonly UserSettingCollection _userSettings;

        public TwilioConfigurator(UserSettingCollection userSettings)
        {
            _userSettings = userSettings;
        }

        public bool SupportsConfigOverride()
        {
            return false;
        }

        public bool RequiresUserConfig()
        {
            return true;
        }

        public IDictionary<string, string> GetConfig()
        {
            IDictionary<string, string> configItems = new ConcurrentDictionary<string, string>();
            configItems.Add("TwilioKey", "AC4ab2111faa92d91c716644ba0271cb81");
            configItems.Add("TwilioSecret", "4aa1908fa8c3c7ba44051684637eb5bb");

            return configItems;
        }

        public IDictionary<string, string> GetConfig(Guid userId)
        {
            UserSetting setting = _userSettings.GetUserSetting(userId, "TwilioAlertNumber");
            IDictionary<string, string> config = new ConcurrentDictionary<string, string>();
            config.Add(setting.SettingKey, setting.SettingValue);
            return config;
        }
    }
}
