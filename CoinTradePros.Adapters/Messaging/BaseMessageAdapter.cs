﻿using System.Collections.Generic;
using CoinTradePros.BusinessObjects;
using CoinTradePros.Interfaces.Messaging;
using CoinTradePros.Interfaces.Messaging.Events;
using CoinTradePros.Messaging.Factory;

namespace CoinTradePros.Adapters.Messaging
{
    public abstract class BaseMessageAdapter : IProvideMessagingSource
    {
        private readonly IMessagingSource _messagingSource;
        private readonly ICollection<TransmitterType> _transmitterTypes;
        private readonly ISupplyMessageData _sourceData;

        protected BaseMessageAdapter(ISupplyMessageData sourceData, ICollection<TransmitterType> transmitterTypes)
        {
            _messagingSource = MessagingSourceFactory.GetInstance();

            _messagingSource.NeedDataItem += SupplyDataItem;
            _messagingSource.NeedDataSourceUserId += SupplyDataSourceUserId;
            _messagingSource.NeedMessageFormatters += SupplyMessageFormatters;
            _messagingSource.NeedMessageSettings += SupplyMessageSettings;
            _messagingSource.NeedRecipients += SupplyRecipients;
            _messagingSource.NeedTransmitterTypes += SupplyTransmitterTypes;
            _messagingSource.NeedSourceUserSettings += SupplySourceUserSettings;

            _sourceData = sourceData;
            _transmitterTypes = transmitterTypes;
        }

        private void SupplySourceUserSettings(object sender, SourceUserSettingsArgs e)
        {
            UserSettingCollection usersettings = new UserSettingCollection();
            IDictionary<string, string> settings = usersettings.GetUserSettings(e.UserId);
            e.Settings = settings;
        }

        protected void SupplyTransmitterTypes(object sender, TransmitterTypeEventArgs e)
        {
            e.TransmitterTypes = _transmitterTypes;
        }

        protected void SupplyDataItem(object sender, DataItemEventArgs e)
        {
            e.DataItem = _sourceData;
        }

        protected abstract void SupplyMessageSettings(object sender, MessageSettingsEventArgs e);

        protected abstract void SupplyRecipients(object sender, RecipientsEventArgs e);

        protected abstract void SupplyDataSourceUserId(object sender, DataSourceUserIdArgs e);

        protected abstract void SupplyMessageFormatters(object sender, MessageFormatterEventArgs e);
        public IMessagingSource GetMessagingSource()
        {
            return _messagingSource;
        }
    }
}
