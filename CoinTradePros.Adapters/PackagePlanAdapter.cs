﻿using System;
using CoinTradePros.BusinessObjects;

namespace CoinTradePros.Adapters
{
    public class PackagePlanAdapter
    {
        public PlanOptions GetPlanOptions(Package package, string interval)
        {
            if (package == null)
            {
                throw new ArgumentNullException(nameof(package));
            }
            if (!package.Id.HasValue)
            {
                throw new InvalidOperationException("The package Id must have a value before creating plan options");
            }
            decimal? amt = package.GetIntervalPaymentAmount(interval);
            int amount = 0;
            if (amt.HasValue)
            {
                amount = (int)amt.Value * 100;
            }
            PlanOptions options = new PlanOptions();
            options.Name = package.Title;
            options.Currency = "USD";
            options.TrialPeriodDays = 0;
            options.Amount = amount;
            options.Interval = interval;
            options.Id = $"{package.Id.Value}_{interval}";
            options.StatementDescriptor = package.Title.Length > 22 ?
                package.Title.Substring(0, 22) : package.Title;

            switch (interval)
            {
                case PlanOptions.Year:
                {
                    if (package.AnnualPrice <= 0)
                    {
                        throw new InvalidOperationException($"The package {package.Id} does not have annual pricing.");
                    }
                    options.IntervalCount = 1;
                    break;
                }
                case PlanOptions.Month:
                {
                    if (package.MonthlyPrice <= 0)
                    {
                        throw new InvalidOperationException($"The package {package.Id} does not have monthly pricing.");
                    }
                    options.IntervalCount = 12;
                    break;
                }
                default:
                {
                    throw new InvalidOperationException($"The interval {interval} is not supported.");
                }
                
            }
            return options;
        }
    }
}
