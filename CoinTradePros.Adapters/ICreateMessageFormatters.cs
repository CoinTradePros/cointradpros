﻿using CoinTradePros.Interfaces.Messaging;

namespace CoinTradePros.Adapters
{
    public interface ICreateMessageFormatters
    {
        IFormatMessages GetInstance(TransmitterType transmitterType);
    }
}
