﻿using System;
using System.Data.SqlClient;
using System.Threading.Tasks;
using CoinTradePros.BusinessObjects;
using CoinTradePros.CoinCapClient;
using Tiraggo.Interfaces;

namespace CoinTradePros.Adapters
{
    public class CoinDataAdapter
    {
        public static void CheckCoins()
        {
            CoinConnector connector = new CoinConnector();
            AssetCollection assets = new AssetCollection();
            bool check = assets.GetLastCheck();
            if (!check) return;
            assets.LoadAll();
            var coins = connector.GetCoinData();
            if (coins.Status == TaskStatus.WaitingForActivation)
            {
                // This happened when the coincap.io API was not running, or running slow.  So, skip it.
                return;
            }

            var volumeData = connector.GetFrontData().Result;
            foreach (var c in coins.Result)
            {
                Asset newCoin;
                // TODO:  Make the sorting updates more efficient going to the DB
                if (assets.ContainsAsset(c.Key, c.Value))
                {
                    // just update the volume
                    newCoin = assets.GetAsset(c.Key, c.Value);
                }
                else
                {
                    newCoin = new Asset();
                    newCoin.Id = Guid.NewGuid();
                    newCoin.CanDenominate = false;
                    newCoin.Name = c.Value;
                    newCoin.Symbol = c.Key;
                    try
                    {
                        newCoin.Save(tgSqlAccessType.DynamicSQL);
                    }
                    catch (SqlException ex)
                    {
                        if (ex.Message.Contains("UNIQUE KEY constraint"))
                        {
                            // just somethihng that slipped through the checks above.  Remove the ID so it does not create a new record below.
                            continue;
                        }
                    }
                }
                if (volumeData.ContainsKey(c.Key))
                {
                    FrontItem fitem = volumeData[c.Key];
                    if (fitem != null)
                    {
                        newCoin.DailyVolume = (long) fitem.usdVolume;
                        newCoin.Save(tgSqlAccessType.DynamicSQL);
                    }
                }
            }
        }
    }
}
