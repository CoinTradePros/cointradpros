﻿namespace CoinTradePros.Interfaces.Exchange
{
    public interface IExchangeRequest
    {
        string ApiKey { get; set; }
        string ApiSecret { get; set; }
    }
}
