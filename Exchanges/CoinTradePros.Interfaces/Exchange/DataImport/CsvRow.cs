﻿using System.Collections.Generic;

namespace CoinTradePros.Interfaces.Exchange.DataImport
{
    /// <inheritdoc />
    /// <summary>
    /// Class to store one CSV row
    /// </summary>
    public class CsvRow : List<string>
    {
        public string LineText { get; set; }
    }
}
