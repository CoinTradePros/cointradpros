﻿namespace CoinTradePros.Interfaces.Exchange
{
    public enum ExchangeOrderType
    {
        MarketBuy,
        MarketSell,
        LimitBuy,
        LimitSell
    }
}
