﻿using System.Collections.Generic;

namespace CoinTradePros.Interfaces.Exchange
{
    public interface IExchangeOrder
    {
        object OrderId { get; set; }

        ICollection<IOrderSegment> Segments { get; }
    }
}
