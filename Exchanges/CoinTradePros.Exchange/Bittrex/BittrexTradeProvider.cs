﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using CoinTradePros.Interfaces.Exchange;
using Newtonsoft.Json;

namespace CoinTradePros.Exchange.Bittrex
{
    public class BittrexTradeProvider : IProvideExchangeData
    {
        const string ApiCallTemplate = "https://bittrex.com/api/{0}/{1}";
        const string ApiVersion = "v1.1";
        const string ApiMethod = "account/getorderhistory";
        public ICollection<IExchangeOrder> GetOrders(IExchangeRequest tradeRequest)
        {
            var nonce = DateTime.Now.Ticks;

            var uri = string.Format(ApiCallTemplate, ApiVersion, ApiMethod + "?apikey=" + tradeRequest.ApiKey + "&nonce=" + nonce);
            var request = WebRequest.Create(uri);

            var sign = HashHmac(uri, tradeRequest.ApiSecret);
            request.Headers.Add("apisign", sign);

            IDictionary<object, IExchangeOrder> orderTable = new Dictionary<object, IExchangeOrder>();
            using (var response = (HttpWebResponse)request.GetResponse())
            {
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    Stream responseStream = response.GetResponseStream();

                    if (responseStream == null)
                    {
                        return orderTable.Values;
                    }
                    using (var sr = new StreamReader(responseStream))
                    {
                        var content = sr.ReadToEnd();
                        ApiResult apiResponse = JsonConvert.DeserializeObject<ApiResult>(content);
                        if (apiResponse.success)
                        {
                            foreach (var orderItem in apiResponse.Result)
                            {
                                if (!orderTable.ContainsKey(orderItem.OrderId))
                                {
                                    IExchangeOrder order = new BittrexOrder();
                                    order.OrderId = orderItem.OrderId;
                                    order.Segments.Add(orderItem);
                                    orderTable.Add(order.OrderId, order);
                                }
                                else
                                {
                                    IExchangeOrder order = orderTable[orderItem.OrderId];
                                    order.Segments.Add(orderItem);
                                }
                            }
                            return orderTable.Values;
                        }
                        return null;
                    }
                }
                throw new Exception("Error - StatusCode=" + response.StatusCode + " Call Details=" + uri);
            }
        }

        private string HashHmac(string message, string secret)
        {
            Encoding encoding = Encoding.UTF8;
            using (HMACSHA512 hmac = new HMACSHA512(encoding.GetBytes(secret)))
            {
                var msg = encoding.GetBytes(message);
                var hash = hmac.ComputeHash(msg);
                return BitConverter.ToString(hash).ToLower().Replace("-", string.Empty);
            }
        }

        public ExchangeApiRequestType GetApiAuthType()
        {
            return ExchangeApiRequestType.KeyPair;
        }

        public string GetExchangeName()
        {
            return "Bittrex";
        }

        public ICollection<IExchangeOrder> GetOrders(Stream incomingData)
        {
            throw new NotImplementedException();
        }
    }
}
