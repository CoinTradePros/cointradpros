﻿using System;
using CoinTradePros.Interfaces.Exchange;

namespace CoinTradePros.Exchange.Bittrex
{
    public class BittrexTradeSegment : IOrderSegment
    {
        private string _orderUuid;
        public IExchangeOrder ExchangeOrder { get; set; }

        public string OrderUuid
        {
            get { return _orderUuid; }
            set { _orderUuid = value; }
        }

        public object OrderId {
            get { return _orderUuid; }
            set { _orderUuid = value.ToString(); }
        }
        public string Exchange { get; set; }
        public DateTime TimeStamp { get; set; }
        public string OrderType { get; set; }
        public decimal Limit { get; set; }
        public decimal Quantity { get; set; }
        public decimal QuantityRemaining { get; set; }
        public decimal Commission { get; set; }
        public decimal Price { get; set; }
        public decimal PricePerUnit { get; set; }
        public bool IsConditional { get; set; }
        public string Condition { get; set; }
        public string ConditionTarget { get; set; }
        public bool ImmediateOrCancel { get; set; }
    }
}
