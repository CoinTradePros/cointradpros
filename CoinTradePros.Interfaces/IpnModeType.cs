﻿
namespace CoinTradePros.Interfaces
{
    public enum IpnModeType
    {
        Simple,
        Button,
        Cart,
        Deposit,
        Api
    }
}
