﻿using System;
using System.Collections.Generic;

namespace CoinTradePros.Interfaces.Messaging
{
    /// <summary>
    /// Supplies required information a message transmitter needs to send messages.
    /// </summary>
    public interface IRequestMessages
    {
        /// <summary>
        /// A list of users who are to receive a message.
        /// </summary>
        ICollection<IReceiveMessages> GetRecipients();

        IDictionary<string, string> GetSourceUserSettings();
            /// <summary>
        /// Gets all the API Key, API Secret, Tokens, etc. that are needed to send out messages.
        /// </summary>
        IDictionary<string, string> GetMessageSettings(TransmitterType transmitterType);


            /// <summary>
        /// Gets the message information to send out.
        /// </summary>
        /// <returns>An IMessage</returns>
        IMessage GetMessage(TransmitterType transmitterType, IDictionary<string, string> messageBag);
    }
}
