﻿using System;
using System.Collections.Generic;
using CoinTradePros.Interfaces.Messaging.Events;

namespace CoinTradePros.Interfaces.Messaging
{
    public interface IReceiveMessages
    {
        /// <summary>
        /// For any supplemental user based information, such as if they prefer HTML or text emails, etc.
        /// /// <param name="transmitterType">The messaging platform being checked for subscription</param>
        /// </summary>
        IDictionary<string, string> GetRecipientSettings(TransmitterType transmitterType);

        /// <summary>
        /// Determines if the recipient is able to accept messages on a given channel.
        /// </summary>
        /// <param name="transmitterType">The messaging platform being checked for subscription</param>
        /// <returns>True if the user subscribes to the particular channel, false otherwise.</returns>
        bool IsSubscribedToChannel(TransmitterType transmitterType);

        /// <summary>
        /// Gets the identifier used to distinguish this user id on a channel.
        /// </summary>
        /// <param name="transmitterType">The messaging platform for which the identifier is needed.</param>
        /// <returns></returns>
        string GetChannelIdentifier(TransmitterType transmitterType);

        void OnNeedRecipientSettings(RecipientSettingsEventArgs args);

        void OnConfirmChannelSubscription(ChannelSubscriberEventArgs args);

        void OnNeedChannelIdentifier(ChannelIdentifierEventArgs args);

        event EventHandler<RecipientSettingsEventArgs> NeedRecipientSettings;
        event EventHandler<ChannelSubscriberEventArgs> ConfirmChannelSubscription;
        event EventHandler<ChannelIdentifierEventArgs> NeedChannelIdentifier;



    }
}
