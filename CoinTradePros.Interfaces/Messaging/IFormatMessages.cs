﻿using System.Collections.Generic;

namespace CoinTradePros.Interfaces.Messaging
{
    public interface IFormatMessages
    {
        /// <summary>
        /// Gets the contents of the messages based on the underlying messaging source, like a Trade item to create the text for a trade alert.
        /// </summary>
        /// <returns>The message text to send, suitable for the intended message transmitter.</returns>
        IMessage GetMessage(object dataSource, IDictionary<string, string> messageBag);
    }
}
