﻿using System;
using System.Threading.Tasks;
using CoinTradePros.Interfaces.Messaging.Events;

namespace CoinTradePros.Interfaces.Messaging
{
    public interface IProcessMessages
    {
        IMessagingResult ProcessMessage(IMessagingSource source);

        Task<IMessagingResult> ProcessMessageAsync(IMessagingSource source);

        void AddMessageField(string key, string value);

        void OnMessageProcessingComplete(MessageProcessingCompleteArgs args);

        event EventHandler<MessageProcessingCompleteArgs> MessageProcessingCompleted;

    }
}
