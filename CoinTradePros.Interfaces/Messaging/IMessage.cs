﻿using System.Collections.Generic;

namespace CoinTradePros.Interfaces.Messaging
{
    public interface IMessage
    {
        /// <summary>
        /// This is the title of the message, like the title of an email.  Text messages processors will most likely omit this field.
        /// </summary>
        string MessageHeading { get; set; }

        /// <summary>
        /// This is the content of the message.
        /// </summary>
        string MessageBody { get; set; }
    }
}
