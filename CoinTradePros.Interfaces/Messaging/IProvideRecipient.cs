﻿namespace CoinTradePros.Interfaces.Messaging
{
    public interface IProvideRecipient
    {
        IReceiveMessages GetRecipient();
    }
}
