﻿namespace CoinTradePros.Interfaces.Messaging
{
    public interface IProvideMessagingSource
    {
        IMessagingSource GetMessagingSource();
    }
}
