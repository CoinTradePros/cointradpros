﻿using System;
using System.Collections.Generic;

namespace CoinTradePros.Interfaces.Messaging
{
    public interface IMessagingResult
    {
        IDictionary<string, string> GetErrors();

        void AddError(string key, string errorDetail);

        void AddErrors(IMessagingResult result);

        void Start();

        void Stop();

        TimeSpan GetProcessingTime();
    }
}
