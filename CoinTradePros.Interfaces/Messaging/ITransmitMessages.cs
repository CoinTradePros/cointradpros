﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CoinTradePros.Interfaces.Messaging.Events;

namespace CoinTradePros.Interfaces.Messaging
{
    public interface ITransmitMessages
    {
        /// <summary>
        /// Relays the message to the message transmitter.
        /// </summary>
        /// <param name="messageRequest">All the things related to sending a message on this message transmitter.</param>
        /// <returns>The results of any erors or whatever mey be returned by this message transmitter.</returns>
        Task<IMessagingResult> SendAsync(IRequestMessages messageRequest);

        IMessagingResult Send(IRequestMessages messageRequest);

        /// <summary>
        /// Indicates if this message transmitter is a group level or individual level platform.  It's like SMS vs a Telegram group.
        /// </summary>
        /// <returns>If the message transmitter is a group, this will be falls.  If the message transmitter supports individual recipients, it is true.</returns>
        bool RequiresRecipients();

        /// <summary>
        /// Indicates if the underlying message transmitter supports callback operations.
        /// </summary>
        /// <returns>True if the message transmitter supports callbacks to receive supplemental message information after the initial send.</returns>
        bool SupportsCallbackProcessing();

        /// <summary>
        /// A list of callback actions and callback urls.  The implementing class will wire up the callbacks to the message transmitter.
        /// Twilio texts are one example of a service which sends error messages via callbacks.
        /// </summary>
        IDictionary<string, string> GetCallbackUrls();

        /// <summary>
        /// Gets the internal name of the mesage transmitter.  This is used for matching subscribed recipients with message transmitters among other things.
        /// </summary>
        /// <returns>The internal name of the message transmitter.</returns>
        string GetInternalName();

        /// <summary>
        /// Gets a friendly name for the message transmitter which can be used for display purposes.
        /// </summary>
        /// <returns>The friendly name of this message transmitter.</returns>
        string GetDisplayName();

        void OnMessageSent(MessageSentArgs args);

        event EventHandler<MessageSentArgs> MessageSent;
    }
}
