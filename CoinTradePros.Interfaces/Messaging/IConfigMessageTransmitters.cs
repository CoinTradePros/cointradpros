﻿using System;
using System.Collections.Generic;

namespace CoinTradePros.Interfaces.Messaging
{
    public interface IConfigMessageTransmitters
    {
        bool SupportsConfigOverride();

        bool RequiresUserConfig();

        IDictionary<string, string> GetConfig();

        IDictionary<string, string> GetConfig(Guid userId);
    }
}
