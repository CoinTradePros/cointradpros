﻿using System;

namespace CoinTradePros.Interfaces.Messaging.Events
{
    public class MessageSentArgs
    {
        public Guid SourceUserId { get; set; }

        public Guid AlertId { get; set; }

        public int TotalMessages { get; set; }

        public int RemainingMessages { get; set; }
    }
}
