﻿using System.Collections.Generic;

namespace CoinTradePros.Interfaces.Messaging.Events
{
    public class RecipientSettingsEventArgs
    {
        public TransmitterType ChannelType { get; set; }

        public IDictionary<string, string> Settings { get; set; }
    }
}
