﻿using System;
using System.Collections.Generic;

namespace CoinTradePros.Interfaces.Messaging.Events
{
    public class RecipientsEventArgs
    {
        public ICollection<IReceiveMessages> Recipients { get; set; }
    }
}
