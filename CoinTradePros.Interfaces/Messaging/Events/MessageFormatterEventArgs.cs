﻿using System.Collections.Concurrent;
using System.Collections.Generic;

namespace CoinTradePros.Interfaces.Messaging.Events
{
    public class MessageFormatterEventArgs
    {
        public MessageFormatterEventArgs()
        {
            MessageFormatters = new ConcurrentDictionary<TransmitterType, IFormatMessages>();
        }

        public IDictionary<TransmitterType, IFormatMessages> MessageFormatters { get; }
    }
}
