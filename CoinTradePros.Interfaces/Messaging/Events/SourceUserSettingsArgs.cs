﻿using System;
using System.Collections.Generic;

namespace CoinTradePros.Interfaces.Messaging.Events
{
    public class SourceUserSettingsArgs
    {
        public Guid UserId { get; set; }

        public IDictionary<string, string> Settings { get; set; }
    }
}
