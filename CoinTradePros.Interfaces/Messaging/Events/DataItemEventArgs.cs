﻿namespace CoinTradePros.Interfaces.Messaging.Events
{
    public class DataItemEventArgs
    {
        public object DataItem { get; set; }
    }
}
