﻿namespace CoinTradePros.Interfaces.Messaging.Events
{
    public class MessageSettingsEventArgs
    {
        public IConfigMessageTransmitters MessageSettings { get; set; }
        public TransmitterType TransmitterType { get; set; }
    }
}
