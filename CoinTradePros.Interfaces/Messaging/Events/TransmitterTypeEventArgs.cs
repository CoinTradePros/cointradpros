﻿using System.Collections.Generic;

namespace CoinTradePros.Interfaces.Messaging.Events
{
    public class TransmitterTypeEventArgs 
    {
        public ICollection<TransmitterType> TransmitterTypes { get; set; }
    }
}
