﻿
using System;

namespace CoinTradePros.Interfaces.Messaging.Events
{
    public class MessageProcessingCompleteArgs
    {
        public IMessagingResult Results { get; set; }

        public Guid MessageId { get; set; }

        public TimeSpan ProcessingTime { get; set; }

        public int TotalMessageCount { get; set; }
    }
}