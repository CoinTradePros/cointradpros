﻿namespace CoinTradePros.Interfaces.Messaging.Events
{
    public class ChannelIdentifierEventArgs
    {
        public TransmitterType ChannelType { get; set; }

        public string ChannelIdentifier { get; set; }
    }
}
