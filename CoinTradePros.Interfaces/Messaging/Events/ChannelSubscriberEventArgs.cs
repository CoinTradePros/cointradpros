﻿namespace CoinTradePros.Interfaces.Messaging.Events
{
    public class ChannelSubscriberEventArgs
    {
        public bool IsChannelSubscriber { get; set; }

        public TransmitterType ChannelType { get; set; }
    }
}
