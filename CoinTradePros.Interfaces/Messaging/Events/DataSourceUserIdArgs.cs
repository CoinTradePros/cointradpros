﻿using System;

namespace CoinTradePros.Interfaces.Messaging.Events
{
    public class DataSourceUserIdArgs
    {
        public Guid? UserId { get; set; }
    }
}
