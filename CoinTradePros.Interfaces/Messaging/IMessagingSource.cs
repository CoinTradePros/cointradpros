﻿using System;
using System.Collections.Generic;
using CoinTradePros.Interfaces.Messaging.Events;

namespace CoinTradePros.Interfaces.Messaging
{
    public interface IMessagingSource
    {
        /// <summary>
        /// Gathers all the recipients for a particular message.
        /// </summary>
        /// <returns>The people who are supposed to get messages related to this particular messaging sourse, like a trade that has been transacted.</returns>
        ICollection<IReceiveMessages> GetMessagingRecipients();

        /// <summary>
        /// Provides a list of message formatters which are able to formatting this message source data into messages for various message transmitters.
        /// </summary>
        /// <returns>A list of message formatters.</returns>
        IDictionary<TransmitterType, IFormatMessages> GetMessageFormatters();

        /// <summary>
        /// Gets the necessary settings to send this message.
        /// </summary>
        /// <returns></returns>
        IDictionary<TransmitterType, IConfigMessageTransmitters> GetMessageSettings();

        IDictionary<string, string> GetSourceUserConfig();

        /// <summary>
        /// Provides the source data item for the contents of the message.
        /// </summary>
        /// <returns>An object which will be used by message processors to prepare the message contents.</returns>
        object GetDataItem();

        /// <summary>
        /// Gets the UserId of of the user from which this message originates, such as the coach who sent a trade alert, or the user who was just registered.
        /// </summary>
        /// <returns></returns>
        Guid GetSourceUserId();

        /// <summary>
        /// Gets the user selected grouping of messaging channels for the messages to be sent on.
        /// </summary>
        /// <returns>The list of transmitter types used for transmission.</returns>
        ICollection<TransmitterType> GetTransmitterTypes();

        void OnNeedTransmitterTypes(TransmitterTypeEventArgs args);

        void OnNeedDataItem(DataItemEventArgs args);

        void OnNeedDataSourceUserId(DataSourceUserIdArgs args);

        void OnNeedMessageFormatter(MessageFormatterEventArgs args);

        void OnNeedMessageSettings(MessageSettingsEventArgs args);

        void OnNeedRecipients(RecipientsEventArgs args);

        void OnNeedSourceUserSettings(SourceUserSettingsArgs args);

        event EventHandler<TransmitterTypeEventArgs> NeedTransmitterTypes;
        event EventHandler<DataItemEventArgs> NeedDataItem;
        event EventHandler<DataSourceUserIdArgs> NeedDataSourceUserId;
        event EventHandler<MessageFormatterEventArgs> NeedMessageFormatters;
        event EventHandler<MessageSettingsEventArgs> NeedMessageSettings;
        event EventHandler<RecipientsEventArgs> NeedRecipients;
        event EventHandler<SourceUserSettingsArgs> NeedSourceUserSettings;

    }
}
