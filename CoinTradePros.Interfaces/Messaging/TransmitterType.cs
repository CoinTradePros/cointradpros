﻿
namespace CoinTradePros.Interfaces.Messaging
{
    public enum TransmitterType
    {
        Discord,
        Telegram,
        Twilio,
        Email,
        Slack,
        Skype,
        Facebook,
        Twitter
    }
}
