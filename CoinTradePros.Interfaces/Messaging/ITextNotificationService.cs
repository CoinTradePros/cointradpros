using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace CoinTradePros.Interfaces.Messaging
{
    public interface ITextNotificationService : IIdentityMessageService
    {
        Task SendSmsNotification(string phoneNumber, string message, string statusCallback);
    }
}