﻿namespace CoinTradePros.Interfaces
{
    /*
 *  IMPORTANT: You should never ship/release your product until the status is >= 100
 *  
 *  
 *  Payments will post with a 'status' field, here are the currently defined values:
-2 = PayPal Refund or Reversal
-1 = Cancelled / Timed Out
0 = Waiting for buyer funds
1 = We have confirmed coin reception from the buyer
2 = Queued for nightly payout (if you have the Payout Mode for this coin set to Nightly)
3 = PayPal Pending (eChecks or other types of holds)
100 = Payment Complete. We have sent your coins to your payment address or 3rd party payment system reports the payment complete
For future-proofing your IPN handler you can use the following rules:
<0 = Failures/Errors
0-99 = Payment is Pending in some way
>=100 = Payment completed successfully
 */
    public enum CoinPaymentStatusType
    {

        Pending = 0,
        PayPalRefund = -2,
        Cancelled = -1,
        ConfirmedPayment = 1,
        QueuedForNightlyPayment = 2,
        PaypalPending = 3,
        Completed = 100
    }
}
