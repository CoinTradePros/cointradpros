﻿namespace CoinTradePros.Interfaces.Exchange
{
    public class BaseExchangeRequest : IExchangeRequest
    {
        public string ApiKey { get; set; }
        public string ApiSecret { get; set; }
    }
}
