﻿using System;

namespace CoinTradePros.Interfaces.Exchange
{
    public interface IOrderSegment
    {
        IExchangeOrder ExchangeOrder { get; set; }

        /// <summary>
        /// This is the id of the order from the exchange.  
        /// It may not be available in all situations
        /// </summary>
        object OrderId { get; set; }

        /// <summary>
        /// This is the currency pair like BTC/USD
        /// </summary>
        string Exchange { get; set; }

        DateTime TimeStamp { get; set; }

       string OrderType { get; set; }

        decimal Limit { get; set; }

        decimal Quantity { get; set; }

        decimal QuantityRemaining { get; set; }

        decimal Commission { get; set; }

        decimal Price { get; set; }

        decimal PricePerUnit { get; set; }

        bool IsConditional { get; set; }

        string Condition { get; set; }

        string ConditionTarget { get; set; }

        bool ImmediateOrCancel { get; set; }
    }
}
