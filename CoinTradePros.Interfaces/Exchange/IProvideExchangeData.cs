﻿using System.Collections.Generic;
using System.IO;

namespace CoinTradePros.Interfaces.Exchange
{
    public interface IProvideExchangeData
    {
        ExchangeApiRequestType GetApiAuthType();

        string GetExchangeName();

        /// <summary>
        /// Takes the data stream of CSV data and creates the IExchangeOrders from it
        /// Use the CsvFileReader class in this project to consume that data.
        /// </summary>
        /// <param name="incomingData">A Stream with the file data.</param>
        /// <returns></returns>
        ICollection<IExchangeOrder> GetOrders(Stream incomingData);

        ICollection<IExchangeOrder> GetOrders(IExchangeRequest request);
    }
}
