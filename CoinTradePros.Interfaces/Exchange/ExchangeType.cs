﻿namespace CoinTradePros.Interfaces.Exchange
{
    public enum ExchangeType
    {
        Poloniex,
        Bittrex,
        Bitfinex,
        Kraken,
        Cryptopia
    }
}
