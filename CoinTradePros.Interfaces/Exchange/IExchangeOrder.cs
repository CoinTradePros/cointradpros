﻿using System;
using System.Collections.Generic;

namespace CoinTradePros.Interfaces.Exchange
{
    public interface IExchangeOrder
    {
        object OrderId { get; set; }

        string OrderUuid { get; set; }

        string Exchange { get; set; }

        DateTime OpenDate { get; set; }

        DateTime CloseDate { get; set; }

        ExchangeOrderType OrderType { get; set; }

        string Market { get; set; }

        decimal BidAsk { get; set; }

        decimal UnitsFilled { get; set; }

        decimal UnitsTotal { get; set; }

        decimal ActualRate { get; set; }

        decimal CostProceeds { get; set; }

        ICollection<IOrderSegment> Segments { get; }
    }
}
