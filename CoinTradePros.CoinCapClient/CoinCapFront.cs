﻿using Newtonsoft.Json;

namespace CoinTradePros.CoinCapClient
{
    [JsonArray]
    public class FrontData
    {
        public FrontItem[] Property1 { get; set; }
    }

    public class FrontItem
    {
        public float cap24hrChange { get; set; }
        public string _long { get; set; }
        public float mktcap { get; set; }
        public float perc { get; set; }
        public float price { get; set; }
        public bool shapeshift { get; set; }
        public string _short { get; set; }
        public long supply { get; set; }
        public float usdVolume { get; set; }
        public float volume { get; set; }
        public float vwapData { get; set; }
        public float vwapDataBTC { get; set; }
    }

}
