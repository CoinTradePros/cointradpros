﻿using System;
using System.Collections.Generic;
using CoinTradePros.Interfaces.Messaging;
using CoinTradePros.Interfaces.Messaging.Events;

namespace CoinTradePros.Messaging.Core
{
    public class BaseMessageReceiver : IReceiveMessages
    {
        private IDictionary<string, string> _recipientSettings;
        private bool? _isSubscribed;
        private string _channelIdentifier;

        public IDictionary<string, string> GetRecipientSettings(TransmitterType transmitterType)
        {
            if (_recipientSettings != null) return _recipientSettings;

            RecipientSettingsEventArgs args = new RecipientSettingsEventArgs();
            args.ChannelType = transmitterType;
            OnNeedRecipientSettings(args);
            if (args.Settings == null)
            {
                throw new InvalidOperationException("Prior to calling GetRecipientSettings, an event handler must be atached to NeedRecipientSettings which supplies the recipient settings of the message");
            }
            _recipientSettings = args.Settings;
            return _recipientSettings;
        }

        public bool IsSubscribedToChannel(TransmitterType transmitterType)
        {
            if (ConfirmChannelSubscription == null)
            {
                throw new InvalidOperationException("IsSubscribedToChannel has been called, but there are no EvenHandlers for ConfirmChannelSubscription");
            }

            //if (_isSubscribed.HasValue)
            //{
            //    return _isSubscribed.Value;
            //}

            ChannelSubscriberEventArgs args = new ChannelSubscriberEventArgs();
            args.ChannelType = transmitterType;
            args.IsChannelSubscriber = false;
            OnConfirmChannelSubscription(args);
            _isSubscribed = args.IsChannelSubscriber;
            return _isSubscribed.Value;
        }

        public string GetChannelIdentifier(TransmitterType transmitterType)
        {
            if (NeedChannelIdentifier == null)
            {
                throw new InvalidOperationException("GetChannelIdentifier has been called, but there are no EvenHandlers for NeedChannelIdentifier");
            }
            //if (!string.IsNullOrEmpty(_channelIdentifier))
            //{
            //    return _channelIdentifier;
            //}
            ChannelIdentifierEventArgs args = new ChannelIdentifierEventArgs();
            args.ChannelType = transmitterType;
            OnNeedChannelIdentifier(args);
            _channelIdentifier = args.ChannelIdentifier;
            return _channelIdentifier;
        }

        public void OnNeedRecipientSettings(RecipientSettingsEventArgs args)
        {
            EventHandler<RecipientSettingsEventArgs> handler = NeedRecipientSettings;
            if (handler == null)
            {
                throw new InvalidOperationException("The EventHandler NeedRecipientSettings must be used for this method to work.");
            }
            handler.Invoke(this, args);
        }

        public void OnConfirmChannelSubscription(ChannelSubscriberEventArgs args)
        {
            EventHandler<ChannelSubscriberEventArgs> handler = ConfirmChannelSubscription;
            if (handler == null)
            {
                throw new InvalidOperationException("The EventHandler ConfirmChannelSubscription must be used for this method to work.");
            }
            handler.Invoke(this, args);
        }

        public void OnNeedChannelIdentifier(ChannelIdentifierEventArgs args)
        {
            EventHandler<ChannelIdentifierEventArgs> handler = NeedChannelIdentifier;
            if (handler == null)
            {
                throw new InvalidOperationException("The EventHandler NeedChannelIdentifier must be used for this method to work.");
            }
            handler.Invoke(this, args);
        }

        public event EventHandler<RecipientSettingsEventArgs> NeedRecipientSettings;
        public event EventHandler<ChannelSubscriberEventArgs> ConfirmChannelSubscription;
        public event EventHandler<ChannelIdentifierEventArgs> NeedChannelIdentifier;
    }
}
