﻿using System;
using System.Collections.Generic;
using System.Net;
using CoinTradePros.BusinessObjects;
using CoinTradePros.Interfaces.Messaging;

namespace CoinTradePros.Messaging.Core
{
    public class MessageSettingsManager
    {

        /// <summary>
        /// Adds settings for a MessagingTransmitter so that a ProTrader can send messages to subscribers on that channel.
        /// </summary>
        /// <param name="userId">The user id of the ProTrader</param>
        /// <param name="type">The messaging transmitter type</param>
        /// <param name="settingItems">Things like a Discord room id or API key/secret, etc.</param>
        public static void AddTransmitterSendSettings(string userId, TransmitterType type,
            IDictionary<string, string> settingItems)
        {
            foreach (var settingItem in settingItems)
            {
                UserSetting.AddSetting(userId, PlatformType.Messaging, settingItem.Key, settingItem.Value);
            }
        }

        public static void AddReceiveSettings(string userId, TransmitterType type, string channelId)
        {
            string subscriptionKey = $"{type}_Receive";
            string channelKey = $"{type}_ChannelId";

            UserSetting.AddSetting(userId, PlatformType.Messaging, subscriptionKey, bool.TrueString);
            UserSetting.AddSetting(userId, PlatformType.Messaging, channelKey, channelId);
        }

        /// <summary>
        /// Adds the user to the messaing transmitter.  It adds a setting for the transmitter identifier and subscribes them to that channel.
        /// </summary>
        /// <param name="userId">The user who will be added</param>
        /// <param name="type">The messaging transmitter the user will be added to.</param>
        public static void AddBasicSendSetting(string userId, TransmitterType type)
        {
            string sendKey = $"{type}_Send";
            UserSetting.AddSetting(userId, PlatformType.Messaging, sendKey, bool.TrueString);
        }

        public static bool HasReceiveSettings(string userId, TransmitterType type)
        {
            UserSetting setting = UserSetting.GetSetting(userId, PlatformType.Messaging, $"{type}_ChannelId");
            return setting != null;
        }

        /// <summary>
        /// Changes the status of a messaging transmitter to false, but keeps the settings intact.
        /// </summary>
        /// <param name="userId">The id of the user to unsubscribe</param>
        /// <param name="type">The messaging platform the user will be unsubscribed from.</param>
        public void Unsubscribe(Guid userId, TransportType type)
        {
            throw new NotImplementedException();
        }

        public void RemoveReceiveSettings(Guid userId, TransportType type)
        {
            throw new NotImplementedException();
        }

        public void RemoveSendSettings(Guid userId, TransportType type)
        {
            throw new NotImplementedException();
        }

    }
}
