﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CoinTradePros.Interfaces.Messaging;
using CoinTradePros.Interfaces.Messaging.Events;
using CoinTradePros.Messaging.Factory;

namespace CoinTradePros.Messaging.Core
{
    public class BaseMessageProcessor : IProcessMessages
    {
        private readonly IDictionary<string, string> _messageFields;
        private int _messageCount;

        public BaseMessageProcessor()
        {
            _messageFields = new Dictionary<string, string>();
            _messageCount = 0;
        }

        public IMessagingResult ProcessMessage(IMessagingSource source)
        {
            throw new NotImplementedException();
        }

        public async Task<IMessagingResult> ProcessMessageAsync(IMessagingSource source)
        {
            /*
             * 1.  Get the configuration
             * 2.  Get the formatters
             * 3.  Get the recipients
             * 4.  Process the formatting
             * 5.  Send message to recipients
             * 6.  Collect processing results/errors
             */
            int messageCount = 0;
            DateTime startTime = DateTime.UtcNow;

            // 1.  Get the configuration
            IDictionary<TransmitterType, IConfigMessageTransmitters> messageConfig = source.GetMessageSettings();

            // 2.  Get the formatters
            IDictionary<TransmitterType, IFormatMessages> messageFormatters = 
                source.GetMessageFormatters();

            // 3.  Get the recipients
            ICollection<IReceiveMessages> recipients = source.GetMessagingRecipients();

            ICollection<TransmitterType> transmitterTypes = source.GetTransmitterTypes();

            ICollection<ITransmitMessages> transmitters = 
                MessageTransmitterFactory.GetInstances(transmitterTypes);

            IDictionary<string, string> sourceUserConfig = source.GetSourceUserConfig();
            foreach (var field in _messageFields)
            {
                if (!sourceUserConfig.ContainsKey(field.Key))
                {
                    sourceUserConfig.Add(field.Key, field.Value);
                }
            }

            IRequestMessages messageRequest = MessageRequestFactory.GetInstance(
                recipients, messageConfig, messageFormatters, source.GetDataItem(),
                sourceUserConfig);

            IMessagingResult results = MessagingResultFactory.GetInstance();

            foreach (var transmitter in transmitters)
            {
                try
                {
                    transmitter.MessageSent += MessageSent;
                    var res = await transmitter.SendAsync(messageRequest);
                    results.AddErrors(res);
                }
                catch (Exception ex)
                {
                    results.AddError("Exception", ex.Message);
                }
            }
            MessageProcessingCompleteArgs processingArgs = new MessageProcessingCompleteArgs();
            processingArgs.ProcessingTime = DateTime.UtcNow - startTime;
            processingArgs.Results = results;
            OnMessageProcessingComplete(processingArgs);
            return results;
        }

        private void MessageSent(object sender, MessageSentArgs e)
        {
            _messageCount++;
        }

        public void AddMessageField(string key, string value)
        {
            if (!_messageFields.ContainsKey(key))
            {
                _messageFields.Add(key, value);
            }
        }

        public void OnMessageProcessingComplete(MessageProcessingCompleteArgs args)
        {
            MessageProcessingCompleted?.Invoke(this, args);
        }

        public event EventHandler<MessageProcessingCompleteArgs> MessageProcessingCompleted;
    }
}
