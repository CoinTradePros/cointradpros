﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoinTradePros.Interfaces.Messaging;

namespace CoinTradePros.Messaging
{

    /// <summary>
    /// Implements a simple IMessage class.
    /// </summary>
    public class BaseMessage : IMessage
    {
        public BaseMessage()
        {
            MessageBag = new ConcurrentDictionary<string, object>();
        }

        public string MessageHeading { get; set; }
        public string MessageBody { get; set; }
        public IDictionary<string, object> MessageBag { get; set; }
    }
}
