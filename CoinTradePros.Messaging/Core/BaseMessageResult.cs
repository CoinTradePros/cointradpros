﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using CoinTradePros.Interfaces.Messaging;

namespace CoinTradePros.Messaging.Core
{
    
    public class BaseMessageResult : IMessagingResult
    {
        private readonly IDictionary<string, string> _errors;
        private TimeSpan _processingTime;
        private DateTimeOffset _startTime;
        private DateTimeOffset _endTime;

        public BaseMessageResult()
        {
            _errors = new ConcurrentDictionary<string, string>();
        }

        public IDictionary<string, string> GetErrors()
        {
            return _errors;
        }

        public void AddError(string key, string errorDetail)
        {
            if (!_errors.ContainsKey(key))
            {
                _errors.Add(key, errorDetail);
            }
            else
            {
                _errors.Add($"{key}_{Guid.NewGuid()}", errorDetail);
            }
        }

        public void AddErrors(IMessagingResult result)
        {
            if (result != null)
            {
                foreach (var error in result.GetErrors())
                {
                    _errors.Add(error.Key, error.Value);
                }
            }
        }

        public void Start()
        {
            _startTime = DateTimeOffset.UtcNow;
        }

        public void Stop()
        {
            _endTime = DateTimeOffset.UtcNow;
        }

        public TimeSpan GetProcessingTime()
        {
            return _endTime - _startTime;
        }
    }
}
