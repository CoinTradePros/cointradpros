﻿using System;
using System.Collections.Generic;
using CoinTradePros.Interfaces.Messaging;

namespace CoinTradePros.Messaging.Core
{
    public class BaseMessageRequest : IRequestMessages
    {
        private readonly ICollection<IReceiveMessages> _recipients;
        private readonly IDictionary<TransmitterType, IConfigMessageTransmitters> _configurators;
        private readonly IDictionary<TransmitterType, IFormatMessages> _messageFormatters;
        private readonly object _dataSource;
        private readonly IDictionary<string, string> _sourceUserConfig;

        public BaseMessageRequest(ICollection<IReceiveMessages> recipients, IDictionary<TransmitterType, IConfigMessageTransmitters> configurators, IDictionary<TransmitterType, IFormatMessages> messageFormatters, object dataSource, IDictionary<string, string> sourceUserConfig)
        {
            _recipients = recipients;
            _configurators = configurators;
            _messageFormatters = messageFormatters;
            _dataSource = dataSource;
            _sourceUserConfig = sourceUserConfig;
        }

        public ICollection<IReceiveMessages> GetRecipients()
        {
            return _recipients;
        }

        public IDictionary<string, string> GetSourceUserSettings()
        {
            return _sourceUserConfig;
        }

        public IDictionary<string, string> GetMessageSettings(TransmitterType transmitterType)
        {
            if (_configurators.ContainsKey(transmitterType))
            {
                IConfigMessageTransmitters config = _configurators[transmitterType];
                return config.GetConfig();
            }
            return new Dictionary<string, string>();
        }

        public IMessage GetMessage(TransmitterType transmitterType, IDictionary<string, string> messageBag)
        {
            IFormatMessages messageFormatter = _messageFormatters[transmitterType];
            IMessage message = messageFormatter.GetMessage(_dataSource, messageBag);
            return message;
        }
    }
}
