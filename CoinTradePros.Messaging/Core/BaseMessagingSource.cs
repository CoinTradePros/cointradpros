﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using CoinTradePros.Interfaces.Messaging;
using CoinTradePros.Interfaces.Messaging.Events;

namespace CoinTradePros.Messaging.Core
{
    public class BaseMessagingSource : IMessagingSource
    {
        private ICollection<IReceiveMessages> _recipients;
        private IDictionary<TransmitterType, IFormatMessages> _messageFormaters;
        private IDictionary<TransmitterType, IConfigMessageTransmitters> _configMessageTransmitters;
        private object _dataSource;
        private Guid? _sourceUserId;
        private ICollection<TransmitterType> _transmitterTypes;
        private readonly IDictionary<string, string> _messageFields;
        private IDictionary<string, string> _sourceUserSettings;

        public event EventHandler<TransmitterTypeEventArgs> NeedTransmitterTypes;
        public event EventHandler<DataItemEventArgs> NeedDataItem;
        public event EventHandler<DataSourceUserIdArgs> NeedDataSourceUserId;
        public event EventHandler<MessageFormatterEventArgs> NeedMessageFormatters;
        public event EventHandler<MessageSettingsEventArgs> NeedMessageSettings;
        public event EventHandler<RecipientsEventArgs> NeedRecipients;
        public event EventHandler<SourceUserSettingsArgs> NeedSourceUserSettings;

        public BaseMessagingSource()
        {
            _messageFields = new Dictionary<string, string>();
            _sourceUserSettings = new ConcurrentDictionary<string, string>();
        }

        public ICollection<IReceiveMessages> GetMessagingRecipients()
        {
            if (_recipients != null && _recipients.Count > 0) return _recipients;
            RecipientsEventArgs args = new RecipientsEventArgs();

            OnNeedRecipients(args);
            if (args.Recipients == null)
            {
                throw new InvalidOperationException("Prior to calling GetMessageRecipients, an event handler must be atached to NeedRecipientSettings which supplies the recipients of the message");
            }
            _recipients = args.Recipients;
            return _recipients;
        }

        public IDictionary<TransmitterType, IFormatMessages> GetMessageFormatters()
        {
            if (_messageFormaters != null && _messageFormaters.Count > 0) return _messageFormaters;
            MessageFormatterEventArgs args = new MessageFormatterEventArgs();

            OnNeedMessageFormatter(args);
            if (args.MessageFormatters == null)
            {
                throw new InvalidOperationException("Prior to calling GetMessageFormatters, an event handler must be atached to NeedMessageFormatters which supplies the message formatters of the message");
            }
            _messageFormaters = args.MessageFormatters;
            return _messageFormaters;
        }

        public IDictionary<TransmitterType, IConfigMessageTransmitters> GetMessageSettings()
        {
            if (_configMessageTransmitters != null && _configMessageTransmitters.Count > 0)
            {
                return _configMessageTransmitters;
            }

            IDictionary<TransmitterType, IConfigMessageTransmitters> allconfig = 
                new Dictionary<TransmitterType, IConfigMessageTransmitters>();

            foreach (var tp in GetTransmitterTypes())
            {
                MessageSettingsEventArgs args = new MessageSettingsEventArgs();
                args.TransmitterType = tp;
                OnNeedMessageSettings(args);
                if (args.MessageSettings == null)
                {
                    throw new InvalidOperationException("Prior to calling GetMessageSettings, an event handler must be atached to NeedMessageSettings which supplies the message settings used in formatting and sending the message.");
                }
                allconfig.Add(tp, args.MessageSettings);
            }
            _configMessageTransmitters = allconfig;

            return allconfig;
        }

        public IDictionary<string, string> GetSourceUserConfig()
        {
            if (_sourceUserSettings != null && _sourceUserSettings.Count > 0) return _sourceUserSettings;
            SourceUserSettingsArgs args = new SourceUserSettingsArgs();
            args.UserId = GetSourceUserId();
            OnNeedSourceUserSettings(args);
            if (args.Settings == null)
            {
                throw new InvalidOperationException("Prior to calling GetSourceUserConfig, an event handler must be atached to NeedSourceUserSettings which supplies the data item of the message");
            }
            _sourceUserSettings = args.Settings;
            return _sourceUserSettings;
        }

        public void AddMessageField(string key, string value)
        {
            _messageFields.Add(key, value);
        }

        public object GetDataItem()
        {
            if (_dataSource != null) return _dataSource;
            DataItemEventArgs args = new DataItemEventArgs();
            OnNeedDataItem(args);
            if (args.DataItem == null)
            {
                throw new InvalidOperationException("Prior to calling GetMessageFormatters, an event handler must be atached to NeedDataItem which supplies the data item of the message");
            }
            _dataSource = args.DataItem;
            return _dataSource;
        }

        public Guid GetSourceUserId()
        {
            DataSourceUserIdArgs args = new DataSourceUserIdArgs();
            OnNeedDataSourceUserId(args);
            if (args.UserId == null)
            {
                throw new InvalidOperationException("Prior to calling GetDataSourceUserId, an event handler must be atached to NeedDataSourceUserId which supplies the UserId of the message");
            }
            _sourceUserId = args.UserId;
            return _sourceUserId.Value;
        }

        public ICollection<TransmitterType> GetTransmitterTypes()
        {
            if (_transmitterTypes != null && _transmitterTypes.Count > 0) return _transmitterTypes;
            TransmitterTypeEventArgs args = new TransmitterTypeEventArgs();
            OnNeedTransmitterTypes(args);
            if (args.TransmitterTypes == null)
            {
                throw new InvalidOperationException("Prior to calling OnNeedTransmitterTypes, an event handler must be atached to NeedOnNeedTransmitterTypes which supplies the TransmitterTypes of the message");
            }
            _transmitterTypes = args.TransmitterTypes;
            return _transmitterTypes;
        }

        public void OnNeedSourceUserSettings(SourceUserSettingsArgs args)
        {
            EventHandler<SourceUserSettingsArgs> handler = NeedSourceUserSettings;
            handler?.Invoke(this, args);
        }

        public void OnNeedRecipients(RecipientsEventArgs args)
        {
            EventHandler<RecipientsEventArgs> handler = NeedRecipients;
            handler?.Invoke(this, args);
        }


        public void OnNeedTransmitterTypes(TransmitterTypeEventArgs args)
        {
            EventHandler<TransmitterTypeEventArgs> handler = NeedTransmitterTypes;
            handler?.Invoke(this, args);
        }

        public void OnNeedDataItem(DataItemEventArgs args)
        {
            EventHandler<DataItemEventArgs> handler = NeedDataItem;
            handler?.Invoke(this, args);
        }

        public void OnNeedDataSourceUserId(DataSourceUserIdArgs args)
        {
            EventHandler<DataSourceUserIdArgs> handler = NeedDataSourceUserId;
            if (handler == null)
            {
                throw new InvalidOperationException("The EventHandler NeedDataSourceUserId must be used for this method to work.");
            }
            handler?.Invoke(this, args);
        }

        public void OnNeedMessageFormatter(MessageFormatterEventArgs args)
        {
            EventHandler<MessageFormatterEventArgs> handler = NeedMessageFormatters;
            if (handler == null)
            {
                throw new InvalidOperationException("The EventHandler NeedMessageFormatters must be used for this method to work.");
            }
            handler?.Invoke(this, args);

        }

        public void OnNeedMessageSettings(MessageSettingsEventArgs args)
        {
            EventHandler<MessageSettingsEventArgs> handler = NeedMessageSettings;
            handler?.Invoke(this, args);
        }
    }
}
