﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CoinTradePros.Interfaces.Messaging;

namespace CoinTradePros.Messaging.Telegram
{
    public class TelegramConfigurator : IConfigMessageTransmitters
    {
        private IDictionary<Guid, IDictionary<string, string>> _userSettings;

        public TelegramConfigurator(IDictionary<Guid, IDictionary<string, string>> userSettings)
        {
            _userSettings = userSettings;
        }

        public bool SupportsConfigOverride()
        {
            bool allow;
            string allowStr = ConfigurationManager.AppSettings["AllowTelegramConfigOverride"];
            bool.TryParse(allowStr, out allow);
            return allow;
        }

        public bool RequiresUserConfig()
        {
            return true;
        }

        public IDictionary<string, string> GetConfig()
        {
            throw new InvalidOperationException("To Configure Telegram, the user specified group name is required, please use GetInstance(Guid userId)");
        }

        public IDictionary<string, string> GetConfig(Guid userId)
        {
            IDictionary<string, string> config = new Dictionary<string, string>();
            if (!SupportsConfigOverride())
            {
                string botToken = ConfigurationManager.AppSettings["TelegramBotToken"];
                config.Add("TelegramBotToken", botToken);
            }

            if (!_userSettings.ContainsKey(userId)) return config;
            IDictionary<string, string> settings = _userSettings[userId];
            foreach (var item in settings)
            {
                config.Add(item.Key, item.Value);
            }
            return config;

        }
    }
}
