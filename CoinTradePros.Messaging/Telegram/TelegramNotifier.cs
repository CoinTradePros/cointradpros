﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using CoinTradePros.Interfaces.Messaging;
using CoinTradePros.Interfaces.Messaging.Events;
using CoinTradePros.Messaging.Factory;
using TeleSharp.TL;
using TLSharp.Core;

namespace CoinTradePros.Messaging.Telegram
{
    public class TelegramNotifier : ITransmitMessages
    {
        public async Task<string> SendCode(string phoneNumber)
        {
            var client = GetClient();
            await client.ConnectAsync();
            var hash = await client.SendCodeRequestAsync(phoneNumber);
            return hash;
        }

        public async Task<TLUser> VerifyCode(string phoneNumber, string hash, string code)
        {
            //var client = GetClient();
            //var user = await client.MakeAuthAsync(phoneNumber, hash, code);
            //return user;
            return null;
        }

        private TelegramClient GetClient()
        {
            int apiid = GetApiId();
            string apihash = GetApiHash();
            var client = new TelegramClient(apiid, apihash);
            return client;
        }
        public virtual async Task AuthUser()
        {
            //var client = NewClient();

            //await client.ConnectAsync();

            //var hash = await client.SendCodeRequestAsync(NumberToAuthenticate);
            //var code = CodeToAuthenticate; // you can change code in debugger too

            //if (String.IsNullOrWhiteSpace(code))
            //{
            //    throw new Exception("CodeToAuthenticate is empty in the app.config file, fill it with the code you just got now by SMS/Telegram");
            //}

            //TLUser user = null;
            //try
            //{
                
            //    user = await client.MakeAuthAsync( hash, code);
            //}
            //catch (CloudPasswordNeededException ex)
            //{
            //    var password = await client.GetPasswordSetting();
            //    var password_str = PasswordToAuthenticate;

            //    user = await client.MakeAuthWithPasswordAsync(password, password_str);
            //}
            //catch (InvalidPhoneCodeException ex)
            //{
            //    throw new Exception("CodeToAuthenticate is wrong in the app.config file, fill it with the code you just got now by SMS/Telegram",
            //        ex);
            //}
            //Assert.IsNotNull(user);
            //Assert.IsTrue(client.IsUserAuthorized());
        }


        private int GetApiId()
        {
            int id;
            string value = ConfigurationManager.AppSettings["TelegramAppId"];
            if (int.TryParse(value, out id))
            {
                return id;
            }
            throw new ConfigurationErrorsException($"The TelegramApiHash value [{value}] was not an int");
        }

        public string GetApiHash()
        {
            return ConfigurationManager.AppSettings["TelegramAppHash"];

        }

        public async Task<IMessagingResult> SendAsync(IRequestMessages messageRequest)
        {
            IMessagingResult result = MessagingResultFactory.GetInstance();
            try
            {
                IDictionary<string, string> settings = messageRequest.GetSourceUserSettings();

                IMessage message = messageRequest.GetMessage(TransmitterType.Telegram, settings);
                string botToken = ConfigurationManager.AppSettings["TelegramBotToken"];
                string roomId = settings["TelegramRoomId"];
                string baseUrl = "https://api.telegram.org/bot";
                string requestUrl = $"{baseUrl}{botToken}/sendMessage?chat_id={roomId}&text={message.MessageBody}&parse_mode=HTML";
                WebRequest req = WebRequest.Create(requestUrl);

                //WebResponse resp = req.GetResponse();
                WebResponse resp = await req.GetResponseAsync();
                Stream stream = resp.GetResponseStream();
                StreamReader sr = new StreamReader(stream);
                string s = sr.ReadToEnd();
                sr.Close();

            }
            catch (Exception e)
            {
                result.AddError("Exception", e.Message);
            }
            return result;
        }

        public IMessagingResult Send(IRequestMessages messageRequest)
        {
            IMessagingResult result = MessagingResultFactory.GetInstance();

            try
            {
                IDictionary<string, string> settings = messageRequest.GetSourceUserSettings();

                IMessage message = messageRequest.GetMessage(TransmitterType.Telegram, settings);
                string botToken = ConfigurationManager.AppSettings["TelegramBotToken"];
                string roomId = settings["TelegramRoomId"];
                string baseUrl = "https://api.telegram.org/bot";
                string requestUrl = $"{baseUrl}{botToken}/sendMessage?chat_id={roomId}&text={message.MessageBody}&parse_mode=HTML";
                WebRequest req = WebRequest.Create(requestUrl);

                WebResponse resp = req.GetResponse();
                Stream stream = resp.GetResponseStream();
                StreamReader sr = new StreamReader(stream);
                string s = sr.ReadToEnd();
                sr.Close();

            }
            catch (Exception e)
            {
                result.AddError("Exception", e.Message);
            }
            return result;

            //try
            //{

            //    // trying to get them
            //    WebRequest req2 = WebRequest.Create("https://api.telegram.org/bot" + YourBotTokenHere + "/getUpdates");
            //    WebResponse resp2 = req2.GetResponse();
            //    Stream stream2 = resp2.GetResponseStream();
            //    StreamReader sr2 = new StreamReader(stream2);
            //    string s2 = sr2.ReadToEnd();
            //    sr2.Close();
            //}
            //catch (Exception e)
            //{
            //    Console.WriteLine("err");
            //}

        }

        public bool RequiresRecipients()
        {
            return false;
        }

        public bool SupportsCallbackProcessing()
        {
            return false;
        }

        public IDictionary<string, string> GetCallbackUrls()
        {
            throw new System.NotImplementedException();
        }

        public string GetInternalName()
        {
            return "Telegram";
        }

        public string GetDisplayName()
        {
            return "Telegram";
        }

        public void OnMessageSent(MessageSentArgs args)
        {
            MessageSent?.Invoke(this, args);
        }

        public event EventHandler<MessageSentArgs> MessageSent;
    }
}