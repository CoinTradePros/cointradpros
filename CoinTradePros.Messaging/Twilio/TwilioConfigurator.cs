﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using CoinTradePros.Interfaces.Messaging;

namespace CoinTradePros.Messaging.Twilio
{
    public class TwilioConfigurator : IConfigMessageTransmitters
    {
        public bool SupportsConfigOverride()
        {
            return false;
        }

        public bool RequiresUserConfig()
        {
            throw new NotImplementedException();
        }

        public IDictionary<string, string> GetConfig()
        {
            IDictionary<string, string> config = new ConcurrentDictionary<string, string>();
            config.Add("TwilioKey",  ConfigurationManager.AppSettings["TwilioKey"]);
            config.Add("TwilioSecret", ConfigurationManager.AppSettings["TwilioSecret"]);
            config.Add("TwilioPhoneNumber", ConfigurationManager.AppSettings["TwilioPhoneNumber"]);
            return config;
        }

        public IDictionary<string, string> GetConfig(Guid userId)
        {
            return GetConfig();
        }
    }
}
