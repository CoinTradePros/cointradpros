﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Threading.Tasks;
using CoinTradePros.Interfaces.Messaging;
using CoinTradePros.Interfaces.Messaging.Events;
using CoinTradePros.Messaging.Factory;
using Microsoft.AspNet.Identity;
using Twilio.Clients;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;

namespace CoinTradePros.Messaging.Twilio
{
    public class TwilioNotificationService : ITextNotificationService, ITransmitMessages
    {
        private readonly ITwilioRestClient _client;
        private readonly string _accountSid = ConfigurationManager.AppSettings["TwilioKey"];
        private readonly string _authToken = ConfigurationManager.AppSettings["TwilioSecret"];
        private readonly string _twilioNumber = ConfigurationManager.AppSettings["TwilioPhoneNumber"];

        public TwilioNotificationService()
        {
            _client = new TwilioRestClient(_accountSid, _authToken);
        }

        public TwilioNotificationService(ITwilioRestClient client)
        {
            _client = client;
        }

        public async Task SendSmsNotification(string phoneNumber, string message, string statusCallback)
        {
            var to = new PhoneNumber(phoneNumber);
            await MessageResource.CreateAsync(
                to,
                from: new PhoneNumber(_twilioNumber),
                body: message,
                //statusCallback: new Uri(statusCallback),
                client: _client);
        }

        public Task SendAsync(IdentityMessage message)
        {
            return SendSmsNotification(message.Destination, message.Body, string.Empty);
        }

        public async Task<IMessagingResult> SendAsync(IRequestMessages messageRequest)
        {
            IMessagingResult result = MessagingResultFactory.GetInstance();
            IMessage message = messageRequest.GetMessage(TransmitterType.Twilio, messageRequest.GetSourceUserSettings());
            ICollection<IReceiveMessages> recipients = messageRequest.GetRecipients();
            //ICollection<string, string> callbacks = null;
            foreach (var recipient in recipients)
            {
               await SendSmsNotification(recipient.GetChannelIdentifier(TransmitterType.Twilio), message.MessageBody, string.Empty);
            }
            return result;
        }

        public IMessagingResult Send(IRequestMessages messageRequest)
        {
            IMessagingResult result = MessagingResultFactory.GetInstance();
            IMessage message = messageRequest.GetMessage(TransmitterType.Twilio, messageRequest.GetSourceUserSettings());
            ICollection<IReceiveMessages> recipients = messageRequest.GetRecipients();
            //ICollection<string, string> callbacks = null;
            foreach (var recipient in recipients)
            {
                SendSmsNotification(recipient.GetChannelIdentifier(TransmitterType.Twilio), message.MessageBody, string.Empty);
            }
            return result;
        }

        public bool RequiresRecipients()
        {
            return true;
        }

        public bool SupportsCallbackProcessing()
        {
            return true;
        }

        public IDictionary<string, string> GetCallbackUrls()
        {
            throw new NotImplementedException();
        }

        public string GetInternalName()
        {
            return "TwilioSMS";
        }

        public string GetDisplayName()
        {
            return "Text/SMS";
        }

        public void OnMessageSent(MessageSentArgs args)
        {
            MessageSent?.Invoke(this, args);
        }

        public event EventHandler<MessageSentArgs> MessageSent;
    }
}