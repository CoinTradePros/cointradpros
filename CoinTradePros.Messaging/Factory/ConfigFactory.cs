﻿using System;
using System.Collections.Generic;
using CoinTradePros.Interfaces.Messaging;
using CoinTradePros.Messaging.Discord;
using CoinTradePros.Messaging.Email;
using CoinTradePros.Messaging.Facebook;
using CoinTradePros.Messaging.Skype;
using CoinTradePros.Messaging.Telegram;
using CoinTradePros.Messaging.Twilio;

namespace CoinTradePros.Messaging.Factory
{
    public class ConfigFactory
    {
        public static IConfigMessageTransmitters GetInstance(TransmitterType transmitterType, IDictionary<Guid, IDictionary<string, string>> settings)
        {
            switch (transmitterType)
            {
                case TransmitterType.Discord:
                {
                    return new DiscordConfigurator(settings);
                }
                case TransmitterType.Email:
                {
                    return new EmailConfigurator();
                }
                case TransmitterType.Facebook:
                {
                    return new FacebookConfigurator();
                }
                case TransmitterType.Skype:
                {
                    return new SkypeConfigurator();
                }
                case TransmitterType.Slack:
                {
                    return new SlackConfigurator();
                }
                case TransmitterType.Twilio:
                {
                    return new TwilioConfigurator();
                }
                case TransmitterType.Telegram:
                {
                    return new TelegramConfigurator(settings);
                }
                default:
                { 
                    throw new ApplicationException($"The TransmitterType: {transmitterType} is not supported");
                }
            }
        }
    }
}
