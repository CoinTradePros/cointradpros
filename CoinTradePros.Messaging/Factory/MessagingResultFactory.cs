﻿using CoinTradePros.Interfaces.Messaging;
using CoinTradePros.Messaging.Core;

namespace CoinTradePros.Messaging.Factory
{
    public class MessagingResultFactory
    {
        public static IMessagingResult GetInstance()
        {
            return new BaseMessageResult();
        }
    }
}
