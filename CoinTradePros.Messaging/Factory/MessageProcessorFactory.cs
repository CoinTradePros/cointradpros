﻿using CoinTradePros.Interfaces.Messaging;
using CoinTradePros.Messaging.Core;

namespace CoinTradePros.Messaging.Factory
{
    public class MessageProcessorFactory
    {
        public static IProcessMessages GetInstance()
        {
            return new BaseMessageProcessor();
        }
    }
}
