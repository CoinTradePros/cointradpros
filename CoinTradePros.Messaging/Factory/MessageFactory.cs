﻿using CoinTradePros.Interfaces.Messaging;

namespace CoinTradePros.Messaging.Factory
{
    public class MessageFactory
    {
        public static IMessage GetInstance()
        {
            return new BaseMessage();
        }
    }
}
