﻿using System;
using System.Collections.Generic;
using CoinTradePros.Interfaces.Messaging;
using CoinTradePros.Messaging.Discord;
using CoinTradePros.Messaging.Email;
using CoinTradePros.Messaging.Telegram;
using CoinTradePros.Messaging.Twilio;

namespace CoinTradePros.Messaging.Factory
{
    public class MessageTransmitterFactory
    {
        public static ICollection<ITransmitMessages> GetInstances(ICollection<TransmitterType> types)
        {
            ICollection<ITransmitMessages> transmitters = new List<ITransmitMessages>();
            foreach (var tt in types)
            {
                transmitters.Add(GetInstance(tt));
            }
            return transmitters;
        }

        public static ITransmitMessages GetInstance(TransmitterType type)
        {
            switch (type)
            {
                case TransmitterType.Discord:
                {
                    return new DiscordNotifier();
                }
                case TransmitterType.Email:
                {
                    return new EmailNotificationService();   
                }
                case TransmitterType.Facebook:
                {
                    throw new NotImplementedException("We are not quite ready to send messages to Facebook.");
                }
                case TransmitterType.Skype:
                {
                    throw new NotImplementedException("We are not quite ready to send messages to Skype.");
                }
                case TransmitterType.Slack:
                {
                    throw new NotImplementedException("We are not quite ready to send messages to Slack.");
                }
                case TransmitterType.Twilio:
                {
                    return new TwilioNotificationService();
                }
                case TransmitterType.Telegram:
                {
                    return new TelegramNotifier();
                }
                default:
                {
                    throw new ApplicationException($"The TransmitterType {type} is not supported.");
                }
            }
        }
    }
}
