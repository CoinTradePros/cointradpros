﻿using System;
using System.Collections.Generic;
using CoinTradePros.Interfaces.Messaging;
using CoinTradePros.Messaging.Core;

namespace CoinTradePros.Messaging.Factory
{
    public class MessageRequestFactory
    {
        public static IRequestMessages GetInstance(
            ICollection<IReceiveMessages> recipients,
            IDictionary<TransmitterType, IConfigMessageTransmitters> configurators,
            IDictionary<TransmitterType, IFormatMessages> messageFormatters, object dataSource,
            IDictionary<string, string> sourceUserConfig)
        {
            return new BaseMessageRequest(recipients, configurators, messageFormatters, dataSource, sourceUserConfig);
        }
    }
}
