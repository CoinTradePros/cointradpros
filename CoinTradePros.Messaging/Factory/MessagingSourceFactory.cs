﻿using CoinTradePros.Interfaces.Messaging;
using CoinTradePros.Messaging.Core;

namespace CoinTradePros.Messaging.Factory
{

    public class MessagingSourceFactory
    {
        public static IMessagingSource  GetInstance()
        {
            return new BaseMessagingSource();
        }
    }
}
