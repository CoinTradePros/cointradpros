﻿using CoinTradePros.Interfaces.Messaging;
using CoinTradePros.Messaging.Core;

namespace CoinTradePros.Messaging.Factory
{
    public class RecipientFactory
    {
        public static IReceiveMessages GetInstance()
        {
            return new BaseMessageReceiver();
        }
    }
}
