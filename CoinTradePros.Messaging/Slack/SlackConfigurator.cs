﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using CoinTradePros.Interfaces.Messaging;

namespace CoinTradePros.Messaging
{
    public class SlackConfigurator : IConfigMessageTransmitters
    {
        public bool SupportsConfigOverride()
        {
            return false;
        }

        public bool RequiresUserConfig()
        {
            throw new NotImplementedException();
        }

        public IDictionary<string, string> GetConfig()
        {
            return new ConcurrentDictionary<string, string>();
        }

        public IDictionary<string, string> GetConfig(Guid userId)
        {
            return new ConcurrentDictionary<string, string>();
        }
    }
}
