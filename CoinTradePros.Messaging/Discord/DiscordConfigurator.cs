﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CoinTradePros.Interfaces.Messaging;

namespace CoinTradePros.Messaging.Discord
{
    public class DiscordConfigurator : IConfigMessageTransmitters
    {
        private readonly IDictionary<Guid, IDictionary<string, string>> _userSettings;

        public DiscordConfigurator(IDictionary<Guid, IDictionary<string, string>> userSettings)
        {
            _userSettings = userSettings;
        }

        public bool SupportsConfigOverride()
        {
            bool allow;
            string allowStr = ConfigurationManager.AppSettings["AllowDiscordConfigOverride"];
            bool.TryParse(allowStr, out allow);
            return allow;
        }

        public bool RequiresUserConfig()
        {
            return true;
        }

        public IDictionary<string, string> GetConfig()
        {
            throw new InvalidOperationException("To Configure Discord, the user specified group name is required, please use GetInstance(Guid userId)");
        }

        public IDictionary<string, string> GetConfig(Guid userId)
        {
            // we need the discord bot token
            IDictionary<string, string> config = new Dictionary<string, string>();
            if (!SupportsConfigOverride())
            {
                string botToken = ConfigurationManager.AppSettings["DiscordBotToken"];
                config.Add("DiscordBotToken", botToken);
            }


            if (!_userSettings.ContainsKey(userId)) return config;
            IDictionary<string, string> settings = _userSettings[userId];
            foreach (var item in settings)
            {
                config.Add(item.Key, item.Value);
            }
            // Load the user config for the Discord group id
            return config;
        }
    }
}
