﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using CoinTradePros.Interfaces.Messaging;
using CoinTradePros.Interfaces.Messaging.Events;
using CoinTradePros.Messaging.Core;
using CoinTradePros.Messaging.Factory;
using Discord;

namespace CoinTradePros.Messaging.Discord
{
    /// <summary>
    ///https://discordapp.com/oauth2/authorize?client_id=368449761117995008&scope=bot&permissions=18432&response_type=code
    /// </summary>
    public class DiscordNotifier : ITransmitMessages
    {
        private static readonly object StaticObject = new object();
        private static readonly IDictionary<string, Server> Servers = new ConcurrentDictionary<string, Server>();
        private static DiscordClient _discordClient;

        public static void Init()
        {
            string botToken =  GetBotToken();

            _discordClient = new DiscordClient();
            _discordClient.JoinedServer += JoinedServer;
            _discordClient.ServerAvailable += ServerAvailable;
            _discordClient.Connect(botToken, TokenType.Bot);
        }

        public IMessagingResult Send(IRequestMessages messageRequest)
        {
            IMessagingResult result = new BaseMessageResult();
            try
            {
                IDictionary<string, string> userSettings = messageRequest.GetSourceUserSettings();
                string serverName = userSettings["DiscordServerId"];
                string channelName = userSettings["DiscordChannelId"];
                IMessage message = messageRequest.GetMessage(TransmitterType.Discord, userSettings);
                SendMessage(serverName, channelName, message.MessageBody);
            }
            catch (Exception ex)
            {
                result.AddError("DiscordException", ex.Message);       
            }
            return result;
        }

        public async Task<IMessagingResult> SendMessageAsync(string serverName, string channelName, string message)
        {
            IMessagingResult result = MessagingResultFactory.GetInstance();
            Server server = null;
            lock (StaticObject)
            {
                if (Servers.ContainsKey(serverName))
                {
                    server = Servers[serverName];
                }
                else
                {
                    result.AddError($"Discord_{serverName}_Error", $"The server named {serverName} was not found.  ");
                }
            }
            if (server != null)
            {
                var textChannel = string.IsNullOrEmpty(channelName) ? 
                    server.TextChannels.SingleOrDefault() : 
                    server.TextChannels.SingleOrDefault(x => string.Equals(x.Id.ToString(), channelName, StringComparison.CurrentCultureIgnoreCase));
                if (textChannel == null)
                {
                    result.AddError($"{serverName}_{channelName}_Error", $"Channel Name: {channelName} not found");
                }
                else
                {

                    Message x = await textChannel.SendMessage(message);
                    
                }
            }
            else
            {
                result.AddError($"{serverName}_{channelName}_Error", $"Server Name: {serverName} not found");
            }
            return result;
        }

        public void SendMessage(string serverName, string channelName, string message)
        {
            Server server;
            lock (StaticObject)
            {
                server = Servers[serverName];
            }
            var textChannel = !string.IsNullOrEmpty(channelName) ? 
                server?.TextChannels.SingleOrDefault(x => x.Name == channelName) :
                server?.TextChannels.SingleOrDefault();
            textChannel?.SendMessage(message);
        }

        public void SendMessage(string serverName, string message)
        {
            SendMessage(serverName, string.Empty, message);
        }

        private static void ServerAvailable(object sender, ServerEventArgs e)
        {
            CheckServer(e.Server);
        }

        private static void JoinedServer(object sender, ServerEventArgs e)
        {
            CheckServer(e.Server);
        }

        private static void CheckServer(Server server)
        {
            lock (StaticObject)
            {
                if (!Servers.ContainsKey(server.Id.ToString()))
                {
                    Servers.Add(server.Id.ToString(), server);
                }
            }
        }

        public static string GetClientId()
        {
            return ConfigurationManager.AppSettings["DiscordClientId"];
        }

        public static string GetClientSecret()
        {
            return ConfigurationManager.AppSettings["DiscordClientSecret"];
        }

        public static string GetBotUsername()
        {
            return ConfigurationManager.AppSettings["DiscordBotUsername"];
        }

        public static string GetBotToken()
        {
            return ConfigurationManager.AppSettings["DiscordBotToken"];
        }

        public IMessagingResult SendMessage(IRequestMessages messageRequest)
        {
            IMessagingResult result = MessagingResultFactory.GetInstance();
            var userSettings = messageRequest.GetSourceUserSettings();
            string serverName= string.Empty, channelName = string.Empty;
            if (userSettings.ContainsKey("DiscordServerId"))
            {
                serverName = userSettings["DiscordServerId"];
            }
            else
            {
                result.AddError("DiscordServerId",  $"The setting key DiscordServerId was not found");
            }

            if (userSettings.ContainsKey("DiscordChannelId"))
            {
                channelName = userSettings["DiscordChannelId"];
            }
            else
            {
                result.AddError("DiscordServerId", $"The setting key DiscordServerId was not found");
            }

            IMessage message =
                messageRequest.GetMessage(TransmitterType.Discord, messageRequest.GetSourceUserSettings());

            SendMessage(serverName, channelName, message.MessageBody);
            return result;
        }

        public async Task<IMessagingResult> SendAsync(IRequestMessages messageRequest)
        {
            IMessagingResult result = MessagingResultFactory.GetInstance();
            result.Start();
            var userSettings = messageRequest.GetSourceUserSettings();
            string serverName = string.Empty, channelName = string.Empty;
            if (userSettings.ContainsKey("DiscordServerId"))
            {
                serverName = userSettings["DiscordServerId"];
            }
            else
            {
                result.AddError("DiscordServerId", $"The setting key DiscordServerId was not found");
            }

            if (userSettings.ContainsKey("DiscordChannelId"))
            {
                channelName = userSettings["DiscordChannelId"];
            }
            else
            {
                result.AddError("DiscordServerId", $"The setting key DiscordServerId was not found");
            }

            IMessage message =
                messageRequest.GetMessage(TransmitterType.Discord, messageRequest.GetSourceUserSettings());

            IMessagingResult more = await SendMessageAsync(serverName, channelName, message.MessageBody);
            result.AddErrors(more);
            result.Stop();
            return result;
        }

        public bool RequiresRecipients()
        {
            return false;
        }

        public bool SupportsCallbackProcessing()
        {
            return false;
        }

        public IDictionary<string, string> GetCallbackUrls()
        {
            throw new System.NotImplementedException();
        }

        public string GetInternalName()
        {
            return "Discord";
        }

        public string GetDisplayName()
        {
            return "Discord";
        }

        public void OnMessageSent(MessageSentArgs args)
        {
            MessageSent?.Invoke(this, args);
        }

        public event EventHandler<MessageSentArgs> MessageSent;
    }
}