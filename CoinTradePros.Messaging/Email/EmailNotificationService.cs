﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Threading.Tasks;
using CoinTradePros.Interfaces.Messaging;
using CoinTradePros.Interfaces.Messaging.Events;
using CoinTradePros.Messaging.Factory;
using Microsoft.AspNet.Identity;
using RestSharp.Extensions;

namespace CoinTradePros.Messaging.Email
{
    public class EmailNotificationService : IIdentityMessageService, ITransmitMessages
    {
        public async Task SendAsync(IdentityMessage message)
        {
            var mail = new MailMessage();
            mail.Body = message.Body;
            mail.To.Add(new MailAddress(message.Destination));
            // From address is in the Web.config mail settings.
            mail.Subject = message.Subject;
            mail.IsBodyHtml = true;
            using (SmtpClient smtp = new SmtpClient())
            {
                // config for SMTP client is in the Web.config mail settings.
                smtp.SendCompleted += (s, e) =>
                {
                    smtp.Dispose();
                };
                await smtp.SendMailAsync(mail);
            }
        }


        public async Task<IMessagingResult> SendAsync(IRequestMessages messageRequest)
        {
            IMessage message = messageRequest.GetMessage(TransmitterType.Email, messageRequest.GetSourceUserSettings());
            var recipients = messageRequest.GetRecipients();
            IMessagingResult result = MessagingResultFactory.GetInstance();
            foreach (IReceiveMessages recipient in recipients)
            {
                try
                {
                    string address = recipient.GetChannelIdentifier(TransmitterType.Email);
                    IMessagingResult tmp = await
                        SendMessageAsync(message.MessageBody, message.MessageHeading, true, address);
                    result.AddErrors(tmp);

                    //SendMessage(message.MessageBody, message.MessageHeading, true, address, true);
                }
                catch (Exception ex)
                {
                    result.AddError(Guid.NewGuid().ToString(), ex.Message);
                }
            }
            return result;
        }

        public IMessagingResult Send(IRequestMessages messageRequest)
        {
            IMessage message = messageRequest.GetMessage(TransmitterType.Email, messageRequest.GetSourceUserSettings());
            var recipients = messageRequest.GetRecipients();
            IMessagingResult result = MessagingResultFactory.GetInstance();
            foreach (IReceiveMessages recipient in recipients)
            {
                try
                {
                    string address = recipient.GetChannelIdentifier(TransmitterType.Email);
                    SendMessage(message.MessageBody, message.MessageHeading, true, address, false);
                }
                catch (Exception ex)
                {
                    result.AddError(Guid.NewGuid().ToString(), ex.Message);
                }
            }
            return result;
        }

        //public void SendMessage(string body, string subject, bool isBodyHtml, string recipient)
        //{
        //    var mail = new MailMessage();
        //    mail.Body = body;
        //    mail.To.Add(new MailAddress(recipient));
        //    // From address is in the Web.config mail settings.
        //    mail.Subject = subject;
        //    mail.IsBodyHtml = isBodyHtml;
        //    using (SmtpClient smtp = new SmtpClient())
        //    {
        //        // config for SMTP client is in the Web.config mail settings.

        //        smtp.SendCompleted += (s, e) =>
        //        {
        //            smtp.Dispose();
        //        };
        //        smtp.SendAsync(mail, null);
        //    }
        //}

        public async Task<IMessagingResult> SendMessageAsync(string body, string subject, bool isBodyHtml, string recipient)
        {
            var mail = new MailMessage();
            mail.Body = body;
            mail.To.Add(new MailAddress(recipient));
            // From address is in the Web.config mail settings.
            mail.Subject = subject;
            mail.IsBodyHtml = isBodyHtml;
            IMessagingResult result = MessagingResultFactory.GetInstance();
            result.Start();

            using (SmtpClient smtp = new SmtpClient())
            {
                // config for SMTP client is in the Web.config mail settings.
                smtp.SendCompleted += (s, e) =>
                {
                    if (e.Error != null)
                    {
                        result.AddError(Guid.NewGuid().ToString(), e.Error.Message);
                    }
                    smtp?.Dispose();
                };
                await smtp.SendMailAsync(mail);
            }
            result.Stop();
            return result;
        }


        public void SendMessage(string body, string subject, bool isBodyHtml, string recipient, bool useAsync)
        {
            var mail = new MailMessage();
            mail.Body = body;
            mail.To.Add(new MailAddress(recipient));
            // From address is in the Web.config mail settings.
            mail.Subject = subject;
            mail.IsBodyHtml = isBodyHtml;
            using (SmtpClient smtp = new SmtpClient())
            {
                // config for SMTP client is in the Web.config mail settings.
                if (useAsync)
                {
                    smtp.SendCompleted += (s, e) =>
                    {
                        smtp?.Dispose();
                    };
                    smtp.SendMailAsync(mail);
                }
                else
                {
                    smtp.Send(mail);
                }
            }
        }

        public bool RequiresRecipients()
        {
            return true;
        }

        public bool SupportsCallbackProcessing()
        {
            return false;
        }

        public IDictionary<string, string> GetCallbackUrls()
        {
            throw new System.NotImplementedException();
        }

        public string GetInternalName()
        {
            return "Email";
        }

        public string GetDisplayName()
        {
            return "Email";
        }

        public void OnMessageSent(MessageSentArgs args)
        {
            MessageSent?.Invoke(this, args);
        }

        public event EventHandler<MessageSentArgs> MessageSent;
    }
}