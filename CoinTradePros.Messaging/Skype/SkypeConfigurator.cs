﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using CoinTradePros.Interfaces.Messaging;

namespace CoinTradePros.Messaging.Skype
{
    public class SkypeConfigurator : IConfigMessageTransmitters
    {
        public bool SupportsConfigOverride()
        {
            return true;
        }

        public bool RequiresUserConfig()
        {
            throw new NotImplementedException();
        }

        public IDictionary<string, string> GetConfig()
        {
            return new ConcurrentDictionary<string, string>();
        }

        public IDictionary<string, string> GetConfig(Guid userId)
        {
            return new ConcurrentDictionary<string, string>();
        }
    }
}
