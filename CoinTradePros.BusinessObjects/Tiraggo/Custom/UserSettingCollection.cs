//==============================================
// Generated by http://www.My2ndGeneration.com
//==============================================

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace CoinTradePros.BusinessObjects
{
    public partial class UserSettingCollection : tgUserSettingCollection
    {

        public static UserSettingCollection GetGroupSettings(ICollection<Guid> userIds, string platform)
        {
            UserSettingCollection settings = new UserSettingCollection();
            UserSettingQuery usq = new UserSettingQuery("us");

            usq.Select().Where(usq.Platform == platform && usq.UserId.In());
            settings.Load(usq);
            return settings;
        }
        public static UserSettingCollection GetExchangeSettingbyName(string userId,string ExchangeName)
        {
            UserSettingCollection settings = new UserSettingCollection();
            UserSettingQuery usq = new UserSettingQuery("us");

            usq.Select().Where(usq.UserId==userId && usq.Platform=="Exchange" && usq.SettingKey.Like(ExchangeName+"%"));
            settings.Load(usq);
            return settings;
        }
       
        public static UserSettingCollection GetPlatformSettingGroup(Guid userId, string platform)
        {
            UserSettingCollection settings = new UserSettingCollection();
            UserSettingQuery usq = new UserSettingQuery("s");
            usq.Select().Where(usq.UserId == userId.ToString() && usq.Platform == platform);
            settings.Load(usq);
            UserSettingCollection otherSettings = new UserSettingCollection();

            foreach (var setting in settings)
            {
                if (setting.UserId == userId.ToString())
                {
                    otherSettings.Add(setting);
                }
            }
            return otherSettings;
        }

        //public UserSettingCollection GetPlatformSettingGroup(Guid userId, string platform)
        //{
        //    if (!HasData)
        //    {
        //        UserSettingQuery usq = new UserSettingQuery("s");
        //        usq.Select().Where(usq.UserId == userId.ToString() && usq.Platform == platform);
        //        Load(usq);
        //    }
        //    UserSettingCollection otherSettings = new UserSettingCollection();
        //    foreach (var setting in this.AsEnumerable())
        //    {
        //        if (setting.UserId == userId.ToString())
        //        {
        //            otherSettings.Add(setting);
        //        }
        //    }
        //    return otherSettings;
        //}

        public IDictionary<string, string> GetPlatformSettings(Guid userId, string platform)
        {
            IDictionary<string, string> platformSettings = new ConcurrentDictionary<string, string>();
            if (!HasData)
            {
                UserSettingQuery usq = new UserSettingQuery("s");
                usq.Select().Where(usq.UserId == userId.ToString() && usq.Platform == platform);
                Load(usq);
            }
            foreach (var setting in this.AsEnumerable())
            {
                if (setting.UserId == userId.ToString() && setting.Platform == platform)
                {
                    platformSettings.Add(setting.SettingKey, setting.SettingValue);
                }
            }
            return platformSettings;
        }
        public UserSetting GetUserSetting(Guid userId, string settingKey)
        {
            if (!HasData)
            {
                LoadUserSettings(userId);
            }

            foreach (var setting in this.AsEnumerable())
            {
                if (setting.UserId == userId.ToString() && setting.SettingKey == settingKey)
                {
                    return setting;
                }
            }
            return null;
        }
      

        public IDictionary<string, string> GetUserSettings(Guid userId)
        {
            IDictionary<string, string> settings = new ConcurrentDictionary<string, string>();
            if (!HasData)
            {
                LoadUserSettings(userId);
            }
            foreach (var setting in this.AsEnumerable())
            {
                settings.Add(setting.SettingKey, setting.SettingValue);
            }
            return settings;
        }

        public void LoadUserSettings(Guid userId)
        {
            UserSettingQuery usq = new UserSettingQuery("s");
            usq.Select().Where(usq.UserId == userId.ToString());
            Load(usq);
        }
    }
}
