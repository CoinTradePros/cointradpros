//==============================================
// Generated by http://www.My2ndGeneration.com
//==============================================


using System;
using System.Collections.Generic;
using System.Linq;
using Tiraggo.Interfaces;

namespace CoinTradePros.BusinessObjects
{
    public partial class Feature
	{
		public Feature()
		{

		}

	    public static ICollection<Feature> GetProTraderFeatures(Guid proTraderProfileId)
	    {
	        FeatureQuery fq = new FeatureQuery("f");
	        fq.Select().Where(fq.ProTraderProfileId == proTraderProfileId);
            fq.OrderBy(fq.SortOrder.Ascending);
	        FeatureCollection features = new FeatureCollection();
            features.Load(fq);
	        return features;
	    }

	    public static FeatureCollection GetDefaultFeatures(Guid proTraderProfileId)
	    {
            // Trade Alerts, Live Trading, Telegram Group, Crypto Watchlist, 
            FeatureCollection coll= new FeatureCollection();
	        FeatureType alertFeature = FeatureType.GetFeatureTypeByName("Trade Alerts");
	        FeatureType telegramGroupFeature = FeatureType.GetFeatureTypeByName("Telegram Group");
	        FeatureType liveTradingFeature = FeatureType.GetFeatureTypeByName("Live Trading");
	        FeatureType groupTradingFeature = FeatureType.GetFeatureTypeByName("Group Trade Coaching");
	        FeatureType soloTradeCoaching = FeatureType.GetFeatureTypeByName("One on One Trade Coaching");


            Feature feature = new Feature();
	        feature.Id = Guid.NewGuid();
	        feature.Name = "[Rocket Alerts]";
	        feature.Description = "[When we are about to make a great trade, we'll let you know.  ]";
	        feature.FeatureTypeId = alertFeature.Id;
	        feature.ProTraderProfileId = proTraderProfileId;
            feature.Save(tgSqlAccessType.DynamicSQL);

            coll.Add(feature);

            feature = new Feature();
	        feature.Id = Guid.NewGuid();
	        feature.Name = "[1 on 1 Trade Coaching]";
	        feature.Description = "Learn to trade like a pro by learning 1 on 1.  We'll help you understand trading fundamentals, technical analysis and more!";
	        feature.FeatureTypeId = soloTradeCoaching.Id;
	        feature.ProTraderProfileId = proTraderProfileId;
            feature.Save(tgSqlAccessType.DynamicSQL);

            coll.Add(feature);

            feature = new Feature();
	        feature.Id = Guid.NewGuid();
	        feature.Name = "[Private Telegram Group]";
	        feature.Description = "[Join everyone in the private Telegram group.  When you get a trade alert, jump in and trade with us there live.]";
	        feature.FeatureTypeId = telegramGroupFeature.Id;
	        feature.ProTraderProfileId = proTraderProfileId;
            feature.Save(tgSqlAccessType.DynamicSQL);

            coll.Add(feature);

            feature = new Feature();
	        feature.Id = Guid.NewGuid();
	        feature.Name = "[Live Group Trading]";
	        feature.Description = "[Once a week, we'll all go to the Telegram group and livestream our trades for an hour.]";
	        feature.FeatureTypeId = liveTradingFeature.Id;
	        feature.ProTraderProfileId = proTraderProfileId;
            feature.Save(tgSqlAccessType.DynamicSQL);

            coll.Add(feature);

            feature = new Feature();
	        feature.Id = Guid.NewGuid();
	        feature.Name = "[Weekly Group Trade Coaching]";
	        feature.Description = "[Each week, we'll hit the Telegram group and livestream coaching on trading fundamentals and technical analysis.]";
	        feature.FeatureTypeId = groupTradingFeature.Id;
	        feature.ProTraderProfileId = proTraderProfileId;
            feature.Save(tgSqlAccessType.DynamicSQL);

            coll.Add(feature);

	        return coll;
	    }
	}
}
