//==============================================
// Generated by http://www.My2ndGeneration.com
//==============================================

using System;
using System.Collections.Generic;

namespace CoinTradePros.BusinessObjects
{
	public partial class ChapterExamQuestionCollection : tgChapterExamQuestionCollection
	{
		public ChapterExamQuestionCollection()
		{

		}

	    public static ICollection<ChapterExamQuestion> GetExamQuestions(Guid examId)
	    {
	        ChapterExamQuestionQuery q = new ChapterExamQuestionQuery("ceq");
	        q.Where(q.ChapterExamId == examId);
            ChapterExamQuestionCollection coll = new ChapterExamQuestionCollection();
	        coll.Load(q);
	        return coll;
	    }
	}
}
