
//==============================================
// Generated by http://www.My2ndGeneration.com
//==============================================

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Linq;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Runtime.Serialization;

using Tiraggo.Core;
using Tiraggo.Interfaces;
using Tiraggo.DynamicQuery;

namespace CoinTradePros.BusinessObjects
{

	//===============================================
	// Concrete Entity Class
	//===============================================
	[DebuggerDisplay("Data = {Debug}")]
	[Serializable]
	[DataContract]
	[KnownType(typeof(MessageTransmitter))]
	[XmlType("MessageTransmitter")]
	public partial class MessageTransmitter : tgMessageTransmitter
	{
		[DebuggerBrowsable(DebuggerBrowsableState.RootHidden | DebuggerBrowsableState.Never)]
		protected override tgEntityDebuggerView[] Debug
		{
			get { return base.Debug; }
		}

		override public tgEntity CreateInstance()
		{
			return new MessageTransmitter();
		}

		#region Static Quick Access Methods
		static public void Delete(System.Guid id)
		{
			var obj = new MessageTransmitter();
			obj.Id = id;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save();
		}

		static public void Delete(System.Guid id, tgSqlAccessType sqlAccessType)
		{
			var obj = new MessageTransmitter();
			obj.Id = id;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save(sqlAccessType);
		}
		#endregion
	}

	//===============================================
	// Abstract Entity Class
	//===============================================	
	[DataContract]
	[Serializable]
	abstract public partial class tgMessageTransmitter : tgEntity
	{
		public tgMessageTransmitter()
		{

		}

		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Guid id)
		{
			if (this.tg.Connection.SqlAccessType == tgSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		public virtual bool LoadByPrimaryKey(tgSqlAccessType sqlAccessType, System.Guid id)
		{
			if (sqlAccessType == tgSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		private bool LoadByPrimaryKeyDynamic(System.Guid id)
		{
			MessageTransmitterQuery query = new MessageTransmitterQuery();
			query.Where(query.Id == id);
			return this.Load(query);
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Guid id)
		{
			tgParameters parms = new tgParameters();
			parms.Add("Id", id);
			return this.Load(tgQueryType.StoredProcedure, this.tg.spLoadByPrimaryKey, parms);
		}
		#endregion

		#region Properties

		/// <summary>
		/// Maps to MessageTransmitter.Id
		/// </summary>
		[DataMember(EmitDefaultValue = false)]
		virtual public Guid? Id
		{
			get
			{
				return base.GetSystemGuid(MessageTransmitterMetadata.ColumnNames.Id);
			}

			set
			{
				if (base.SetSystemGuid(MessageTransmitterMetadata.ColumnNames.Id, value))
				{
					OnPropertyChanged(MessageTransmitterMetadata.PropertyNames.Id);
				}
			}
		}

		/// <summary>
		/// Maps to MessageTransmitter.Name
		/// </summary>
		[DataMember(EmitDefaultValue = false)]
		virtual public string Name
		{
			get
			{
				return base.GetSystemString(MessageTransmitterMetadata.ColumnNames.Name);
			}

			set
			{
				if (base.SetSystemString(MessageTransmitterMetadata.ColumnNames.Name, value))
				{
					OnPropertyChanged(MessageTransmitterMetadata.PropertyNames.Name);
				}
			}
		}

		/// <summary>
		/// Maps to MessageTransmitter.SysCode
		/// </summary>
		[DataMember(EmitDefaultValue = false)]
		virtual public string SysCode
		{
			get
			{
				return base.GetSystemString(MessageTransmitterMetadata.ColumnNames.SysCode);
			}

			set
			{
				if (base.SetSystemString(MessageTransmitterMetadata.ColumnNames.SysCode, value))
				{
					OnPropertyChanged(MessageTransmitterMetadata.PropertyNames.SysCode);
				}
			}
		}

		/// <summary>
		/// Maps to MessageTransmitter.Description
		/// </summary>
		[DataMember(EmitDefaultValue = false)]
		virtual public string Description
		{
			get
			{
				return base.GetSystemString(MessageTransmitterMetadata.ColumnNames.Description);
			}

			set
			{
				if (base.SetSystemString(MessageTransmitterMetadata.ColumnNames.Description, value))
				{
					OnPropertyChanged(MessageTransmitterMetadata.PropertyNames.Description);
				}
			}
		}

		/// <summary>
		/// Maps to MessageTransmitter.ImagePath
		/// </summary>
		[DataMember(EmitDefaultValue = false)]
		virtual public string ImagePath
		{
			get
			{
				return base.GetSystemString(MessageTransmitterMetadata.ColumnNames.ImagePath);
			}

			set
			{
				if (base.SetSystemString(MessageTransmitterMetadata.ColumnNames.ImagePath, value))
				{
					OnPropertyChanged(MessageTransmitterMetadata.PropertyNames.ImagePath);
				}
			}
		}

		/// <summary>
		/// Maps to MessageTransmitter.ShowExternal
		/// </summary>
		[DataMember(EmitDefaultValue = false)]
		virtual public bool? ShowExternal
		{
			get
			{
				return base.GetSystemBoolean(MessageTransmitterMetadata.ColumnNames.ShowExternal);
			}

			set
			{
				if (base.SetSystemBoolean(MessageTransmitterMetadata.ColumnNames.ShowExternal, value))
				{
					OnPropertyChanged(MessageTransmitterMetadata.PropertyNames.ShowExternal);
				}
			}
		}

		/// <summary>
		/// Maps to MessageTransmitter.SupportsGroups
		/// </summary>
		[DataMember(EmitDefaultValue = false)]
		virtual public bool? SupportsGroups
		{
			get
			{
				return base.GetSystemBoolean(MessageTransmitterMetadata.ColumnNames.SupportsGroups);
			}

			set
			{
				if (base.SetSystemBoolean(MessageTransmitterMetadata.ColumnNames.SupportsGroups, value))
				{
					OnPropertyChanged(MessageTransmitterMetadata.PropertyNames.SupportsGroups);
				}
			}
		}

		/// <summary>
		/// Maps to MessageTransmitter.SupportsUsers
		/// </summary>
		[DataMember(EmitDefaultValue = false)]
		virtual public bool? SupportsUsers
		{
			get
			{
				return base.GetSystemBoolean(MessageTransmitterMetadata.ColumnNames.SupportsUsers);
			}

			set
			{
				if (base.SetSystemBoolean(MessageTransmitterMetadata.ColumnNames.SupportsUsers, value))
				{
					OnPropertyChanged(MessageTransmitterMetadata.PropertyNames.SupportsUsers);
				}
			}
		}


		// For Tiraggo.js support and basic serialization of extra properties
		// brought back via Joins
		[DataMember(EmitDefaultValue = false)]
        public tgKeyValuePair[] tgExtendedData
        {
            get
            {
                Dictionary<string, object> extra = GetExtraColumns();

                if (extra.Keys.Count > 0)
                {
                    List<tgKeyValuePair> extended = new List<tgKeyValuePair>();

                    foreach (string key in extra.Keys)
                    {
                        extended.Add(new tgKeyValuePair { Key = key, Value = extra[key] });
                    }

                    return extended.ToArray();
                }

                return null;
            }

            set
            {
                foreach (tgKeyValuePair pair in value)
                {
                    this.SetColumn(pair.Key, pair.Value);
                }
            }
        }

		#endregion

		#region Housekeeping methods

		override protected IMetadata Meta
		{
			get
			{
				return MessageTransmitterMetadata.Meta();
			}
		}

		#endregion

		#region Query Logic

		public MessageTransmitterQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new MessageTransmitterQuery();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(MessageTransmitterQuery query)
		{
			this.query = query;
			InitQuery(this.query);
			return this.Query.Load();
		}

		protected void InitQuery(MessageTransmitterQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;

			if (!query.tg2.HasConnection)
			{
				query.tg2.Connection = ((IEntity)this).Connection;
			}
		}

		#endregion

		[IgnoreDataMember]
		private MessageTransmitterQuery query;
	}

	//===============================================
	// Concrete Collection Class
	//===============================================
	[DebuggerDisplay("Count = {Count}")]
	[Serializable]
	[CollectionDataContract]
	[XmlType("MessageTransmitterCollection")]
	public partial class MessageTransmitterCollection : tgMessageTransmitterCollection, IEnumerable<MessageTransmitter>
	{
		public MessageTransmitter FindByPrimaryKey(Guid id)
		{
			return this.SingleOrDefault(e => e.Id == id);
		}

		#region WCF Service Class

		[DataContract]
		[KnownType(typeof(MessageTransmitter))]
		public class MessageTransmitterCollectionWCFPacket : tgCollectionWCFPacket<MessageTransmitterCollection>
		{
			public static implicit operator MessageTransmitterCollection(MessageTransmitterCollectionWCFPacket packet)
			{
				return packet.Collection;
			}

			public static implicit operator MessageTransmitterCollectionWCFPacket(MessageTransmitterCollection collection)
			{
				return new MessageTransmitterCollectionWCFPacket() { Collection = collection };
			}
		}

		#endregion
	}

	//===============================================
	// Abstract Collection Class
	//===============================================	
	[Serializable]
	abstract public partial class tgMessageTransmitterCollection : tgEntityCollection<MessageTransmitter>
	{
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return MessageTransmitterMetadata.Meta();
			}
		}

		protected override string GetCollectionName()
		{
			return "MessageTransmitterCollection";
		}

		#endregion

		#region Query Logic

		[BrowsableAttribute(false)]
		public MessageTransmitterQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new MessageTransmitterQuery();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(MessageTransmitterQuery query)
		{
			this.query = query;
			InitQuery(this.query);
			return Query.Load();
		}

		override protected tgDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new MessageTransmitterQuery();
				this.InitQuery(query);
			}
			return this.query;
		}

		protected void InitQuery(MessageTransmitterQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			if (!query.tg2.HasConnection)
			{
				query.tg2.Connection = ((IEntityCollection)this).Connection;
			}
		}

		protected override void HookupQuery(tgDynamicQuery query)
		{
			this.InitQuery((MessageTransmitterQuery)query);
		}

		#endregion

		private MessageTransmitterQuery query;
	}	

	//===============================================
	// Concrete Query Class
	//===============================================	
	[DebuggerDisplay("Query = {Parse()}")]
	public partial class MessageTransmitterQuery : tgMessageTransmitterQuery
	{
		public MessageTransmitterQuery(string joinAlias)
		{
			this.tg.JoinAlias = joinAlias;
		}

		override protected string GetQueryName()
		{
			return "MessageTransmitterQuery";
		}

		#region Explicit Casts

		public static explicit operator string(MessageTransmitterQuery query)
		{
			return MessageTransmitterQuery.SerializeHelper.ToXml(query);
		}

		public static explicit operator MessageTransmitterQuery(string query)
		{
			return (MessageTransmitterQuery)MessageTransmitterQuery.SerializeHelper.FromXml(query, typeof(MessageTransmitterQuery));
		}

		#endregion
	}

	//===============================================
	// Abstract Query Class
	//===============================================
	abstract public partial class tgMessageTransmitterQuery : tgDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return MessageTransmitterMetadata.Meta();
			}
		}	

		#region QueryItemFromName

		protected override tgQueryItem QueryItemFromName(string name)
		{
			switch (name)
			{
				case "Id": return this.Id;
				case "Name": return this.Name;
				case "SysCode": return this.SysCode;
				case "Description": return this.Description;
				case "ImagePath": return this.ImagePath;
				case "ShowExternal": return this.ShowExternal;
				case "SupportsGroups": return this.SupportsGroups;
				case "SupportsUsers": return this.SupportsUsers;

				default: return null;
			}
		}		

		#endregion

		#region tgQueryItems

		public tgQueryItem Id
		{
			get { return new tgQueryItem(this, MessageTransmitterMetadata.ColumnNames.Id, tgSystemType.Guid); }
		}

		public tgQueryItem Name
		{
			get { return new tgQueryItem(this, MessageTransmitterMetadata.ColumnNames.Name, tgSystemType.String); }
		}

		public tgQueryItem SysCode
		{
			get { return new tgQueryItem(this, MessageTransmitterMetadata.ColumnNames.SysCode, tgSystemType.String); }
		}

		public tgQueryItem Description
		{
			get { return new tgQueryItem(this, MessageTransmitterMetadata.ColumnNames.Description, tgSystemType.String); }
		}

		public tgQueryItem ImagePath
		{
			get { return new tgQueryItem(this, MessageTransmitterMetadata.ColumnNames.ImagePath, tgSystemType.String); }
		}

		public tgQueryItem ShowExternal
		{
			get { return new tgQueryItem(this, MessageTransmitterMetadata.ColumnNames.ShowExternal, tgSystemType.Boolean); }
		}

		public tgQueryItem SupportsGroups
		{
			get { return new tgQueryItem(this, MessageTransmitterMetadata.ColumnNames.SupportsGroups, tgSystemType.Boolean); }
		}

		public tgQueryItem SupportsUsers
		{
			get { return new tgQueryItem(this, MessageTransmitterMetadata.ColumnNames.SupportsUsers, tgSystemType.Boolean); }
		}


		#endregion
	}

	//===============================================
	// Concrete Metadata Class
	//===============================================
	[Serializable]
	public partial class MessageTransmitterMetadata : tgMetadata, IMetadata
	{
		#region Protected Constructor
		protected MessageTransmitterMetadata()
		{
			m_columns = new tgColumnMetadataCollection();
			tgColumnMetadata c;

			c = new tgColumnMetadata(MessageTransmitterMetadata.ColumnNames.Id, 0, typeof(System.Guid), tgSystemType.Guid);
			c.PropertyName = MessageTransmitterMetadata.PropertyNames.Id;
			c.IsInPrimaryKey = true;
			m_columns.Add(c);

			c = new tgColumnMetadata(MessageTransmitterMetadata.ColumnNames.Name, 1, typeof(System.String), tgSystemType.String);
			c.PropertyName = MessageTransmitterMetadata.PropertyNames.Name;
			c.CharacterMaxLength = 100;
			m_columns.Add(c);

			c = new tgColumnMetadata(MessageTransmitterMetadata.ColumnNames.SysCode, 2, typeof(System.String), tgSystemType.String);
			c.PropertyName = MessageTransmitterMetadata.PropertyNames.SysCode;
			c.CharacterMaxLength = 20;
			m_columns.Add(c);

			c = new tgColumnMetadata(MessageTransmitterMetadata.ColumnNames.Description, 3, typeof(System.String), tgSystemType.String);
			c.PropertyName = MessageTransmitterMetadata.PropertyNames.Description;
			c.IsNullable = true;
			c.CharacterMaxLength = 500;
			m_columns.Add(c);

			c = new tgColumnMetadata(MessageTransmitterMetadata.ColumnNames.ImagePath, 4, typeof(System.String), tgSystemType.String);
			c.PropertyName = MessageTransmitterMetadata.PropertyNames.ImagePath;
			c.IsNullable = true;
			c.CharacterMaxLength = 250;
			m_columns.Add(c);

			c = new tgColumnMetadata(MessageTransmitterMetadata.ColumnNames.ShowExternal, 5, typeof(System.Boolean), tgSystemType.Boolean);
			c.PropertyName = MessageTransmitterMetadata.PropertyNames.ShowExternal;
			m_columns.Add(c);

			c = new tgColumnMetadata(MessageTransmitterMetadata.ColumnNames.SupportsGroups, 6, typeof(System.Boolean), tgSystemType.Boolean);
			c.PropertyName = MessageTransmitterMetadata.PropertyNames.SupportsGroups;
			m_columns.Add(c);

			c = new tgColumnMetadata(MessageTransmitterMetadata.ColumnNames.SupportsUsers, 7, typeof(System.Boolean), tgSystemType.Boolean);
			c.PropertyName = MessageTransmitterMetadata.PropertyNames.SupportsUsers;
			m_columns.Add(c);
		}
		#endregion

		static public MessageTransmitterMetadata Meta()
		{
			return meta;
		}

		public Guid DataID
		{
			get { return base.m_dataID; }
		}

		public bool MultiProviderMode
		{
			get { return false; }
		}

		public tgColumnMetadataCollection Columns
		{
			get { return base.m_columns; }
		}

		#region ColumnNames
		public class ColumnNames
		{
			public const string Id = "Id";
			public const string Name = "Name";
			public const string SysCode = "SysCode";
			public const string Description = "Description";
			public const string ImagePath = "ImagePath";
			public const string ShowExternal = "ShowExternal";
			public const string SupportsGroups = "SupportsGroups";
			public const string SupportsUsers = "SupportsUsers";
		}
		#endregion

		#region PropertyNames
		public class PropertyNames
		{
			public const string Id = "Id";
			public const string Name = "Name";
			public const string SysCode = "SysCode";
			public const string Description = "Description";
			public const string ImagePath = "ImagePath";
			public const string ShowExternal = "ShowExternal";
			public const string SupportsGroups = "SupportsGroups";
			public const string SupportsUsers = "SupportsUsers";
		}
		#endregion

		public tgProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];
			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}

		#region MAP esDefault

		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(MessageTransmitterMetadata))
			{
				if (MessageTransmitterMetadata.mapDelegates == null)
				{
					MessageTransmitterMetadata.mapDelegates = new Dictionary<string, MapToMeta>();
				}

				if (MessageTransmitterMetadata.meta == null)
				{
					MessageTransmitterMetadata.meta = new MessageTransmitterMetadata();
				}

				MapToMeta mapMethod = new MapToMeta(meta.tgDefault);
				mapDelegates.Add("tgDefault", mapMethod);
				mapMethod("tgDefault");
			}
			return 0;
		}

		private tgProviderSpecificMetadata tgDefault(string mapName)
		{
			if (!m_providerMetadataMaps.ContainsKey(mapName))
			{
				tgProviderSpecificMetadata meta = new tgProviderSpecificMetadata();


				meta.AddTypeMap("Id", new tgTypeMap("uniqueidentifier", "System.Guid"));
				meta.AddTypeMap("Name", new tgTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("SysCode", new tgTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("Description", new tgTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("ImagePath", new tgTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("ShowExternal", new tgTypeMap("bit", "System.Boolean"));
				meta.AddTypeMap("SupportsGroups", new tgTypeMap("bit", "System.Boolean"));
				meta.AddTypeMap("SupportsUsers", new tgTypeMap("bit", "System.Boolean"));

				meta.Source = "MessageTransmitter";
				meta.Destination = "MessageTransmitter";

				meta.spInsert = "proc_MessageTransmitterInsert";
				meta.spUpdate = "proc_MessageTransmitterUpdate";
				meta.spDelete = "proc_MessageTransmitterDelete";
				meta.spLoadAll = "proc_MessageTransmitterLoadAll";
				meta.spLoadByPrimaryKey = "proc_MessageTransmitterLoadByPrimaryKey";

				this.m_providerMetadataMaps["tgDefault"] = meta;
			}

			return this.m_providerMetadataMaps["tgDefault"];
		}

		#endregion

		static private MessageTransmitterMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _tgDefault = RegisterDelegateesDefault();
	}
}

