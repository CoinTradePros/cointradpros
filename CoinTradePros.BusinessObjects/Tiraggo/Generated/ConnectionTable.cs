
//==============================================
// Generated by http://www.My2ndGeneration.com
//==============================================

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Linq;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Runtime.Serialization;

using Tiraggo.Core;
using Tiraggo.Interfaces;
using Tiraggo.DynamicQuery;

namespace CoinTradePros.BusinessObjects
{

	//===============================================
	// Concrete Entity Class
	//===============================================
	[DebuggerDisplay("Data = {Debug}")]
	[Serializable]
	[DataContract]
	[KnownType(typeof(ConnectionTable))]
	[XmlType("ConnectionTable")]
	public partial class ConnectionTable : tgConnectionTable
	{
		[DebuggerBrowsable(DebuggerBrowsableState.RootHidden | DebuggerBrowsableState.Never)]
		protected override tgEntityDebuggerView[] Debug
		{
			get { return base.Debug; }
		}

		override public tgEntity CreateInstance()
		{
			return new ConnectionTable();
		}

		#region Static Quick Access Methods
		static public void Delete(System.Guid id)
		{
			var obj = new ConnectionTable();
			obj.Id = id;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save();
		}

		static public void Delete(System.Guid id, tgSqlAccessType sqlAccessType)
		{
			var obj = new ConnectionTable();
			obj.Id = id;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save(sqlAccessType);
		}
		#endregion
	}

	//===============================================
	// Abstract Entity Class
	//===============================================	
	[DataContract]
	[Serializable]
	abstract public partial class tgConnectionTable : tgEntity
	{
		public tgConnectionTable()
		{

		}

		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Guid id)
		{
			if (this.tg.Connection.SqlAccessType == tgSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		public virtual bool LoadByPrimaryKey(tgSqlAccessType sqlAccessType, System.Guid id)
		{
			if (sqlAccessType == tgSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		private bool LoadByPrimaryKeyDynamic(System.Guid id)
		{
			ConnectionTableQuery query = new ConnectionTableQuery();
			query.Where(query.Id == id);
			return this.Load(query);
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Guid id)
		{
			tgParameters parms = new tgParameters();
			parms.Add("Id", id);
			return this.Load(tgQueryType.StoredProcedure, this.tg.spLoadByPrimaryKey, parms);
		}
		#endregion

		#region Properties

		/// <summary>
		/// Maps to ConnectionTable.Id
		/// </summary>
		[DataMember(EmitDefaultValue = false)]
		virtual public Guid? Id
		{
			get
			{
				return base.GetSystemGuid(ConnectionTableMetadata.ColumnNames.Id);
			}

			set
			{
				if (base.SetSystemGuid(ConnectionTableMetadata.ColumnNames.Id, value))
				{
					OnPropertyChanged(ConnectionTableMetadata.PropertyNames.Id);
				}
			}
		}

		/// <summary>
		/// Maps to ConnectionTable.ConnectionID
		/// </summary>
		[DataMember(EmitDefaultValue = false)]
		virtual public string ConnectionID
		{
			get
			{
				return base.GetSystemString(ConnectionTableMetadata.ColumnNames.ConnectionID);
			}

			set
			{
				if (base.SetSystemString(ConnectionTableMetadata.ColumnNames.ConnectionID, value))
				{
					OnPropertyChanged(ConnectionTableMetadata.PropertyNames.ConnectionID);
				}
			}
		}

		/// <summary>
		/// Maps to ConnectionTable.Users
		/// </summary>
		[DataMember(EmitDefaultValue = false)]
		virtual public string Users
		{
			get
			{
				return base.GetSystemString(ConnectionTableMetadata.ColumnNames.Users);
			}

			set
			{
				if (base.SetSystemString(ConnectionTableMetadata.ColumnNames.Users, value))
				{
					OnPropertyChanged(ConnectionTableMetadata.PropertyNames.Users);
				}
			}
		}


		// For Tiraggo.js support and basic serialization of extra properties
		// brought back via Joins
		[DataMember(EmitDefaultValue = false)]
        public tgKeyValuePair[] tgExtendedData
        {
            get
            {
                Dictionary<string, object> extra = GetExtraColumns();

                if (extra.Keys.Count > 0)
                {
                    List<tgKeyValuePair> extended = new List<tgKeyValuePair>();

                    foreach (string key in extra.Keys)
                    {
                        extended.Add(new tgKeyValuePair { Key = key, Value = extra[key] });
                    }

                    return extended.ToArray();
                }

                return null;
            }

            set
            {
                foreach (tgKeyValuePair pair in value)
                {
                    this.SetColumn(pair.Key, pair.Value);
                }
            }
        }

		#endregion

		#region Housekeeping methods

		override protected IMetadata Meta
		{
			get
			{
				return ConnectionTableMetadata.Meta();
			}
		}

		#endregion

		#region Query Logic

		public ConnectionTableQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ConnectionTableQuery();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(ConnectionTableQuery query)
		{
			this.query = query;
			InitQuery(this.query);
			return this.Query.Load();
		}

		protected void InitQuery(ConnectionTableQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;

			if (!query.tg2.HasConnection)
			{
				query.tg2.Connection = ((IEntity)this).Connection;
			}
		}

		#endregion

		[IgnoreDataMember]
		private ConnectionTableQuery query;
	}

	//===============================================
	// Concrete Collection Class
	//===============================================
	[DebuggerDisplay("Count = {Count}")]
	[Serializable]
	[CollectionDataContract]
	[XmlType("ConnectionTableCollection")]
	public partial class ConnectionTableCollection : tgConnectionTableCollection, IEnumerable<ConnectionTable>
	{
		public ConnectionTable FindByPrimaryKey(Guid id)
		{
			return this.SingleOrDefault(e => e.Id == id);
		}

		#region WCF Service Class

		[DataContract]
		[KnownType(typeof(ConnectionTable))]
		public class ConnectionTableCollectionWCFPacket : tgCollectionWCFPacket<ConnectionTableCollection>
		{
			public static implicit operator ConnectionTableCollection(ConnectionTableCollectionWCFPacket packet)
			{
				return packet.Collection;
			}

			public static implicit operator ConnectionTableCollectionWCFPacket(ConnectionTableCollection collection)
			{
				return new ConnectionTableCollectionWCFPacket() { Collection = collection };
			}
		}

		#endregion
	}

	//===============================================
	// Abstract Collection Class
	//===============================================	
	[Serializable]
	abstract public partial class tgConnectionTableCollection : tgEntityCollection<ConnectionTable>
	{
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return ConnectionTableMetadata.Meta();
			}
		}

		protected override string GetCollectionName()
		{
			return "ConnectionTableCollection";
		}

		#endregion

		#region Query Logic

		[BrowsableAttribute(false)]
		public ConnectionTableQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ConnectionTableQuery();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(ConnectionTableQuery query)
		{
			this.query = query;
			InitQuery(this.query);
			return Query.Load();
		}

		override protected tgDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ConnectionTableQuery();
				this.InitQuery(query);
			}
			return this.query;
		}

		protected void InitQuery(ConnectionTableQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			if (!query.tg2.HasConnection)
			{
				query.tg2.Connection = ((IEntityCollection)this).Connection;
			}
		}

		protected override void HookupQuery(tgDynamicQuery query)
		{
			this.InitQuery((ConnectionTableQuery)query);
		}

		#endregion

		private ConnectionTableQuery query;
	}	

	//===============================================
	// Concrete Query Class
	//===============================================	
	[DebuggerDisplay("Query = {Parse()}")]
	public partial class ConnectionTableQuery : tgConnectionTableQuery
	{
		public ConnectionTableQuery(string joinAlias)
		{
			this.tg.JoinAlias = joinAlias;
		}

		override protected string GetQueryName()
		{
			return "ConnectionTableQuery";
		}

		#region Explicit Casts

		public static explicit operator string(ConnectionTableQuery query)
		{
			return ConnectionTableQuery.SerializeHelper.ToXml(query);
		}

		public static explicit operator ConnectionTableQuery(string query)
		{
			return (ConnectionTableQuery)ConnectionTableQuery.SerializeHelper.FromXml(query, typeof(ConnectionTableQuery));
		}

		#endregion
	}

	//===============================================
	// Abstract Query Class
	//===============================================
	abstract public partial class tgConnectionTableQuery : tgDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return ConnectionTableMetadata.Meta();
			}
		}	

		#region QueryItemFromName

		protected override tgQueryItem QueryItemFromName(string name)
		{
			switch (name)
			{
				case "Id": return this.Id;
				case "ConnectionID": return this.ConnectionID;
				case "Users": return this.Users;

				default: return null;
			}
		}		

		#endregion

		#region tgQueryItems

		public tgQueryItem Id
		{
			get { return new tgQueryItem(this, ConnectionTableMetadata.ColumnNames.Id, tgSystemType.Guid); }
		}

		public tgQueryItem ConnectionID
		{
			get { return new tgQueryItem(this, ConnectionTableMetadata.ColumnNames.ConnectionID, tgSystemType.String); }
		}

		public tgQueryItem Users
		{
			get { return new tgQueryItem(this, ConnectionTableMetadata.ColumnNames.Users, tgSystemType.String); }
		}


		#endregion
	}

	//===============================================
	// Concrete Metadata Class
	//===============================================
	[Serializable]
	public partial class ConnectionTableMetadata : tgMetadata, IMetadata
	{
		#region Protected Constructor
		protected ConnectionTableMetadata()
		{
			m_columns = new tgColumnMetadataCollection();
			tgColumnMetadata c;

			c = new tgColumnMetadata(ConnectionTableMetadata.ColumnNames.Id, 0, typeof(System.Guid), tgSystemType.Guid);
			c.PropertyName = ConnectionTableMetadata.PropertyNames.Id;
			c.IsInPrimaryKey = true;
			m_columns.Add(c);

			c = new tgColumnMetadata(ConnectionTableMetadata.ColumnNames.ConnectionID, 1, typeof(System.String), tgSystemType.String);
			c.PropertyName = ConnectionTableMetadata.PropertyNames.ConnectionID;
			c.CharacterMaxLength = 2147483647;
			m_columns.Add(c);

			c = new tgColumnMetadata(ConnectionTableMetadata.ColumnNames.Users, 2, typeof(System.String), tgSystemType.String);
			c.PropertyName = ConnectionTableMetadata.PropertyNames.Users;
			c.IsNullable = true;
			c.CharacterMaxLength = 128;
			m_columns.Add(c);
		}
		#endregion

		static public ConnectionTableMetadata Meta()
		{
			return meta;
		}

		public Guid DataID
		{
			get { return base.m_dataID; }
		}

		public bool MultiProviderMode
		{
			get { return false; }
		}

		public tgColumnMetadataCollection Columns
		{
			get { return base.m_columns; }
		}

		#region ColumnNames
		public class ColumnNames
		{
			public const string Id = "ID";
			public const string ConnectionID = "ConnectionID";
			public const string Users = "Users";
		}
		#endregion

		#region PropertyNames
		public class PropertyNames
		{
			public const string Id = "Id";
			public const string ConnectionID = "ConnectionID";
			public const string Users = "Users";
		}
		#endregion

		public tgProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];
			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}

		#region MAP esDefault

		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(ConnectionTableMetadata))
			{
				if (ConnectionTableMetadata.mapDelegates == null)
				{
					ConnectionTableMetadata.mapDelegates = new Dictionary<string, MapToMeta>();
				}

				if (ConnectionTableMetadata.meta == null)
				{
					ConnectionTableMetadata.meta = new ConnectionTableMetadata();
				}

				MapToMeta mapMethod = new MapToMeta(meta.tgDefault);
				mapDelegates.Add("tgDefault", mapMethod);
				mapMethod("tgDefault");
			}
			return 0;
		}

		private tgProviderSpecificMetadata tgDefault(string mapName)
		{
			if (!m_providerMetadataMaps.ContainsKey(mapName))
			{
				tgProviderSpecificMetadata meta = new tgProviderSpecificMetadata();


				meta.AddTypeMap("Id", new tgTypeMap("uniqueidentifier", "System.Guid"));
				meta.AddTypeMap("ConnectionID", new tgTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("Users", new tgTypeMap("nvarchar", "System.String"));

				meta.Source = "ConnectionTable";
				meta.Destination = "ConnectionTable";

				meta.spInsert = "proc_ConnectionTableInsert";
				meta.spUpdate = "proc_ConnectionTableUpdate";
				meta.spDelete = "proc_ConnectionTableDelete";
				meta.spLoadAll = "proc_ConnectionTableLoadAll";
				meta.spLoadByPrimaryKey = "proc_ConnectionTableLoadByPrimaryKey";

				this.m_providerMetadataMaps["tgDefault"] = meta;
			}

			return this.m_providerMetadataMaps["tgDefault"];
		}

		#endregion

		static private ConnectionTableMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _tgDefault = RegisterDelegateesDefault();
	}
}

