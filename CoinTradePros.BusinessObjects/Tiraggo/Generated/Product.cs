
//==============================================
// Generated by http://www.My2ndGeneration.com
//==============================================

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Linq;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Runtime.Serialization;

using Tiraggo.Core;
using Tiraggo.Interfaces;
using Tiraggo.DynamicQuery;

namespace CoinTradePros.BusinessObjects
{

	//===============================================
	// Concrete Entity Class
	//===============================================
	[DebuggerDisplay("Data = {Debug}")]
	[Serializable]
	[DataContract]
	[KnownType(typeof(Product))]
	[XmlType("Product")]
	public partial class Product : tgProduct
	{
		[DebuggerBrowsable(DebuggerBrowsableState.RootHidden | DebuggerBrowsableState.Never)]
		protected override tgEntityDebuggerView[] Debug
		{
			get { return base.Debug; }
		}

		override public tgEntity CreateInstance()
		{
			return new Product();
		}

		#region Static Quick Access Methods
		static public void Delete(System.Guid id)
		{
			var obj = new Product();
			obj.Id = id;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save();
		}

		static public void Delete(System.Guid id, tgSqlAccessType sqlAccessType)
		{
			var obj = new Product();
			obj.Id = id;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save(sqlAccessType);
		}
		#endregion
	}

	//===============================================
	// Abstract Entity Class
	//===============================================	
	[DataContract]
	[Serializable]
	abstract public partial class tgProduct : tgEntity
	{
		public tgProduct()
		{

		}

		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Guid id)
		{
			if (this.tg.Connection.SqlAccessType == tgSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		public virtual bool LoadByPrimaryKey(tgSqlAccessType sqlAccessType, System.Guid id)
		{
			if (sqlAccessType == tgSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		private bool LoadByPrimaryKeyDynamic(System.Guid id)
		{
			ProductQuery query = new ProductQuery();
			query.Where(query.Id == id);
			return this.Load(query);
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Guid id)
		{
			tgParameters parms = new tgParameters();
			parms.Add("Id", id);
			return this.Load(tgQueryType.StoredProcedure, this.tg.spLoadByPrimaryKey, parms);
		}
		#endregion

		#region Properties

		/// <summary>
		/// Maps to Product.Id
		/// </summary>
		[DataMember(EmitDefaultValue = false)]
		virtual public Guid? Id
		{
			get
			{
				return base.GetSystemGuid(ProductMetadata.ColumnNames.Id);
			}

			set
			{
				if (base.SetSystemGuid(ProductMetadata.ColumnNames.Id, value))
				{
					OnPropertyChanged(ProductMetadata.PropertyNames.Id);
				}
			}
		}

		/// <summary>
		/// Maps to Product.Title
		/// </summary>
		[DataMember(EmitDefaultValue = false)]
		virtual public string Title
		{
			get
			{
				return base.GetSystemString(ProductMetadata.ColumnNames.Title);
			}

			set
			{
				if (base.SetSystemString(ProductMetadata.ColumnNames.Title, value))
				{
					OnPropertyChanged(ProductMetadata.PropertyNames.Title);
				}
			}
		}

		/// <summary>
		/// Maps to Product.Description
		/// </summary>
		[DataMember(EmitDefaultValue = false)]
		virtual public string Description
		{
			get
			{
				return base.GetSystemString(ProductMetadata.ColumnNames.Description);
			}

			set
			{
				if (base.SetSystemString(ProductMetadata.ColumnNames.Description, value))
				{
					OnPropertyChanged(ProductMetadata.PropertyNames.Description);
				}
			}
		}

		/// <summary>
		/// Maps to Product.Price
		/// </summary>
		[DataMember(EmitDefaultValue = false)]
		virtual public decimal? Price
		{
			get
			{
				return base.GetSystemDecimal(ProductMetadata.ColumnNames.Price);
			}

			set
			{
				if (base.SetSystemDecimal(ProductMetadata.ColumnNames.Price, value))
				{
					OnPropertyChanged(ProductMetadata.PropertyNames.Price);
				}
			}
		}

		/// <summary>
		/// Maps to Product.ImagePath
		/// </summary>
		[DataMember(EmitDefaultValue = false)]
		virtual public string ImagePath
		{
			get
			{
				return base.GetSystemString(ProductMetadata.ColumnNames.ImagePath);
			}

			set
			{
				if (base.SetSystemString(ProductMetadata.ColumnNames.ImagePath, value))
				{
					OnPropertyChanged(ProductMetadata.PropertyNames.ImagePath);
				}
			}
		}

		/// <summary>
		/// Maps to Product.RequireShipping
		/// </summary>
		[DataMember(EmitDefaultValue = false)]
		virtual public bool? RequireShipping
		{
			get
			{
				return base.GetSystemBoolean(ProductMetadata.ColumnNames.RequireShipping);
			}

			set
			{
				if (base.SetSystemBoolean(ProductMetadata.ColumnNames.RequireShipping, value))
				{
					OnPropertyChanged(ProductMetadata.PropertyNames.RequireShipping);
				}
			}
		}

		/// <summary>
		/// Maps to Product.RequiresAccessCode
		/// </summary>
		[DataMember(EmitDefaultValue = false)]
		virtual public bool? RequiresAccessCode
		{
			get
			{
				return base.GetSystemBoolean(ProductMetadata.ColumnNames.RequiresAccessCode);
			}

			set
			{
				if (base.SetSystemBoolean(ProductMetadata.ColumnNames.RequiresAccessCode, value))
				{
					OnPropertyChanged(ProductMetadata.PropertyNames.RequiresAccessCode);
				}
			}
		}


		// For Tiraggo.js support and basic serialization of extra properties
		// brought back via Joins
		[DataMember(EmitDefaultValue = false)]
        public tgKeyValuePair[] tgExtendedData
        {
            get
            {
                Dictionary<string, object> extra = GetExtraColumns();

                if (extra.Keys.Count > 0)
                {
                    List<tgKeyValuePair> extended = new List<tgKeyValuePair>();

                    foreach (string key in extra.Keys)
                    {
                        extended.Add(new tgKeyValuePair { Key = key, Value = extra[key] });
                    }

                    return extended.ToArray();
                }

                return null;
            }

            set
            {
                foreach (tgKeyValuePair pair in value)
                {
                    this.SetColumn(pair.Key, pair.Value);
                }
            }
        }

		#endregion

		#region Housekeeping methods

		override protected IMetadata Meta
		{
			get
			{
				return ProductMetadata.Meta();
			}
		}

		#endregion

		#region Query Logic

		public ProductQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ProductQuery();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(ProductQuery query)
		{
			this.query = query;
			InitQuery(this.query);
			return this.Query.Load();
		}

		protected void InitQuery(ProductQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;

			if (!query.tg2.HasConnection)
			{
				query.tg2.Connection = ((IEntity)this).Connection;
			}
		}

		#endregion

		[IgnoreDataMember]
		private ProductQuery query;
	}

	//===============================================
	// Concrete Collection Class
	//===============================================
	[DebuggerDisplay("Count = {Count}")]
	[Serializable]
	[CollectionDataContract]
	[XmlType("ProductCollection")]
	public partial class ProductCollection : tgProductCollection, IEnumerable<Product>
	{
		public Product FindByPrimaryKey(Guid id)
		{
			return this.SingleOrDefault(e => e.Id == id);
		}

		#region WCF Service Class

		[DataContract]
		[KnownType(typeof(Product))]
		public class ProductCollectionWCFPacket : tgCollectionWCFPacket<ProductCollection>
		{
			public static implicit operator ProductCollection(ProductCollectionWCFPacket packet)
			{
				return packet.Collection;
			}

			public static implicit operator ProductCollectionWCFPacket(ProductCollection collection)
			{
				return new ProductCollectionWCFPacket() { Collection = collection };
			}
		}

		#endregion
	}

	//===============================================
	// Abstract Collection Class
	//===============================================	
	[Serializable]
	abstract public partial class tgProductCollection : tgEntityCollection<Product>
	{
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return ProductMetadata.Meta();
			}
		}

		protected override string GetCollectionName()
		{
			return "ProductCollection";
		}

		#endregion

		#region Query Logic

		[BrowsableAttribute(false)]
		public ProductQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ProductQuery();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(ProductQuery query)
		{
			this.query = query;
			InitQuery(this.query);
			return Query.Load();
		}

		override protected tgDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ProductQuery();
				this.InitQuery(query);
			}
			return this.query;
		}

		protected void InitQuery(ProductQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			if (!query.tg2.HasConnection)
			{
				query.tg2.Connection = ((IEntityCollection)this).Connection;
			}
		}

		protected override void HookupQuery(tgDynamicQuery query)
		{
			this.InitQuery((ProductQuery)query);
		}

		#endregion

		private ProductQuery query;
	}	

	//===============================================
	// Concrete Query Class
	//===============================================	
	[DebuggerDisplay("Query = {Parse()}")]
	public partial class ProductQuery : tgProductQuery
	{
		public ProductQuery(string joinAlias)
		{
			this.tg.JoinAlias = joinAlias;
		}

		override protected string GetQueryName()
		{
			return "ProductQuery";
		}

		#region Explicit Casts

		public static explicit operator string(ProductQuery query)
		{
			return ProductQuery.SerializeHelper.ToXml(query);
		}

		public static explicit operator ProductQuery(string query)
		{
			return (ProductQuery)ProductQuery.SerializeHelper.FromXml(query, typeof(ProductQuery));
		}

		#endregion
	}

	//===============================================
	// Abstract Query Class
	//===============================================
	abstract public partial class tgProductQuery : tgDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return ProductMetadata.Meta();
			}
		}	

		#region QueryItemFromName

		protected override tgQueryItem QueryItemFromName(string name)
		{
			switch (name)
			{
				case "Id": return this.Id;
				case "Title": return this.Title;
				case "Description": return this.Description;
				case "Price": return this.Price;
				case "ImagePath": return this.ImagePath;
				case "RequireShipping": return this.RequireShipping;
				case "RequiresAccessCode": return this.RequiresAccessCode;

				default: return null;
			}
		}		

		#endregion

		#region tgQueryItems

		public tgQueryItem Id
		{
			get { return new tgQueryItem(this, ProductMetadata.ColumnNames.Id, tgSystemType.Guid); }
		}

		public tgQueryItem Title
		{
			get { return new tgQueryItem(this, ProductMetadata.ColumnNames.Title, tgSystemType.String); }
		}

		public tgQueryItem Description
		{
			get { return new tgQueryItem(this, ProductMetadata.ColumnNames.Description, tgSystemType.String); }
		}

		public tgQueryItem Price
		{
			get { return new tgQueryItem(this, ProductMetadata.ColumnNames.Price, tgSystemType.Decimal); }
		}

		public tgQueryItem ImagePath
		{
			get { return new tgQueryItem(this, ProductMetadata.ColumnNames.ImagePath, tgSystemType.String); }
		}

		public tgQueryItem RequireShipping
		{
			get { return new tgQueryItem(this, ProductMetadata.ColumnNames.RequireShipping, tgSystemType.Boolean); }
		}

		public tgQueryItem RequiresAccessCode
		{
			get { return new tgQueryItem(this, ProductMetadata.ColumnNames.RequiresAccessCode, tgSystemType.Boolean); }
		}


		#endregion
	}

	//===============================================
	// Concrete Metadata Class
	//===============================================
	[Serializable]
	public partial class ProductMetadata : tgMetadata, IMetadata
	{
		#region Protected Constructor
		protected ProductMetadata()
		{
			m_columns = new tgColumnMetadataCollection();
			tgColumnMetadata c;

			c = new tgColumnMetadata(ProductMetadata.ColumnNames.Id, 0, typeof(System.Guid), tgSystemType.Guid);
			c.PropertyName = ProductMetadata.PropertyNames.Id;
			c.IsInPrimaryKey = true;
			m_columns.Add(c);

			c = new tgColumnMetadata(ProductMetadata.ColumnNames.Title, 1, typeof(System.String), tgSystemType.String);
			c.PropertyName = ProductMetadata.PropertyNames.Title;
			c.CharacterMaxLength = 100;
			m_columns.Add(c);

			c = new tgColumnMetadata(ProductMetadata.ColumnNames.Description, 2, typeof(System.String), tgSystemType.String);
			c.PropertyName = ProductMetadata.PropertyNames.Description;
			c.IsNullable = true;
			c.CharacterMaxLength = 2147483647;
			m_columns.Add(c);

			c = new tgColumnMetadata(ProductMetadata.ColumnNames.Price, 3, typeof(System.Decimal), tgSystemType.Decimal);
			c.PropertyName = ProductMetadata.PropertyNames.Price;
			c.NumericPrecision = 19;
			c.NumericScale = 4;
			m_columns.Add(c);

			c = new tgColumnMetadata(ProductMetadata.ColumnNames.ImagePath, 4, typeof(System.String), tgSystemType.String);
			c.PropertyName = ProductMetadata.PropertyNames.ImagePath;
			c.IsNullable = true;
			c.CharacterMaxLength = 250;
			m_columns.Add(c);

			c = new tgColumnMetadata(ProductMetadata.ColumnNames.RequireShipping, 5, typeof(System.Boolean), tgSystemType.Boolean);
			c.PropertyName = ProductMetadata.PropertyNames.RequireShipping;
			m_columns.Add(c);

			c = new tgColumnMetadata(ProductMetadata.ColumnNames.RequiresAccessCode, 6, typeof(System.Boolean), tgSystemType.Boolean);
			c.PropertyName = ProductMetadata.PropertyNames.RequiresAccessCode;
			m_columns.Add(c);
		}
		#endregion

		static public ProductMetadata Meta()
		{
			return meta;
		}

		public Guid DataID
		{
			get { return base.m_dataID; }
		}

		public bool MultiProviderMode
		{
			get { return false; }
		}

		public tgColumnMetadataCollection Columns
		{
			get { return base.m_columns; }
		}

		#region ColumnNames
		public class ColumnNames
		{
			public const string Id = "Id";
			public const string Title = "Title";
			public const string Description = "Description";
			public const string Price = "Price";
			public const string ImagePath = "ImagePath";
			public const string RequireShipping = "RequireShipping";
			public const string RequiresAccessCode = "RequiresAccessCode";
		}
		#endregion

		#region PropertyNames
		public class PropertyNames
		{
			public const string Id = "Id";
			public const string Title = "Title";
			public const string Description = "Description";
			public const string Price = "Price";
			public const string ImagePath = "ImagePath";
			public const string RequireShipping = "RequireShipping";
			public const string RequiresAccessCode = "RequiresAccessCode";
		}
		#endregion

		public tgProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];
			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}

		#region MAP esDefault

		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(ProductMetadata))
			{
				if (ProductMetadata.mapDelegates == null)
				{
					ProductMetadata.mapDelegates = new Dictionary<string, MapToMeta>();
				}

				if (ProductMetadata.meta == null)
				{
					ProductMetadata.meta = new ProductMetadata();
				}

				MapToMeta mapMethod = new MapToMeta(meta.tgDefault);
				mapDelegates.Add("tgDefault", mapMethod);
				mapMethod("tgDefault");
			}
			return 0;
		}

		private tgProviderSpecificMetadata tgDefault(string mapName)
		{
			if (!m_providerMetadataMaps.ContainsKey(mapName))
			{
				tgProviderSpecificMetadata meta = new tgProviderSpecificMetadata();


				meta.AddTypeMap("Id", new tgTypeMap("uniqueidentifier", "System.Guid"));
				meta.AddTypeMap("Title", new tgTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Description", new tgTypeMap("text", "System.String"));
				meta.AddTypeMap("Price", new tgTypeMap("money", "System.Decimal"));
				meta.AddTypeMap("ImagePath", new tgTypeMap("varchar", "System.String"));
				meta.AddTypeMap("RequireShipping", new tgTypeMap("bit", "System.Boolean"));
				meta.AddTypeMap("RequiresAccessCode", new tgTypeMap("bit", "System.Boolean"));

				meta.Source = "Product";
				meta.Destination = "Product";

				meta.spInsert = "proc_ProductInsert";
				meta.spUpdate = "proc_ProductUpdate";
				meta.spDelete = "proc_ProductDelete";
				meta.spLoadAll = "proc_ProductLoadAll";
				meta.spLoadByPrimaryKey = "proc_ProductLoadByPrimaryKey";

				this.m_providerMetadataMaps["tgDefault"] = meta;
			}

			return this.m_providerMetadataMaps["tgDefault"];
		}

		#endregion

		static private ProductMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _tgDefault = RegisterDelegateesDefault();
	}
}

