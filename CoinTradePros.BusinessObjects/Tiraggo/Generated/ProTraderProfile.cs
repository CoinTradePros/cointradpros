//==============================================
// Generated by http://www.My2ndGeneration.com
//==============================================

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Linq;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Runtime.Serialization;

using Tiraggo.Core;
using Tiraggo.Interfaces;
using Tiraggo.DynamicQuery;

namespace CoinTradePros.BusinessObjects
{

	//===============================================
	// Concrete Entity Class
	//===============================================
	[DebuggerDisplay("Data = {Debug}")]
	[Serializable]
	[DataContract]
	[KnownType(typeof(ProTraderProfile))]
	[XmlType("ProTraderProfile")]
	public partial class ProTraderProfile : tgProTraderProfile
	{
		[DebuggerBrowsable(DebuggerBrowsableState.RootHidden | DebuggerBrowsableState.Never)]
		protected override tgEntityDebuggerView[] Debug
		{
			get { return base.Debug; }
		}

		override public tgEntity CreateInstance()
		{
			return new ProTraderProfile();
		}

		#region Static Quick Access Methods
		static public void Delete(System.Guid id)
		{
			var obj = new ProTraderProfile();
			obj.Id = id;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save();
		}

		static public void Delete(System.Guid id, tgSqlAccessType sqlAccessType)
		{
			var obj = new ProTraderProfile();
			obj.Id = id;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save(sqlAccessType);
		}
		#endregion
	}

	//===============================================
	// Abstract Entity Class
	//===============================================	
	[DataContract]
	[Serializable]
	abstract public partial class tgProTraderProfile : tgEntity
	{
		public tgProTraderProfile()
		{

		}

		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Guid id)
		{
			if (this.tg.Connection.SqlAccessType == tgSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		public virtual bool LoadByPrimaryKey(tgSqlAccessType sqlAccessType, System.Guid id)
		{
			if (sqlAccessType == tgSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		private bool LoadByPrimaryKeyDynamic(System.Guid id)
		{
			ProTraderProfileQuery query = new ProTraderProfileQuery();
			query.Where(query.Id == id);
			return this.Load(query);
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Guid id)
		{
			tgParameters parms = new tgParameters();
			parms.Add("Id", id);
			return this.Load(tgQueryType.StoredProcedure, this.tg.spLoadByPrimaryKey, parms);
		}
		#endregion

		#region Properties

		/// <summary>
		/// Maps to ProTraderProfile.Id
		/// </summary>
		[DataMember(EmitDefaultValue = false)]
		virtual public Guid? Id
		{
			get
			{
				return base.GetSystemGuid(ProTraderProfileMetadata.ColumnNames.Id);
			}

			set
			{
				if (base.SetSystemGuid(ProTraderProfileMetadata.ColumnNames.Id, value))
				{
					OnPropertyChanged(ProTraderProfileMetadata.PropertyNames.Id);
				}
			}
		}

		/// <summary>
		/// Maps to ProTraderProfile.TraderId
		/// </summary>
		[DataMember(EmitDefaultValue = false)]
		virtual public Guid? TraderId
		{
			get
			{
				return base.GetSystemGuid(ProTraderProfileMetadata.ColumnNames.TraderId);
			}

			set
			{
				if (base.SetSystemGuid(ProTraderProfileMetadata.ColumnNames.TraderId, value))
				{
					OnPropertyChanged(ProTraderProfileMetadata.PropertyNames.TraderId);
				}
			}
		}

		/// <summary>
		/// Maps to ProTraderProfile.Description
		/// </summary>
		[DataMember(EmitDefaultValue = false)]
		virtual public string Description
		{
			get
			{
				return base.GetSystemString(ProTraderProfileMetadata.ColumnNames.Description);
			}

			set
			{
				if (base.SetSystemString(ProTraderProfileMetadata.ColumnNames.Description, value))
				{
					OnPropertyChanged(ProTraderProfileMetadata.PropertyNames.Description);
				}
			}
		}

		/// <summary>
		/// Maps to ProTraderProfile.TagLine
		/// </summary>
		[DataMember(EmitDefaultValue = false)]
		virtual public string TagLine
		{
			get
			{
				return base.GetSystemString(ProTraderProfileMetadata.ColumnNames.TagLine);
			}

			set
			{
				if (base.SetSystemString(ProTraderProfileMetadata.ColumnNames.TagLine, value))
				{
					OnPropertyChanged(ProTraderProfileMetadata.PropertyNames.TagLine);
				}
			}
		}

		/// <summary>
		/// Maps to ProTraderProfile.VideoEmbed
		/// </summary>
		[DataMember(EmitDefaultValue = false)]
		virtual public string VideoEmbed
		{
			get
			{
				return base.GetSystemString(ProTraderProfileMetadata.ColumnNames.VideoEmbed);
			}

			set
			{
				if (base.SetSystemString(ProTraderProfileMetadata.ColumnNames.VideoEmbed, value))
				{
					OnPropertyChanged(ProTraderProfileMetadata.PropertyNames.VideoEmbed);
				}
			}
		}

		/// <summary>
		/// Maps to ProTraderProfile.PortraitPath
		/// </summary>
		[DataMember(EmitDefaultValue = false)]
		virtual public string PortraitPath
		{
			get
			{
				return base.GetSystemString(ProTraderProfileMetadata.ColumnNames.PortraitPath);
			}

			set
			{
				if (base.SetSystemString(ProTraderProfileMetadata.ColumnNames.PortraitPath, value))
				{
					OnPropertyChanged(ProTraderProfileMetadata.PropertyNames.PortraitPath);
				}
			}
		}

		/// <summary>
		/// Maps to ProTraderProfile.ChatHash
		/// </summary>
		[DataMember(EmitDefaultValue = false)]
		virtual public string ChatHash
		{
			get
			{
				return base.GetSystemString(ProTraderProfileMetadata.ColumnNames.ChatHash);
			}

			set
			{
				if (base.SetSystemString(ProTraderProfileMetadata.ColumnNames.ChatHash, value))
				{
					OnPropertyChanged(ProTraderProfileMetadata.PropertyNames.ChatHash);
				}
			}
		}


		// For Tiraggo.js support and basic serialization of extra properties
		// brought back via Joins
		[DataMember(EmitDefaultValue = false)]
        public tgKeyValuePair[] tgExtendedData
        {
            get
            {
                Dictionary<string, object> extra = GetExtraColumns();

                if (extra.Keys.Count > 0)
                {
                    List<tgKeyValuePair> extended = new List<tgKeyValuePair>();

                    foreach (string key in extra.Keys)
                    {
                        extended.Add(new tgKeyValuePair { Key = key, Value = extra[key] });
                    }

                    return extended.ToArray();
                }

                return null;
            }

            set
            {
                foreach (tgKeyValuePair pair in value)
                {
                    this.SetColumn(pair.Key, pair.Value);
                }
            }
        }

		#endregion

		#region Housekeeping methods

		override protected IMetadata Meta
		{
			get
			{
				return ProTraderProfileMetadata.Meta();
			}
		}

		#endregion

		#region Query Logic

		public ProTraderProfileQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ProTraderProfileQuery();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(ProTraderProfileQuery query)
		{
			this.query = query;
			InitQuery(this.query);
			return this.Query.Load();
		}

		protected void InitQuery(ProTraderProfileQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;

			if (!query.tg2.HasConnection)
			{
				query.tg2.Connection = ((IEntity)this).Connection;
			}
		}

		#endregion

		[IgnoreDataMember]
		private ProTraderProfileQuery query;
	}

	//===============================================
	// Concrete Collection Class
	//===============================================
	[DebuggerDisplay("Count = {Count}")]
	[Serializable]
	[CollectionDataContract]
	[XmlType("ProTraderProfileCollection")]
	public partial class ProTraderProfileCollection : tgProTraderProfileCollection, IEnumerable<ProTraderProfile>
	{
		public ProTraderProfile FindByPrimaryKey(Guid id)
		{
			return this.SingleOrDefault(e => e.Id == id);
		}

		#region WCF Service Class

		[DataContract]
		[KnownType(typeof(ProTraderProfile))]
		public class ProTraderProfileCollectionWCFPacket : tgCollectionWCFPacket<ProTraderProfileCollection>
		{
			public static implicit operator ProTraderProfileCollection(ProTraderProfileCollectionWCFPacket packet)
			{
				return packet.Collection;
			}

			public static implicit operator ProTraderProfileCollectionWCFPacket(ProTraderProfileCollection collection)
			{
				return new ProTraderProfileCollectionWCFPacket() { Collection = collection };
			}
		}

		#endregion
	}

	//===============================================
	// Abstract Collection Class
	//===============================================	
	[Serializable]
	abstract public partial class tgProTraderProfileCollection : tgEntityCollection<ProTraderProfile>
	{
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return ProTraderProfileMetadata.Meta();
			}
		}

		protected override string GetCollectionName()
		{
			return "ProTraderProfileCollection";
		}

		#endregion

		#region Query Logic

		[BrowsableAttribute(false)]
		public ProTraderProfileQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ProTraderProfileQuery();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(ProTraderProfileQuery query)
		{
			this.query = query;
			InitQuery(this.query);
			return Query.Load();
		}

		override protected tgDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ProTraderProfileQuery();
				this.InitQuery(query);
			}
			return this.query;
		}

		protected void InitQuery(ProTraderProfileQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			if (!query.tg2.HasConnection)
			{
				query.tg2.Connection = ((IEntityCollection)this).Connection;
			}
		}

		protected override void HookupQuery(tgDynamicQuery query)
		{
			this.InitQuery((ProTraderProfileQuery)query);
		}

		#endregion

		private ProTraderProfileQuery query;
	}	

	//===============================================
	// Concrete Query Class
	//===============================================	
	[DebuggerDisplay("Query = {Parse()}")]
	public partial class ProTraderProfileQuery : tgProTraderProfileQuery
	{
		public ProTraderProfileQuery(string joinAlias)
		{
			this.tg.JoinAlias = joinAlias;
		}

		override protected string GetQueryName()
		{
			return "ProTraderProfileQuery";
		}

		#region Explicit Casts

		public static explicit operator string(ProTraderProfileQuery query)
		{
			return ProTraderProfileQuery.SerializeHelper.ToXml(query);
		}

		public static explicit operator ProTraderProfileQuery(string query)
		{
			return (ProTraderProfileQuery)ProTraderProfileQuery.SerializeHelper.FromXml(query, typeof(ProTraderProfileQuery));
		}

		#endregion
	}

	//===============================================
	// Abstract Query Class
	//===============================================
	abstract public partial class tgProTraderProfileQuery : tgDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return ProTraderProfileMetadata.Meta();
			}
		}	

		#region QueryItemFromName

		protected override tgQueryItem QueryItemFromName(string name)
		{
			switch (name)
			{
				case "Id": return this.Id;
				case "TraderId": return this.TraderId;
				case "Description": return this.Description;
				case "TagLine": return this.TagLine;
				case "VideoEmbed": return this.VideoEmbed;
				case "PortraitPath": return this.PortraitPath;
				case "ChatHash": return this.ChatHash;

				default: return null;
			}
		}		

		#endregion

		#region tgQueryItems

		public tgQueryItem Id
		{
			get { return new tgQueryItem(this, ProTraderProfileMetadata.ColumnNames.Id, tgSystemType.Guid); }
		}

		public tgQueryItem TraderId
		{
			get { return new tgQueryItem(this, ProTraderProfileMetadata.ColumnNames.TraderId, tgSystemType.Guid); }
		}

		public tgQueryItem Description
		{
			get { return new tgQueryItem(this, ProTraderProfileMetadata.ColumnNames.Description, tgSystemType.String); }
		}

		public tgQueryItem TagLine
		{
			get { return new tgQueryItem(this, ProTraderProfileMetadata.ColumnNames.TagLine, tgSystemType.String); }
		}

		public tgQueryItem VideoEmbed
		{
			get { return new tgQueryItem(this, ProTraderProfileMetadata.ColumnNames.VideoEmbed, tgSystemType.String); }
		}

		public tgQueryItem PortraitPath
		{
			get { return new tgQueryItem(this, ProTraderProfileMetadata.ColumnNames.PortraitPath, tgSystemType.String); }
		}

		public tgQueryItem ChatHash
		{
			get { return new tgQueryItem(this, ProTraderProfileMetadata.ColumnNames.ChatHash, tgSystemType.String); }
		}


		#endregion
	}

	//===============================================
	// Concrete Metadata Class
	//===============================================
	[Serializable]
	public partial class ProTraderProfileMetadata : tgMetadata, IMetadata
	{
		#region Protected Constructor
		protected ProTraderProfileMetadata()
		{
			m_columns = new tgColumnMetadataCollection();
			tgColumnMetadata c;

			c = new tgColumnMetadata(ProTraderProfileMetadata.ColumnNames.Id, 0, typeof(System.Guid), tgSystemType.Guid);
			c.PropertyName = ProTraderProfileMetadata.PropertyNames.Id;
			c.IsInPrimaryKey = true;
			m_columns.Add(c);

			c = new tgColumnMetadata(ProTraderProfileMetadata.ColumnNames.TraderId, 1, typeof(System.Guid), tgSystemType.Guid);
			c.PropertyName = ProTraderProfileMetadata.PropertyNames.TraderId;
			m_columns.Add(c);

			c = new tgColumnMetadata(ProTraderProfileMetadata.ColumnNames.Description, 2, typeof(System.String), tgSystemType.String);
			c.PropertyName = ProTraderProfileMetadata.PropertyNames.Description;
			c.IsNullable = true;
			c.CharacterMaxLength = 2147483647;
			m_columns.Add(c);

			c = new tgColumnMetadata(ProTraderProfileMetadata.ColumnNames.TagLine, 3, typeof(System.String), tgSystemType.String);
			c.PropertyName = ProTraderProfileMetadata.PropertyNames.TagLine;
			c.IsNullable = true;
			c.CharacterMaxLength = 150;
			m_columns.Add(c);

			c = new tgColumnMetadata(ProTraderProfileMetadata.ColumnNames.VideoEmbed, 4, typeof(System.String), tgSystemType.String);
			c.PropertyName = ProTraderProfileMetadata.PropertyNames.VideoEmbed;
			c.IsNullable = true;
			c.CharacterMaxLength = 2147483647;
			m_columns.Add(c);

			c = new tgColumnMetadata(ProTraderProfileMetadata.ColumnNames.PortraitPath, 5, typeof(System.String), tgSystemType.String);
			c.PropertyName = ProTraderProfileMetadata.PropertyNames.PortraitPath;
			c.IsNullable = true;
			c.CharacterMaxLength = 250;
			m_columns.Add(c);

			c = new tgColumnMetadata(ProTraderProfileMetadata.ColumnNames.ChatHash, 6, typeof(System.String), tgSystemType.String);
			c.PropertyName = ProTraderProfileMetadata.PropertyNames.ChatHash;
			c.IsNullable = true;
			c.CharacterMaxLength = 50;
			m_columns.Add(c);
		}
		#endregion

		static public ProTraderProfileMetadata Meta()
		{
			return meta;
		}

		public Guid DataID
		{
			get { return base.m_dataID; }
		}

		public bool MultiProviderMode
		{
			get { return false; }
		}

		public tgColumnMetadataCollection Columns
		{
			get { return base.m_columns; }
		}

		#region ColumnNames
		public class ColumnNames
		{
			public const string Id = "Id";
			public const string TraderId = "TraderId";
			public const string Description = "Description";
			public const string TagLine = "TagLine";
			public const string VideoEmbed = "VideoEmbed";
			public const string PortraitPath = "PortraitPath";
			public const string ChatHash = "ChatHash";
		}
		#endregion

		#region PropertyNames
		public class PropertyNames
		{
			public const string Id = "Id";
			public const string TraderId = "TraderId";
			public const string Description = "Description";
			public const string TagLine = "TagLine";
			public const string VideoEmbed = "VideoEmbed";
			public const string PortraitPath = "PortraitPath";
			public const string ChatHash = "ChatHash";
		}
		#endregion

		public tgProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];
			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}

		#region MAP esDefault

		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(ProTraderProfileMetadata))
			{
				if (ProTraderProfileMetadata.mapDelegates == null)
				{
					ProTraderProfileMetadata.mapDelegates = new Dictionary<string, MapToMeta>();
				}

				if (ProTraderProfileMetadata.meta == null)
				{
					ProTraderProfileMetadata.meta = new ProTraderProfileMetadata();
				}

				MapToMeta mapMethod = new MapToMeta(meta.tgDefault);
				mapDelegates.Add("tgDefault", mapMethod);
				mapMethod("tgDefault");
			}
			return 0;
		}

		private tgProviderSpecificMetadata tgDefault(string mapName)
		{
			if (!m_providerMetadataMaps.ContainsKey(mapName))
			{
				tgProviderSpecificMetadata meta = new tgProviderSpecificMetadata();


				meta.AddTypeMap("Id", new tgTypeMap("uniqueidentifier", "System.Guid"));
				meta.AddTypeMap("TraderId", new tgTypeMap("uniqueidentifier", "System.Guid"));
				meta.AddTypeMap("Description", new tgTypeMap("text", "System.String"));
				meta.AddTypeMap("TagLine", new tgTypeMap("varchar", "System.String"));
				meta.AddTypeMap("VideoEmbed", new tgTypeMap("text", "System.String"));
				meta.AddTypeMap("PortraitPath", new tgTypeMap("varchar", "System.String"));
				meta.AddTypeMap("ChatHash", new tgTypeMap("nvarchar", "System.String"));

				meta.Source = "ProTraderProfile";
				meta.Destination = "ProTraderProfile";

				meta.spInsert = "proc_ProTraderProfileInsert";
				meta.spUpdate = "proc_ProTraderProfileUpdate";
				meta.spDelete = "proc_ProTraderProfileDelete";
				meta.spLoadAll = "proc_ProTraderProfileLoadAll";
				meta.spLoadByPrimaryKey = "proc_ProTraderProfileLoadByPrimaryKey";

				this.m_providerMetadataMaps["tgDefault"] = meta;
			}

			return this.m_providerMetadataMaps["tgDefault"];
		}

		#endregion

		static private ProTraderProfileMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _tgDefault = RegisterDelegateesDefault();
	}
}

