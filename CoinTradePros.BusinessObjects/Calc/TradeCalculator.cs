﻿using System;
using System.Collections.Generic;

namespace CoinTradePros.BusinessObjects.Calc
{
    public class TradeCalculator
    {
        private readonly decimal _entryPrice;
        private readonly decimal _exitPrice;
        private readonly decimal _positionSize;
        private readonly ICollection<ITradeItem> _trades;


        public TradeCalculator(ICollection<ITradeItem> trades)
        {
            _trades = trades;
        }

        public TradeCalculator(ITradeItem tradeItem)
        {
            if (tradeItem == null)
            {
                throw new ArgumentNullException(nameof(tradeItem));
            }
            _entryPrice = tradeItem.EntryPrice ?? 0;
            _exitPrice = tradeItem.ExitPrice ?? 0;
            _positionSize = tradeItem.PositionSize ?? 0;

            if (_entryPrice == 0 || _exitPrice == 0 || _positionSize == 0)
            {
                throw new ArgumentException($"EntryPrice({tradeItem.EntryPrice}), ExitPrice({tradeItem.ExitPrice}), and PositionSize({tradeItem.PositionSize}) must both be greater than zero.");
            }

        }

        public TradeCalculator(decimal? entryPrice, decimal? exitPrice, decimal? positionSize)
        {
            _entryPrice = entryPrice ?? 0;
            _exitPrice = exitPrice ?? 0;
            _positionSize = positionSize ?? 0;
            if (_entryPrice == 0 || _positionSize == 0)
            {
                throw new ArgumentException($"EntryPrice({entryPrice}) and PositionSize({positionSize}) must both be greater than zero.");
            }
        }
        public TradeCalculator(decimal entryPrice, decimal exitPrice, decimal positionSize)
        {
            _entryPrice = entryPrice;
            _exitPrice = exitPrice;
            _positionSize = positionSize;
            if (_entryPrice == 0 || _exitPrice == 0 || _positionSize == 0)
            {
                throw new ArgumentException($"EntryPrice({entryPrice}) and ExitPrice({exitPrice}) must both be greater than zero.");
            }
        }

        public decimal  CalculateProfitLossAmount()
        {
            decimal profitLoss = _exitPrice - _entryPrice;
            return profitLoss * _positionSize;
        }

        public decimal CalculateProfitLossPercent()
        {
            if (_entryPrice == 0)
            {
                return 0;
            }
            decimal percent = (_exitPrice / _entryPrice) - 1;
            decimal profitpercent = Math.Round(percent, 5);
            return profitpercent;
        }

        public decimal GetCostBasis()
        {
            return _entryPrice * _positionSize;
        }

        public decimal GetGrossProfit()
        {
            decimal grossProfit = 0;
            foreach (var t in _trades)
            {
                if (!t.IsClosed())
                {
                    continue;
                }
                decimal profitLoss = t.ExitPrice.Value - t.EntryPrice.Value;
                decimal tempProfitLoss = profitLoss * t.PositionSize.Value;
                if (tempProfitLoss > 0)
                {
                    grossProfit += tempProfitLoss;
                }
            }
            return grossProfit;
        }

        public decimal GetGrossLoss()
        {
            decimal grossProfit = 0;
            foreach (var t in _trades)
            {
                if (!t.IsClosed())
                {
                    continue;
                }
                decimal profitLoss = t.ExitPrice ?? 0 - t.EntryPrice ?? 0;
                decimal tempProfitLoss = profitLoss * t.PositionSize ?? 0;
                if (tempProfitLoss < 0)
                {
                    grossProfit -= tempProfitLoss;
                }
            }
            return grossProfit;
        }

        public decimal GetTotalCostBasis()
        {
            decimal costBasis = 0;
            foreach (var trade in _trades)
            {
                if (!trade.IsClosed())
                {
                    continue;
                }
                costBasis += trade.EntryPrice.Value * trade.PositionSize.Value;
            }
            return costBasis;
        }



        public decimal GetTotalNetProfit()
        {
            decimal totalProfit = GetGrossProfit();
            decimal totalLoss = GetGrossLoss();

            return totalProfit - totalLoss;
        }

        public decimal GetTotalProfitPercent()
        {
            decimal grossProfit = GetGrossProfit();
            decimal totalCostBasis = GetTotalCostBasis();

            if (totalCostBasis == 0) return 0;
            return grossProfit / totalCostBasis;
        }

        public int TotalTrades()
        {
            return _trades.Count;
        }

        public int ProfitableTradeCount()
        {
            int count = 0;
            foreach(var t in _trades)
            {
                if (_exitPrice > _entryPrice)
                {
                    count++;
                }
            }
            return count;
        }

        public int LosingTradeCount()
        {
            int count = 0;
            foreach (var t in _trades)
            {
                if (_exitPrice < _entryPrice)
                {
                    count++;
                }
            }
            return count;
        }
    }
}