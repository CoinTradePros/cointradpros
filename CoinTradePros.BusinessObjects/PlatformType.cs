﻿namespace CoinTradePros.BusinessObjects
{
    public enum PlatformType
    {
        Messaging,
        Trade,
        Exchange,
        Account
    }
}
