﻿using System;

namespace CoinTradePros.BusinessObjects
{
    public class PlanOptions
    {
        public const string Month = "month";
        public const string Year = "year";
        public const string Lifetime = "lifetime";

        public string Id { get; set; }

        public string Name { get; set; }

        public int Amount { get; set; }

        public string Currency { get; set; }

        public string Interval { get; set; }

        public int IntervalCount { get; set; }

        public string StatementDescriptor { get; set; }

        public int TrialPeriodDays { get; set; }
    }
}
