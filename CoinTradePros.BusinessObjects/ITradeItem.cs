﻿using System;

namespace CoinTradePros.BusinessObjects
{
    public interface ITradeItem
    {
        decimal? EntryPrice { get; set; }

        decimal? ExitPrice { get; set; }

        decimal? PositionSize { get; set; }

        bool IsAttributedTo(Guid proTraderProfileId);

        bool IsClosed();

        Asset GetAsset(bool alldata);

        Exchange GetExchange(bool alldata);


    }
}
