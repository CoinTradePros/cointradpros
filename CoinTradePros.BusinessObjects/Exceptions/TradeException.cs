﻿using System;

namespace CoinTradePros.BusinessObjects.Exceptions
{
    public class TradeException : Exception
    {
        public TradeException(string message) : base(message)
        {

        }
    }
}
