/// <autosync enabled="true" />
/// <reference path="../App/app.js" />
/// <reference path="../App/main.js" />
/// <reference path="../App/router.js" />
/// <reference path="../assets/js/bs.Modal.min.js" />
/// <reference path="../assets/js/modal.min.js" />
/// <reference path="../assets/js/toastr.js" />
/// <reference path="../content/assets/js/chart-chartjs.min.js" />
/// <reference path="../content/assets/js/chart-easypie.min.js" />
/// <reference path="../content/assets/js/chart-echarts.min.js" />
/// <reference path="../content/assets/js/chart-flot.min.js" />
/// <reference path="../content/assets/js/chart-knobs.min.js" />
/// <reference path="../content/assets/js/chart-morris.min.js" />
/// <reference path="../content/assets/js/chart-rickshaw.min.js" />
/// <reference path="../content/assets/js/chart-sparkline.min.js" />
/// <reference path="../content/assets/js/dashboard.min.js" />
/// <reference path="../content/assets/js/form-validation.min.js" />
/// <reference path="../content/assets/js/jquery.easing.min.js" />
/// <reference path="../content/assets/js/jquery-1.12.4.min.js" />
/// <reference path="../content/assets/js/messenger.min.js" />
/// <reference path="../content/assets/js/scripts.min.js" />
/// <reference path="../Content/assets/plugins/bootstrap/js/bootstrap.min.js" />
/// <reference path="../Content/assets/plugins/datatables/extensions/Responsive/bootstrap/3/dataTables.bootstrap.js" />
/// <reference path="../Content/assets/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js" />
/// <reference path="../Content/assets/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js" />
/// <reference path="../Content/assets/plugins/datatables/js/jquery.dataTables.min.js" />
/// <reference path="../Content/assets/plugins/jquery-ui/smoothness/jquery-ui.min.js" />
/// <reference path="../content/assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" />
/// <reference path="../content/js/jquery-1.12.4.min.js" />
/// <reference path="_customsite.min.js" />
/// <reference path="Account/Account.js" />
/// <reference path="affix.min.js" />
/// <reference path="alert.min.js" />
/// <reference path="anchor.min.js" />
/// <reference path="application.min.js" />
/// <reference path="bootstrap-select.min.js" />
/// <reference path="button.min.js" />
/// <reference path="carousel.min.js" />
/// <reference path="chat/chat.min.js" />
/// <reference path="clipboard.min.js" />
/// <reference path="collapse.min.js" />
/// <reference path="Custom/_dashBoardLayout.js" />
/// <reference path="Custom/Layout.js" />
/// <reference path="Custom/myAjax.min.js" />
/// <reference path="docs.min.js" />
/// <reference path="dropdown.min.js" />
/// <reference path="fastclick.js" />
/// <reference path="history.js/amplify.store.js" />
/// <reference path="history.js/history.adapter.dojo.js" />
/// <reference path="history.js/history.adapter.jquery.js" />
/// <reference path="history.js/history.adapter.mootools.js" />
/// <reference path="history.js/history.adapter.prototype.js" />
/// <reference path="history.js/history.adapter.yui.js" />
/// <reference path="history.js/history.adapter.zepto.js" />
/// <reference path="history.js/history.html4.js" />
/// <reference path="history.js/history.js" />
/// <reference path="history.js/json2.js" />
/// <reference path="holder.min.js" />
/// <reference path="html2canvas.min.js" />
/// <reference path="ie10-viewport-bug-workaround.js" />
/// <reference path="ie-emulation-modes-warning.min.js" />
/// <reference path="image-grid.min.js" />
/// <reference path="jquery.printarea.js" />
/// <reference path="jquery.showLoading.min.js" />
/// <reference path="jquery.signalr-2.2.2.min.js" />
/// <reference path="jquery.unobtrusive-ajax.min.js" />
/// <reference path="jquery.validate.min.js" />
/// <reference path="jquery.validate.unobtrusive.min.js" />
/// <reference path="jquery.validate-vsdoc.min.js" />
/// <reference path="jquery-3.2.1.min.js" />
/// <reference path="jquery-3.2.1.slim.min.js" />
/// <reference path="jquery-slim.min.js" />
/// <reference path="knockout-3.4.2.debug.min.js" />
/// <reference path="knockout-3.4.2.min.js" />
/// <reference path="locales/bootstrap-datepicker.ar.min.js" />
/// <reference path="locales/bootstrap-datepicker.az.min.js" />
/// <reference path="locales/bootstrap-datepicker.bg.min.js" />
/// <reference path="locales/bootstrap-datepicker.bs.min.js" />
/// <reference path="locales/bootstrap-datepicker.ca.min.js" />
/// <reference path="locales/bootstrap-datepicker.cs.min.js" />
/// <reference path="locales/bootstrap-datepicker.cy.min.js" />
/// <reference path="locales/bootstrap-datepicker.da.min.js" />
/// <reference path="locales/bootstrap-datepicker.de.min.js" />
/// <reference path="locales/bootstrap-datepicker.el.min.js" />
/// <reference path="locales/bootstrap-datepicker.en-au.min.js" />
/// <reference path="locales/bootstrap-datepicker.en-gb.min.js" />
/// <reference path="locales/bootstrap-datepicker.eo.min.js" />
/// <reference path="locales/bootstrap-datepicker.es.min.js" />
/// <reference path="locales/bootstrap-datepicker.et.min.js" />
/// <reference path="locales/bootstrap-datepicker.eu.min.js" />
/// <reference path="locales/bootstrap-datepicker.fa.min.js" />
/// <reference path="locales/bootstrap-datepicker.fi.min.js" />
/// <reference path="locales/bootstrap-datepicker.fo.min.js" />
/// <reference path="locales/bootstrap-datepicker.fr.min.js" />
/// <reference path="locales/bootstrap-datepicker.fr-ch.min.js" />
/// <reference path="locales/bootstrap-datepicker.gl.min.js" />
/// <reference path="locales/bootstrap-datepicker.he.min.js" />
/// <reference path="locales/bootstrap-datepicker.hr.min.js" />
/// <reference path="locales/bootstrap-datepicker.hu.min.js" />
/// <reference path="locales/bootstrap-datepicker.hy.min.js" />
/// <reference path="locales/bootstrap-datepicker.id.min.js" />
/// <reference path="locales/bootstrap-datepicker.is.min.js" />
/// <reference path="locales/bootstrap-datepicker.it.min.js" />
/// <reference path="locales/bootstrap-datepicker.it-ch.min.js" />
/// <reference path="locales/bootstrap-datepicker.ja.min.js" />
/// <reference path="locales/bootstrap-datepicker.ka.min.js" />
/// <reference path="locales/bootstrap-datepicker.kh.min.js" />
/// <reference path="locales/bootstrap-datepicker.kk.min.js" />
/// <reference path="locales/bootstrap-datepicker.ko.min.js" />
/// <reference path="locales/bootstrap-datepicker.kr.min.js" />
/// <reference path="locales/bootstrap-datepicker.lt.min.js" />
/// <reference path="locales/bootstrap-datepicker.lv.min.js" />
/// <reference path="locales/bootstrap-datepicker.me.min.js" />
/// <reference path="locales/bootstrap-datepicker.mk.min.js" />
/// <reference path="locales/bootstrap-datepicker.mn.min.js" />
/// <reference path="locales/bootstrap-datepicker.ms.min.js" />
/// <reference path="locales/bootstrap-datepicker.nb.min.js" />
/// <reference path="locales/bootstrap-datepicker.nl.min.js" />
/// <reference path="locales/bootstrap-datepicker.nl-be.min.js" />
/// <reference path="locales/bootstrap-datepicker.no.min.js" />
/// <reference path="locales/bootstrap-datepicker.pl.min.js" />
/// <reference path="locales/bootstrap-datepicker.pt.min.js" />
/// <reference path="locales/bootstrap-datepicker.pt-br.min.js" />
/// <reference path="locales/bootstrap-datepicker.ro.min.js" />
/// <reference path="locales/bootstrap-datepicker.rs.min.js" />
/// <reference path="locales/bootstrap-datepicker.rs-latin.min.js" />
/// <reference path="locales/bootstrap-datepicker.ru.min.js" />
/// <reference path="locales/bootstrap-datepicker.sk.min.js" />
/// <reference path="locales/bootstrap-datepicker.sl.min.js" />
/// <reference path="locales/bootstrap-datepicker.sq.min.js" />
/// <reference path="locales/bootstrap-datepicker.sr.min.js" />
/// <reference path="locales/bootstrap-datepicker.sr-latin.min.js" />
/// <reference path="locales/bootstrap-datepicker.sv.min.js" />
/// <reference path="locales/bootstrap-datepicker.sw.min.js" />
/// <reference path="locales/bootstrap-datepicker.th.min.js" />
/// <reference path="locales/bootstrap-datepicker.tr.min.js" />
/// <reference path="locales/bootstrap-datepicker.uk.min.js" />
/// <reference path="locales/bootstrap-datepicker.vi.min.js" />
/// <reference path="locales/bootstrap-datepicker.zh-cn.min.js" />
/// <reference path="locales/bootstrap-datepicker.zh-tw.min.js" />
/// <reference path="modal.min.js" />
/// <reference path="modernizr-2.6.2.min.js" />
/// <reference path="moment.min.js" />
/// <reference path="moment-with-locales.min.js" />
/// <reference path="popover.min.js" />
/// <reference path="popper.js" />
/// <reference path="popper-utils.min.js" />
/// <reference path="r.js" />
/// <reference path="require.min.js" />
/// <reference path="respond.min.js" />
/// <reference path="scriptViews.js" />
/// <reference path="scrollspy.min.js" />
/// <reference path="select2/select2.full.min.js" />
/// <reference path="select2/select2-tab-fix.min.js" />
/// <reference path="tab.min.js" />
/// <reference path="tempusdominus-bootstrap-4.min.js" />
/// <reference path="tiraggo.jqAjax.debug.min.js" />
/// <reference path="tiraggo.jqajax.js" />
/// <reference path="tiraggo.XHR.debug.min.js" />
/// <reference path="tiraggo.xhr.js" />
/// <reference path="toolkit.min.js" />
/// <reference path="tooltip.min.js" />
/// <reference path="util.min.js" />
/// <reference path="zoom.min.js" />
