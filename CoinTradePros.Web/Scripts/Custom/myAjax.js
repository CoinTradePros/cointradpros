﻿
function Connect(data) {
    Ajax_Post("/Exchange/Connect?exchangeName=" + data)
}
function Connected(data) {
   Ajax_Post("/Exchange/Connected")
}
function Import(data) {
    Ajax_Post("/Exchange/Import?exchangeRequest=" + data)
}
function Ajax_Post(url) {
    var exchangeRequest = { "exchangeName": "", "ApiKey": "", "ApiSecret": "" }
    exchangeRequest.exchangeName = $("#exchangeName").val();
    exchangeRequest.ApiKey = $("#ApiKey").val();
    exchangeRequest.ApiSecret = $("#ApiSecret").val();
    $.ajax({
        type: 'POST',
        url: url,
        data: { exchangeRequest: exchangeRequest },
        dataType: 'html',
        success: function (result) {
            $("#LoadExchange").html(result)
            $('#myModal').modal('show');
            
        },
        error: function (request, message, error) {

            alert(request['responseText']);
        }
    })
}
function Close() {
    $('body').removeClass('modal-open');
    $('.modal-backdrop').removeClass('fade in')
    $('.modal-backdrop').removeClass('modal-backdrop')
    $('#myModal').modal('hide');
    $('.modal').removeClass('show');
}