﻿
//GET SUBSCRIBERS
//$("#chatbutton").click(function () {
    
//});

//HUB FUNCTION, START CONNECTION
$(document).ready(function () {

    //for AppendMessageNotificationOnLoad
    //var url = "/Users/ApplentMessageNotificationOnLoad/";
    //$.ajax(
    //        {
    //            url: url,
    //            type: "Post",
    //            async: false,
    //            cache: false,
    //            data: {},
    //            success: function (result) {
    //                //alert("Success");
    //                $("#message_notification").html(result);
    //            },
    //            error: function () {
    //                alert('Something went wrong');
    //            }
    //        }
    //    );



    // for ToLoadSubcribers
    var url = "/Users/GetSubscribersforChatBar/";
    $.ajax(
            {
                url: url,
                type: "Post",
                async: false,
                cache: false,
                data: {},
                success: function (result) {
                    //alert("Success");
                    $("#chat-bar").html(result);
                },
                error: function () {
                    alert('Something went wrong');
                }
            }
        );

    //var chat = $.connection.ChatHub;
    $.connection.hub.start().done(function () {
        $.connection.chatHub.server.onConnected();
    });

    
});

//HUB FUNSTION, SEND MESSAGE TO CHAT HUB
$(document).on('keypress', ".chatapi-windows .user-window .typearea input", function (e) {
    if (e.keyCode == 13) {
        var id = $(this).attr("data-user-id");
        var msg = $(this).val();

        $(".chatapi-windows #user-window" + id + " .chatarea").perfectScrollbar({
            suppressScrollX: true
        });
        $(this).val("");
        var chat = $.connection.chatHub;
        var flag = chat.server.send(id, msg);
    }
    
});

//HUB FUNCTION, APPEND RECIVED MESSAGE IN CHAT BAR
$.connection.chatHub.client.appendmsg = function (msg, type, name, username, image, dt) {
    debugger;
    var id = "#user-window" + username;
    if ($(id).length == 0) {

        debugger;

        if (!image) {
            image = "/Content/img/blank_avatar.png";
        }

        var h1 = "<li class=' msg-notification' id='" + username + "' onclick='NotifClick(this)' ><a href='javascript:;'><div class='user-img'><img src='" + image + "' alt='user-image' class='img-circle img-inline'></div><div><span class='name'><strong>";
        var h2 = "</strong><span class='time small'>   -" + dt + "</span></span><span class='desc small'>";
        var h3 = "</span></div></a></li>"
        var html = h1 + name + h2 + msg + h3;
        $("#message_notification").prepend(html);
        //UPDATE BADGE
        var badgetext = $("#messagebadge").text();
        var count = Number(badgetext);
        var temp = count + 1;
        $("#messagebadge").text(temp);
        if ($("#messagebadge").hasClass("hidden")) {
            $("#messagebadge").removeClass("hidden");
        }
    }
    else if (($(id).length != 0) && ($(id).css('display') == 'none')) {
        debugger;
        var image = "~/Content/data/profile/avatar-2.png";
        var h1 = "<li class='msg-notification' id='" + username + "' onclick='NotifClick(this)'><a href='javascript:;'><div class='user-img'><img src=" + image + "alt='user-image' class='img-circle img-inline'></div><div><span class='name'><strong>";
        var h2 = "</strong><span class='time small'>- 45 mins ago</span></span><span class='desc small'>";
        var h3 = "</span></div></a></li>";
        var html = h1 + name + h2 + msg + h3;
        $("#message_notification").prepend(html);
        var d = new Date();
        var h = d.getHours();
        var m = d.getMinutes();
        var html = "<div class='chatmsg msg_" + type + "'><span class='name'>" + name + "</span><span class='text'>" + msg + "</span><span class='ts'>" + h + ":" + m + "</span></div>";

        $(id).find(".chatarea").append(html);
        //UPDATE BADGE
        var badgetext = $("#messagebadge").text();
        var count = Number(badgetext);
        var temp = count + 1;
        $("#messagebadge").text(temp);
        if ($("#messagebadge").hasClass("hidden")) {
            $("#messagebadge").removeClass("hidden");
        }
    }
    else {
        var d = new Date();
        var h = d.getHours();
        var m = d.getMinutes();
        var html = "<div class='chatmsg msg_" + type + "'><span class='name'>" + name + "</span><span class='text'>" + msg + "</span><span class='ts'>" + h + ":" + m + "</span></div>";

        $(id).find(".chatarea").append(html);
    }
    debugger;
    var tid = $(this).attr("data-user-id");

    $(".chatapi-windows #user-window" + tid + " .chatarea").scrollTop($(".chatapi-windows #user-window" + tid + " .chatarea").prop("scrollHeight"));
    $(".chatapi-windows #user-window" + tid + " .chatarea").perfectScrollbar('update');


};


//HUB FUNCTION, SHOW STATUS OF SENT MESSAGE
$.connection.chatHub.client.status = function (msg, type, name, username) {
    debugger;
    var id = "#user-window" + username;
    if ($(id).length == 0) {
        //Notification
    }
    else {
        var d = new Date();
        var h = d.getHours();
        var m = d.getMinutes();
        var html = "<div class='chatmsg msg_" + type + "'><span class='name'>" + name + "</span><span class='text'>" + msg + "</span><span class='ts'>" + h + ":" + m + "</span></div>";

        $(id).find(".chatarea").append(html);
    }
};

//OPEN CHAT WINDOW ON CLICK MESSAGE NOTIFICATION 
$("#msg-notification").click(function () {
    var id = $(this).attr("id");
    ChatWindow(id);
});

//OPEN CHAT WINDOW FOR NOTIFICATION CLICK
function ChatWindow(username) {
    var tempid = "#contacts-list-" + username;
    //debugger;
    var name = $(tempid).find(".user-info h4 a").html();
    var img = $(tempid).find(".user-img a img").attr("src");
    var id = $(tempid).attr("data-user-id");
    var status = $(tempid).find(".user-info .status").attr("data-status");
    //debugger;
    if ($(tempid).hasClass("active")) {
        $(tempid).toggleClass("active");

        $(".chatapi-windows #user-window" + id).hide();

    } else {
        $(tempid).toggleClass("active");

        if ($(".chatapi-windows #user-window" + id).length) {

            $(".chatapi-windows #user-window" + id).removeClass("minimizeit").show();

        } else {

            //Append older messages here
            var msg = chatformat_msg('Wow! What a Beautiful theme!', 'receive', name);
            msg += chatformat_msg('Yes! Crest Admin Theme ;)', 'sent', 'You');
            var html = "<div class='user-window' id='user-window" + id + "' data-user-id='" + id + "'>";
            html += "<div class='controlbar'><img src='" + img + "' data-user-id='" + id + "' rel='tooltip' data-animate='animated fadeIn' data-toggle='tooltip' data-original-title='" + name + "' data-placement='top' data-color-class='primary'><span class='status " + status + "'><i class='fa fa-circle'></i></span><span class='name'>" + name + "</span><span class='opts'><i class='fa fa-times closeit' data-user-id='" + id + "'></i><i class='fa fa-minus minimizeit' data-user-id='" + id + "'></i></span></div>";
            html += "<div class='chatarea'>" + msg + "</div>";
            html += "<div class='typearea'><input type='text' data-user-id='" + id + "' placeholder='Type & Enter' class='form-control msgbox'></div>";
            html += "</div>";
            $(".chatapi-windows").append(html);
        }
    }

};

function NotifClick(item)
{
    debugger;
    var id = $(item).attr("id");
    var clid = "#contacts-list-" + id;
    if ($(clid).length == 0)
    {
        $("#chatbutton").trigger("click");
    }
    NotifClick2(clid);
    
};


function NotifClick2(clid) {
    
    $(clid).trigger("click");

};
//ON CLICK MESSAGE NOTIFICATION BAR, HIDE BADGE AND SET VALUE TO ZERO
$("#messagenotifbtn").click(function() {
    debugger;
    var badgetext = $("#messagebadge").text();
    var count = Number(badgetext);
    var temp = count - 1;
    $("#messagebadge").text(0);
    $("#messagebadge").addClass("hidden");
});