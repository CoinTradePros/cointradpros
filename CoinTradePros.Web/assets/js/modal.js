﻿$(document).ready(function () {
    $('#coachparahide').hide();
    $('#imgaccountcoach').hide();
})


var registermodalShow = function () {

    $('#registermodal').modal('show');
};

var loginmodalShow = function () {

    $('#loginmodal').modal('show');
};

var chkButton = function () {
    var chkval = $('#status input:radio:checked').val();
    if(chkval=="Student")
    {
        $('#imgaccountstd').show();
        $('#imgaccountcoach').hide();
        $('#coachparahide').hide();
        $('#stdparahide').show();
    }else if(chkval=="Coach")
    {
        $('#imgaccountstd').hide();
        $('#imgaccountcoach').show();
        $('#coachparahide').show();
        $('#stdparahide').hide();
    }
};