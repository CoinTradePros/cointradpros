﻿using System;

namespace CoinTradePros.Web.Models
{
    public class AlertDetailModel
    {
        public Guid MessageId { get; set; }

        public TimeSpan ProcessingTime { get; set; }

        public int TotalMessageCount { get; set; }
    }
}