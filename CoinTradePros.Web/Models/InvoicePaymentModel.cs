﻿namespace CoinTradePros.Web.Models
{
    public class InvoicePaymentModel
    {
        public int ConfirmsProcessed { get; set; }

        public decimal AmountReceived { get; set; }

        public int ConfirmsNeeded { get; set; }

        public bool PaymentComplete { get; set; }
    }
}