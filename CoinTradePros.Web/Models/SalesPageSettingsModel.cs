﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;

namespace CoinTradePros.Web.Models
{
    [ValidateInput(false)]
    public class SalesPageSettingsModel
    {
        public SalesPageSettingsModel()
        {
            Questions = new List<QuestionAnswerModel>();
            Packages = new List<PackageModel>();
            ProFileDetails = new List<ProfileDetailModel>();
        }

        [HiddenInput]
        public Guid ProTraderProfileId { get; set; }

        public string ProTraderUserName { get; set; }

        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [DataType(DataType.Text)]
        public string Tagline { get; set; }
        [DataType(DataType.Html)]
        [AllowHtml]
        [Display(Name = "Video Embed Code")]
        public string VideoEmbedCode { get; set; }

        public Guid? ProxyProTraderId { get; set; }

        public string PortraitPath { get; set; }

        public Guid? ProxyTraderId { get; set; }

        [DataType(DataType.Upload)]
        [Display(Name = "Sales Page Portrait")]
        //[RegularExpression(@"(?i).*\.(gif|jpe?g|png|bmp)$", ErrorMessage = "Only image files are allowed")]
        public HttpPostedFileBase ProTraderPortrait { get; set; }

        public ICollection<QuestionAnswerModel> Questions { get; set; }

        public ICollection<PackageModel> Packages { get; set; }

        public ICollection<ProfileDetailModel> ProFileDetails { get; set; }

        public bool IsProxyForProMode()
        {
            bool x =
                   ProxyProTraderId.HasValue &&
                   (ProxyProTraderId != Guid.Empty || ProTraderProfileId != Guid.Empty);
            return x;
        }

        public bool IsProxyForTraderMode()
        {
            return ProxyTraderId.HasValue && ProxyTraderId != Guid.Empty;
        }
    }
}