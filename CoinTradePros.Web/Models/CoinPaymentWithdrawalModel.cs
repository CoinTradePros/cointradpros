﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoinTradePros.Web.Models
{
    public class CoinPaymentWithdrawalModel : CoinPaymentIpnModelBase
    {
        /*
         * Withdrawal Information (ipn_type = 'withdrawal')
         */

        /// <summary>
        /// The ID of the withdrawal ('id' field returned from 'create_withdrawal'.)	
        /// </summary>
        public string Id { get; set; }

        public override void Fill(IDictionary<string, string> fields)
        {
            throw new NotImplementedException();
        }

        public override bool IsValid()
        {
            throw new NotImplementedException();
        }
    }
}