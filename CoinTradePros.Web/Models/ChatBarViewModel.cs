﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoinTradePros.Web.Models
{
    public class ChatBarViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
        public string ImageUrl { get; set; }
        public string Status { get; set; }
    }
}