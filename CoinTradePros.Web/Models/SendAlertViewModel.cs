﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoinTradePros.Web.Models
{
    public class SendAlertViewModel
    {
        public string AlertBody { get; set; }
        public ICollection<PackageModel> Packages { get; set; }
        public ICollection<MessagingOptionModel> MessagingOptions { get; set; }
        public HttpPostedFileBase ChartImage { get; set; }


    }
}