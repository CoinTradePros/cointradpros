﻿using System.Collections.Generic;

namespace CoinTradePros.Web.Models
{
    public class EnrollmentModel
    {
        public ICollection<CourseModel> MyEnrollments { get; set; }

        public ICollection<CourseModel> StudentEnrollments { get; set; }
    }
}