﻿using System;

namespace CoinTradePros.Web.Models
{
    public class ProfileDetailTypeModel
    {
        public Guid DetailTypeId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }
    }
}