﻿using System;
using System.Collections.Generic;

namespace CoinTradePros.Web.Models
{
    public class FeatureGroupModel
    {
        public FeatureGroupModel()
        {
            FeatureModels = new List<FeatureModel>();
        }

        public ICollection<FeatureModel> FeatureModels { get; set; }

        public Guid? ProxyProTraderProfileId { get; set; }

        public bool IsProxyMode()
        {
            return ProxyProTraderProfileId.HasValue && ProxyProTraderProfileId != Guid.Empty;
        }
    }
}