﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CoinTradePros.BusinessObjects;

namespace CoinTradePros.Web.Models
{
    public class TradeModel : ITradeItem
    {
        public TradeModel()
        {
            Exchanges = new List<SelectListItem>();
            Assets = new List<SelectListItem>();
            Denominations = new List<SelectListItem>();
            Packages = new List<PackageModel>();
        }

        [HiddenInput]
        public Guid? TradeId { get; set; }

        [Required]
        [HiddenInput]
        public Guid? TraderId { get; set; }
        [Display(Name = "Exchange")]
        public Guid? ExchangeId { get; set; }

        [Display(Name = "Tip From Coach")]
        public Guid? ProTraderProfileId { get; set; }

        [Required]
        [Display(Name = "Denomination")]
        public Guid? DenominationId { get; set; }

        [Required]
        [Display(Name = "Traded Asset")]
        public Guid? AssetId { get; set; }

        [Required]
        [Display(Name = "Entry Date")]
        //[DataType(DataType.Date)]
        public DateTimeOffset? EntryDate { get; set; }

        [Required]
        [Display(Name = "Entry Price")]
        public decimal? EntryPrice { get; set; }

        [Required]
        [Display(Name = "Position Size")]
        public decimal? PositionSize { get; set; }

        [Display(Name = "Exit Date")]
        //[DataType(DataType.Date)]
        public DateTimeOffset? ExitDate { get; set; }

        [Display(Name = "Exit Price")]
        public decimal? ExitPrice { get; set; }

        [Display(Name = "Trade Comments")]
        [DataType(DataType.MultilineText)]
        public string Comments { get; set; }

        [DataType(DataType.Upload)]
        [Display(Name = "Chart")]
        //[RegularExpression(@"(?i).*\.(gif|jpe?g|png|bmp)$", ErrorMessage = "Only image files are allowed")]
        public HttpPostedFileBase ChartImage { get; set; }

        public string ChartImageUrl { get; set; }

        public ICollection<SelectListItem> Exchanges { get; set; }

        public ICollection<SelectListItem> Assets { get; set; }

        public ICollection<SelectListItem> Denominations { get; set; }

        public ICollection<SelectListItem> Subscriptions { get; set; }

        [HiddenInput]
        public decimal? ProfitLossAmount { get; set; }

        [HiddenInput]
        public decimal? ProfitLossPercent { get; set; }

        public ICollection<PackageModel> Packages { get; set; }

        public ICollection<MessagingOptionModel> MessagingOptions { get; set; }

        public bool IsAttributedTo(Guid proTraderProfileId)
        {
            return ProTraderProfileId == proTraderProfileId;
        }

        public bool IsClosed()
        {
            return ExitDate != null;
        }

        public Asset GetAsset(bool fullData)
        {
            Asset asset = new Asset();
            if (fullData)
            {

                if (AssetId.HasValue && asset.LoadByPrimaryKey(AssetId.Value))
                {
                    return asset;
                }
            }
            else
            {
                SelectListItem assetitem = Assets.Single(x => x.Value == (AssetId ?? Guid.Empty).ToString());
                asset.Id = AssetId;
                asset.Name = assetitem.Text;
            }
            return asset;
        }

        public BusinessObjects.Exchange GetExchange(bool fullData)
        {
            if (ExchangeId == null)
            {
                return null;
            }
            BusinessObjects.Exchange exchange = new BusinessObjects.Exchange();
            if (fullData)
            {

                if (ExchangeId.HasValue && exchange.LoadByPrimaryKey(ExchangeId.Value))
                {
                    return exchange;
                }
            }
            else
            {
                SelectListItem exchangetitem = Exchanges.Single(x => x.Value == (ExchangeId ?? Guid.Empty).ToString());
                exchange.Id = ExchangeId;
                exchange.Name = exchangetitem.Text;
            }
            return exchange;
        }

        public void Fill(Trade trade)
        {
            ProTraderProfileId = trade.ProTraderProfileId;
            AssetId = trade.AssetId;
            ChartImageUrl = trade.ChartImagePath;
            Comments = trade.Comments;
            DenominationId = trade.DenominationId;
            EntryDate = trade.EntryDate;
            ExitDate = trade.ExitDate;
            EntryPrice = trade.EntryPrice;
            ExitPrice = trade.ExitPrice;
            PositionSize = trade.PositionSize;
            ExchangeId = trade.ExchangeId;
            ProfitLossAmount = trade.ProfitLossAmount;
            ProfitLossPercent = trade.ProfitLossPercent;
            DenominationId = trade.DenominationId;
        }
    }
}
