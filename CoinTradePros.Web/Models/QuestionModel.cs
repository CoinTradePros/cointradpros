﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CoinTradePros.BusinessObjects;

namespace CoinTradePros.Web.Models
{
    public class QuestionModel
    {
        public Guid? QuestionId { get; set; }

        public Guid? ExamId { get; set; }

        public string QuestionText { get; set; }

        public string OptionA { get; set; }

        public string OptionB { get; set; }

        public string OptionC { get; set; }

        public string OptionD { get; set; }

        public string OptionE { get; set; }

        public char? CorrectOption { get; set; }

        public int QuestionOrder { get; set; }

        public char? SelectedOption { get; set; }

        public void Fill(ChapterExamQuestion question)
        {
            QuestionId = question.Id;
            ExamId = question.ChapterExamId;
            QuestionText = question.QuestionText;
            OptionA = question.OptionA;
            OptionB = question.OptionB;
            OptionC = question.OptionC;
            OptionD = question.OptionD;
            OptionE = question.OptionE;
            CorrectOption = question.CorrectOption;
        }

        public bool IsCorrect()
        {
            return SelectedOption == CorrectOption;
        }

        public static ICollection<QuestionModel> Fill(ICollection<ChapterExamQuestion> questions)
        {
            ICollection<QuestionModel> qs = new List<QuestionModel>();

            foreach (var c in questions)
            {
                QuestionModel m = new QuestionModel();
                m.Fill(c);
                qs.Add(m);
            }
            return qs;
        }
    }
}