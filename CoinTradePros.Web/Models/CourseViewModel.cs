﻿using System;

namespace CoinTradePros.Web.Models
{
    public class CourseViewModel
    {
        internal int? ClassPeriods{get;set;}
        internal string CourseImplementerFirstName{get;set;}
        internal Guid? CourseImplementerId{get;set;}
        internal string CourseImplementerLastName{get;set;}
        internal string CourseOwnerFirstName{get;set;}
        internal Guid? CourseOwnerId{get;set;}
        internal string CourseOwnerLastName{get;set;}
        internal DateTime? EnrollmentDate{get;set;}
        internal Guid? EnrollmentId{get;set;}
        internal string InstanceDesc{get;set;}
        internal Guid? InstanceId{get;set;}
        internal string InstanceLocation{get;set;}
        internal string InstanceName{get;set;}
        internal Guid? InstanceTypeId{get;set;}
        internal string InstanceTypeName{get;set;}
        internal int? MaxStudents{get;set;}
        internal string OfferingDesc{get;set;}
        internal Guid? OfferingId{get;set;}
        internal string OfferingName{get;set;}
        internal string StudentFirstName{get;set;}
        internal Guid? StudentId{get;set;}
        internal string StudentLastName{get;set;}
        internal string StudentUserId{get;set;}
        internal decimal? TotalCost{get;set;}
        internal short? UnlockedSectionCount{get;set;}
    }
}