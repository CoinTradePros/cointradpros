﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using CoinTradePros.Interfaces.Messaging;

namespace CoinTradePros.Web.Models
{
    public class AlertModel
    {
        public AlertModel()
        {
            MessageFields = new ConcurrentDictionary<string, string>();
        }

        public string AlertGroups { get; set; }

        public string AlertChannels { get; set; }

        public Guid DataSourceId { get; set; }

        public string SenderId { get; set; }

        public TransmitterType MessageType { get; set; }

        public ISupplyMessageData DataItem { get; set; }

        public IDictionary<string, string> MessageFields { get; set; }
    }
}