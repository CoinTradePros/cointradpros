﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;
using CoinTradePros.BusinessObjects;
using CoinTradePros.Payments;

namespace CoinTradePros.Web.Models
{
    public class PackageModel
    {
        private CurrencyConverter _convertor;

        public PackageModel()
        {
            SelectedFeatures = new List<FeatureModel>();
            FeatureItems = new List<SelectListItem>();
        }

        [HiddenInput]
        public Guid? PackageId { get; set; }

        [Required]
        [HiddenInput]
        public Guid? ProTraderProfileId { get; set; }

        [Required]
        [Display(Name = "Title")]
        [DataType(DataType.Text)]
        public string Title { get; set; }

        [Required]
        [Display(Name = "Description")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [DataType(DataType.Currency)]
        [Display(Name = "Monthly Price")]
        public decimal? MonthlyPrice { get; set; }

        [DataType(DataType.Currency)]
        [Display(Name = "Annual Price")]
        public decimal? AnnualPrice { get; set; }

        [DataType(DataType.Currency)]
        [Display(Name = "Lifetime Price")]
        public decimal? LifetimePrice { get; set; }

        [DataType(DataType.Upload)]
        [Display(Name = "Package Icon")]
        //[RegularExpression(@"(?i).*\.(gif|jpe?g|png|bmp)$", ErrorMessage = "Only image files are allowed")]
        public HttpPostedFileBase PackageIcon { get; set; }

        public string IconPath { get; set; }

        public long CurrentSubscriberCount { get; set; }

        public string Username { get; set; }

        public bool UseAlternateCurrency { get; set; }

        public string AlternateCurrency { get; set; }

        public ICollection<FeatureModel> SelectedFeatures { get; set; }

        public ICollection<FeatureModel> AvailableFeatures { get; set; }

        public ICollection<SelectListItem> FeatureItems { get; set; }

        public Guid? ProxyProTraderProfileId { get; set; }

        public bool IsProxyMode()
        {
            return ProxyProTraderProfileId.HasValue && ProxyProTraderProfileId != Guid.Empty;
        }

        public decimal GetMonthlyAmount()
        {
            if (UseAlternateCurrency)
            {
                if (MonthlyPrice.HasValue)
                {
                    decimal result = MonthlyPrice.Value * GetConvertor().Convert();
                    return result;
                }
                return 0;
            }
            return MonthlyPrice ?? 0;
        }

        public decimal GetAnnualAmount()
        {
            if (UseAlternateCurrency)
            {
                if (AnnualPrice.HasValue)
                {
                    decimal result = AnnualPrice.Value * GetConvertor().Convert();
                    return result;
                }
                return 0;
            }
            return AnnualPrice ?? 0;
        }

        public decimal GetLifeTimePrice()
        {
            if (UseAlternateCurrency)
            {
                if (LifetimePrice.HasValue)
                {
                    decimal result = LifetimePrice.Value * GetConvertor().Convert();
                    return result;
                }
                return 0;
            }
            return LifetimePrice ?? 0;
        }

        public bool HasFeature(Guid featureId)
        {
            foreach (var featureModel in SelectedFeatures)
            {
                if (featureModel.FeatureId == featureId)
                {
                    return true;
                }
            }
            return false;
        }

        public bool IsRecurringInterval(string interval)
        {
            if (string.IsNullOrEmpty(interval))
            {
                throw new ArgumentNullException(nameof(interval));
            }
            if (interval == PlanOptions.Year || interval == PlanOptions.Month)
            {
                return true;
            }
            return false;
        }

        public decimal? GetIntervalPaymentAmount(string interval)
        {
            if (interval == PlanOptions.Year)
            {
                return AnnualPrice;
            }
            if (interval == PlanOptions.Month)
            {
                return MonthlyPrice;
            }
            if (interval == PlanOptions.Lifetime)
            {
                return LifetimePrice;
            }
            throw new InvalidOperationException($"The interval [{interval}] is not supported,");
        }

        public int GetPaymentFrequency(string interval)
        {
            if (interval == PlanOptions.Year)
            {
                return 1;
            }
            if (interval == PlanOptions.Month)
            {
                return 12;
            }
            return -1;
        }

        public string GetIntervalText(decimal intervalAmount)
        {
            if (intervalAmount == AnnualPrice)
            {
                return PlanOptions.Year;
            }
            if (intervalAmount == MonthlyPrice)
            {
                return PlanOptions.Month;
            }
            if (intervalAmount == LifetimePrice)
            {
                return "Lifetime";
            }
            return "Unsupported";
        }

        private CurrencyConverter GetConvertor()
        {
            if (UseAlternateCurrency)
            {
                if (_convertor == null)
                {
                    _convertor = new CurrencyConverter();
                    _convertor.SetBaseCurrency("USD");
                    _convertor.SetSecondaryCurrency(AlternateCurrency);
                }
            }
            return _convertor;
        }
    }
}
