﻿using System;
using System.Collections.Generic;
using CoinTradePros.BusinessObjects;

namespace CoinTradePros.Web.Models
{
    public class CourseModel
    {
        public CourseModel()
        {
            Sections = new List<SectionModel>();
        }

        public Guid? OfferingId { get; set; }

        public string OfferingName { get; set; }

        public string OfferingDesc { get; set; }

        public short? UnlockedSectionCount { get; set; }

        public Guid? CourseOwnerId { get; set; }

        public Guid? InstanceId { get; set; }

        public string InstanceName { get; set; }

        public string InstanceDesc { get; set; }

        public int? ClassPeriods { get; set; }

        public decimal? TotalCost { get; set; }

        public int? MaxStudents { get; set; }

        public string InstanceLocation { get; set; }

        public string InstanceTypeName { get; set; }

        public Guid? InstanceTypeId { get; set; }

        public Guid? EnrollmentId { get; set; }

        public DateTime? EnrollmentDate { get; set; }

        public string StudentUserId { get; set; }

        public string StudentFirstName { get; set; }

        public string StudentLastName { get; set; }

        public Guid? StudentId { get; set; }

        public Guid? CourseImplementerId { get; set; }

        public string CourseImpementerFirstName { get; set; }

        public string CourseImplementerLastName { get; set; }

        public string CourseOwnerLastName { get; set; }

        public string CourseOwnerFirstName { get; set; }

        public ICollection<SectionModel> Sections { get; set; }

        public void Fill(CourseView course, bool fillAllData)
        {
            OfferingId = course.OfferingId;
            OfferingName = course.OfferingName;
            OfferingDesc = course.OfferingDesc;
            UnlockedSectionCount = course.UnlockedSectionCount;
            CourseOwnerId = course.CourseOwnerId;
            InstanceId = course.InstanceId;
            InstanceName = course.InstanceName;
            InstanceDesc = course.InstanceDesc;
            ClassPeriods = course.ClassPeriods;
            TotalCost = course.TotalCost;
            MaxStudents = course.MaxStudents;
            InstanceLocation = course.InstanceLocation;
            InstanceTypeName = course.InstanceTypeName;
            InstanceTypeId = course.InstanceTypeId;
            EnrollmentId = course.EnrollmentId;
            EnrollmentDate = course.EnrollmentDate;
            StudentUserId = course.StudentUserId;
            StudentFirstName = course.StudentFirstName;
            StudentLastName = course.StudentLastName;
            StudentId = course.StudentId;
            CourseImplementerId = course.CourseImplementerId;
            CourseImpementerFirstName = course.CourseImplementerFirstName;
            CourseImplementerLastName = course.CourseImplementerLastName;
            CourseOwnerLastName = course.CourseOwnerLastName;
            CourseOwnerFirstName = course.CourseOwnerFirstName;

            if (!fillAllData) return;
            if (OfferingId == null) return;

            ICollection<CourseSection> sections = CourseSectionCollection.GetCourseSections(OfferingId.Value);
            Sections = SectionModel.Fill(sections, this, true);
        }

        public static ICollection<CourseModel> Fill(ICollection<CourseView> courses, bool fillAllData)
        {
            ICollection<CourseModel> coursegroup = new List<CourseModel>();

            foreach (var c in courses)
            {
                CourseModel m = new CourseModel();
                m.Fill(c, fillAllData);
                coursegroup.Add(m);                                 
            }
            return coursegroup;
        }
    }
}