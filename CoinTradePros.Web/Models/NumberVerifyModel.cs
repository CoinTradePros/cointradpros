﻿namespace CoinTradePros.Web.Models
{
    public class NumberVerifyModel
    {
        public string PhoneNumber { get; set; }

        public string Code { get; set; }
    }
}