﻿using System;
using System.Collections.Generic;
using System.Linq;
using CoinTradePros.BusinessObjects;

namespace CoinTradePros.Web.Models
{
    public class ChapterModel
    {
        public ChapterModel(SectionModel parent)
        {
            Parent = parent;
            Pages = new List<PageModel>();
        }

        public SectionModel Parent { get; set; }

        public Guid? Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public Guid? CourseSectionId { get; set; }

        public ICollection<PageModel> Pages { get; set; }

        public void Fill(CourseChapter chapter, SectionModel parent, bool fillAllData)
        {
            Id = chapter.Id;
            Name = chapter.Name;
            Description = chapter.Description;
            CourseSectionId = chapter.CourseSectionId;
            Parent = parent;

            if (!fillAllData) return;
            // load the page data
            if (chapter.Id == null) return;

            ICollection<CoursePage> pages = CoursePageCollection.GetChapterPages(chapter.Id.Value);
            Pages = PageModel.Fill(pages, this);
        }

        public static ICollection<ChapterModel> Fill(ICollection<CourseChapter> chapters, SectionModel parent, bool fillAllData)
        {
            ICollection<ChapterModel> coursegroup = new List<ChapterModel>();

            foreach (var c in chapters)
            {
                ChapterModel m = new ChapterModel(parent);
                m.Fill(c, parent, fillAllData);
                coursegroup.Add(m);
            }
            return coursegroup;
        }

        public PageModel GetNextPage(PageModel currentPage)
        {
            if (currentPage == null) return Pages.FirstOrDefault();
            foreach (var page in Pages)
            {
                if (currentPage.PageNumber + 1 == page.PageNumber)
                {
                    return page;
                }
            }
            return currentPage;
        }

        public PageModel GetPreviousPage(PageModel currentPage)
        {
            if (currentPage == null) return Pages.FirstOrDefault();
            foreach (var page in Pages)
            {
                if (currentPage.PageNumber - 1 == page.PageNumber)
                {
                    return page;
                }
            }
            return Pages.FirstOrDefault();
        }
    }
}