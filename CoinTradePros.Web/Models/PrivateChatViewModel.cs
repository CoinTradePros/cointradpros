﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoinTradePros.Web.Models
{
    public class PrivateChatViewModel
    {

        public Guid ID { get; set; }
        public Guid SubscriberID { get; set; }
        public string SenderID { get; set; }

        public string ReceiverID { get; set; }

        public string Message { get; set; }

        public DateTime SenderDate { get; set; }

        public TimeSpan SenderTime { get; set; }

        public bool Status { get; set; }

        public bool SenderIsDeleted { get; set; }

        public bool ReceiverIsDeleted { get; set; }

        public string MessageStatus { get; set; }

        public string Name { get; set; }

        public string Time { get; set; }
        public string Date { get; set; }


    }
}