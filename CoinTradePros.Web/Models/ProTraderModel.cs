﻿using System;
using System.Collections.Generic;

namespace CoinTradePros.Web.Models
{
    public class ProTraderModel
    {
        public Guid ProTraderProfileId { get; set; }

        public string AvatarUrl { get; set; }

        public string Username { get; set; }

        public string FullName { get; set; }

        public decimal ProfitAmount { get; set; }

        public decimal ProfitPercent { get; set; }

        public string ProfileDescription { get; set; }

        public string ProfileTagline { get; set; }

        public int TradeCount { get; set; }

        public string VideoEmbed { get; set; }

        public ICollection<PackageModel> Packages { get; set; }
    }
}