﻿using CoinTradePros.Interfaces.Exchange;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoinTradePros.Web.Models
{
    public class ExchangeRequestViewModel : IExchangeRequest
    {
        public string exchangeName { get; set; }
        public string ApiKey { get; set; }
        public string ApiSecret { get; set; }
    }
}