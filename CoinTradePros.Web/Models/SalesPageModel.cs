﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace CoinTradePros.Web.Models
{
    public class SalesPageModel
    {
        public SalesPageModel()
        {
            Questions = new List<QuestionAnswerModel>();
            Packages = new List<PackageModel>();
            AllFeatures = new List<FeatureModel>();
        }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Username { get; set; }

        public string Tagline { get; set; }

        public string Description { get; set; }

        public string VideoUrl { get; set; }

        public string PortraitUrl { get; set; }

        public string AvatarUrl { get; set; }

        public decimal ProfitAmount { get; set; }

        public decimal ProfitPercent { get; set; }

        public int TradeCount { get; set; }

        public bool IsProTraderViewing { get; set; }

        public ICollection<QuestionAnswerModel> Questions { get; set; }

        public ICollection<PackageModel> Packages { get; set; }

        public ICollection<FeatureModel> AllFeatures { get; set; }

        public ICollection<SelectListItem> PaymentCurrencies { get; set; }

        public ICollection<ProfileDetailModel> ProfileDetails { get; set; }

        public string AlternateCurrency { get; set; }

        public bool UseAlternateCurrency { get; set; }
    }
}