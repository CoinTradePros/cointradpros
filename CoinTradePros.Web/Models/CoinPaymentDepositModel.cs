﻿using System.Collections.Generic;
using CoinTradePros.Interfaces;

namespace CoinTradePros.Web.Models
{
    public class CoinPaymentDepositModel : CoinPaymentIpnModelBase
    {
        /*
        * Fields for Deposit Information (ipn_type = 'deposit')
        */

        /// <summary>
        /// Coin address the payment was received on
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// The coin transaction ID of the payment.	
        /// </summary>
        public int Txn_id { get; set; }

        /// <summary>
        /// Numeric status of the payment, currently 0 = pending and 100 = confirmed/complete. For future proofing you should use the same logic as Payment Statuses.
        /// IMPORTANT: You should never ship/release your product until the status is >= 100
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// The same as Status, but wrapped in an enum.
        /// </summary>
        public CoinPaymentStatusType StatusType { get; set; }

        /// <summary>
        /// A text string describing the status of the payment. (useful for displaying in order comments)
        /// </summary>
        public string Status_text { get; set; }

        /// <summary>
        /// The coin the buyer paid with.	
        /// </summary>
        public string Currency { get; set; }

        /// <summary>
        /// The number of confirms the payment has.	
        /// </summary>
        public int Confirms { get; set; }

        /// <summary>
        /// The total amount of the payment	
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// The total amount of the payment in Satoshis	
        /// </summary>
        public decimal Amounti { get; set; }

        /// <summary>
        /// The fee deducted by CoinPayments (only sent when status >= 100)	
        /// </summary>
        public decimal Fee { get; set; }

        /// <summary>
        /// The fee deducted by CoinPayments in Satoshis (only sent when status >= 100)	
        /// </summary>
        public decimal Feei { get; set; }

        public override void Fill(IDictionary<string, string> fields)
        {
            // Fill up this model with the incoming data.
        }

        public override bool IsValid()
        {
            return true;
        }
    }
}