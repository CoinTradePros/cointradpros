﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace CoinTradePros.Web.Models
{
    public class QuestionAnswerModel
    {
        [HiddenInput]
        public Guid QuestionId { get; set; }

        [HiddenInput]
        public Guid ProTraderProfileId { get; set; }

        [Required]
        [DataType(DataType.MultilineText)]
        public string Question { get; set; }

        [Required]
        [DataType(DataType.MultilineText)]
        public string Answer { get; set; }

        public Guid? ProxyProTraderProfileId { get; set; }

        public bool IsProxyMode()
        {
            return ProxyProTraderProfileId.HasValue && ProxyProTraderProfileId != Guid.Empty;
        }
    }
}