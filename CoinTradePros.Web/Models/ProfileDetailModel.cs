﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace CoinTradePros.Web.Models
{
    public class ProfileDetailModel
    {
        public Guid DetailId { get; set; }

        [Required]
        public Guid TraderProfileId { get; set; }

        public Guid? ProxyTraderProfileId { get; set; }

        [Required]
        [Display(Name = "Detail Type")]
        public Guid DetailTypeId { get; set; }

        public SelectListItem DetailType { get; set; }

        public ICollection<SelectListItem> DetailTypes { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Display Text")]
        public string DisplayText { get; set; }

        [DataType(DataType.Url)]
        [Display(Name = "Detail Link")]
        public string DetailUrl { get; set; }

        [Required]
        [Display(Name = "Show Link")]
        public bool ShowUrl { get; set; }

        public bool SalesPageDetail { get; set; }

        public string IconName { get; set; }

        [Display(Name = "Show Icon Only")]
        public bool ShowIconOnly { get; set; }

        [Display(Name = "Hide Icon")]
        public bool HideIcon { get; set; }

        [Display(Name = "Display Order")]
        public int SortOrder { get; set; }

        public bool IsProxyMode()
        {
            return ProxyTraderProfileId.HasValue && ProxyTraderProfileId != Guid.Empty;
        }
    }
}