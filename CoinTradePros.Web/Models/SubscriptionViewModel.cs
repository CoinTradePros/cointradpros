﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using CoinTradePros.BusinessObjects;
using CoinTradePros.BusinessObjects.Calc;

namespace CoinTradePros.Web.Models
{
    public class SubscriptionViewModel
    {
        public SubscriptionViewModel()
        {
            Trades = new List<TradeModel>();
        }

        public SubscriptionView SubscriptionInfo { get; set; }
        
        public ICollection<TradeModel> Trades { get; set; }

        public ICollection<TradeModel> GetAttributedTrades()
        {
            ICollection<ITradeItem> attrTrades = new List<ITradeItem>();
            foreach (var trade in Trades)
            {
                if (trade.IsAttributedTo(SubscriptionInfo.ProTraderProfileId ?? Guid.Empty))
                {
                    attrTrades.Add(trade);
                }
            }
            return attrTrades.Cast<TradeModel>().ToList();
        }

        public int GetAttributedTradeCount()
        {
            int count = 0;
            foreach (var trade in Trades)
            {
                if (trade.IsAttributedTo(SubscriptionInfo.ProTraderProfileId ?? Guid.Empty))
                {
                    count++;
                }
            }
            return count;
        }

        public decimal GetAttributionPercent()
        {
            if (Trades.Count == 0)
            {
                return 0;
            }
            int count = GetAttributedTradeCount();
            decimal percent = (decimal)count / Trades.Count;
            return percent;
        }

        public decimal GetAttributedTradeTotal()
        {
            var attrTrades = GetAttributedTrades();
            if (attrTrades.Count == 0)
            {
                return 0;
            }
            TradeCalculator calc = new TradeCalculator(attrTrades.Cast<ITradeItem>().ToList());
            return calc.GetGrossProfit();
        }

        public decimal GetTradeTotal()
        {
            TradeCalculator calc = new TradeCalculator(Trades.Cast<ITradeItem>().ToList());
            return calc.GetGrossProfit();
        }

        public decimal GetAttributedTradeAmountPercent()
        {
            decimal attrTotal = GetAttributedTradeTotal();
            decimal total = GetTradeTotal();
            if (total > 0)
            {
                return attrTotal / total;
            }
            return 0;
        }

        public ICollection<Asset> GetTopAssets(int count, bool alldata)
        {
            IDictionary<Guid, int> tally = new Dictionary<Guid, int>();
            ICollection<Asset> result = new List<Asset>();
            IDictionary<Guid, Asset> assetList = new ConcurrentDictionary<Guid, Asset>();
            foreach (var trade in Trades)
            {
                Asset asset = trade.GetAsset(alldata);
                if (asset?.Id == null)
                {
                    continue;
                }
                if (!assetList.ContainsKey(asset.Id.Value))
                {
                    assetList.Add(asset.Id.Value, asset);
                }
                if (tally.ContainsKey(asset.Id.Value))
                {
                    tally[asset.Id.Value]++;
                }
                else
                {
                    tally.Add(new KeyValuePair<Guid, int>(asset.Id.Value, 1));
                }
            }

            var itemlist = tally.ToList();
            itemlist.Sort((pair1, pair2) => pair2.Value.CompareTo(pair1.Value));

            foreach (var item in itemlist)
            {
                if (result.Count < count)
                {
                    Asset asset = assetList[item.Key];
                    result.Add(asset);
                    //count++;
                }
            }
            return result;
        }

        public ICollection<BusinessObjects.Exchange> GetTopExchanges(int count, bool alldata)
        {
            IDictionary<Guid, int> tally = new Dictionary<Guid, int>();
            ICollection<BusinessObjects.Exchange> result = new List<BusinessObjects.Exchange>();
            IDictionary<Guid, BusinessObjects.Exchange> exchanges = new ConcurrentDictionary<Guid, BusinessObjects.Exchange>();
            foreach (var trade in Trades)
            {
                BusinessObjects.Exchange exchange = trade.GetExchange(alldata);
                if (exchange?.Id == null)
                {
                    continue;
                }
                if (!exchanges.ContainsKey(exchange.Id.Value))
                {
                    exchanges.Add(exchange.Id.Value, exchange);
                }
                if (tally.ContainsKey(exchange.Id.Value))
                {
                    tally[exchange.Id.Value]++;
                }
                else
                {
                    tally.Add(new KeyValuePair<Guid, int>(exchange.Id.Value, 1));
                }
            }

            var itemlist = tally.ToList();
            itemlist.Sort((pair1, pair2) => pair2.Value.CompareTo(pair1.Value));

            foreach (var item in itemlist)
            {
                if (result.Count < count)
                {
                    BusinessObjects.Exchange exchange = exchanges[item.Key];
                    result.Add(exchange);
                    //count++;
                }
            }
            return result;
        }
    }
}