﻿using System;

namespace CoinTradePros.Web.Models
{
    public class StudentCourseModel
    {
        public CourseModel MyCourse { get; set; }

        public SectionModel CurrentSection { get; set; }

        public ChapterModel CurrentChapter { get; set; }

        public PageModel CurrentPage { get; set; }

        public Guid? Id { get; set; }

        public Guid? CourseInstanceId { get; set; }

        public DateTime? EnrollmentDate { get; set; }
        
        public Guid? TraderId { get; set; }

        public Guid? CurrentPageId { get; set; }

        public DateTime? LastAccessDate { get; set; }

        public string NextPageUrl { get; set; }

        public string PreviousPageUrl { get; set; }

        public string QuizUrl { get; set; }

    }
}