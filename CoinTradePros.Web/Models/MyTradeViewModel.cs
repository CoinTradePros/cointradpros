﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CoinTradePros.BusinessObjects;
using CoinTradePros.BusinessObjects.Calc;
using System.Globalization;

namespace CoinTradePros.Web.Models
{
    public class MyTradeViewModel
    {


        public class TraderViewCollectionClass
        {
            public TradeView tvc { get; set; }
            public string DateRange { get; set; }
        }



        public List<string> ExchangesList { get; set; }
        public List<string> ProTradersList { get; set; }
        public List<TraderViewCollectionClass> TvccList { get; set; }


        public List<TraderViewCollectionClass> abc(TradeViewCollection tvc)
        {
            
            List<TraderViewCollectionClass> listtvcc = new List<TraderViewCollectionClass>();


            foreach (TradeView item in tvc)
            {
                TraderViewCollectionClass tvcc = new TraderViewCollectionClass();
                tvcc.tvc = item;
                
                Double day = (DateTimeOffset.Now - item.EntryDate.Value).TotalDays;

                
                if(day < 7)
                {
                    tvcc.DateRange = "Last7";
                }
                else if(day < 30)
                {
                    tvcc.DateRange = "Last30";
                }
                else if(day < 365)
                {
                    tvcc.DateRange = "Last365";
                }
                else
                {
                    tvcc.DateRange = "Older";
                }
                listtvcc.Add(tvcc);

            }

            return listtvcc;
        }

        public List<string> SortExchanges(TradeViewCollection tvc)
        {
            var SortedList = from x in tvc
                    group x by x.ExchangeName into g
                    let count = g.Count()
                    orderby count descending select new { g.Key };

            List<string> TempList = new List<string>();
            foreach (var item in SortedList)
            {

                if(item.Key !=null)
                {
                    TempList.Add(item.Key.ToString());
                }   
            }
            return TempList;
        }

        public List<string> SortProTraders(TradeViewCollection tvc)
        {
            var SortedList = from x in tvc
                             group x by x.TraderUserName into g
                             let count = g.Count()
                             orderby count descending
                             select new { g.Key };

            List<string> TempList = new List<string>();
            foreach (var item in SortedList)
            {
                if (item.Key!=null)
                {
                    TempList.Add(item.Key.ToString());
                }
            }
            return TempList;
        }
    }
}