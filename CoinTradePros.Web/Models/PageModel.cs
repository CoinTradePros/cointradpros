﻿using System;
using System.Collections.Generic;
using CoinTradePros.BusinessObjects;

namespace CoinTradePros.Web.Models
{
    public class PageModel
    {
        public ChapterModel Parent { get; set; }

        public PageModel(ChapterModel parent)
        {
            Parent = parent;
        }

        public Guid? Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public Guid? CourseChapterId { get; set; }

        public short? PageNumber { get; set; }


        public void Fill(CoursePage page, ChapterModel parent)
        {
            Id = page.Id;
            Name = page.Name;
            Description = page.Description;
            CourseChapterId = page.CourseChapterId;
            PageNumber = page.PageNumber;
            Parent = parent;
        }

        public static ICollection<PageModel> Fill(ICollection<CoursePage> pages, ChapterModel parent)
        {
            ICollection<PageModel> coursegroup = new List<PageModel>();

            foreach (var p in pages)
            {
                PageModel m = new PageModel(parent);
                m.Fill(p, parent);
                coursegroup.Add(m);
            }
            return coursegroup;
        }
    }
}