﻿using CoinTradePros.BusinessObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CoinTradePros.Web.Models
{
    public class SubscriberAlertModel
    {

        //public Guid TraderId { get; set; }
        //public string EntryDate { get; set; }
        //public string EntryPrice { get; set; }
        //public string PositionSize { get; set; }
        //public string Comments { get; set; }
        //public Guid Exchange { get; set; }
        //public Guid Assets { get; set; }

        public TradeView TradeView { get; set; }

        public SendAlertModel SendAlertModel { get; set; }
        public ICollection<SelectListItem> TardeViewList { get; set; }
        public ICollection<SelectListItem> ExchangeList { get; set; }
        public ICollection<SelectListItem> AssetsList { get; set; }
        public ICollection<SelectListItem> DenominationList { get; set; }


        
        public HttpPostedFileBase ChartImage { get; set; }

    }
}