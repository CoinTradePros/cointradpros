﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoinTradePros.Web.Models
{
    public class ExamModel
    {
        public ExamModel()
        {
            Questions = new List<QuestionModel>();
        }

        public Guid? ExamId { get; set; }

        public Guid? ChapterId { get; set; }

        public Guid? EnrollmentId { get; set; }

        public SectionModel CurrentSection { get; set; }

        public ChapterModel CurrentChapter { get; set; }

        public QuestionModel CurrentQuestion { get; set; }

        public string NextQuestionUrl { get; set; }

        public string PreviousQuestionUrl { get; set; }

        public decimal MinPassingScore { get; set; }

        public decimal Score { get; set; }

        public ICollection<QuestionModel> Questions { get; set; }

        public QuestionModel GetNextQuestion()
        {
            foreach (var q in Questions)
            {
                if (CurrentQuestion == null)
                {
                    CurrentQuestion = q;
                    return q;
                }
                if (CurrentQuestion.QuestionOrder + 1 == q.QuestionOrder)
                {
                    return q;
                }
            }
            return CurrentQuestion;
        }

        public QuestionModel GetPreviousQuestion()
        {
            foreach (var q in Questions)
            {
                if (CurrentQuestion == null)
                {
                    CurrentQuestion = q;
                    return q;
                }
                if (CurrentQuestion.QuestionOrder - 1 == q.QuestionOrder)
                {
                    return q;
                }
            }
            return Questions.FirstOrDefault();
        }

        public void SetScore()
        {
            decimal total = new decimal(Questions.Count);
            decimal correct = new decimal(0);

            foreach (var q in Questions)
            {
                if (q.IsCorrect())
                {
                    correct++;
                }
            }

            decimal result = correct / total;
            Score = Math.Round(result, 2);
        }
    }
}