﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CoinTradePros.Web.Models
{
    public class MessagingOptionModel
    {
        // id, name, desc, imagepath

        [HiddenInput]
        public Guid Id { get; set; }

        [Display(Name = "Name")]
        public string Name { get; set; }

        public string Description { get; set; }

        public string ImagePath { get; set; }

        public string SysCode { get; set; }
    }
}