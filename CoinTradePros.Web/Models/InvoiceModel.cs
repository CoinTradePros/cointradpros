﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoinTradePros.Web.Models
{
    public class InvoiceModel
    {
        public Guid Id { get; set; }
        public DateTimeOffset DueDate { get; set; }
        public string Currency { get; set; }
        public Decimal Amount { get; set; }
        public int PaymentStatus { get; set; }

        public DateTimeOffset? PaymentDate { get; set; }

        public Guid SubscriptionId { get; set; }

        public string Status {
            get
            {
                if (PaymentStatus == 1)
                {
                    return "Paid";
                }
                else
                    return "Pending";
            }
        }

    }
}