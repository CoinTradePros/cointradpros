﻿using System;
using System.Collections.Generic;
using CoinTradePros.BusinessObjects;

namespace CoinTradePros.Web.Models
{
    public class SectionModel
    {
        public SectionModel(CourseModel parent)
        {
            Parent = parent;
            Chapters = new List<ChapterModel>();
        }

        public CourseModel Parent { get; set; }

        public Guid? Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public Guid? CourseOfferingId { get; set; }

        public short? SectionNumber { get; set; }

        public ICollection<ChapterModel> Chapters { get; set; }

        public void Fill(CourseSection section, CourseModel parent, bool fillAllData)
        {
            Id = section.Id;
            Name = section.Name;
            Description = section.Description;
            CourseOfferingId = section.CourseOfferingId;
            SectionNumber = section.SectionNumber;
            Parent = parent;

            if (!fillAllData) return;
            if (section.Id == null) return;

            ICollection<CourseChapter> chapters = CourseChapterCollection.GetSectionChapters(section.Id.Value);
            Chapters = ChapterModel.Fill(chapters, this, true);
        }

        public static ICollection<SectionModel> Fill(ICollection<CourseSection> sections, CourseModel parent, bool fillAllData)
        {
            ICollection<SectionModel> sectiongroup = new List<SectionModel>();

            foreach (var s in sections)
            {
                SectionModel m = new SectionModel(parent);
                m.Fill(s, parent, fillAllData);
                sectiongroup.Add(m);
            }
            return sectiongroup;
        }
    }
}