﻿namespace CoinTradePros.Web.Models
{
    public class CurrencyChangeModel
    {
        public string ProTraderUserName { get; set; }

        public string SelectedCurrency { get; set; }
    }
}