﻿using System;

namespace CoinTradePros.Web.Models
{
    public class FeatureTypeModel
    {
        public Guid FeatureTypeId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public bool RequiresProcessing { get; set; }

        public string FeatureProcessor { get; set; }

        public string DefaultIconClass { get; set; }
    
    }
}