﻿using System;
using System.Collections.Generic;

namespace CoinTradePros.Web.Models
{
    public class CoinPaymentApiModel :CoinPaymentIpnModelBase
    {
        /*
         * [0]: "ipn_version"
    [1]: "ipn_id"
    [2]: "ipn_mode"
    [3]: "merchant"
    [4]: "ipn_type"
    [5]: "txn_id"
    [6]: "status"
    [7]: "status_text"
    [8]: "currency1"
    [9]: "currency2"
    [10]: "amount1"
    [11]: "amount2"
    [12]: "fee"
    [13]: "buyer_name"
    [14]: "item_name"
    [15]: "item_number"
    [16]: "custom"
    [17]: "received_amount"
    [18]: "received_confirms"
         * 
         */

        /// <summary>
        /// The unique ID of the payment. Your IPN handler should be able to handle a txn_id composed 
        /// of any combination of a-z, A-Z, 0-9, and - up to 128 characters long for future proofing.
        /// </summary>
        public string Txn_id { get; set; }

        /// <summary>
        /// The original currency/coin submitted.	
        /// </summary>
        public string Currency1 { get; set; }

        /// <summary>
        /// The coin the buyer paid with.	
        /// </summary>
        public string Currency2 { get; set; }

        /// <summary>
        /// The amount of the payment in your original currency/coin.	
        /// </summary>
        public decimal Amount1 { get; set; }

        /// <summary>
        /// The amount of the payment in the buyer's coin.	
        /// </summary>
        public decimal Amount2 { get; set; }

        /// <summary>
        /// The fee on the payment in the buyer's selected coin.	
        /// </summary>
        public decimal Fee { get; set; }

        /// <summary>
        /// The name of the buyer.	
        /// </summary>
        public string Buyer_name { get; set; }

        /// <summary>
        /// The name of the item that was purchased.	
        /// </summary>
        public string Item_name { get; set; }

        /// <summary>
        /// This is a passthru variable for your own use.	
        /// </summary>
        public string Item_number { get; set; }

        /// <summary>
        /// This is a passthru variable for your own use.
        /// For CoinTradePros, this is the ID of the Invoice table.
        /// </summary>
        public string Invoice { get; set; }

        /// <summary>
        /// This is a passthru variable for your own use.	
        /// For CoinTradePros, this is the selected inteval price for a package, denominated in USD.
        /// TODO: Once the dust settles with the crypto payment processing code, check back here to make sure any other info in the custom field is documented.
        /// </summary>
        public string Custom { get; set; }

        /// <summary>
        /// The TX ID of the payment to the merchant. 
        /// Only included when 'status' >= 100 and if the 
        /// payment mode is set to ASAP or Nightly or if the
        ///  payment is PayPal Passthru.	
        /// </summary>
        public string Send_tx { get; set; }

        /// <summary>
        /// The amount of currency2 received at the time the IPN was generated.	
        /// </summary>
        public decimal Received_amount { get; set; }


        public string Status { get; set; }

        /// <summary>
        /// The number of confirms of 'received_amount' at the time the IPN was generated.	
        /// </summary>
        public int Received_confirms { get; set; }

        /// <summary>
        /// Not part of the regular post data, but used for the UI updates.
        /// </summary>
        public int TotalConfirmations { get; set; }

        /// <summary>
        /// Fills the model with the data from the IPN call.
        /// </summary>
        /// <param name="fields"></param>
        public override void Fill(IDictionary<string, string> fields)
        {
            throw new NotImplementedException();
        }

        public override bool IsValid()
        {
            throw new NotImplementedException();
        }

        public bool ProcessingComplete()
        {
            int s;
            if (int.TryParse(Status, out s))
            {
                return s >= 100;
            }
            return false;
        }
    }
}