﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace CoinTradePros.Web.Models
{
    public class SendAlertModel
    {
        public SendAlertModel()
        {
            Packages = new List<PackageModel>();
        }

        [Required]
        public Guid ProTraderProfileId { get; set; }

        [Required]
        [DataType(DataType.MultilineText)]
        [Display(Name = "Alert Message", Prompt = "What do you want to say?")]
        public string AlertBody { get; set; }

        [Required]
        public string AlertGroups { get; set; }

        [Required]
        public string AlertChannels { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string ImageData { get; set; }

        [Required]
        public bool IncludeChart { get; set; }

        public ICollection<PackageModel> Packages { get; set; }

        public ICollection<MessagingOptionModel> MessagingOptions { get; set; }
    }
}