﻿using System;
using CoinTradePros.BusinessObjects;

namespace CoinTradePros.Web.Models
{
    public class ProcessPaymentModel
    {
        public PackageModel ChosenPackage { get; set; }

        public Guid InvoiceId { get; set; }

        public int StripePaymentAmount { get; set; }

        public string StripePublicKey { get; set; }

        public bool UseAlternateCurrency { get; set; }

        public string HeaderImageUrl { get; set; }

        public string QrCodeUrl { get; set; }

        public int ConfirmationsNeeded { get; set; }

        public double Timeout { get; set; }

        public DateTimeOffset TimeLimit { get; set; }

        public string PaymentAddress { get; set; }

        public string AlternateCurrencyCode { get; set; }
        
        public decimal AlternateCurrencyAmount { get; set; }

        public string AlternateCurrencyName { get; set; }

        public string StatusUrl { get; set; }

        public decimal BaseUsdIntervalAmount { get; set; }

        public string StatusCheckUrl { get; set; }

        public int CheckInterval { get; set; }
    }
}