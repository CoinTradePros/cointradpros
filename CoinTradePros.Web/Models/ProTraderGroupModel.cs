﻿using System.Collections.Generic;

namespace CoinTradePros.Web.Models
{
    public class ProTraderGroupModel
    {
        public ProTraderGroupModel()
        {
            ProTraders = new List<ProTraderModel>();
        }

        public ICollection<ProTraderModel> ProTraders { get; set; }
    }
}