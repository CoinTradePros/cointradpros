﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace CoinTradePros.Web.Models
{
    public class AvatarUpdateModel
    {
        public Guid TraderId { get; set; }

        [DataType(DataType.Upload)]
        [Display(Name = "Avatar")]
        //[RegularExpression(@"(?i).*\.(gif|jpe?g|png|bmp)$", ErrorMessage = "Only image files are allowed")]
        public HttpPostedFileBase AvatarImage { get; set; }
    }
}