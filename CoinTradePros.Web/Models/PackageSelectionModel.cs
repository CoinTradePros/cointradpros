﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace CoinTradePros.Web.Models
{
    public class PackageSelectionModel
    {
        public string AlternateCurrency { get; set; }

        [Required]
        public decimal IntervalAmount { get; set; }

        [Required]
        [HiddenInput]
        public Guid PackageId { get; set; }
    }
}