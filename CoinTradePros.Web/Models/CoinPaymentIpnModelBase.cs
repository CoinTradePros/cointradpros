﻿using System.Collections.Generic;
using CoinTradePros.Interfaces;

namespace CoinTradePros.Web.Models
{
    /// <summary>
    /// Documentation for this class can be found here: https://www.coinpayments.net/merchant-tools-ipn#fields
    /// </summary>
    public abstract class CoinPaymentIpnModelBase
    {
        /*
         * Feilds for All IPN Types
         */
         /// <summary>
         /// 
         /// </summary>
        public string Ipn_version { get; set; }

        /// <summary>
        /// Currently: 'simple, 'button', 'cart', 'donation', 'deposit', or 'api'
        /// </summary>
        public string Ipn_mode { get; set; }

        /// <summary>
        /// Same to Ipn_mode, but wrapped in an enum.
        /// </summary>
        public IpnModeType IpnMode { get; set; }

        /// <summary>
        /// The unique identifier of this IPN
        /// </summary>
        public string Ipn_id { get; set; }

        /// <summary>
        /// Your merchant ID (you can find this on the My Account page).	
        /// </summary>
        public string Merchant { get; set; }

        public abstract void Fill(IDictionary<string, string> fields);

        public abstract bool IsValid();
    }
}