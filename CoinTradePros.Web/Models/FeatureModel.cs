﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;

namespace CoinTradePros.Web.Models
{
    public class FeatureModel
    {
        public FeatureModel()
        {
            FeatureTypes = new List<SelectListItem>();
        }

        [HiddenInput]
        public Guid? FeatureId { get; set; }

        [Required]
        [HiddenInput]
        public Guid? ProTraderProfileId { get; set; }

        [Required]
        [HiddenInput]
        [Display(Name = "Feature Type")]
        public Guid? FeatureTypeId { get; set; }

        [Required]
        [Display(Name = "Name")]
        [DataType(DataType.Text)]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Description")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [DataType(DataType.Upload)]
        [Display(Name = "Feature Icon")]
        //[RegularExpression(@"(?i).*\.(gif|jpe?g|png|bmp)$", ErrorMessage = "Only image files are allowed")]
        public HttpPostedFileBase FeatureIcon { get; set; }

        [Display(Name = "Sort Order")]
        [DataType(DataType.Text)]
        public byte? SortOrder { get; set; }

        public string FeatureIconUrl { get; set; }

        public ICollection<SelectListItem> FeatureTypes { get; set; }

        public FeatureTypeModel SelectedFeatureType { get; set; }

        public Guid? ProxyProTraderProfileId { get; set; }

        public bool IsProxyMode()
        {
            return ProxyProTraderProfileId.HasValue && ProxyProTraderProfileId != Guid.Empty;
        }

        public string GetIconClass()
        {
            return !string.IsNullOrEmpty(SelectedFeatureType?.DefaultIconClass) ? 
                SelectedFeatureType.DefaultIconClass : "fa fa-check-circle-o";
        }
    }
}