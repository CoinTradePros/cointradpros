﻿using System.Web;

namespace CoinTradePros.Session
{
    public static class SessionExtension
    {
        public static SessionContainer GetSessionContainer(this HttpContext current)
        {
            return (SessionContainer) current?.Session["__MySessionObject"];
        }
    }
}