﻿using System;
using System.Collections;

namespace CoinTradePros.Session
{
    public class SessionContainer
    {
        private readonly Hashtable _items;

        public SessionContainer()
        {
            _items = new Hashtable();
        }

        public void Push<T>(string key, T val)
        {
            _items.Add(key, val);
        }

        public T Pull<T>(string key)
        {
            return (T) _items[key];
        }
    }
}