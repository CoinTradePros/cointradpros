namespace CoinTradePros.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class valiglogin : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.QuestionAnswers",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ProTraderProfileId = c.Guid(),
                        Question = c.String(),
                        Answer = c.String(),
                        SortOrder = c.Byte(),
                        RowState = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.Subscriptions",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        TraderProfileId = c.Guid(),
                        PackageId = c.Guid(),
                        DiscountId = c.Guid(),
                        SubscriptionStart = c.DateTimeOffset(precision: 7),
                        SubscriptionEnd = c.DateTimeOffset(precision: 7),
                        IsCancelled = c.Boolean(),
                        PendingPayment = c.Boolean(),
                        PaymentFrequency = c.Int(),
                        PaymentAmount = c.Decimal(precision: 18, scale: 2),
                        PaymentCurrency = c.String(),
                        PaymentEmail = c.String(),
                        StripeSourceToken = c.String(),
                        StripeCustomerId = c.String(),
                        StripeSubscriptionId = c.String(),
                        StripeSinglePaymentId = c.String(),
                        RowState = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Trades",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        TraderId = c.Guid(),
                        DenominationId = c.Guid(),
                        ExchangeId = c.Guid(),
                        AssetId = c.Guid(),
                        ProTraderProfileId = c.Guid(),
                        EntryDate = c.DateTimeOffset(precision: 7),
                        ExitDate = c.DateTimeOffset(precision: 7),
                        EntryPrice = c.Decimal(precision: 18, scale: 2),
                        ExitPrice = c.Decimal(precision: 18, scale: 2),
                        PositionSize = c.Decimal(precision: 18, scale: 2),
                        ProfitLossAmount = c.Decimal(precision: 18, scale: 2),
                        ProfitLossPercent = c.Decimal(precision: 18, scale: 2),
                        Comments = c.String(),
                        AllowSocialSharing = c.Boolean(),
                        ChartImagePath = c.String(),
                        PostDate = c.Binary(),
                        ExchangeExternalIdentifier = c.String(),
                        RowState = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.Trades");
            DropTable("dbo.Subscriptions");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.QuestionAnswers");
        }
    }
}
