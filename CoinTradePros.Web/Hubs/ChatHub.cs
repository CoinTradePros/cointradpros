﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using System.Threading.Tasks;
using CoinTradePros.BusinessObjects;
using Microsoft.AspNet.Identity;
using CoinTradePros.Web.Models;
using Microsoft.AspNet.Identity.EntityFramework;

namespace CoinTradePros.Web.Hubs
{
    public class ChatHub : Hub
    {
        //public override Task OnConnected()
        //{
        //    string name;
        //    if (Context.User.Identity.IsAuthenticated)
        //    {
        //        name = Context.User.Identity.Name;
        //    }
        //    else
        //    {
        //        name = Context.Request.Cookies["ASP.NET_SessionId"].Value;
        //    }

        //    return base.OnConnected();
        //}

        public new void OnConnected()
       {

            //Connection id by SignalR
            string connectionid = Context.ConnectionId;

            //Get user by username
            string name = Context.User.Identity.Name;

            ////Delete if exsist
            //DeleteConnectionId(name);

            //Get user
            var x = AspNetUsers.GetUserByUserName(name);

            //Save connection id and user in DB
            var flag =ConnectionTable.AddConnectionId(x.Id,connectionid);
            //return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            //Delete Connection from DB
            string name = Context.User.Identity.Name;
            DeleteConnectionId(name);
            return base.OnDisconnected(stopCalled);

        }

        public void DeleteConnectionId(string name)
        {
            var user = AspNetUsers.GetUserByUserName(name);

            bool flag = ConnectionTable.CheckByUserId(user.Id);
            if (flag)
            {
                ConnectionTable.DeleteByUserId(user.Id);
            }
        }


        public override Task OnReconnected()
        {
            //Re-Connection logic
            return base.OnReconnected();
        }

        public bool Send(string tousername, string message)
        {
            //Get this user
            string name = Context.User.Identity.Name;
            var from = AspNetUsers.GetUserByUserName(name);
            var to = AspNetUsers.GetUserByUserName(tousername);


            ApplicationDbContext context = new ApplicationDbContext();
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            var s = UserManager.GetRoles(from.Id);
            Guid? Subscriptionid;
            if (s[0].ToString() == "ProTrader")
            {
                Subscriptionid = SubscriptionView.GetSubscriptionId(to.Id, from.Id);
            }
            else
            {
                Subscriptionid = SubscriptionView.GetSubscriptionId(from.Id, to.Id);
            }

            TraderProfile tp = TraderProfile.GetByUserId(to.Id);
            string tofullname = tp.FirstName + " " + tp.LastName;
            //Hardcoded flag for testing purpose
            ChatRecord cr;

            ConnectionTableCollection connectionlist = ConnectionTable.GetConnectionId(to.Id);



            if (connectionlist == null)
            {
                cr = ChatRecord.AddMessage(from.Id, to.Id, message, false, Subscriptionid);
            }
            else
            {
                cr = ChatRecord.AddMessage(from.Id, to.Id, message, true, Subscriptionid);

                DateTime date = cr.SenderDate ?? DateTime.Now;
                TimeSpan time = cr.SenderTime ?? DateTime.Now.TimeOfDay;

                DateTime tempdatetime = date.Add(time);
                string datestring = tempdatetime.ToString("G");
                TraderProfile temptp = TraderProfile.GetByUserId(from.Id);

                foreach (var connectionid in connectionlist)
                {
                    string clientconid = connectionid.ConnectionID.ToString();
                    Clients.Client(clientconid).appendmsg(message, "received", tofullname, from.UserName, temptp.PortraitPath, datestring);
                }
            }

            string myconnectionid = Context.ConnectionId;

            if (cr != null)
            {
                Clients.Client(myconnectionid).status(message, "sent", "You", to.UserName);
                return true;
            }
            else
            {
                Clients.Client(myconnectionid).status(message, "failed", "You", to.UserName);
                return false;
            }
        }



    }
}