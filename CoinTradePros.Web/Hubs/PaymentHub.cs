﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;

namespace CoinTradePros.Web.Hubs
{
    public class PaymentHub : Hub
    {
        private static readonly ConnectionMapping<string> Connections =
            new ConnectionMapping<string>();

        public override Task OnConnected()
        {
            string name;
            if (Context.User.Identity.IsAuthenticated)
            {
                name = Context.User.Identity.Name;
            }
            else
            {
                name = Context.Request.Cookies["ASP.NET_SessionId"].Value;
            }
            Connections.Add(name, Context.ConnectionId);
            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            string name = Context.User.Identity.Name;
            Connections.Remove(name, Context.ConnectionId);
            return base.OnDisconnected(stopCalled);
        }

        public override Task OnReconnected()
        {
            string name = Context.User.Identity.Name;
            if (!Connections.GetConnections(name).Contains(Context.ConnectionId))
            {
                Connections.Add(name, Context.ConnectionId);
            }
            return base.OnReconnected();
        }
    }
}