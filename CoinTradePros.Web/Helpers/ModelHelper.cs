﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using CoinTradePros.BusinessObjects;
using CoinTradePros.Helpers;
using CoinTradePros.Payments;
using CoinTradePros.Web.Models;
using Tiraggo.Interfaces;

namespace CoinTradePros.Web.Helpers
{
    public class ModelHelper
    {
        public static ICollection<SelectListItem> GetMessageTransmitterList(Guid? selectedId)
        {
            MessageTransmitterCollection mtc = new MessageTransmitterCollection();
            mtc.LoadAll();
            ICollection<SelectListItem> transmitters = new List<SelectListItem>();

            SelectListItem item = new SelectListItem();
            item.Value = string.Empty;
            item.Text = "-- Choose a Message Platform --";
            transmitters.Add(item);
            foreach (MessageTransmitter mt in mtc)
            {
                item = new SelectListItem();
                item.Text = mt.Name;
                item.Value = (mt.Id ?? Guid.Empty).ToString();
                item.Selected = (mt.Id == selectedId);
                transmitters.Add(item);
            }
            return transmitters;
        }

        public static ICollection<SelectListItem> GetAssetList(Guid? selectedId)
        {
            AssetCollection ac = AssetCollection.GetVolumeSortedAssets();
            ICollection<SelectListItem> assets = new List<SelectListItem>();
            SelectListItem item = new SelectListItem();
            item.Value = string.Empty;
            item.Text = "-- Choose an Asset --";
            assets.Add(item);
            foreach (Asset a in ac.OrderBy(x=>x.Name))
            {
                item = new SelectListItem();
                item.Text = a.Name;
                item.Value = (a.Id ?? Guid.Empty).ToString();
                item.Selected = (a.Id == selectedId);
                assets.Add(item);
            }
            return assets;
        }

        public static ICollection<SelectListItem> GetDenominationList(Guid? selectedId)
        {
            AssetCollection ac = new AssetCollection();
            AssetQuery aq = new AssetQuery();
            aq.Where(aq.CanDenominate == true);
            ac.Load(aq);
            ICollection<SelectListItem> assets = new List<SelectListItem>();
            SelectListItem item = new SelectListItem();
            item.Value = string.Empty;
            item.Text = "-- Choose a Denomination --";
            assets.Add(item);
            foreach (Asset a in ac)
            {
                item = new SelectListItem
                {
                    Text = a.Name,
                    Value = (a.Id ?? Guid.Empty).ToString(),
                    Selected = (a.Id == selectedId)
                };
                assets.Add(item);
            }
            return assets;
        }

        public static ICollection<SelectListItem> GetPaymentCoins(string selectedCode)
        {
            CoinPaymentsHelper helper = new CoinPaymentsHelper();
            ICollection<SelectListItem> items = new List<SelectListItem>();
            IDictionary<string, CoinPaymentsAsset> assets = helper.GetCoins();
            SelectListItem item = new SelectListItem();
            item.Value = string.Empty;
            item.Text = "-- Payment Currency --";
            items.Add(item);

            foreach (var asset in assets.Values)
            {
                if (!asset.IsFiat)
                {
                    item = new SelectListItem();
                    item.Value = asset.Code;
                    item.Text = $"{asset.Code} - {asset.Name} (BTC {asset.RateBtc:0.00000000})";
                    item.Selected = asset.Code == selectedCode;
                    items.Add(item);
                }
            }
            return items;
        }

        public static ICollection<SelectListItem> GetExchangeList(Guid? selectedId)
        {
            ExchangeCollection ec = ExchangeCollection.GetExchanges();
            ICollection<SelectListItem> exchanges = new List<SelectListItem>();

            SelectListItem item = new SelectListItem();
            item.Value = string.Empty;
            item.Text = "-- Choose an Exchange --";
            exchanges.Add(item);
            foreach (BusinessObjects.Exchange ex in ec)
            {
                item = new SelectListItem
                {
                    Text = ex.Name,
                    Value = (ex.Id ?? Guid.Empty).ToString(),
                    Selected = (ex.Id == selectedId)
                };
                exchanges.Add(item);
            }
            return exchanges;
        }

        public static ICollection<SelectListItem> GetFeatureTypeList(Guid? selectedId)
        {
            FeatureTypeCollection ac = new FeatureTypeCollection();
            ac.LoadAll();
            ICollection<SelectListItem> featureTypes = new List<SelectListItem>();

            SelectListItem item = new SelectListItem();
            item.Value = string.Empty;
            item.Text = "-- Choose a Feature Type --";
            featureTypes.Add(item);
            foreach (FeatureType ft in ac)
            {
                item = new SelectListItem();
                item.Text = ft.Name;
                item.Value = (ft.Id ?? Guid.Empty).ToString();
                item.Selected = (ft.Id == selectedId);
                featureTypes.Add(item);
            }
            return featureTypes;
        }

        public static ICollection<SelectListItem> GetSubscriptions(Guid userId, Guid? selectedId)
        {
            SubscriptionViewCollection sc = SubscriptionCollection.GetUserSubscriptions(userId);
            ICollection<SelectListItem> subscriptions = new List<SelectListItem>();

            SelectListItem item = new SelectListItem();
            item.Value = string.Empty;
            item.Text = "-- Choose a Coach --";
            subscriptions.Add(item);
            foreach (SubscriptionView sub in sc)
            {
                item = new SelectListItem
                {
                    Text = $"{sub.ProTraderFirstName} {sub.ProTraderLastName}",
                    Value = (sub.ProTraderProfileId ?? Guid.Empty).ToString(),
                    Selected = (sub.ProTraderProfileId.HasValue && 
                                sub.ProTraderProfileId.Value == selectedId)
                };
                subscriptions.Add(item);
            }
            return subscriptions;
        }

        public static ICollection<FeatureModel> GetBaseFeatureModels(Guid proTraderProfileId)
        {
            FeatureCollection items = new FeatureCollection();
            FeatureQuery fq = new FeatureQuery("f");
            fq.Select().Where(fq.ProTraderProfileId == proTraderProfileId);
            items.Load(fq);
            ICollection<FeatureModel> models = new List<FeatureModel>();
            ICollection<FeatureTypeModel> typeModels = new List<FeatureTypeModel>();
            FeatureTypeCollection featureTypes = new FeatureTypeCollection();
            featureTypes.LoadAll(tgSqlAccessType.DynamicSQL);
            foreach (var tp in featureTypes)
            {
                FeatureTypeModel model = new FeatureTypeModel();
                model.Description = tp.Description;
                model.DefaultIconClass = tp.DefaultIconClass;
                model.FeatureProcessor = tp.FeatureProcessor;
                model.FeatureTypeId = tp.Id ?? Guid.Empty;
                model.Name = tp.Name;
                model.RequiresProcessing = tp.RequiresProcessing ?? false;
                typeModels.Add(model);
            }

            foreach (var f in items)
            {
                FeatureModel model = new FeatureModel();
                model.Description = f.Description;
                model.Name = f.Name;
                model.FeatureIconUrl = f.IconPath;
                model.FeatureId = f.Id;
                model.FeatureTypeId = f.FeatureTypeId;
                model.SelectedFeatureType = typeModels.Single(x => x.FeatureTypeId == (f.FeatureTypeId ?? Guid.Empty));
                models.Add(model);
            }
            return models;
        }

        public static ICollection<SelectListItem> GetBaseFeatureSelectItems(Guid proTraderProfileId)
        {
            FeatureCollection items = new FeatureCollection();
            FeatureQuery fq = new FeatureQuery("f");
            fq.Select().Where(fq.ProTraderProfileId == proTraderProfileId);
            items.Load(fq);
            ICollection<SelectListItem> selectItems = new List<SelectListItem>();
            foreach (Feature f in items)
            {
                SelectListItem item = new SelectListItem();
                item.Value = f.Id?.ToString();
                item.Text = f.Name;
                selectItems.Add(item);
            }
            return selectItems;
        }

        public static ICollection<SelectListItem> GetFeatures(Guid packageId, Guid proTraderProfileId)
        {
            Package package = new Package();
            package.LoadByPrimaryKey(packageId);
            return GetFeatures(package, proTraderProfileId);
        }

        public static ICollection<SelectListItem> GetFeatures(Package package, Guid proTraderProfileId)
        {
            if (package == null)
            {
                throw new ArgumentNullException(nameof(package), "The Package object must not be null.");
            }
            ICollection<SelectListItem> items = new List<SelectListItem>();
            ICollection<Feature> features;
            if (package.Id.HasValue)
            {
                features = PackageFeature.GetPackageFeatures(package.Id.Value);
            }
            else
            {
                features = new List<Feature>();
            }
            ICollection<Feature> allFeatures = Feature.GetProTraderFeatures(proTraderProfileId);
            foreach (var feature in allFeatures)
            {
                if (feature.Id.HasValue)
                {
                    SelectListItem item = new SelectListItem();
                    item.Text = feature.Name;
                    item.Value = feature.Id.Value.ToString();
                    var value = features.Select(x => x.Id == feature.Id);
                    if (value.Any(x => x.Equals(true)))
                    {
                        item.Selected = true;
                    }
                    else
                    {
                        item.Selected = false;
                    }
                    items.Add(item);
                }
            }
            return items;
        }

        public static ICollection<QuestionAnswerModel> FillQuestions(Guid? proTraderProfileId)
        {
            if (!proTraderProfileId.HasValue)
            {
                throw new InvalidOperationException(nameof(proTraderProfileId));
            }
            QuestionAnswerCollection questionData = new QuestionAnswerCollection();
            QuestionAnswerQuery qaq = new QuestionAnswerQuery("qa");
            qaq.Select();
            qaq.Where(qaq.ProTraderProfileId == proTraderProfileId);
            questionData.Load(qaq);

            ICollection<QuestionAnswerModel> items = new List<QuestionAnswerModel>();
            foreach (var question in questionData)
            {
                if (question.ProTraderProfileId == null || question.Id == null) continue;
                var item = new QuestionAnswerModel
                {
                    ProTraderProfileId = question.ProTraderProfileId.Value,
                    QuestionId = question.Id.Value,
                    Answer = question.Answer,
                    Question = question.Question
                };
                items.Add(item);
            }
            return items;
        }

        public static PackageModel FillPackage (Package package)
        {
            PackageModel model = new PackageModel();
            model.LifetimePrice = package.LifetimePrice;
            model.AnnualPrice = package.AnnualPrice;
            model.CurrentSubscriberCount = package.GetCurrentSubscriberCount();
            model.Description = package.Description;
            model.MonthlyPrice = package.MonthlyPrice;
            model.PackageId = package.Id;
            model.Title = package.Title;
            model.ProTraderProfileId = package.ProTraderProfileId;
            model.IconPath = package.IconPath;

            model.SelectedFeatures = new List<FeatureModel>();
            model.FeatureItems = new List<SelectListItem>();
            ICollection<Feature> packageFeatures = package.GetFeatures();
            foreach (Feature pf in packageFeatures)
            {
                FeatureModel item = new FeatureModel();
                item.ProTraderProfileId = pf.ProTraderProfileId;
                item.Description = pf.Description;
                item.FeatureIconUrl = pf.IconPath;
                item.FeatureId = pf.Id;
                item.FeatureTypeId = pf.FeatureTypeId;
                item.Name = pf.Name;
                item.SortOrder = pf.SortOrder;
                model.SelectedFeatures.Add(item);
                SelectListItem listItem = new SelectListItem();
                if (pf.Id != null) listItem.Value = pf.Id.Value.ToString();
                listItem.Text = pf.Name;
                model.FeatureItems.Add(listItem);
            }

            if (package.ProTraderProfileId.HasValue)
            {
                model.AvailableFeatures = GetBaseFeatureModels(package.ProTraderProfileId.Value);
            }

            if (model.PackageId.HasValue && model.ProTraderProfileId.HasValue)
            {
                model.FeatureItems = GetFeatures(model.PackageId.Value, model.ProTraderProfileId.Value);
            }
            return model;
        }

        public static PackageModel FillPackage(Guid packageId)
        {
            Package package = new Package();
            package.LoadByPrimaryKey(packageId);
            return FillPackage(package);
        }

        public static ICollection<PackageModel> FillPackagesForUser(Guid userId)
        {
            var items = Package.GetProTraderPackagesByUserId(userId);
            return FillPackages(items);
        }

        public static ICollection<PackageModel> FillPackagesForProTrader(Guid proTraderProfileId)
        {
            var items = Package.GetProTraderPackages(proTraderProfileId);
            return FillPackages(items);
        }

        public static ICollection<PackageModel> FillPackages(ICollection<Package> packageData)
        {
            ICollection<PackageModel> modelItems = new List<PackageModel>();

            foreach (Package package in packageData)
            {
                PackageModel model = FillPackage(package);
                modelItems.Add(model);
            }
            return modelItems;
        }

        public static ICollection<ProfileDetailModel> FillProfileDetails(Guid traderId)
        {
            ProfileDetailCollection coll = ProfileDetailCollection.GetProfileDetails(traderId);
            ICollection<ProfileDetailModel> models = new List<ProfileDetailModel>();
            ProfileDetailTypeCollection typeColl = ProfileDetailTypeCollection.GetProfileDetailTypes();
            foreach (var detail in coll)
            {
                ProfileDetailModel model = new ProfileDetailModel();
                model.DetailId = detail.Id ?? Guid.Empty;
                model.DetailTypeId = detail.DetailTypeId ?? Guid.Empty;
                model.DetailUrl = detail.DetailUrl;
                model.DisplayText = detail.DisplayText;
                model.SalesPageDetail = detail.SalesPageDetail ?? true;
                ProfileDetailType detailType = typeColl.SingleOrDefault(x => x.Id == detail.DetailTypeId);
                if (detailType != null)
                {
                    model.IconName = detailType.IconName;
                }
                model.ShowUrl = detail.ShowUrl ?? true;
                model.TraderProfileId = detail.TraderProfileId ?? Guid.Empty;
                model.HideIcon = detail.HideIcon ?? false;
                model.ShowIconOnly = detail.ShowIconOnly ?? false;
                model.DetailTypes = FillProfileDetailTypes(detail.DetailTypeId);
                model.DetailType = model.DetailTypes.SingleOrDefault(x => x.Value == model.DetailTypeId.ToString());
                models.Add(model);                
            }
            return models;
        }

        public static ICollection<SelectListItem> FillProfileDetailTypes(Guid? selectedId)
        {
            ICollection<SelectListItem> items = new List<SelectListItem>();
            ProfileDetailTypeCollection coll = ProfileDetailTypeCollection.GetProfileDetailTypes();
            SelectListItem item = new SelectListItem();
            item.Value = string.Empty;
            item.Text = "-- Select a Detail Type --";
            items.Add(item);
            foreach (var tp in coll)
            {
                item = new SelectListItem();
                item.Value = tp.Id.ToString();
                item.Text = tp.Name;
                if (tp.Id == selectedId)
                {
                    item.Selected = true;
                }

                items.Add(item);
            }
            return items;
        }

        public static ICollection<MessagingOptionModel> FillMessagingOptions(Guid userId)
        {
            ICollection<MessagingOptionModel> options = new List<MessagingOptionModel>();
            MessageTransmitterCollection transmitters = MessageTransmitterCollection.GetExternalTransmitters();

            UserSettingCollection settings = new UserSettingCollection();
            IDictionary<string, string> returnSettings = settings.GetPlatformSettings(userId, PlatformType.Messaging.ToString());

            foreach (var transmitter in transmitters)
            {
                string key = $"{transmitter.SysCode}_Send";

                if (!returnSettings.ContainsKey(key))
                {
                    continue;
                }
                string setting = returnSettings[key];
                bool hasSetting;

                if (!bool.TryParse(setting, out hasSetting) || !hasSetting) continue;

                MessagingOptionModel model = new MessagingOptionModel();
                model.Description = transmitter.Description;
                model.Id = transmitter.Id ?? Guid.Empty;
                model.ImagePath = $"{transmitter.ImagePath}";
                model.Name = transmitter.Name;
                model.SysCode = transmitter.SysCode;
                
                options.Add(model);
            }
            return options;
        }
    }
}