﻿using System;
using System.Configuration;
using System.Security.Principal;
using CoinTradePros.BusinessObjects;
using Microsoft.AspNet.Identity;

namespace CoinTradePros.Web.Helpers
{
    public class IdentityHelper
    {

        // TODO: Cache the id information to avoid so many DB lookups.
        public static Guid GetCurrentUserId(IPrincipal principal, bool failIfNotFound)
        {
            string id = principal.Identity.GetUserId();
            Guid userId;
            if (!Guid.TryParse(id, out userId))
            {
                if (failIfNotFound)
                {
                    throw new InvalidOperationException("A current user was not found.");
                }
                userId = Guid.Empty;
            }
            return userId;
        }

        public static Guid GetCurrentUserId(IPrincipal principal)
        {
            Guid userId = GetCurrentUserId(principal, true);
            return userId;
        }

        public static Guid GetCurrentUserTraderId(IPrincipal principal, bool failIfNotFound)
        {
            Guid userId = GetCurrentUserId(principal, failIfNotFound);
            if (userId == Guid.Empty)
            {
                return Guid.Empty;
            }
            TraderProfile profile = TraderProfile.GetTraderProfileFromUserId(userId);
            return profile.Id ?? Guid.Empty;
        }

        public static Guid GetCurrentUserTraderId(IPrincipal principal)
        {
            return GetCurrentUserTraderId(principal, true);
        }

        public static Guid GetCurrentUserProTraderId(IPrincipal principal)
        {
            Guid userId = GetCurrentUserId(principal);
            ProTraderProfile profile = ProTraderProfile.GetFromUserId(userId);
            return profile.Id ?? Guid.Empty;
        }

        public static string GetAvatarUrl(IPrincipal principal)
        {
            return GetAvatarUrlFromUserId(principal.Identity.GetUserId());
        }

        public static string GetAvatarUrlFromUserId(string userId)
        {
            Guid id;
            if (Guid.TryParse(userId, out id))
            {
                var trader = TraderProfile.GetTraderProfileFromUserId(id);
                return GetTraderAvatarUrl(trader);
            }
            return GetBlankAvatarUrl();
        }

        public static string GetAvatarUrlFromTraderId(Guid traderId)
        {
            if (traderId == Guid.Empty)
            {
                return GetBlankAvatarUrl();
            }
            TraderProfile trader = new TraderProfile();
            trader.LoadByPrimaryKey(traderId);
            return GetTraderAvatarUrl(trader);
        }

        public static string GetTraderAvatarUrl(TraderProfile profile)
        {
            string avatarUrl = GetBlankAvatarUrl();
            if (!string.IsNullOrEmpty(profile?.PortraitPath))
            {
                avatarUrl = profile.PortraitPath;
            }
            return avatarUrl;
        }

        public static string GetBlankAvatarUrl()
        {
            string avatarUrl = "/Content/img/blank_avatar.png";
            return avatarUrl;
        }
    }
}