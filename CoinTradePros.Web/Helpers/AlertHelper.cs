﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CoinTradePros.Adapters;
using CoinTradePros.Adapters.Factory;
using CoinTradePros.BusinessObjects;
using CoinTradePros.Interfaces.Messaging;
using CoinTradePros.Messaging.Factory;

namespace CoinTradePros.Web.Helpers
{
    public class AlertHelper
    {
        public static async Task<IMessagingResult> SendTradeAlert(Trade trade, string packages, string channels, string moreInfoUrl)
        {
            if (trade?.Id == null)
            {
                throw new ArgumentException("The Trade or the Trade Id did not have a value.");
            }
            TradeView tradeView = TradeView.LoadByTradeId(trade.Id.Value);

            IProvideMessagingSource alertSource =
                MessagingSourceProviderFactory.GetInstance(tradeView, 
                MessagingHelper.GetTransmitterTypes(channels), MessagingType.TradeAlert, GetPackageIds(packages));

            IProcessMessages processor = MessageProcessorFactory.GetInstance();
            processor.AddMessageField("TradeLink", moreInfoUrl);

            var results = await processor.ProcessMessageAsync(alertSource.GetMessagingSource());
            return results;
        }

        public static ICollection<Guid> GetPackageIds(string packages)
        {
            if (string.IsNullOrEmpty(packages))
            {
                return new List<Guid>();
            }
            string[] parts = packages.Split(',');
            ICollection<Guid> packageIds = new List<Guid>();
            foreach (var p in parts)
            {
                Guid id;
                if (Guid.TryParse(p, out id))
                {
                    packageIds.Add(id);
                }
            }
            return packageIds;
        }
    }
}