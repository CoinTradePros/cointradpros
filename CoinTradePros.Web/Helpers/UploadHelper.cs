﻿using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

namespace CoinTradePros.Web.Helpers
{
    public class UploadHelper
    {
        private readonly string _rootPath;
        private readonly Guid _traderId;

        public UploadHelper(Guid traderId, string rootPath)
        {
            _rootPath = rootPath;
            _traderId = traderId;
        }
        public string GetUploadPath()
        {
            string path = $"{_rootPath}/Uploads/{_traderId}/";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            return path;
        }

        public string GetFinalItemRelativePath(HttpPostedFileBase content)
        {
            if (content == null)
            {
                return string.Empty;
            }
            string itempath = GetUploadPath();
            string finalpath = $"{itempath}{content.FileName}";
            Image image = CheckOrientation(content);
            image.Save(finalpath);
            return GetRelativeUploadPath(_traderId) + content.FileName;
        }

        public static string GetRelativeUploadPath(Guid traderId)
        {
            return $"/Uploads/{traderId}/";
        }

        private Image CheckOrientation(HttpPostedFileBase fileUpload)
        {
            byte[] imageData = new byte[fileUpload.ContentLength];
            fileUpload.InputStream.Read(imageData, 0, fileUpload.ContentLength);

            MemoryStream ms = new MemoryStream(imageData);
            Image originalImage = Image.FromStream(ms);
            bool hasProperty = originalImage.PropertyIdList.Any(x => x == 0x0112);
            if (hasProperty)
            {
                int rotationValue = originalImage.GetPropertyItem(0x0112).Value[0];
                switch (rotationValue)
                {
                    case 1: // landscape, do nothing
                        break;

                    case 8: // rotated 90 right
                        // de-rotate:
                        originalImage.RotateFlip(RotateFlipType.Rotate270FlipNone);
                        break;

                    case 3: // bottoms up
                        originalImage.RotateFlip(RotateFlipType.Rotate180FlipNone);
                        break;

                    case 6: // rotated 90 left
                        originalImage.RotateFlip(RotateFlipType.Rotate90FlipNone);
                        break;
                }
            }
            return originalImage;
        }

        public string SaveBase64Image(Guid objectId, string imageData)
        {
            //data:image/gif;base64,
            //this image is a single pixel (black)

            if (imageData.StartsWith("data:image/png;base64,"))
            {
                imageData = imageData.Split(',')[1];
            }

            byte[] bytes = Convert.FromBase64String(imageData);

            string fileName = $"{objectId}.png";
            using (MemoryStream ms = new MemoryStream(bytes))
            {
                var image = Image.FromStream(ms);
                image.Save($"{GetUploadPath()}{fileName}");
            }
            string relativePath = GetRelativeUploadPath(_traderId) + fileName;
            return relativePath;
        }

        public string SaveFromImagrUrl(Guid objectId, string imageUrl)
        {
            WebClient webClient = new WebClient();
            string fileName = $"{objectId}.png";

            webClient.DownloadFile(imageUrl, $"{GetUploadPath()}{fileName}");

            string relativePath = GetRelativeUploadPath(_traderId) + fileName;
            return relativePath;
        }
    }
}