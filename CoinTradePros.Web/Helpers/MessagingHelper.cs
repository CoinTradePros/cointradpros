﻿using System;
using System.Collections.Generic;
using System.Linq;
using CoinTradePros.BusinessObjects;
using CoinTradePros.Interfaces.Messaging;

namespace CoinTradePros.Web.Helpers
{
    public class MessagingHelper
    {
        /// <summary>
        /// Takes a comma delimited list of message transmitter ids and returns a list 
        /// </summary>
        /// <param name="transmitterTypes"></param>
        /// <returns></returns>
        public static ICollection<TransmitterType> GetTransmitterTypes(string transmitterTypes)
        {
            // TODO: LImit the TransmitterTypes for all to only the ones which the user has configured.

            MessageTransmitterCollection mt = new MessageTransmitterCollection();
            mt.LoadAll();
            ICollection<TransmitterType> returnTypes = new List<TransmitterType>();
            if (string.IsNullOrEmpty(transmitterTypes))
            {
                return new List<TransmitterType>();
            }

            TransmitterType[] ttypes = (TransmitterType[]) Enum.GetValues(typeof(TransmitterType));
            if (transmitterTypes.ToLower().Contains("all"))
            {
                foreach (var val in mt)
                {
                    foreach (var tt in ttypes)
                    {
                        if (val.SysCode == tt.ToString() && val.ShowExternal.HasValue && val.ShowExternal.Value)
                        {
                            returnTypes.Add(tt);
                        }
                    }
                }
            }
            else
            {
                string[] types = transmitterTypes.Split(',');
                foreach (var type in types)
                {
                    TransmitterType tp;
                    if (Enum.TryParse(type, out tp))
                    {
                        returnTypes.Add(tp);
                    }
                }
            }
            return returnTypes;
        }
    }
}