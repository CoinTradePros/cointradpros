﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using CoinTradePros.BusinessObjects;
using CoinTradePros.CoinCapClient;
using CoinTradePros.Web.Helpers;
using CoinTradePros.Web.Models;

namespace CoinTradePros.Web.Controllers
{

    public partial class HomeController : Controller
    {
        public virtual ActionResult Index()
        {
            //TelegramNotifier t = new TelegramNotifier();
            //await t.Send();
            //DiscordNotifier not = new DiscordNotifier();
            //var result = await not.SendAsync("This is a test.", 368449761117995008);

            return View();
        }

        public virtual ActionResult About()
        {
            ViewBag.Message = "We get great cryptocurrency traders instant revenue from their audience.";

            return View();
        }

        public virtual ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public virtual ActionResult Join()
        {
            ViewBag.Message = "Do some stuff here to let people join the site.";

            return View();
        }

        public virtual ActionResult ProPricing()
        {
            ViewBag.Message = "Pro Pricing page.";

            return View();
        }

        public virtual ActionResult Support()
        {
            ViewBag.Message = "Support page.";

            return View();
        }

        public virtual ActionResult Questions()
        {
            ViewBag.Message = "FAQ page.";

            return View();
        }

        [Authorize(Roles = "ProTrader")]
        public virtual ActionResult SendAlert()
        {
            //TradeModel model = GetTradeModel(null);
            //ViewBag.Title = "New Trade";
            //if (User.Identity.IsAuthenticated)
            //{
            //    var user = User.Identity;
            //    ViewBag.Name = user.Name;

            //}
            //return View(model);

            ////GET ALL EXCHANGES

            //ExchangeCollection ec = new ExchangeCollection();
            //ec = BusinessObjects.Exchange.GetAllExchanges();
            //List<ExchangesViewModel> evml = new List<ExchangesViewModel>();

            //foreach (var item in ec)
            //{
            //    ExchangesViewModel evm = new ExchangesViewModel();
            //    evm.ExchangeID = item.Id;
            //    evm.ExchangeName = item.Name;
            //    evml.Add(evm);
            //}
            ////evml have all exchanges

            ////GET ALL ASSETS
            //AssetCollection ac = new AssetCollection(); 

            SendAlertModel model = new SendAlertModel();
            Guid userId = IdentityHelper.GetCurrentUserId(User);
            model.ProTraderProfileId = IdentityHelper.GetCurrentUserProTraderId(User);
            model.Packages = ModelHelper.FillPackagesForUser(userId);
            model.MessagingOptions = ModelHelper.FillMessagingOptions(userId);


            SubscriberAlertModel sam = new SubscriberAlertModel();
            sam.AssetsList = ModelHelper.GetAssetList(null);
            sam.ExchangeList = ModelHelper.GetExchangeList(null);
            sam.DenominationList = ModelHelper.GetDenominationList(null);
            //sam.TradeViewCollection = tvc;
            sam.TardeViewList = GetTradeViewList(null);
            sam.SendAlertModel = model;

            return View(sam);
        }

        public ICollection<SelectListItem> GetTradeViewList(Guid? selectedId)
        {

            Guid traderid = IdentityHelper.GetCurrentUserTraderId(User);
            TradeViewCollection tvc = TradeView.LoadRecentByTraderId(traderid);

            ICollection<SelectListItem> trades = new List<SelectListItem>();

            SelectListItem item = new SelectListItem();

            item.Value = string.Empty;
            item.Text = "-- Choose a Recent Open Trade --";
            trades.Add(item);

            //foreach(var v in tvc.Take(5))
            //{
            //    item = new SelectListItem
            //    {
            //        Text = v.Comments,
            //        Value = (v.TradeId ?? Guid.Empty).ToString(),
            //        Selected = (v.TradeId == selectedId)
            //    };
            //    trades.Add(item);
            //}

            //for(int i=0; i<5; i++)
            //{
            //    item = new SelectListItem
            //    {
            //        Text = tvc[i].Comments,
            //        Value = (tvc[i].TradeId ?? Guid.Empty).ToString(),
            //        Selected = (tvc[i].TradeId == selectedId)
            //    };
            //    trades.Add(item);
            //}

            foreach (BusinessObjects.TradeView ex in tvc.Take(5))
            {
                item = new SelectListItem
                {
                    Text = ex.Comments,
                    Value = (ex.TradeId ?? Guid.Empty).ToString(),
                    Selected = (ex.TradeId == selectedId)
                };
                trades.Add(item);
            }
            return trades;
        }


        [HttpPost]
        public JsonResult GetOpenTradeData(Guid id)
        {
            TradeView tvc = TradeView.LoadByTradeId(id);

            string t = string.Format("{0:MM/dd/yyyy}", tvc.EntryDate);

            return Json(new
            {
                Username = tvc.TraderUserName,
                AssetId = tvc.AssetKey,
                AssetName = tvc.AssetName,
                ExchangeId = tvc.ExchangeId,
                ExchangeName = tvc.ExchangeName,
                EntryPrice = tvc.EntryPrice,
                EntryDate = t,
                PositionSize = tvc.PositionSize,
                Comments = tvc.Comments,
                TradeId = tvc.TradeId,
                ExitPrice = tvc.ExitPrice

            });
        }



        public ActionResult Spa()
        {
            return View();
        }
    }
}