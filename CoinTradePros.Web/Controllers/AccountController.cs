﻿
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using CoinTradePros.BusinessObjects;
using CoinTradePros.Interfaces.Messaging;
using CoinTradePros.Messaging.Core;
using CoinTradePros.Web.Helpers;
using CoinTradePros.Web.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Tiraggo.Interfaces;

namespace CoinTradePros.Web.Controllers
{
    [Authorize]
    public partial class AccountController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        private readonly ApplicationDbContext _context;
        public AccountController()
        {
            _context = new ApplicationDbContext();
        }
        
        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public virtual ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }
        //public LoginViewModel CompareLoginToDb(LoginViewModel loginModel)
        //{
        //    var userName = _context.Users.FirstOrDefault(c => c.UserName == loginModel.UserName);
        //    var password = _context.Users.FirstOrDefault(c => c.PasswordHash == loginModel.Password);
        //    ModelState model = new ModelState();
        //    if (userName == null && password == null)
        //    { 
        //        ModelState.AddModelError("", "User Name and Passwrod not Found");
        //        return loginModel;
        //    }
        //    else
        //    {
        //        if (userName != null && password == null)  
        //        {
        //                ModelState.AddModelError("Password", "Passwrod not Found");
        //                return loginModel;
        //        }
        //        ModelState.AddModelError("Username", "User Name not Found");
        //        return loginModel;
        //    }
        //}
        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            SignInStatus result = await SignInManager.PasswordSignInAsync(model.UserName, model.Password, model.RememberMe, shouldLockout: false);


            switch (result)
            {
                case SignInStatus.Success:  
                    {

                        // This checks for stranded subscriptions for people who were not logged in when making the purchase.
                        AspNetUsers user = AspNetUsers.GetUserByUserName(model.UserName);
                        Subscription subscription = Subscription.GetStrandedSubscriptionByEmail(user.Email);
                        if (subscription.Id != null)
                        {
                            // attach the subscription to the matching user.
                            Guid traderId = IdentityHelper.GetCurrentUserTraderId(User, false);
                            if (traderId != Guid.Empty)
                            {
                                subscription.TraderProfileId = traderId;

                                subscription.Save(tgSqlAccessType.DynamicSQL);
                            }
                        }
                        return RedirectToAction("Index", "Users");
                    }
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
                case SignInStatus.Failure:
                    {
                        //model = CompareLoginToDb(model);
                        ModelState.AddModelError("", "Invalid login attempt.");
                        return View(model);
                    }
                    //return HttpNotFound();
                    
                //return RedirectToAction("Login","Account");
                default:
                    ModelState.AddModelError("", "Invalid login attempt.");
                    return View(model);
            }
        }

        //
        // GET: /Account/VerifyCode
        [AllowAnonymous]
        public virtual async Task<ActionResult> VerifyCode(string provider, string returnUrl, bool rememberMe)
        {
            // Require that the user has already logged in via username/password or external login
            if (!await SignInManager.HasBeenVerifiedAsync())
            {
                return View("Error");
            }
            return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/VerifyCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> VerifyCode(VerifyCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // The following code protects for brute force attacks against the two factor codes. 
            // If a user enters incorrect codes for a specified amount of time then the user account 
            // will be locked out for a specified amount of time. 
            // You can configure the account lockout settings in IdentityConfig
            var result = await SignInManager.TwoFactorSignInAsync(model.Provider, model.Code, isPersistent: model.RememberMe, rememberBrowser: model.RememberBrowser);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(model.ReturnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid code.");
                    return View(model);
            }
        }

        
        //
        // GET: /Account/Register
        [AllowAnonymous]
        public virtual ActionResult Register()
        {
            ViewBag.Name = new SelectList(_context.Roles.Where(u => !u.Name.Contains("Admin"))
                                            .ToList(), "Name", "Name");
            if (HttpContext.Session["SubscriptionId"] != null)
            {
                Guid? subId = (Guid?)HttpContext.Session["SubscriptionId"];
                if (subId != null)
                {
                    Subscription subscription = new Subscription();
                    if (subscription.LoadByPrimaryKey(subId.Value))
                    {
                        ViewBag.SubEmail = subscription.PaymentEmail;
                    }
                }
            }
            return View();
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> Register(RegisterViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = new ApplicationUser { UserName = model.UserName, Email = model.Email };
                    var result = await UserManager.CreateAsync(user, model.Password);
                    if (result.Succeeded)
                    {
                        //await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);

                        // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                        // Send an email with this link
                        string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                        var callbackUrl = Url.Action("ConfirmEmail", "Account", new {userId = user.Id, code = code},
                            protocol: Request.Url.Scheme);
                        await UserManager.SendEmailAsync(user.Id, "Confirm your account",
                            $"Please confirm your account by clicking <a href=\"{callbackUrl}\">here</a>");
                        // Assign Role to user Here

                        IdentityResult roleResult = await UserManager.AddToRoleAsync(user.Id, model.SelectedRole);

                        var res = await UserManager.AddClaimAsync(user.Id,
                            new Claim(ClaimTypes.Role, model.SelectedRole));

                        // Create the Trader and ProTrader profiles
                        TraderProfile tp = new TraderProfile();
                        tp.Id = Guid.NewGuid();
                        tp.FirstName = model.FirstName;
                        tp.LastName = model.LastName;
                        tp.UserId = user.Id;
                        tp.Save(tgSqlAccessType.DynamicSQL);
                        MessageSettingsManager.AddReceiveSettings(tp.UserId, TransmitterType.Email, user.Email);
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                        if (model.SelectedRole == "ProTrader")
                        {
                            ProTraderProfile ptp = new ProTraderProfile();
                            ptp.Id = Guid.NewGuid();
                            ptp.TraderId = tp.Id;
                            ptp.Save(tgSqlAccessType.DynamicSQL);

                            // Create the default messaging subscriptions
                            MessageSettingsManager.AddBasicSendSetting(tp.UserId, TransmitterType.Email);
                            MessageSettingsManager.AddBasicSendSetting(tp.UserId, TransmitterType.Twilio);



                        }
                        Subscription subscription = new Subscription();
                        // Check for a subscription ID, in case that was done prior to registration
                        if (HttpContext.Session["SubscriptionId"] != null)
                        {
                            Guid? subscriptionId = (Guid?) HttpContext.Session["SubscriptionId"];
                            if (subscriptionId != null && subscription.LoadByPrimaryKey(subscriptionId.Value))
                            {
                                subscription.TraderProfileId = tp.Id;
                                subscription.Save(tgSqlAccessType.DynamicSQL);
                            }
                        }
                        else
                        {
                            // No SubscriptionId availalbe, check the email
                            SubscriptionQuery sq = new SubscriptionQuery("s");
                            sq.Select().Where(sq.PaymentEmail == model.Email);
                            if (subscription.Load(sq))
                            {
                                subscription.TraderProfileId = tp.Id;
                                subscription.Save(tgSqlAccessType.DynamicSQL);
                            }
                        }

                    }
                    else
                    {
                        ModelState.AddModelError("", "Registration Failed.");
                        return View(model);
                    }
                    ViewBag.Name = new SelectList(_context.Roles.Where(u => !u.Name.Contains("Admin"))
                        .ToList(), "Name", "Name");
                    AddErrors(result);
                }
                return RedirectToAction("Index", "Users");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
          
            // If we got this far, something failed, redisplay form
          
        }

        public List<String> CreateJsonErrorList(IdentityResult result)
        {
            List<string> errors = new List<string>();
            foreach (var error in result.Errors)
            {
                errors.Add(error);
            }
            return errors;
        }

        //
        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public virtual async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var result = await UserManager.ConfirmEmailAsync(userId, code);
            return View("ConfirmEmail");
        }

        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public virtual ActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)

        {

            if (ModelState.IsValid)
            {
                var user = new ApplicationUser
                {
                    UserName = model.Email,
                    Email = model.Email
                };
                var result = await UserManager.CreateAsync(user,
             model.Email);

                System.Net.Mail.MailMessage m = new
           System.Net.Mail.MailMessage(
                    new System.Net.Mail.MailAddress("myemail@gmail.com",
            "Reset Password"),
                    new System.Net.Mail.MailAddress(model.Email));
                m.Subject = "Reset Password";



                string code = user.Id;

    m.Body =string.Format("Dear {0}<BR/>Please reset your password by clicking the following link: < a href =\"{1}\" title =\"User Email Confirm\">{1}</a>", user.UserName,
           Url.Action("ResetPassword", "Account", new
           {
               userId = user.Id,
               code = code
           }, protocol: Request.Url.Scheme));


                m.IsBodyHtml = true;
                System.Net.Mail.SmtpClient smtp = new
              System.Net.Mail.SmtpClient("smtp.gmail.com", 587);

                smtp.Credentials = new
              System.Net.NetworkCredential("cointradepro12@gmail.com",
               "asimgoheer0700");
                smtp.EnableSsl = true;
                smtp.Send(m);




                return RedirectToAction("ForgotPasswordConfirmation",
          "Account", new { Email = user.Email });

            }


            //    var user = await UserManager.FindByEmailAsync(model.Email);
            //    if (user == null || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
            //    {
            //        // Don't reveal that the user does not exist or is not confirmed
            //        return View("ForgotPasswordConfirmation");
            //    }

            //    var code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
            //    var callbackUrl = Url.Action("ResetPassword", "Account",
            //new { UserId = user.Id, code = code }, protocol: Request.Url.Scheme);
            //    await UserManager.SendEmailAsync(user.Id, "Reset Password",
            //"Please reset your password by clicking here: <a href=\"" + callbackUrl + "\">link</a>");
            //    return View("ForgotPasswordConfirmation");

            return View(model);
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public virtual ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public virtual ActionResult ResetPassword(string code)
        {
            return code == null ? View("Error") : View();
            
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            
           
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await UserManager.FindByEmailAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            var code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
            var result= await UserManager.ResetPasswordAsync(user.Id,code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            AddErrors(result);
            return View();
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public virtual ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public virtual ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/SendCode
        [AllowAnonymous]
        public virtual async Task<ActionResult> SendCode(string returnUrl, bool rememberMe)
        {
            var userId = await SignInManager.GetVerifiedUserIdAsync();
            if (userId == null)
            {
                return View("Error");
            }
            var userFactors = await UserManager.GetValidTwoFactorProvidersAsync(userId);
            var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();
            return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/SendCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> SendCode(SendCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            // Generate the token and send it
            if (!await SignInManager.SendTwoFactorCodeAsync(model.SelectedProvider))
            {
                return View("Error");
            }
            return RedirectToAction("VerifyCode", new { Provider = model.SelectedProvider, ReturnUrl = model.ReturnUrl, RememberMe = model.RememberMe });
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public virtual async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var result = await SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = false });
                case SignInStatus.Failure:
                default:
                    // If the user does not have an account, then prompt the user to create an account
                    ViewBag.ReturnUrl = returnUrl;
                    ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                    return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Email });
            }
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Manage");
            }

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        //
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public virtual ActionResult ExternalLoginFailure()
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }

      
   

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Json(new { result = "Redirect", url = returnUrl });
            }
            return Json(new { result = "Redirect", url = Url.Action("Index", "Users") });
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }

        [AllowAnonymous]
        public JsonResult IsEmailExist(string email)
        {
            var user = AspNetUsers.GetUserByEmail(email);
            //var test = _db.Users.Any(x => x.Email == email);
            bool isExist = true;
            if (user.Email == null)
            {
                isExist = true;
            }
            else
            {
                isExist = false;
            }
            return Json(isExist, JsonRequestBehavior.AllowGet);
        }
        [AllowAnonymous]
        public JsonResult IsUsernameExist(string username)
        {
            var user = AspNetUsers.GetUserByUserName(username);
            //var test = _db.Users.Any(x => x.Email == email);
            bool isExist = true;
            if (user.UserName == null)
            {
                isExist = true;
            }
            else
            {
                isExist = false;
            }
            return Json(isExist, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}