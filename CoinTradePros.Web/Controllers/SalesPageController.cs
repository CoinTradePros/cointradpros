﻿using System;
using System.Web;
using System.Web.Mvc;
using CoinTradePros.BusinessObjects;
using CoinTradePros.Web.Helpers;
using CoinTradePros.Web.Models;
using Microsoft.AspNet.Identity;
using Tiraggo.Interfaces;

namespace CoinTradePros.Web.Controllers
{
    public partial class SalesPageController : Controller
    {
        [Authorize(Roles = "Admin")]
        //public ActionResult EditSalesPage(Guid traderProfileId)
        //{
        //    //Only an admin can edit the sales page of another pro trader.
        //    SalesPageSettingsModel model = GetSettingsModel(traderProfileId);
        //    return View(model);
        //}

        private SalesPageSettingsModel GetSettingsModel(Guid proTraderProfileId)
        {
            SalesPageSettingsModel model = new SalesPageSettingsModel();
            ProTraderProfile profile = new ProTraderProfile();
            profile.LoadByPrimaryKey(proTraderProfileId);
            AspNetUsers user = TraderProfile.GetUserFromTraderId(profile.TraderId ?? Guid.Empty);
            model.ProTraderProfileId = proTraderProfileId;
            model.Description = profile.Description;
            model.Tagline = profile.TagLine;
            model.ProTraderUserName = user.UserName;
            model.VideoEmbedCode = HttpUtility.HtmlDecode(profile.VideoEmbed);
            model.PortraitPath = profile.PortraitPath;
            model.Questions = ModelHelper.FillQuestions(proTraderProfileId);
            model.Packages = ModelHelper.FillPackagesForProTrader(proTraderProfileId);
            model.ProFileDetails = ModelHelper.FillProfileDetails(profile.TraderId ?? Guid.Empty);
            return model;
        }

        [Authorize(Roles = "Admin, ProTrader")]
        public virtual ActionResult Index(Guid? proTraderProfileId)
        {
            bool isProxy = proTraderProfileId != null;
            //if (!User.IsInRole("Admin") && proTraderProfileId.HasValue)
            //{
            //    proTraderProfileId = IdentityHelper.GetCurrentUserProTraderId(User);
            //}
            if (proTraderProfileId == null)
            {
                proTraderProfileId = IdentityHelper.GetCurrentUserProTraderId(User);
            }
            SalesPageSettingsModel model = GetSettingsModel(proTraderProfileId.Value);
            if (User.IsInRole("Admin") && isProxy)
            {
                ProTraderProfile profile = new ProTraderProfile();
                profile.LoadByPrimaryKey(proTraderProfileId.Value);
                model.ProxyProTraderId = proTraderProfileId.Value;
                model.ProxyTraderId = profile.TraderId;
            }
            if (User.Identity.IsAuthenticated)
            {
                var user = User.Identity;
                ViewBag.Name = user.Name;

            }
            return View(model);
        }

        public virtual ActionResult Stats()
        {
            if (User.Identity.IsAuthenticated)
            {
                var user = User.Identity;
                ViewBag.Name = user.Name;

            }
            return View();
        }

        public virtual ActionResult Promote()
        {
            if (User.Identity.IsAuthenticated)
            {
                var user = User.Identity;
                ViewBag.Name = user.Name;

            }
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "ProTrader, Admin")]
        public virtual ActionResult Index(SalesPageSettingsModel model)
        {
            ProTraderProfile profile = new ProTraderProfile();
            bool isProxy = model.ProxyProTraderId.HasValue && model.ProxyProTraderId != Guid.Empty;
            Guid proxyId = Guid.Empty;
            if (isProxy)
            {
                profile.LoadByPrimaryKey(model.ProxyProTraderId.Value);
                proxyId = model.ProxyProTraderId.Value;
            }
            else
            {
                profile.LoadByPrimaryKey(model.ProTraderProfileId);
            }
            if (ModelState.IsValid)
            {
                SaveSalesPageSettings(profile, model);
            }
            model = GetSettingsModel(profile.Id ?? Guid.Empty);
            if (isProxy)
            {
                model.ProxyProTraderId = proxyId;
            }
            return View(model);
        }

        public void SaveSalesPageSettings(ProTraderProfile profile, SalesPageSettingsModel model)
        {
            bool isProxy = model.ProxyProTraderId.HasValue && model.ProxyProTraderId != Guid.Empty;

            Guid traderId;
            if (isProxy)
            {
                if (profile == null)
                {
                    profile = new ProTraderProfile();
                    profile.LoadByPrimaryKey(model.ProxyProTraderId ?? Guid.Empty);
                }
                traderId = profile.TraderId ?? Guid.Empty;
            }
            else
            {
                if (profile != null)
                {
                    traderId = profile.TraderId ?? Guid.Empty;
                }
                else
                {
                    traderId = IdentityHelper.GetCurrentUserTraderId(User);
                    profile = ProTraderProfile.GetFromTraderId(traderId);
                }
            }
            UploadHelper uploadHelper = new UploadHelper(traderId, Server.MapPath("~/"));

            profile.PortraitPath = uploadHelper.GetFinalItemRelativePath(model.ProTraderPortrait);
            profile.Description = model.Description;
            profile.TagLine = model.Tagline;
            profile.TraderId = traderId;
            profile.VideoEmbed = HttpUtility.HtmlEncode(model.VideoEmbedCode);
            if (profile.Id == null)
            {
                profile.Id = Guid.NewGuid();
            }
            profile.Save(tgSqlAccessType.DynamicSQL);
        }

        [Authorize(Roles = "Admin, ProTrader, Trader")]
        public virtual ActionResult CreateDetail(Guid? traderProfileId)
        {
            ProfileDetailModel model = new ProfileDetailModel();
            if (traderProfileId.HasValue && traderProfileId != Guid.Empty)
            {
                model.TraderProfileId = traderProfileId.Value;
                model.ProxyTraderProfileId = traderProfileId.Value;
            }
            else
            {
                model.TraderProfileId = IdentityHelper.GetCurrentUserTraderId(User);

            }
            model.DetailTypes = ModelHelper.FillProfileDetailTypes(null);
            return View(model);
        }

        [Authorize(Roles = "Admin, ProTrader")]
        public virtual ActionResult CreateFaq(Guid? proTraderProfileId)
        {
            QuestionAnswerModel model = new QuestionAnswerModel();
            if (proTraderProfileId.HasValue)
            {
                model.ProTraderProfileId = proTraderProfileId.Value;
                model.ProxyProTraderProfileId = proTraderProfileId.Value;
            }
            else
            {
                model.ProTraderProfileId = IdentityHelper.GetCurrentUserProTraderId(User);
            }
            if (model.ProTraderProfileId == Guid.Empty)
            {
                string userId = User.Identity.GetUserId();
                if (User.IsInRole("ProTrader"))
                {
                    throw new InvalidOperationException(
                        $"The user with key '{userId}' is in the ProTrader role, but is missing a ProTraderProfile record.");
                }
                throw new InvalidOperationException($"The user with key '{userId}' is does not have the ProTrader role or a ProTraderProfile record.");

            }
            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = "ProTrader, Admin")]
        public virtual ActionResult CreateFaq(QuestionAnswerModel model)
        {
            if (ModelState.IsValid)
            {
                QuestionAnswer question = new QuestionAnswer();
                if (model.ProxyProTraderProfileId.HasValue)
                {
                    question.ProTraderProfileId = model.ProxyProTraderProfileId;
                }
                else
                {
                    question.ProTraderProfileId = model.ProTraderProfileId;
                }

                question.Answer = model.Answer;
                question.Question = model.Question;
                question.Id = Guid.NewGuid();
                question.Save(tgSqlAccessType.DynamicSQL);
                if (model.IsProxyMode())
                {
                    return RedirectToAction("Index", "SalesPage",
                        new { proTraderProfileId = model.ProxyProTraderProfileId });
                }
                return RedirectToAction("Index", "SalesPage");
            }
            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = "Trader, ProTrader, Admin")]
        public virtual ActionResult CreateDetail(ProfileDetailModel model)
        {
            if (ModelState.IsValid)
            {
                ProfileDetail detail = new ProfileDetail();
                if (model.ProxyTraderProfileId.HasValue)
                {
                    detail.TraderProfileId = model.ProxyTraderProfileId;
                }
                else
                {
                    detail.TraderProfileId = model.TraderProfileId;
                }
                detail.DetailTypeId = model.DetailTypeId;
                detail.DetailUrl = model.DetailUrl;
                detail.DisplayText = model.DisplayText;
                detail.HideIcon = model.HideIcon;
                detail.SortOrder = model.SortOrder;
                detail.ShowIconOnly = model.ShowIconOnly;
                detail.Id = Guid.NewGuid();
                detail.SalesPageDetail = model.SalesPageDetail;
                detail.ShowUrl = model.ShowUrl;
                detail.Save(tgSqlAccessType.DynamicSQL);

                if (model.IsProxyMode() && model.ProxyTraderProfileId.HasValue)
                {
                    ProTraderProfile profile = ProTraderProfile.GetFromTraderId(model.ProxyTraderProfileId.Value);

                    return RedirectToAction("Index", "SalesPage", new { proTraderProfileId = profile.Id });
                }
                return RedirectToAction("Index", "SalesPage");
            }
            model.DetailTypes = ModelHelper.FillProfileDetailTypes(model.DetailTypeId);
            return View(model);
        }

        [Authorize(Roles = "ProTrader, Admin")]
        public virtual ActionResult EditFaq(Guid questionId, Guid? proTraderProfileId)
        {
            QuestionAnswer question = new QuestionAnswer();
            QuestionAnswerModel model = new QuestionAnswerModel();
            question.LoadByPrimaryKey(questionId);

            if (proTraderProfileId.HasValue)
            {
                model.ProTraderProfileId = proTraderProfileId.Value;
                model.ProxyProTraderProfileId = proTraderProfileId;
            }
            else
            {
                model.ProTraderProfileId =
                    question.ProTraderProfileId ??
                    IdentityHelper.GetCurrentUserProTraderId(User);
            }

            model.Answer = question.Answer;
            model.Question = question.Question;
            model.QuestionId = question.Id ?? Guid.Empty;

            return View(model);
        }

        [Authorize(Roles = "Trader, ProTrader, Admin")]
        public virtual ActionResult EditDetail(Guid detailId, Guid? traderProfileId)
        {
            ProfileDetail detail = new ProfileDetail();
            ProfileDetailModel model = new ProfileDetailModel();
            if (detail.LoadByPrimaryKey(detailId))
            {
                model.DetailTypeId = detail.DetailTypeId ?? Guid.Empty;
                model.DetailUrl = detail.DetailUrl;
                model.DisplayText = detail.DisplayText;
                model.SalesPageDetail = detail.SalesPageDetail ?? true;
                model.ShowUrl = detail.ShowUrl ?? true;
                if (traderProfileId.HasValue)
                {
                    model.ProxyTraderProfileId = traderProfileId;
                    model.TraderProfileId = traderProfileId.Value;
                }
                else
                {
                    model.TraderProfileId = detail.TraderProfileId ?? Guid.NewGuid();
                }
                model.DetailId = detail.Id ?? Guid.Empty;
                model.SortOrder = detail.SortOrder ?? 0;
                model.HideIcon = detail.HideIcon ?? false;
                model.ShowIconOnly = detail.ShowIconOnly ?? false;
                model.DetailTypes = ModelHelper.FillProfileDetailTypes(detail.DetailTypeId ?? Guid.Empty);
                if (model.IsProxyMode() && model.ProxyTraderProfileId.HasValue)
                {
                    ProTraderProfile profile = ProTraderProfile.GetFromTraderId(model.ProxyTraderProfileId.Value);
                    return RedirectToAction("Index", "SalesPage", new { proTraderProfileId = profile.Id });
                }
            }
            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = "Trader, ProTrader, Admin")]
        public virtual ActionResult EditDetail(ProfileDetailModel model)
        {
            if (ModelState.IsValid)
            {
                ProfileDetail detail = new ProfileDetail();
                detail.LoadByPrimaryKey(model.DetailId);
                detail.DetailTypeId = model.DetailTypeId;
                detail.DetailUrl = model.DetailUrl;
                detail.DisplayText = model.DisplayText;
                detail.Id = model.DetailId;
                detail.SalesPageDetail = true;
                detail.HideIcon = model.HideIcon;
                detail.ShowIconOnly = model.ShowIconOnly;
                detail.SortOrder = model.SortOrder;
                detail.ShowUrl = model.ShowUrl;
                if (model.ProxyTraderProfileId.HasValue)
                {
                    detail.TraderProfileId = model.ProxyTraderProfileId;
                }
                else
                {
                    detail.TraderProfileId = model.TraderProfileId;
                }
                detail.Save(tgSqlAccessType.DynamicSQL);
                return RedirectToAction("Index");
            }
            model.DetailTypes = ModelHelper.FillProfileDetailTypes(model.DetailTypeId);
            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = "ProTrader, Admin")]
        public virtual ActionResult EditFaq(QuestionAnswerModel model)
        {
            if (ModelState.IsValid)
            {
                QuestionAnswer question = new QuestionAnswer();
                question.LoadByPrimaryKey(model.QuestionId);
                if (model.ProxyProTraderProfileId.HasValue)
                {
                    question.ProTraderProfileId = model.ProxyProTraderProfileId;
                }
                else
                {
                    question.ProTraderProfileId = model.ProTraderProfileId;
                }

                question.Answer = model.Answer;
                question.Question = model.Question;
                question.Save(tgSqlAccessType.DynamicSQL);
                return RedirectToAction("Index");
            }
            return View(model);
        }

        [Authorize(Roles = "Trader, ProTrader, Admin")]
        public virtual ActionResult DeleteDetail(Guid detailId)
        {
            ProfileDetail detail = new ProfileDetail();
            if (detail.LoadByPrimaryKey(detailId))
            {
                detail.MarkAsDeleted();
                detail.Save(tgSqlAccessType.DynamicSQL);
            }
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "ProTrader, Admin")]
        public virtual ActionResult DeleteFaq(Guid questionId)
        {
            QuestionAnswer question = new QuestionAnswer();
            question.LoadByPrimaryKey(questionId);
            question.MarkAsDeleted();
            question.Save(tgSqlAccessType.DynamicSQL);

            return RedirectToAction("Index");
        }
    }
}