﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using CoinTradePros.BusinessObjects;
using CoinTradePros.Web.Helpers;
using CoinTradePros.Web.Models;
using Tiraggo.Interfaces;

namespace CoinTradePros.Web.Controllers
{
    [Authorize]
    public partial class CourseController : Controller
    {
        public virtual ActionResult Index()
        {
            EnrollmentModel model = new EnrollmentModel();
            Guid traderId = IdentityHelper.GetCurrentUserTraderId(User);
            var myCourses = CourseViewCollection.GetStudentCourses(traderId);
            model.MyEnrollments = CourseModel.Fill(myCourses, false);

            if (User.IsInRole("ProTrader"))
            {
                Guid proTraderId = IdentityHelper.GetCurrentUserProTraderId(User);
                var studentCourses = CourseViewCollection.GetCoachesCourses(proTraderId);
                model.StudentEnrollments = CourseModel.Fill(studentCourses, false);
            }
            if (User.Identity.IsAuthenticated)
            {
                var user = User.Identity;
                ViewBag.Name = user.Name;

            }
            return View(model);
        }

        public virtual ActionResult CourseSidebar(Guid? enrollmentId, Guid? pageId)
        {
            if (enrollmentId == null)
            {
                return new RedirectResult(Url.Action("Index", "Course"));
            }
            CourseEnrollment enrollment = new CourseEnrollment();
            StudentCourseModel model = new StudentCourseModel();
            CourseModel courseModel = new CourseModel();

            if (!enrollment.LoadByPrimaryKey(enrollmentId.Value))
            {
                throw new InvalidOperationException($"Unable to load an Enrollment with {enrollmentId}");
            }
            CourseView courseView = new CourseView();
            if (enrollment.CourseInstanceId != null)
            {
                CourseViewQuery cvq = new CourseViewQuery("cv");
                cvq.Select().Where(cvq.InstanceId == enrollment.CourseInstanceId);
                courseView.Load(cvq);
                courseModel.Fill(courseView, true);
            }
            if (pageId != null)
            {
                enrollment.CurrentPageId = pageId;
                enrollment.Save(tgSqlAccessType.DynamicSQL);
            }

            model.EnrollmentDate = enrollment.EnrollmentDate;
            model.CourseInstanceId = courseView.InstanceId;
            model.Id = enrollmentId;
            model.TraderId = enrollment.TraderId;
            model.MyCourse = courseModel;
            if (enrollment.CurrentPageId == null) return View(model);
            // load the section, chapter, and page the user was on.
            CoursePage currentPage = new CoursePage();
            CourseChapter currentChapter = new CourseChapter();
            CourseSection currentSection = new CourseSection();
            if (!currentPage.LoadByPrimaryKey(enrollment.CurrentPageId.Value))
            {
                throw new InvalidOperationException(
                    $"The current page id of {enrollment.CurrentPageId} does not match a coursepage record.");
            }
            if (currentPage.CourseChapterId != null &&
                !currentChapter.LoadByPrimaryKey(currentPage.CourseChapterId.Value))
            {
                throw new InvalidOperationException(
                    $"The current page id of {currentPage.CourseChapterId} does not match a coursechapter record.");
            }
            if (currentChapter.CourseSectionId != null &&
                !currentSection.LoadByPrimaryKey(currentChapter.CourseSectionId.Value))
            {
                throw new InvalidOperationException(
                    $"The current page id of {currentChapter.CourseSectionId} does not match a coursesection record.");
            }

            if (currentChapter.Id != null)
            {
                CourseChapterExam exam = CourseChapterExam.GetExam(currentChapter.Id.Value);
                model.QuizUrl = Url.Action("Examinator3000", "Course",
                    new { enrollmentId = enrollment.Id, courseExamId = exam.Id });
            }

            SectionModel sectionModel = new SectionModel(courseModel);
            sectionModel.Fill(currentSection, courseModel, false);

            ChapterModel chapterModel = new ChapterModel(sectionModel);
            chapterModel.Fill(currentChapter, sectionModel, true);

            PageModel pageModel = new PageModel(chapterModel);
            pageModel.Fill(currentPage, chapterModel);
            PageModel nextPage = chapterModel.GetNextPage(pageModel);
            PageModel prevPage = chapterModel.GetPreviousPage(pageModel);

            model.CurrentSection = sectionModel;
            model.CurrentChapter = chapterModel;
            model.CurrentPage = pageModel;
            Session["StudentCourseMode"] = model;
          
            if (prevPage.PageNumber == currentPage.PageNumber)
            {
                model.PreviousPageUrl = string.Empty;
            }
            else
            {
                string prevUrl = Url.Action("Run", "Course",
                    new { enrollmentId = enrollmentId, pageId = prevPage.Id });
                model.PreviousPageUrl = prevUrl;
            }

            if (nextPage.PageNumber == currentPage.PageNumber)
            {
                model.NextPageUrl = string.Empty;
            }
            else
            {
                string nextUrl = Url.Action("Run", "Course",
                    new { enrollmentId = enrollmentId, pageId = nextPage.Id });
                model.NextPageUrl = nextUrl;
            }
            return View(model);
        }
        public virtual ActionResult Run(Guid? enrollmentId, Guid? pageId)
        {
          
            if (enrollmentId == null)
            {
                return new RedirectResult(Url.Action("Index", "Course"));
            }
            CourseEnrollment enrollment = new CourseEnrollment();
            StudentCourseModel model = new StudentCourseModel();
            CourseModel courseModel = new CourseModel();

            if (!enrollment.LoadByPrimaryKey(enrollmentId.Value))
            {
                throw new InvalidOperationException($"Unable to load an Enrollment with {enrollmentId}");
            }
            CourseView courseView = new CourseView();
            if (enrollment.CourseInstanceId != null)
            {
                CourseViewQuery cvq = new CourseViewQuery("cv");
                cvq.Select().Where(cvq.InstanceId == enrollment.CourseInstanceId);
                courseView.Load(cvq);
                courseModel.Fill(courseView, true);
            }
            if (pageId != null)
            {
                enrollment.CurrentPageId = pageId;
                enrollment.Save(tgSqlAccessType.DynamicSQL);
            }

            model.EnrollmentDate = enrollment.EnrollmentDate;
            model.CourseInstanceId = courseView.InstanceId;
            model.Id = enrollmentId;
            model.TraderId = enrollment.TraderId;
            model.MyCourse = courseModel;
            if (enrollment.CurrentPageId == null) return View(model);
            // load the section, chapter, and page the user was on.
            CoursePage currentPage = new CoursePage();
            CourseChapter currentChapter = new CourseChapter();
            CourseSection currentSection = new CourseSection();
            if (!currentPage.LoadByPrimaryKey(enrollment.CurrentPageId.Value))
            {
                throw new InvalidOperationException(
                    $"The current page id of {enrollment.CurrentPageId} does not match a coursepage record.");
            }
            if (currentPage.CourseChapterId != null &&
                !currentChapter.LoadByPrimaryKey(currentPage.CourseChapterId.Value))
            {
                throw new InvalidOperationException(
                    $"The current page id of {currentPage.CourseChapterId} does not match a coursechapter record.");
            }
            if (currentChapter.CourseSectionId != null &&
                !currentSection.LoadByPrimaryKey(currentChapter.CourseSectionId.Value))
            {
                throw new InvalidOperationException(
                    $"The current page id of {currentChapter.CourseSectionId} does not match a coursesection record.");
            }

            if (currentChapter.Id != null)
            {
                CourseChapterExam exam = CourseChapterExam.GetExam(currentChapter.Id.Value);
                model.QuizUrl = Url.Action("Examinator3000", "Course",
                    new {enrollmentId = enrollment.Id, courseExamId = exam.Id});
            }

            SectionModel sectionModel = new SectionModel(courseModel);
            sectionModel.Fill(currentSection, courseModel, false);

            ChapterModel chapterModel = new ChapterModel(sectionModel);
            chapterModel.Fill(currentChapter, sectionModel, true);

            PageModel pageModel = new PageModel(chapterModel);
            pageModel.Fill(currentPage, chapterModel);
            PageModel nextPage = chapterModel.GetNextPage(pageModel);
            PageModel prevPage = chapterModel.GetPreviousPage(pageModel);

            model.CurrentSection = sectionModel;
            model.CurrentChapter = chapterModel;
            model.CurrentPage = pageModel;
            Session["StudentCourseMode"] = model;
            if (prevPage.PageNumber == currentPage.PageNumber)
            {
                model.PreviousPageUrl = string.Empty;
            }
            else
            {
                string prevUrl = Url.Action("Run", "Course",
                    new {enrollmentId = enrollmentId, pageId = prevPage.Id});
                model.PreviousPageUrl = prevUrl;
            }

            if (nextPage.PageNumber == currentPage.PageNumber)
            {
                model.NextPageUrl = string.Empty;
            }
            else
            {
                string nextUrl = Url.Action("Run", "Course",
                    new {enrollmentId = enrollmentId, pageId = nextPage.Id});
                model.NextPageUrl = nextUrl;
            }
            return View(model);
        }

        public virtual ActionResult Examinator3000(Guid? enrollmentId, Guid? courseExamId, Guid? questionId)
        {
            if (courseExamId == null || enrollmentId == null)
            {
                return new RedirectResult(Url.Action("Index", "Course"));
            }

            ExamModel model;
            if (Session["ExamModel"] != null)
            {
                // Change this to not use the session to cache questions?
                model = (ExamModel) Session["ExamModel"];
            }
            else
            {
                model = new ExamModel();
            }
            CourseEnrollment enrollment = new CourseEnrollment();
            enrollment.LoadByPrimaryKey(enrollmentId.Value);
            CourseChapterExam exam = new CourseChapterExam();
            if (!exam.LoadByPrimaryKey(courseExamId.Value) || !enrollment.LoadByPrimaryKey(enrollmentId.Value))
            {
                return new RedirectResult(Url.Action("Index", "Course"));
            }

            model.ExamId = exam.Id;
            model.ChapterId = exam.CourseChapterId;
            if (exam.Id != null)
            {
                ICollection<ChapterExamQuestion> questions = ChapterExamQuestionCollection.GetExamQuestions(exam.Id.Value);
                ICollection<QuestionModel> questionModels = QuestionModel.Fill(questions);
                
                model.Questions = questionModels;
            }

            CoursePage currentPage = new CoursePage();
            CourseChapter currentChapter = new CourseChapter();
            CourseSection currentSection = new CourseSection();
            if (enrollment.CurrentPageId != null && !currentPage.LoadByPrimaryKey(enrollment.CurrentPageId.Value))
            {
                throw new InvalidOperationException(
                    $"The current page id of {enrollment.CurrentPageId} does not match a coursepage record.");
            }
            if (currentPage.CourseChapterId != null &&
                !currentChapter.LoadByPrimaryKey(currentPage.CourseChapterId.Value))
            {
                throw new InvalidOperationException(
                    $"The current page id of {currentPage.CourseChapterId} does not match a coursechapter record.");
            }
            if (currentChapter.CourseSectionId != null &&
                !currentSection.LoadByPrimaryKey(currentChapter.CourseSectionId.Value))
            {
                throw new InvalidOperationException(
                    $"The current page id of {currentChapter.CourseSectionId} does not match a coursesection record.");
            }
            CourseModel cmodel = new CourseModel();
            SectionModel sectionModel = new SectionModel(cmodel);
            sectionModel.Fill(currentSection, cmodel, false);

            ChapterModel chapterModel = new ChapterModel(sectionModel);
            chapterModel.Fill(currentChapter, sectionModel, true);

            model.CurrentChapter = chapterModel;
            model.CurrentSection = sectionModel;

            PageModel pageModel = new PageModel(chapterModel);
            pageModel.Fill(currentPage, chapterModel);
            QuestionModel nextQuestion = model.GetNextQuestion();
            QuestionModel prevQuestion = model.GetPreviousQuestion();

            if (model.CurrentQuestion == null)
            {
                model.CurrentQuestion = nextQuestion;
            }
            if (prevQuestion.QuestionOrder == model.CurrentQuestion.QuestionOrder)
            {
                model.PreviousQuestionUrl = string.Empty;
            }
            else
            {
                string prevUrl = Url.Action("Examinator3000", "Course",
                    new { enrollmentId = enrollmentId, courseExamlId = courseExamId, questionId = prevQuestion.QuestionId });
                model.PreviousQuestionUrl = prevUrl;
            }

            if (nextQuestion.QuestionOrder == model.CurrentQuestion.QuestionOrder)
            {
                model.NextQuestionUrl = string.Empty;

            } else { 

                string prevUrl = Url.Action("Examinator3000", "Course",
                    new { enrollmentId = enrollmentId, courseExamlId = courseExamId, questionId = prevQuestion.QuestionId });
                model.PreviousQuestionUrl = prevUrl;
            }
                
            if (Session["ExamModel"] == null)
            {
                Session["ExamModel"] = model;
            }
            
            Session["StudentCourseModel"] = GetStudentCourseModel(enrollmentId);
            return View(model);
        }

        [HttpPost]
        public virtual ActionResult CompleteExam()
        {
            string enrollmentIdStr = Request["enrollmentId"];
            string courseExamIdStr = Request["courseExamId"];

            ExamModel model = (ExamModel)Session["ExamModel"];

            Guid enrollmentId, courseExamId;
            Guid.TryParse(enrollmentIdStr, out enrollmentId);
            Guid.TryParse(courseExamIdStr, out courseExamId);
            model.EnrollmentId = enrollmentId;
            foreach (var q in model.Questions)
            {
                string key = $"Answer_{q.QuestionId}";
                string val = Request[key];
                if (!string.IsNullOrEmpty(val))
                {
                    q.SelectedOption = val.ToCharArray()[0];
                }
            }
            model.SetScore();
            Session["StudentCourseModel"] = GetStudentCourseModel(enrollmentId);
            return View(model);
        }

        public virtual ActionResult Runner(Guid? enrollmentId, Guid? pageId)
        {
            if (enrollmentId == null)
            {
                return new RedirectResult(Url.Action("Index", "Course"));
            }
            CourseEnrollment enrollment = new CourseEnrollment();
            StudentCourseModel model = new StudentCourseModel();
            CourseModel courseModel = new CourseModel();

            if (!enrollment.LoadByPrimaryKey(enrollmentId.Value))
            {
                throw new InvalidOperationException($"Unable to load an Enrollment with {enrollmentId}");
            }
            CourseView courseView = new CourseView();
            if (enrollment.CourseInstanceId != null)
            {
                CourseViewQuery cvq = new CourseViewQuery("cv");
                cvq.Select().Where(cvq.InstanceId == enrollment.CourseInstanceId);
                courseView.Load(cvq);
                courseModel.Fill(courseView, true);
            }
            if (pageId != null)
            {
                enrollment.CurrentPageId = pageId;
                enrollment.Save(tgSqlAccessType.DynamicSQL);
            }

            model.EnrollmentDate = enrollment.EnrollmentDate;
            model.CourseInstanceId = courseView.InstanceId;
            model.Id = enrollmentId;
            model.TraderId = enrollment.TraderId;
            model.MyCourse = courseModel;
            if (enrollment.CurrentPageId != null)
            {
                // load the section, chapter, and page the user was on.
                CoursePage currentPage = new CoursePage();
                CourseChapter currentChapter = new CourseChapter();
                CourseSection currentSection = new CourseSection();
                if (!currentPage.LoadByPrimaryKey(enrollment.CurrentPageId.Value))
                {
                    throw new InvalidOperationException(
                        $"The current page id of {enrollment.CurrentPageId} does not match a coursepage record.");
                }
                if (currentPage.CourseChapterId != null &&
                    !currentChapter.LoadByPrimaryKey(currentPage.CourseChapterId.Value))
                {
                    throw new InvalidOperationException(
                        $"The current page id of {currentPage.CourseChapterId} does not match a coursechapter record.");
                }
                if (currentChapter.CourseSectionId != null &&
                    !currentSection.LoadByPrimaryKey(currentChapter.CourseSectionId.Value))
                {
                    throw new InvalidOperationException(
                        $"The current page id of {currentChapter.CourseSectionId} does not match a coursesection record.");
                }

                SectionModel sectionModel = new SectionModel(courseModel);
                sectionModel.Fill(currentSection, courseModel, false);

                ChapterModel chapterModel = new ChapterModel(sectionModel);
                chapterModel.Fill(currentChapter, sectionModel, false);

                PageModel pageModel = new PageModel(chapterModel);
                pageModel.Fill(currentPage, chapterModel);

                model.CurrentSection = sectionModel;
                model.CurrentChapter = chapterModel;
                model.CurrentPage = pageModel;
            }
            return View(model);
        }

        private StudentCourseModel GetStudentCourseModel(Guid? enrollmentId)
        {
            CourseEnrollment enrollment = new CourseEnrollment();
            StudentCourseModel model = new StudentCourseModel();
            CourseModel courseModel = new CourseModel();

            if (enrollmentId != null && !enrollment.LoadByPrimaryKey(enrollmentId.Value))
            {
                throw new InvalidOperationException($"Unable to load an Enrollment with {enrollmentId}");
            }
            CourseView courseView = new CourseView();
            if (enrollment.CourseInstanceId != null)
            {
                CourseViewQuery cvq = new CourseViewQuery("cv");
                cvq.Select().Where(cvq.InstanceId == enrollment.CourseInstanceId);
                courseView.Load(cvq);
                courseModel.Fill(courseView, true);
            }

            model.EnrollmentDate = enrollment.EnrollmentDate;
            model.CourseInstanceId = courseView.InstanceId;
            model.Id = enrollmentId;
            model.TraderId = enrollment.TraderId;
            model.MyCourse = courseModel;
            if (enrollment.CurrentPageId == null) return model;
            // load the section, chapter, and page the user was on.
            CoursePage currentPage = new CoursePage();
            CourseChapter currentChapter = new CourseChapter();
            CourseSection currentSection = new CourseSection();
            if (!currentPage.LoadByPrimaryKey(enrollment.CurrentPageId.Value))
            {
                throw new InvalidOperationException(
                    $"The current page id of {enrollment.CurrentPageId} does not match a coursepage record.");
            }
            if (currentPage.CourseChapterId != null &&
                !currentChapter.LoadByPrimaryKey(currentPage.CourseChapterId.Value))
            {
                throw new InvalidOperationException(
                    $"The current page id of {currentPage.CourseChapterId} does not match a coursechapter record.");
            }
            if (currentChapter.CourseSectionId != null &&
                !currentSection.LoadByPrimaryKey(currentChapter.CourseSectionId.Value))
            {
                throw new InvalidOperationException(
                    $"The current page id of {currentChapter.CourseSectionId} does not match a coursesection record.");
            }

            if (currentChapter.Id != null)
            {
                CourseChapterExam exam = CourseChapterExam.GetExam(currentChapter.Id.Value);
                model.QuizUrl = Url.Action("Examinator3000", "Course",
                    new { enrollmentId = enrollment.Id, courseExamId = exam.Id });
            }

            SectionModel sectionModel = new SectionModel(courseModel);
            sectionModel.Fill(currentSection, courseModel, false);

            ChapterModel chapterModel = new ChapterModel(sectionModel);
            chapterModel.Fill(currentChapter, sectionModel, true);

            PageModel pageModel = new PageModel(chapterModel);
            pageModel.Fill(currentPage, chapterModel);
            PageModel nextPage = chapterModel.GetNextPage(pageModel);
            PageModel prevPage = chapterModel.GetPreviousPage(pageModel);

            model.CurrentSection = sectionModel;
            model.CurrentChapter = chapterModel;
            model.CurrentPage = pageModel;
            
            if (prevPage.PageNumber == currentPage.PageNumber)
            {
                model.PreviousPageUrl = string.Empty;
            }
            else
            {
                string prevUrl = Url.Action("Run", "Course",
                    new { enrollmentId = enrollmentId, pageId = prevPage.Id });
                model.PreviousPageUrl = prevUrl;
            }

            if (nextPage.PageNumber == currentPage.PageNumber)
            {
                model.NextPageUrl = string.Empty;
            }
            else
            {
                string nextUrl = Url.Action("Run", "Course",
                    new { enrollmentId = enrollmentId, pageId = nextPage.Id });
                model.NextPageUrl = nextUrl;
            }
            return model;
        }
    }
}