﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using CoinTradePros.BusinessObjects;
using CoinTradePros.Web.Helpers;
using CoinTradePros.Web.Models;
using Tiraggo.Interfaces;

namespace CoinTradePros.Web.Controllers
{
    public partial class FeatureController : Controller
    {
        [Authorize(Roles = "ProTrader, Admin")]
        public virtual ActionResult Index(Guid? proTraderProfileId)
        {
            Guid proTraderId;
            FeatureGroupModel model = new FeatureGroupModel();
            bool isProxy = false;

            if (proTraderProfileId.HasValue && proTraderProfileId != Guid.Empty)
            {
                model.ProxyProTraderProfileId = proTraderProfileId;
                proTraderId = proTraderProfileId.Value;
                isProxy = true;
            }
            else
            {
                proTraderId = IdentityHelper.GetCurrentUserProTraderId(User);
            }
            var features = Feature.GetProTraderFeatures(proTraderId);
            ICollection<FeatureModel> featureModels = new List<FeatureModel>();

            foreach (var f in features)
            {
                FeatureModel modelItem = new FeatureModel();
                if (isProxy)
                {
                    modelItem.ProxyProTraderProfileId = proTraderProfileId;
                }
                modelItem.ProTraderProfileId = f.ProTraderProfileId;
                modelItem.Description = f.Description;
                modelItem.FeatureIconUrl = f.IconPath;
                modelItem.FeatureId = f.Id;
                modelItem.SortOrder = f.SortOrder;
                modelItem.FeatureTypeId = f.FeatureTypeId;
                modelItem.Name = f.Name;
                modelItem.FeatureTypes = ModelHelper.GetFeatureTypeList(null);
                featureModels.Add(modelItem);
            }
            model.FeatureModels = featureModels;
            if (User.Identity.IsAuthenticated)
            {
                var user = User.Identity;
                ViewBag.Name = user.Name;

            }
            return View(model);
        }

        [Authorize(Roles = "ProTrader, Admin")]
        public virtual ActionResult Create(Guid? proTraderProfileId)
        {
            FeatureModel model = new FeatureModel();
            model.FeatureTypes = ModelHelper.GetFeatureTypeList(null);
            if (proTraderProfileId.HasValue && proTraderProfileId != Guid.Empty)
            {
                model.ProxyProTraderProfileId = proTraderProfileId;
            }
            else
            {
                model.ProTraderProfileId = IdentityHelper.GetCurrentUserProTraderId(User);
            }
            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = "ProTrader, Admin")]
        public virtual ActionResult Create(FeatureModel model)
        {
            if (ModelState.IsValid)
            {
                Feature feature = new Feature();
                feature.Id = Guid.NewGuid();
                feature.Description = model.Description;
                feature.FeatureTypeId = model.FeatureTypeId;
                feature.Name = model.Name;
                feature.SortOrder = model.SortOrder;
                if (model.IsProxyMode())
                {
                    feature.ProTraderProfileId = model.ProxyProTraderProfileId;
                }
                else
                {
                    feature.ProTraderProfileId = model.ProTraderProfileId;
                }
                feature.Save(tgSqlAccessType.DynamicSQL);
                if (model.IsProxyMode())
                {
                    return RedirectToAction("Index", "Feature",
                        new { proTraderProfileId = model.ProxyProTraderProfileId });
                }
                return RedirectToAction("Index", "Feature");
            }
            return View(model);
        }

        [Authorize(Roles = "ProTrader, Admin")]
        public virtual ActionResult Edit(Guid id, Guid? proTraderProfileId)
        {
            FeatureModel model = new FeatureModel();
            Feature feature = new Feature();
            feature.LoadByPrimaryKey(id);
            if (proTraderProfileId.HasValue && proTraderProfileId != Guid.Empty)
            {
                model.ProxyProTraderProfileId = proTraderProfileId;
                model.ProTraderProfileId = proTraderProfileId;
            }
            else
            {
                model.ProTraderProfileId = feature.ProTraderProfileId;
            }
            model.Description = feature.Description;
            model.FeatureIconUrl = feature.IconPath;
            model.FeatureId = feature.Id;
            model.FeatureTypeId = feature.FeatureTypeId;
            model.Name = feature.Name;
            model.SortOrder = feature.SortOrder;
            model.FeatureTypes = ModelHelper.GetFeatureTypeList(feature.FeatureTypeId);
            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = "ProTrader, Admin")]
        public virtual ActionResult Edit(FeatureModel model)
        {
            if (ModelState.IsValid)
            {
                Feature feature = new Feature();
                if (model.FeatureId.HasValue && feature.LoadByPrimaryKey(model.FeatureId.Value))
                {
                    if (model.IsProxyMode())
                    {
                        feature.ProTraderProfileId = model.ProxyProTraderProfileId;
                    }
                    else
                    {
                        feature.ProTraderProfileId = model.ProTraderProfileId;
                    }
                    feature.Description = model.Description;
                    feature.FeatureTypeId = model.FeatureTypeId;
                    feature.IconPath = model.FeatureIconUrl;
                    feature.Name = model.Name;
                    feature.SortOrder = model.SortOrder;
                    feature.Save(tgSqlAccessType.DynamicSQL);
                    if (model.IsProxyMode())
                    {
                        return RedirectToAction("Index", "Feature",
                            new { proTraderProfileId = model.ProxyProTraderProfileId });
                    }
                    return RedirectToAction("Index", "Feature");
                }

            }
            return View(model);
        }

        [Authorize(Roles = "ProTrader, Admin")]
        public virtual ActionResult Delete(Guid id, Guid? proTraderProfileId)
        {
            Feature feature = new Feature();
            feature.LoadByPrimaryKey(id);
            feature.MarkAsDeleted();
            feature.Save(tgSqlAccessType.DynamicSQL);

            if (proTraderProfileId.HasValue && proTraderProfileId != Guid.Empty)
            {
                return RedirectToAction("Index", "Feature", new { proTraderProfileId });

            }
            return RedirectToAction("Index", "Feature");
        }
    }
}