﻿using CoinTradePros.BusinessObjects;
using CoinTradePros.Messaging.Email;
using CoinTradePros.Web.Helpers;
using CoinTradePros.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Rotativa.Options;

namespace CoinTradePros.Web.Controllers
{
    public class InvoiceController : Controller
    {
        private List<InvoiceModel> inv = new List<InvoiceModel>();

        public InvoiceController()
        {
            var invoiceList = CryptoInvoice.GetAllInvoices();
            foreach(var invoice in invoiceList)
            {
                inv.Add(new InvoiceModel()
                {
                    Id = invoice.Id.HasValue ? invoice.Id.Value : Guid.NewGuid(),
                    Amount = invoice.Amount.HasValue ? invoice.Amount.Value : 0,
                    DueDate = invoice.DueDate.HasValue ? invoice.DueDate.Value : DateTime.Now,
                    Currency = invoice.Currency,
                    PaymentStatus = invoice.PaymentStatus.HasValue ? invoice.PaymentStatus.Value : 0,
                    PaymentDate = DateTime.Now,
                    SubscriptionId = invoice.SubscriptionId.HasValue ? invoice.SubscriptionId.Value : Guid.NewGuid()
                });
            }
        }
        // GET: Invoice
        public ActionResult Index()
        {
            return View(inv);
        }

        public ActionResult Mine(Guid? id)
        {
            if (id.HasValue)
            {
                var subscriptionViewCol = GetAllSubscriptionView().Where(s=>s.SubscriberUserId == id.Value.ToString());
                return View("index", subscriptionViewCol);
            }
            else
            {
                return Content("Please login");
            }
        }


        public async Task<ActionResult> Notify(Guid? id)
        {
            try
            {
                await SendNotification(id.Value);
                var userId = IdentityHelper.GetCurrentUserId(User);
                var subscribers = SubscriptionView.GetSubscribers(userId);

                return RedirectToAction("Subscribers", new { id = userId });
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }



        }

        
        public ActionResult Details(Guid id)
        {
            SubscriptionViewCollection model = new SubscriptionViewCollection();

            model = SubscriptionView.SubscriptionViewbyId(id);
            
            
                return View(model);
            }
        public ActionResult _Details(Guid id)
        {
            SubscriptionViewCollection model = new SubscriptionViewCollection();

            model = SubscriptionView.SubscriptionViewbyId(id);
            return PartialView("_Details", model);
        }
        public ActionResult GeneratePDF(Guid? pId)
        {

            return new Rotativa.ActionAsPdf("_Details", new { id = pId })
            {
                PageSize = Rotativa.Options.Size.A4,

                PageOrientation = Rotativa.Options.Orientation.Landscape,

                //PageMargins = new Margins(12, 12, 12, 12),// it’s in millimeters

                //PageWidth = 180,

                //PageHeight = 280,
                FileName = "Invoice.pdf"
            };
        }
        public virtual ActionResult Subscribers(Guid? id)
        {
            var subscribers = SubscriptionView.GetSubscribers(id.Value)
                .Where(s => s.IsCancelled == false && s.PaymentAmount > 0 
                && s.SubscriberUserId != null);
            if (User.Identity.IsAuthenticated)
            {
                var user = User.Identity;
                ViewBag.Name = user.Name;

            }
            return View("Subscribers", subscribers);
        }
        
        private async Task SendNotification(Guid subscriptionId)
        {
            var emailService = new EmailNotificationService();
            var subscriptionView = GetSubscriptionView(subscriptionId);
            var userId = subscriptionView.SubscriberUserId;

            var userEmail = AspNetUsers.GetUserByUserId(userId.ToString()).Email;
            var messageBody = $"Your bill for the package {subscriptionView.PackageTitle} is due for payment.<br/>Current bill total:{subscriptionView.PaymentAmount} ";


           await emailService.SendMessageAsync(messageBody, "Your Subscription bill is now available", true, userEmail);
        }

        private SubscriptionView GetSubscriptionView(Guid? subscriptionId)
        {
            var sub = new SubscriptionView();
            var sq = new SubscriptionViewQuery("s");
            sq.Select().Where(sq.SubscriptionId == subscriptionId.Value);
            sub.Load(sq);
            return sub;
        }

        private SubscriptionViewCollection GetAllSubscriptionView()
        {
            var sub = new SubscriptionViewCollection();
            var sq = new SubscriptionViewQuery("s");
            sq.Select();
            sub.Load(sq);
            return sub;
        }
    }
}