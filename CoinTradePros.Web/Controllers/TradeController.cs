﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Threading.Tasks;
using System.Web.Mvc;
using CoinTradePros.BusinessObjects;
using CoinTradePros.Interfaces.Messaging;
using CoinTradePros.Messaging.Core;
using CoinTradePros.Web.Helpers;
using CoinTradePros.Web.Models;
using Microsoft.AspNet.Identity;

namespace CoinTradePros.Web.Controllers
{
    public partial class TradeController : Controller
    {
        

        // GET: Trade
        [Authorize(Roles = "Trader,ProTrader")]
        public virtual ActionResult Index()
        {
            //MY CODE--- START
            string userId = User.Identity.GetUserId();
            Guid id;

            MyTradeViewModel mtvm = new MyTradeViewModel();

            //mtvm.tvc = Guid.TryParse(userId, out id) ? TradeViewCollection.GetUserTrades(id) : new TradeViewCollection();

            TradeViewCollection tvc = new TradeViewCollection();
            tvc = Guid.TryParse(userId, out id) ? TradeViewCollection.GetUserTrades(id) : new TradeViewCollection();
            mtvm.TvccList = mtvm.abc(tvc);
            mtvm.ExchangesList = mtvm.SortExchanges(tvc);
            mtvm.ProTradersList = mtvm.SortProTraders(tvc);

            ApplicationUser currentUser = new ApplicationUser();
            ViewBag.Title = "My Trades";
            if (User.Identity.IsAuthenticated)
            {
                string imageUrl = IdentityHelper.GetAvatarUrl(User);
                ViewBag.ImageUrl = imageUrl;
                var user = User.Identity;
                ViewBag.Name = user.Name;
            }

            return View(mtvm);
            //MY CODE--- END


            //ORIGNAL CODE---- START
            //string userId = User.Identity.GetUserId();
            //Guid id;
            //var items = Guid.TryParse(userId, out id) ?
            //    TradeViewCollection.GetUserTrades(id) :
            //    new TradeViewCollection();
            //ApplicationUser currentUser = new ApplicationUser();
            //            ViewBag.Title = "My Trades";
            //if (User.Identity.IsAuthenticated)
            //{
            //    string imageUrl = IdentityHelper.GetAvatarUrl(User);    
            //    ViewBag.ImageUrl = imageUrl;
            //    var user = User.Identity;
            //    ViewBag.Name = user.Name;

            //}

            //return View(items);
            //ORIGNAL CODE---- END
        }

        // GET: Trades from any traders the user follows.
        [Authorize(Roles = "Trader,ProTrader")]
        public virtual ActionResult Tradestream()
        {
            TradeCollection items = new TradeCollection();
            items.LoadAll();
            ViewBag.Title = "Tradestream";
            if (User.Identity.IsAuthenticated)
            {
                var user = User.Identity;
                ViewBag.Name = user.Name;

            }
            return View(items);
        }

        // GET: Trade/Details/5
        [Authorize(Roles = "Trader,ProTrader")]
        public virtual ActionResult Details(Guid tradeid)
        {
            TradeModel model = GetTradeModel(tradeid);
            ViewBag.Title = "Trade Details";
            if (User.Identity.IsAuthenticated)
            {
                var user = User.Identity;
                ViewBag.Name = user.Name;

            }
            return View(model);
        }

        // GET: Trade/Create
        [Authorize(Roles = "Trader,ProTrader")]
        public virtual ActionResult Create()
        {
            TradeModel model = GetTradeModel(null);
            ViewBag.Title = "New Trade";
            if (User.Identity.IsAuthenticated)
            {
                var user = User.Identity;
                ViewBag.Name = user.Name;

            }
            return View(model);
        }

        // POST: Trade/Create
        [HttpPost]
        [Authorize(Roles = "Trader,ProTrader")]
        public virtual ActionResult Create(TradeModel model)
        {
            try
            {
                ViewBag.Title = "New Trade";
                if (!model.TraderId.HasValue)
                {
                    model.TraderId = IdentityHelper.GetCurrentUserTraderId(User);
                }
                if (ModelState.IsValid)
                {
                    UploadHelper uploadHelper = new UploadHelper(model.TraderId.Value, Server.MapPath("~/"));
                    Trade trade = new Trade();
                    trade.Id = Guid.NewGuid();
                    trade.PositionSize = model.PositionSize;
                    trade.ProTraderProfileId = model.ProTraderProfileId;
                    trade.TraderId = model.TraderId;
                    trade.AssetId = model.AssetId;
                    trade.ChartImagePath = uploadHelper.GetFinalItemRelativePath(model.ChartImage);
                    trade.Comments = model.Comments;
                    trade.DenominationId = model.DenominationId;
                    trade.EntryDate = model.EntryDate;
                    trade.EntryPrice = model.EntryPrice;
                    trade.ExchangeId = model.ExchangeId;
                    trade.ExitDate = model.ExitDate;
                    trade.ExitPrice = model.ExitPrice;
                    trade.Id = Guid.NewGuid();
                    if (trade.IsClosed())
                    {
                        trade.ProfitLossAmount = trade.CalculateProfitLossAmount();
                        trade.ProfitLossPercent = trade.CalculateProfitLossPercent();
                    }
                    trade.Save(Tiraggo.Interfaces.tgSqlAccessType.DynamicSQL);

                    // Check to see if we're supposed to send out any alerts
                    if (Request["AlertGroups"] != null && Request["MessagingGroup"] != null)
                    {
                        // send the alert
                        AlertModel alertModel = new AlertModel();
                        alertModel.AlertChannels = Request["MessagingGroup"];
                        alertModel.AlertGroups = Request["AlertGroups"];
                        alertModel.SenderId = User.Identity.GetUserId();
                        if (trade.Id != null)
                        {
                            var tradeView = TradeView.LoadByTradeId(trade.Id ?? Guid.Empty);
                            TempData.Add("TradeView", tradeView);
                        }
                        string moreUrl =
                            $"{Url.Action("Details", "Trade", new { tradeid = trade.Id })}";
                        IDictionary<string, string> items = new Dictionary<string, string>();
                        items.Add("MoreInfoUrl", moreUrl);
                        TempData.Add("MessageFields", items);

                        return RedirectToAction("SendAlert", "Alert", alertModel);

                    }
                    return RedirectToAction("Index");
                }
                CheckPackages(model);
                FillDropDowns(model);
                return View(model);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Exception", ex.ToString());
                CheckPackages(model);
                FillDropDowns(model);
                return View(model);
            }
        }

        // GET: Trade/Edit/5
        [Authorize(Roles = "Trader,ProTrader")]
        public virtual ActionResult Edit(Guid? tradeid)
        {
            if (!tradeid.HasValue)
            {
                // Just sent them to the create page to make a new trade.
                return RedirectToAction("Create");
            }
            ViewBag.Title = "Edit Trade";
            TradeModel model = GetTradeModel(tradeid);
            return View(model);
        }

        // POST: Trade/Edit/5
        [HttpPost]
        [Authorize(Roles = "Trader,ProTrader")]
        public virtual ActionResult Edit(TradeModel model)
        {
            try
            {
                ViewBag.Title = "Edit Trade";
                if (ModelState.IsValid)
                {
                    Trade trade = new Trade();
                    if (model.TradeId.HasValue && trade.LoadByPrimaryKey(model.TradeId.Value))
                    {
                        trade.AssetId = model.AssetId;
                        //trade.ChartImage = model.ChartImage;
                        trade.Comments = model.Comments;
                        trade.DenominationId = model.DenominationId;
                        trade.EntryDate = model.EntryDate;
                        trade.EntryPrice = model.EntryPrice;
                        trade.ExchangeId = model.ExchangeId;
                        trade.ExitDate = model.ExitDate;
                        trade.ExitPrice = model.ExitPrice;
                        trade.PositionSize = model.PositionSize;
                        trade.ProfitLossAmount = trade.CalculateProfitLossAmount();
                        trade.ProfitLossPercent = trade.CalculateProfitLossPercent();

                        trade.Save(Tiraggo.Interfaces.tgSqlAccessType.DynamicSQL);

                        AlertCheck(trade, Request["AlertGroups"], Request["MessagingGroup"]);
                        return RedirectToAction("Index");
                    }
                    ModelState.AddModelError("TradeId", $"The trade id {model.TradeId ?? Guid.Empty} was not found.");
                }
                else
                {
                    ModelState.AddModelError("General", "Please double check your trade information for any errors.");
                }
                // repopulate the dropdowns and return to sender
                CheckPackages(model);
                FillDropDowns(model);
                return View(model);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("GeneralError", ex);
                // repopulate the dropdowns and return to sender
                CheckPackages(model);
                FillDropDowns(model);
                return View(model);
            }
        }

        // GET: Trade/Delete/5
        [Authorize(Roles = "Trader,ProTrader")]
        public virtual ActionResult Delete(Guid? tradeid)
        {
            ViewBag.Title = "Delete Trade";
            Trade trade = new Trade();
            if (tradeid.HasValue && trade.LoadByPrimaryKey(tradeid.Value))
            {
                trade.MarkAsDeleted();
                trade.Save(Tiraggo.Interfaces.tgSqlAccessType.DynamicSQL);
            }
            return RedirectToAction("Index");
        }

        private TradeModel GetTradeModel(Guid? tradeid)
        {
            Trade trade = new Trade();
            TradeModel model = new TradeModel();

            // attempt to load the trade with the id given.
            if (tradeid.HasValue && trade.LoadByPrimaryKey(tradeid.Value))
            {
                model.AssetId = trade.AssetId;
                //model.ChartImage = trade.ChartImage;
                model.Comments = trade.Comments;
                model.DenominationId = trade.DenominationId;
                model.EntryDate = trade.EntryDate;
                model.EntryPrice = trade.EntryPrice;
                model.ExchangeId = trade.ExchangeId;
                model.ExitDate = trade.ExitDate;
                model.ExitPrice = trade.ExitPrice;
                model.PositionSize = trade.PositionSize;
                model.ProTraderProfileId = trade.ProTraderProfileId;
                model.TraderId = trade.TraderId;
                model.TradeId = trade.Id;
                model.ProfitLossAmount = trade.ProfitLossAmount;
                model.ProfitLossPercent = trade.ProfitLossPercent;
            }
            else
            {
                model.TraderId = IdentityHelper.GetCurrentUserTraderId(User);
            }
            CheckPackages(model);
            FillDropDowns(model);
            return model;
        }

        private void CheckPackages(TradeModel model)
        {
            if (User.IsInRole("ProTrader"))
            {
                Guid userid;
                if (Guid.TryParse(User.Identity.GetUserId(), out userid))
                {
                    model.Packages = ModelHelper.FillPackagesForUser(userid);
                }
            }
        }

        private void FillDropDowns(TradeModel model)
        {
            bool hasAccount = !string.IsNullOrEmpty(User.Identity.GetUserId());
            // repopulate the dropdowns and return to sender
            model.Denominations = ModelHelper.GetDenominationList(model.DenominationId);
            model.Assets = ModelHelper.GetAssetList(model.AssetId);
            model.Exchanges = ModelHelper.GetExchangeList(model.ExchangeId);
            if (!hasAccount)
            {
                // Should be logged in, but just in case.
                RedirectToAction("Login", "Account");
            }

            if (User.IsInRole("ProTrader"))
            {
                model.MessagingOptions = ModelHelper.FillMessagingOptions(IdentityHelper.GetCurrentUserId(User));
            }
            model.Subscriptions =
                ModelHelper.GetSubscriptions(IdentityHelper.GetCurrentUserId(User), model.ProTraderProfileId);
            model.TraderId = IdentityHelper.GetCurrentUserTraderId(User);
        }

        private async Task<IMessagingResult> AlertCheck(Trade trade, string alertGroups, string messagingGroups)
        {
            if (User.IsInRole("ProTrader") && !string.IsNullOrEmpty(alertGroups))
            {
                string moreUrl =
                    $"{Url.Action("Details", "Trade", new { tradeid = trade.Id })}";
                var result = await AlertHelper.SendTradeAlert(trade, alertGroups, messagingGroups, moreUrl);
                return result;
            }
            return new BaseMessageResult();
        }
    }
}
