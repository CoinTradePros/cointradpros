﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.Mvc;
using CoinTradePros.BusinessObjects;
using CoinTradePros.Helpers;
using CoinTradePros.Payments;
using CoinTradePros.Payments.Stripe;
using CoinTradePros.Web.Helpers;
using CoinTradePros.Web.Models;
using Microsoft.AspNet.Identity;
using Tiraggo.Interfaces;
using CoinTradePros.Web.Hubs;
using Microsoft.AspNet.SignalR;
using System.Linq;

namespace CoinTradePros.Web.Controllers
{
    public partial class SubscriptionController : Controller
    {
        public virtual ActionResult UpdateCurrency(CurrencyChangeModel model)
        {
            Session["AlternateCurrency"] = model.SelectedCurrency;
            return RedirectToAction("Index", "Pro", new { username = model.ProTraderUserName });
        }

        public virtual ActionResult Subscribe()
        {
            Guid? packageId = (Guid?)HttpContext.Session["SelectedPackageId"];
            decimal? intervalAmount = (decimal?)HttpContext.Session["IntervalAmount"];
            string selectedCurrency = (string)Session["AlternateCurrency"];
            if (!packageId.HasValue || !intervalAmount.HasValue)
            {
                return RedirectToAction("Index", "Pro");
            }
            PackageSelectionModel model = new PackageSelectionModel();
            model.AlternateCurrency = selectedCurrency;
            model.IntervalAmount = intervalAmount.Value;
            model.PackageId = packageId.Value;
            return Subscribe(model);

        }

        [HttpPost]
        public virtual ActionResult Subscribe(PackageSelectionModel selectionModel)
        {
            bool useAlternateCurrency = !string.IsNullOrEmpty(selectionModel.AlternateCurrency);
            ProcessPaymentModel model = new ProcessPaymentModel();
            PackageModel package = ModelHelper.FillPackage(selectionModel.PackageId);
            model.ChosenPackage = package;

            if (User.Identity.IsAuthenticated)
            {
                // find out if the current user has this package or one from the curren pro trader.  Send them to the their account page to manage subscriptions.
                Guid traderId = IdentityHelper.GetCurrentUserTraderId(User);
                Subscription subscription = new Subscription();

                if (subscription.LoadByTraderPackage(traderId, selectionModel.PackageId) && subscription.PendingPayment.HasValue && !subscription.PendingPayment.Value)
                {
                    if (subscription.HasOtherPackageOptions(selectionModel.PackageId) ||
                        subscription.HasOtherProTraderOptions())
                    {
                        return RedirectToAction("Manage", "Subscription", new { subscriptionid = subscription.Id ?? Guid.Empty });
                    }
                }
            }
            if (useAlternateCurrency)
            {
                CoinPaymentsTxRequest request = new CoinPaymentsTxRequest();
                CurrencyConverter converter = new CurrencyConverter();
                Subscription subscription = null;
                CryptoInvoice invoice = null;
                CoinPaymentsHelper helper = new CoinPaymentsHelper();
                CoinPaymentsAsset asset = helper.GetCoin(selectionModel.AlternateCurrency);
                bool isSubLoaded = false;

                converter.SetBaseCurrency("USD");
                converter.SetSecondaryCurrency(asset);

                if (Session["CryptoSubscription"] != null)
                {
                    // Get the current subscription object to avoid loading a new payment request during each page load.
                    subscription = (Subscription)Session["CryptoSubscription"];
                    invoice = subscription.GetCurrentInvoice();
                }
                if (User.Identity.IsAuthenticated)
                {
                    Guid traderId = IdentityHelper.GetCurrentUserTraderId(User);
                    TraderProfile profile = new TraderProfile();
                    AspNetUsers user = AspNetUsers.GetUserByUserName(User.Identity.GetUserName());

                    if (subscription == null)
                    {
                        subscription = new Subscription();
                        // check to see if there is a pending subscription.
                        isSubLoaded = subscription.LoadByTraderPackage(traderId, model.ChosenPackage.PackageId ?? Guid.NewGuid());
                    }
                    if (!subscription.TraderProfileId.HasValue && isSubLoaded)
                    {
                        subscription.TraderProfileId = traderId;
                        subscription.Save(tgSqlAccessType.DynamicSQL);
                    }
                    if (profile.LoadByPrimaryKey(traderId))
                    {
                        request.BuyerName = $"{profile.FirstName} {profile.LastName}";
                    }
                    if (user != null)
                    {
                        request.BuyerEmail = user.Email;
                    }

                }
                if (!isSubLoaded)
                {
                    // create new subscription and invoice items
                    subscription = new Subscription();
                    subscription.Id = Guid.NewGuid();
                    subscription.PaymentAmount = model.BaseUsdIntervalAmount;
                    subscription.PaymentCurrency = model.AlternateCurrencyCode;
                    subscription.PendingPayment = true;
                    subscription.SubscriptionStart = DateTimeOffset.UtcNow;
                    subscription.PackageId = model.ChosenPackage.PackageId;
                    subscription.PaymentCurrency = asset.Code;

                    if (User.Identity.IsAuthenticated)
                    {
                        subscription.TraderProfileId = IdentityHelper.GetCurrentUserTraderId(User);
                    }
                    subscription.Save(tgSqlAccessType.DynamicSQL);
                }
                Session["CryptoSubscription"] = subscription;
                if (invoice == null)
                {
                    request.Amount = converter.Convert() * selectionModel.IntervalAmount;
                    request.Currency1 = selectionModel.AlternateCurrency;
                    request.Currency2 = selectionModel.AlternateCurrency;
                    request.ItemName = package.Title;
                    request.Custom = subscription.Id.ToString();
                    request.ItemNumber = package.PackageId?.ToString() ?? Guid.Empty.ToString();
                    request.IpnUrl =
                        $"{Url.Action("ProcessCryptoPayment", "Subscription")}";
                    if (package.PackageId != null)
                    {
                        request.ItemNumber = package.PackageId.Value.ToString();
                    }

                    CoinPaymentsTransaction transaction = helper.CreateTransaction(request);

                    invoice = new CryptoInvoice();
                    invoice.Amount = Math.Round(request.Amount, 8);
                    invoice.ConfirmsNeeded = transaction.ConfirmsNeeded;
                    invoice.Currency = asset.Code;
                    invoice.DueDate = DateTimeOffset.UtcNow.AddSeconds(transaction.Timeout);
                    invoice.PaymentAddress = transaction.Address;
                    invoice.PaymentStatusUrl = Server.UrlEncode(transaction.StatusUrl);
                    invoice.QrCodeUrl = Server.UrlEncode(transaction.QrCodeUrl);
                    invoice.Timeout = DateTimeOffset.UtcNow.AddSeconds(transaction.Timeout);
                    invoice.Id = Guid.NewGuid();
                    invoice.CoinPayTxId = transaction.TxId;
                    invoice.SubscriptionId = subscription.Id;
                    invoice.SessionId = HttpContext.Session.SessionID;
                    invoice.Save(tgSqlAccessType.DynamicSQL);
                }

                model.UseAlternateCurrency = true;
                model.QrCodeUrl = Server.UrlDecode(invoice.QrCodeUrl);
                model.InvoiceId = invoice.Id ?? Guid.Empty;
                model.ConfirmationsNeeded = invoice.ConfirmsNeeded ?? asset.Confirms;
                model.Timeout = invoice.Timeout.HasValue ? (invoice.Timeout.Value - DateTimeOffset.UtcNow).TotalSeconds : 7200;
                model.TimeLimit = invoice.Timeout ?? DateTimeOffset.UtcNow.AddSeconds(7200);
                model.PaymentAddress = invoice.PaymentAddress;
                model.AlternateCurrencyCode = invoice.Currency;
                model.AlternateCurrencyAmount = invoice.Amount ?? 0;
                model.StatusUrl = Server.UrlDecode(invoice.PaymentStatusUrl);
                model.StatusCheckUrl = Url.Action("Invoice", "Subscription", new { id = invoice.Id ?? Guid.Empty });
                model.CheckInterval = 15000;
            }

            else
            {
                var stripePublicKey = ConfigurationManager.AppSettings["StripePublicKey"];
                HttpContext.Session["SelectedPackageId"] = selectionModel.PackageId;
                HttpContext.Session["IntervalAmount"] = selectionModel.IntervalAmount;
                model.StripePublicKey = stripePublicKey;
                model.StripePaymentAmount = (int)selectionModel.IntervalAmount * 100;
                model.HeaderImageUrl = $"/Content/img/icons/ms-icon-310x310.png";
            }
            return View(model);
        }

        [HttpPost]
        public virtual ActionResult ProcessPayment(string stripeEmail, string stripeToken)
        {
            Guid? packageId = (Guid?)HttpContext.Session["SelectedPackageId"];
            decimal? intervalAmount = (decimal?)HttpContext.Session["IntervalAmount"];
            if (!intervalAmount.HasValue || intervalAmount <= 0)
            {
                throw new InvalidOperationException("There is no IntervalAmount in the Session data.  Packages with a zero price are not supported at this time.");
            }
            Package package = new Package();
            package.LoadByPrimaryKey(packageId ?? Guid.Empty);
            string interval = package.GetIntervalText(intervalAmount.Value);
            StripeHelper stripeHelper = new StripeHelper(interval);

            // The previous step checked for current subscriptions/management, etc., so assume no current plans here.

            Subscription subscription = new Subscription();

            string customerId = stripeHelper.CheckCustomer(stripeEmail, stripeToken);
            subscription.Id = Guid.NewGuid();
            subscription.SubscriptionStart = DateTimeOffset.UtcNow;
            subscription.PaymentFrequency = package.GetPaymentFrequency(interval);
            subscription.PendingPayment = false;
            subscription.PackageId = packageId;
            subscription.PaymentAmount = intervalAmount;
            subscription.PaymentEmail = stripeEmail;
            subscription.StripeSourceToken = stripeToken;
            subscription.StripeCustomerId = customerId;
            subscription.PaymentCurrency = "USD";

            if (User.Identity.IsAuthenticated)
            {
                subscription.TraderProfileId = IdentityHelper.GetCurrentUserTraderId(User);
            }

            if (package.IsRecurringInterval(interval))
            {
                // check the plan and add the customer's subscription
                string planId = stripeHelper.CheckPlan(package);
                string subscriptionId = stripeHelper.SubscribeCustomer(customerId, planId);
                subscription.StripeSubscriptionId = subscriptionId;
            }
            else
            {
                // create a one time charge on the customer account
                string chargeDescription = $"{package.Title} via CTP";
                string chargeId = stripeHelper.CreateCharge(intervalAmount.Value, customerId, chargeDescription);
                subscription.StripeSinglePaymentId = chargeId;
            }

            subscription.Save(tgSqlAccessType.DynamicSQL);

            return RedirectToAction("PaymentComplete", "Subscription", new { subscriptionId = subscription.Id });
        }

        public virtual ActionResult ProcessCryptoPayment(CoinPaymentApiModel model)
        {
            // TODO:  record the amount received before the confirmations since that comes first.

            CoinPaymentsHelper h = new CoinPaymentsHelper();
            var coin = h.GetCoin(model.Currency1);
            model.TotalConfirmations = coin.Confirms;

            Subscription subscription = new Subscription();
            Guid subId;
            if (Guid.TryParse(model.Custom, out subId) && subscription.LoadByPrimaryKey(subId))
            {
                CryptoInvoice invoice = subscription.GetCurrentInvoice();
                if (invoice == null)
                {
                    return new EmptyResult();
                }
                invoice.PaymentStatusUrl = Server.UrlEncode(invoice.PaymentStatusUrl);
                invoice.QrCodeUrl = Server.UrlEncode(invoice.QrCodeUrl);
                if (!model.ProcessingComplete())
                {
                    // update the confirmations on the invoice
                    if (!invoice.ConfirmsNeeded.HasValue)
                    {
                        invoice.ConfirmsNeeded = model.TotalConfirmations;
                    }
                    invoice.ConfirmsProcessed = model.Received_confirms;

                    int status;
                    if (int.TryParse(model.Status, out status))
                    {
                        invoice.PaymentStatus = status;
                    }
                    invoice.UpdateConfirmations(status);
                }
                else
                {
                    // update the invoice with the payment information.
                    subscription.SubscriptionStart = DateTimeOffset.UtcNow;
                    invoice.ConfirmsProcessed = model.Received_confirms;
                    int status;
                    if (int.TryParse(model.Status, out status))
                    {
                        invoice.PaymentStatus = status;
                        invoice.RecordPayment(model.Amount1);  // this also updates the status and confirms processed if set
                    }
                }


                var context = GlobalHost.ConnectionManager.GetHubContext<PaymentHub>();
                context.Clients.All.sendPaymentUpdate("test", model.TotalConfirmations, model.TotalConfirmations);

            }
            return new EmptyResult();
        }

        public virtual JsonResult Invoice(Guid id)
        {
            CryptoInvoice invoice = CryptoInvoice.GetInvoice(id);
            if (invoice != null)
            {
                InvoicePaymentModel model = new InvoicePaymentModel();
                model.ConfirmsNeeded = invoice.ConfirmsNeeded ?? -1;
                model.ConfirmsProcessed = invoice.ConfirmsProcessed ?? -1;
                model.PaymentComplete = invoice.IsPaid();
                decimal paymentAmount;
                if (decimal.TryParse(invoice.PaymentAmount, out paymentAmount))
                {
                    model.AmountReceived = paymentAmount;
                }
                return Json(model, JsonRequestBehavior.AllowGet);
            }

            return Json("Not found", JsonRequestBehavior.AllowGet);
        }

        public virtual ActionResult CryptoPaymentComplete(Guid invoiceId)
        {
            CryptoInvoice invoice = CryptoInvoice.GetInvoice(invoiceId);
            if (invoice != null)
            {
                return PaymentComplete(invoice.SubscriptionId ?? Guid.Empty);
            }
            throw new ApplicationException($"Unable to find an invoice with {invoiceId}");
        }

        public virtual ActionResult PaymentComplete(Guid subscriptionId)
        {
            Subscription subscription = new Subscription();
            subscription.LoadByPrimaryKey(subscriptionId);
            HttpContext.Session["SubscriptionId"] = subscription.Id;
            if (subscription.TraderProfileId == null)
            {
                return RedirectToAction("Register", "Account");
            }
            ViewBag.SubscriptionMessage = "Your subscription purchase is complete.";
            return RedirectToAction("Index", "Users");
        }

        public virtual ActionResult Subscriptions()
        {
            var subscriptions = SubscriptionView.GetSubscriptions(IdentityHelper.GetCurrentUserId(User, false));
           
            if (User.Identity.IsAuthenticated)
            {
                var user = User.Identity;
                ViewBag.Name = user.Name;

            }
            return View(subscriptions);
        }

        public virtual ActionResult Subscribers()
        {

          
            var subscribers =
                SubscriptionView.GetSubscribers(IdentityHelper.GetCurrentUserId(User, false))
                    .Where(s => s.SubscriberTraderProfileId != null);

            var modelItems = new List<SubscriptionViewModel>();
            AssetCollection ac = AssetCollection.GetVolumeSortedAssets();
            ViewBag.asset = ac;
            var assets = ModelHelper.GetAssetList(null);
            var exchanges = ModelHelper.GetExchangeList(null);
            var denominations = ModelHelper.GetDenominationList(null);

            foreach (var sub in subscribers)
            {
                var subModel = new SubscriptionViewModel();
                Guid id;
                // sub.SubscriberAvatarUrl = IdentityHelper.GetAvatarUrlFromTraderId(sub.SubscriberTraderProfileId ?? Guid.Empty);
                subModel.SubscriptionInfo = sub;
                if (Guid.TryParse(sub.SubscriberUserId, out id))
                {
                    TradeCollection trades = TradeCollection.GetUserTrades(id);
                    foreach (var trade in trades)
                    {
                        var model = new TradeModel();
                        model.Fill(trade);
                        model.Assets = assets;
                        model.Exchanges = exchanges;
                        model.Denominations = denominations;
                        subModel.Trades.Add(model);
                    }
                }
                modelItems.Add(subModel);
            }
            if (User.Identity.IsAuthenticated)
            {
                var user = User.Identity;
                ViewBag.Name = user.Name;

            }
            return View(modelItems);
        }

        public virtual ActionResult Details(Guid subscriptionid)
        {
            SubscriptionView sub = new SubscriptionView();
            SubscriptionViewQuery sq = new SubscriptionViewQuery("s");
            sq.Select().Where(sq.SubscriptionId == subscriptionid);
            sub.Load(sq);
            return View(sub);
        }

        public virtual ActionResult ChangeSubscriptionPlan(Guid subscriptionid, Guid proTraderId, 
            Guid packageId, Decimal? price, int paymentFrequency)
        {
            UpdateSubscriptionPlan(subscriptionid, price, paymentFrequency);
            var subscriptions = SubscriptionView.GetSubscriptions(IdentityHelper.GetCurrentUserId(User, false));

            if (User.Identity.IsAuthenticated)
            {
                var user = User.Identity;
                ViewBag.Name = user.Name;
            }

            return View("Subscriptions", subscriptions);
        }

        private void UpdateSubscriptionPlan(Guid subscriptionid, Decimal? price, int paymentFrequency)
        {
            var subscription = new Subscription();

            if (subscription.LoadByPrimaryKey(subscriptionid))
            {
                subscription.PaymentFrequency = paymentFrequency;
                subscription.PaymentAmount = price;
                subscription.PendingPayment = true;
                subscription.Save(tgSqlAccessType.DynamicSQL);
            }
        }

        public virtual ActionResult Manage(Guid subscriptionid)
        {
            return View();
        }

        public virtual ActionResult Cancel(Guid subscriptionId)
        {
            return View();
        }

        public virtual ActionResult Chatroom(Guid id)
        {
            ChatViewModel model = new ChatViewModel();
            ProTraderProfile profile = new ProTraderProfile();
            profile.LoadByPrimaryKey(id);
            /*
             * https://www.rumbletalk.com/client/?<chat_hash>
             * 
             */
            string baseurl = "https://www.rumbletalk.com/client/?";
            model.ChatUrl = $"{baseurl}{profile.ChatHash}";
            return View(model);
        }
    }
}