﻿using System;
using System.Web.Mvc;
using CoinTradePros.BusinessObjects;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using CoinTradePros.Exchange;
using CoinTradePros.Interfaces.Exchange;
using CoinTradePros.Web.Models;

namespace CoinTradePros.Web.Controllers
{
    [Authorize(Roles = "Trader,ProTrader")]

    public class ExchangeController : Controller
    {
        //GET: Exchange
        public ActionResult Index()
        {
            List<string> lst = new List<string>();
            var items =
              ExchangeCollection.GetExchangesHasApi();
            string userId = User.Identity.GetUserId();
            Guid id;
            foreach (var item in items)
            {
                var GetExchange = Guid.TryParse(userId, out id) ?
                              UserSettingCollection.GetExchangeSettingbyName(userId, item.Name) :
                              new UserSettingCollection();
                foreach (var item2 in GetExchange)
                {
                    lst.Add(item2.SettingKey);//PoloniexApiKey
                    break;
                }
            }
            if (User.Identity.IsAuthenticated)
            {
                var user = User.Identity;
                ViewBag.Name = user.Name;

            }
            ViewBag.GetExchange = lst;
            return View(items);
        }
        public PartialViewResult Connect(string exchangeName)
        {
            ViewBag.Name = exchangeName;
            return PartialView("_Connect");
        }
        [HttpPost]
        public PartialViewResult Connected(ExchangeRequestViewModel exchangeRequest)
        {
            ExchangeType eType = (ExchangeType)Enum.Parse(typeof(ExchangeType), exchangeRequest.exchangeName);
            try
            {
                IProvideExchangeData pExchanegData = ExchangeProviderFactory.GetExchangeProvider(eType);
                var Orders = pExchanegData.GetOrders(exchangeRequest);
                if (Orders != null)
                {
                    ViewBag.Status = exchangeRequest.exchangeName + " Api Connected.";
                    PlatformType pType = (PlatformType)Enum.Parse(typeof(PlatformType), "Exchange");
                    UserSetting.AddSetting(User.Identity.GetUserId(), pType, exchangeRequest.exchangeName + "ApiKey", exchangeRequest.ApiKey);
                    UserSetting.AddSetting(User.Identity.GetUserId(), pType, exchangeRequest.exchangeName + "ApiSecret", exchangeRequest.ApiSecret);
                }
                else
                {
                    ViewBag.Status = exchangeRequest.exchangeName + " Api not  Connected. Please try again";
                }
            }
            catch (Exception e)
            {
                ViewBag.Status = exchangeRequest.exchangeName + " Api not  Connected. Please try again";
            }
            ViewBag.Name = exchangeRequest.exchangeName;
            return PartialView("_Connected");
        }
        public PartialViewResult Import(string exchangeRequest)
        {
            string userId = User.Identity.GetUserId();
            Guid id;
            var getExchangeSettings = Guid.TryParse(userId, out id) ?
                               UserSettingCollection.GetExchangeSettingbyName(userId, exchangeRequest) :
                               new UserSettingCollection();

            ExchangeType eType = (ExchangeType)Enum.Parse(typeof(ExchangeType), exchangeRequest);
            List<IExchangeOrder> lst = new List<IExchangeOrder>();
            //IExchangeRequest
            
            IProvideExchangeData exchangeData = CoinTradePros.Exchange.ExchangeProviderFactory.GetExchangeProvider(eType);
            //ICollection < IExchangeOrder > Orders = exchanegData.GetOrders(exchangeRequest);
            
            foreach (var item in getExchangeSettings)
            {
                var x = item.SettingKey;
            }

            #region Comment

            //ICollection < IExchangeOrder > Orders = exchangeData.GetOrders();
            //if (Orders.Count > 0)
            //{
            //    foreach (var item in Orders)
            //    {
            //        exModel.Add(item);

            //    }
            //}else
            //{
            //    return PartialView("_ImportError");
            //}
            #endregion

            return PartialView("_Import", lst);

        }
    }

}