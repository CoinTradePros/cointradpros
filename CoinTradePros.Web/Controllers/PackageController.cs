﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using CoinTradePros.BusinessObjects;
using CoinTradePros.Web.Helpers;
using CoinTradePros.Web.Models;
using Tiraggo.Interfaces;

namespace CoinTradePros.Web.Controllers
{
    public partial class PackageController : Controller
    {
        // GET: Trade
        [Authorize(Roles = "ProTrader, Admin")]
        public virtual ActionResult Index()
        {
            ViewBag.Title = "Sales Packages";
            ICollection<PackageModel> packages = ModelHelper.FillPackagesForUser(IdentityHelper.GetCurrentUserId(User));
            return View(packages);
        }

        // GET: Trade/Details/5
        [Authorize(Roles = "Trader,ProTrader, Admin")]
        public virtual ActionResult Details(Guid packageid)
        {
            ViewBag.Title = "Package Details";
            PackageModel model = ModelHelper.FillPackage(packageid);
            return View(model);
        }

        // GET: Trade/Create
        [Authorize(Roles = "Trader,ProTrader, Admin")]
        public virtual ActionResult Create(Guid? proTraderProfileId)
        {
            PackageModel model = new PackageModel();
            if (proTraderProfileId.HasValue && proTraderProfileId != Guid.Empty)
            {
                model.ProxyProTraderProfileId = proTraderProfileId;
                model.ProTraderProfileId = proTraderProfileId;
            }
            else
            {
                model.ProTraderProfileId = IdentityHelper.GetCurrentUserProTraderId(User);
            }
            model.FeatureItems = ModelHelper.GetBaseFeatureSelectItems(model.ProTraderProfileId.Value);
            return View(model);
        }

        // POST: Trade/Create
        [HttpPost]
        [Authorize(Roles = "Trader,ProTrader")]
        public virtual ActionResult Create(PackageModel model)
        {
            try
            {
                bool isProxyMode = model.ProxyProTraderProfileId.HasValue &&
                                   model.ProxyProTraderProfileId != Guid.Empty;
                ViewBag.Title = "New Sales Package";
                if (!model.ProxyProTraderProfileId.HasValue && !model.ProxyProTraderProfileId.HasValue)
                {
                    model.ProTraderProfileId = IdentityHelper.GetCurrentUserProTraderId(User);
                }
                if (ModelState.IsValid)
                {
                    Package package = new Package();
                    package.Id = Guid.NewGuid();
                    package.AnnualPrice = model.AnnualPrice;
                    package.Description = model.Description;
                    package.LifetimePrice = model.LifetimePrice;
                    package.MonthlyPrice = model.MonthlyPrice;
                    package.Title = model.Title;
                    if (isProxyMode)
                    {
                        package.ProTraderProfileId = model.ProxyProTraderProfileId;
                    }
                    else
                    {
                        package.ProTraderProfileId = model.ProTraderProfileId;
                    }
                    ProTraderProfile profile = new ProTraderProfile();
                    profile.LoadByPrimaryKey(model.ProTraderProfileId ?? Guid.Empty);

                    Guid traderId = profile.TraderId ?? Guid.Empty;
                    UploadHelper uploadHelper = new UploadHelper(traderId, Server.MapPath("~/"));


                    package.IconPath = uploadHelper.GetFinalItemRelativePath(model.PackageIcon);
                    package.Save(tgSqlAccessType.DynamicSQL);

                    string[] selectedFeatures = { "" };
                    if (Request.Form["FeatureData"] != null)
                    {
                        selectedFeatures = Request.Form["FeatureData"].Split(',');
                    }
                    foreach (string f in selectedFeatures)
                    {
                        Guid fid;
                        if (Guid.TryParse(f, out fid))
                        {
                            PackageFeature pf = new PackageFeature();

                            pf.PackageId = package.Id;
                            pf.FeatureId = fid;
                            pf.Save(tgSqlAccessType.DynamicSQL);
                        }
                    }

                    if (model.IsProxyMode())
                    {
                        return RedirectToAction("Index", "SalesPage",
                            new { proTraderProfileId = model.ProxyProTraderProfileId });
                    }
                    return RedirectToAction("Index", "SalesPage");
                }
                if (model.IsProxyMode())
                {
                    model.ProTraderProfileId = model.ProxyProTraderProfileId;
                }
                else
                {
                    model.ProTraderProfileId = IdentityHelper.GetCurrentUserProTraderId(User);
                }
                model.FeatureItems = ModelHelper.GetBaseFeatureSelectItems(model.ProTraderProfileId ?? Guid.Empty);
                return View(model);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Exception", ex.ToString());
                FillDefaults(model);
                return View(model);
            }
        }

        // GET: Trade/Edit/5
        [Authorize(Roles = "Trader,ProTrader, Admin")]
        public virtual ActionResult Edit(Guid? packageid, Guid? proTraderProfileId)
        {
            PackageModel model;
            if (packageid.HasValue)
            {
                model = ModelHelper.FillPackage(packageid.Value);
            }
            else
            {
                model = new PackageModel();
            }
            if (proTraderProfileId.HasValue && proTraderProfileId != Guid.Empty)
            {
                model.ProxyProTraderProfileId = proTraderProfileId;
                model.ProTraderProfileId = proTraderProfileId;
            }
            else
            {
                model.ProTraderProfileId = IdentityHelper.GetCurrentUserProTraderId(User);
            }
            return View(model);
        }

        // POST: Trade/Edit/5
        [HttpPost]
        [Authorize(Roles = "Trader,ProTrader, Admin")]
        public virtual ActionResult Edit(PackageModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Package package = new Package();
                    if (model.PackageId.HasValue && package.LoadByPrimaryKey(model.PackageId.Value))
                    {
                        if (model.IsProxyMode())
                        {
                            package.ProTraderProfileId = model.ProxyProTraderProfileId;
                        }
                        else
                        {
                            package.ProTraderProfileId = model.ProTraderProfileId;
                        }

                        package.AnnualPrice = model.AnnualPrice;
                        package.Description = model.Description;
                        package.IconPath = model.IconPath;
                        package.Id = model.PackageId;
                        package.MonthlyPrice = model.MonthlyPrice;
                        package.AnnualPrice = model.AnnualPrice;
                        package.Title = model.Title;
                        package.LifetimePrice = model.LifetimePrice;
                        ProTraderProfile profile = new ProTraderProfile();
                        profile.LoadByPrimaryKey(package.ProTraderProfileId ?? Guid.Empty);

                        Guid traderId = profile.TraderId ?? Guid.Empty;
                        UploadHelper uploadHelper = new UploadHelper(traderId, Server.MapPath("~/"));
                        package.IconPath = uploadHelper.GetFinalItemRelativePath(model.PackageIcon);
                        package.Save(tgSqlAccessType.DynamicSQL);
                        if (Request.Form["FeatureData"] != null)
                        {
                            string[] selectedFeatures = Request.Form["FeatureData"].Split(',');
                            package.SaveFeatures(selectedFeatures);
                        }
                        if (model.IsProxyMode())
                        {
                            return RedirectToAction("Index", "SalesPage", new { proTraderProfileId = model.ProxyProTraderProfileId });
                        }
                        return RedirectToAction("Index", "SalesPage");
                    }
                    ModelState.AddModelError("PackageId", $"The package id {model.PackageId ?? Guid.Empty} was not found.");
                }
                else
                {
                    ModelState.AddModelError("General", "Please double check your package information for any errors.");
                }
                // repopulate the dropdowns and return to sender
                FillDefaults(model);
                return View(model);
            }
            catch (Exception ex)
            {
                // repopulate the dropdowns and return to sender
                ModelState.AddModelError("Unknown Error", ex);
                FillDefaults(model);
                return View(model);
            }
        }

        // GET: Trade/Delete/5
        [Authorize(Roles = "Trader,ProTrader")]
        public virtual ActionResult Delete(Guid? packageid, Guid? proTraderProfileId)
        {
            Package package = new Package();
            if (packageid.HasValue && package.LoadByPrimaryKey(packageid.Value))
            {
                // The PackageFeature items related to this package are deleted via cascade.
                package.MarkAsDeleted();
                package.Save(tgSqlAccessType.DynamicSQL);
            }
            if (proTraderProfileId.HasValue && proTraderProfileId != Guid.Empty)
            {
                return RedirectToAction("Index", "SalesPage", new { proTraderProfileId });

            }
            return RedirectToAction("Index", "SalesPage");
        }

        //private PackageModel GetPackageModel(Guid? packageid)
        //{
        //    Package package = new Package();
        //    PackageModel model = new PackageModel();

        //    // attempt to load the trade with the id given.
        //    if (packageid.HasValue && package.LoadByPrimaryKey(packageid.Value))
        //    {
        //        model.AnnualPrice = package.AnnualPrice;
        //        model.CurrentSubscriberCount = package.GetCurrentSubscriberCount();
        //        model.Description = package.Description;
        //        model.LifetimePrice = package.LifetimePrice;
        //        model.MonthlyPrice = package.MonthlyPrice;
        //        model.PackageId = package.Id;
        //        model.Title = package.Title;
        //        if (package.ProTraderProfileId.HasValue)
        //        {
        //            model.ProTraderProfileId = package.ProTraderProfileId;
        //        }
        //        else
        //        {
        //            model.ProTraderProfileId = IdentityHelper.GetCurrentUserProTraderId(User);
        //        }
        //        FillDefaults(model);
        //    }
        //    else
        //    {
        //        model.ProTraderProfileId = IdentityHelper.GetCurrentUserProTraderId(User);
        //    }
        //    FillDefaults(model);
        //    return model;
        //}

        private void FillDefaults(PackageModel model)
        {
            if (model.ProxyProTraderProfileId.HasValue && model.ProxyProTraderProfileId != Guid.Empty)
            {
                model.ProTraderProfileId = model.ProxyProTraderProfileId;
            }
            else
            {
                model.ProTraderProfileId = IdentityHelper.GetCurrentUserProTraderId(User);
            }
            model.FeatureItems = ModelHelper.GetBaseFeatureSelectItems(model.ProTraderProfileId.Value);
        }
    }
}
