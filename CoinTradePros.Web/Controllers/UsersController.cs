﻿using System;
using System.Web.Mvc;
using CoinTradePros.Web.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using CoinTradePros.BusinessObjects;
using System.Collections.Generic;
using CoinTradePros.Web.Helpers;
using System.Linq;

namespace CoinTradePros.Web.Controllers
{
    [Authorize]
    public partial class UsersController : Controller
    {
        // GET: Users
        public Boolean isAdminUser()
        {
            if (User.Identity.IsAuthenticated)
            {
                var user = User.Identity;
                ApplicationDbContext context = new ApplicationDbContext();
                var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                var s = UserManager.GetRoles(user.GetUserId());
                if (s[0].ToString() == "Admin")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return false;
        }
        public virtual ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                var user = User.Identity;
                ViewBag.Name = user.Name;
                return View();
            }
            return RedirectToAction("Login", "Account", new { returnUrl = Url.Action("Index", "Users") });
        }
        public ActionResult GetSubscribersforChatBar()
        {


            Guid id = IdentityHelper.GetCurrentUserId(User);
            AspNetUsers user = new AspNetUsers();
            user = AspNetUsers.GetUserByUserId(id.ToString());

            SubscriptionViewCollection SubRec = new SubscriptionViewCollection();

            List<ChatBarViewModel> chatViewModel = new List<ChatBarViewModel>();



            if (User.IsInRole("ProTrader"))
            {
                SubRec = SubscriptionView.GetSubscribers(id);
                foreach (var item in SubRec)
                {
                    bool DuplicateFlag = chatViewModel.Any(x => x.UserName == item.SubscriberUsername);
                    if (!DuplicateFlag && item.SubscriberUserId != id.ToString())
                    {
                        ChatBarViewModel TempModel = new ChatBarViewModel();
                        string TempFullName = (item.SubscriberFirstName + " " + item.SubscriberLastName);
                        string TempUsername = item.SubscriberUsername;
                        if (TempFullName != null && TempUsername != null)
                        {
                            TempModel.Name = TempFullName;
                            TempModel.UserName = TempUsername;
                            chatViewModel.Add(TempModel);
                        }
                        if (!string.IsNullOrWhiteSpace(item.SubscriberUserId) && !string.IsNullOrWhiteSpace(item.SubscriberUsername))
                        {
                            TraderProfile tp = TraderProfile.GetByUserId(item.SubscriberUserId);
                            TempModel.ImageUrl = tp.PortraitPath;
                        }
                    }
                }
            }
            else
            {
                ChatBarViewModel TempModel = new ChatBarViewModel();
                SubRec = SubscriptionView.GetSubscriptions(id);
                foreach (var item in SubRec)
                {
                    bool DuplicateFlag = chatViewModel.Any(x => x.UserName == item.ProTraderUsername);
                    if (!DuplicateFlag && item.ProTraderUserId != id.ToString())
                    {
                        string TempFullName = (item.ProTraderFirstName + " " + item.ProTraderLastName);
                        string TempUsername = item.ProTraderUsername;
                        if (TempFullName != null && TempUsername != null)
                        {
                            TempModel.Name = TempFullName;
                            TempModel.UserName = TempUsername;
                            chatViewModel.Add(TempModel);
                        }
                        TraderProfile tp = TraderProfile.GetByUserId(item.ProTraderUserId);
                        TempModel.ImageUrl = tp.PortraitPath;
                    }
                }
            }

            return PartialView("_GetSubscribers", chatViewModel);
        }

        //public ActionResult GetSubscribersforChatBar()
        //{
         

        //     Guid id = IdentityHelper.GetCurrentUserId(User);



        //    SubscriptionViewCollection SubRec = new SubscriptionViewCollection();
         
        //    List<ChatBarViewModel> chatViewModel = new List<ChatBarViewModel>();
    


        //    if (User.IsInRole("ProTrader"))
        //    {
                
        //        SubRec = SubscriptionView.GetSubscribers(id);
        //        foreach (var item in SubRec)
        //        {
        //            ChatBarViewModel TempModel = new ChatBarViewModel();
        //            string TempFullName = (item.SubscriberFirstName + " " + item.SubscriberLastName);
        //            string TempUsername = item.SubscriberUsername;
        //               if (TempFullName != null && TempUsername != null)
        //                {

        //                    TempModel.Name = TempFullName;
        //                    TempModel.UserName = TempUsername;
        //                    chatViewModel.Add(TempModel);
        //                }
        //                if (!string.IsNullOrWhiteSpace(item.SubscriberUserId) && !string.IsNullOrWhiteSpace(item.SubscriberUsername))
        //                {
        //                    TraderProfile tp = TraderProfile.GetByUserId(item.SubscriberUserId);
        //                    TempModel.ImageUrl = tp.PortraitPath;
        //                }
                    
                   
                     
        //        }
        //    }
        //    else
        //    {
        //        ChatBarViewModel TempModel = new ChatBarViewModel();
        //        SubRec = SubscriptionView.GetSubscriptions(id);
        //        foreach (var item in SubRec)
        //        {
        //            string TempFullName = (item.ProTraderFirstName + " " + item.ProTraderLastName);
        //            string TempUsername = item.ProTraderUsername;
        //            if (TempFullName != null && TempUsername != null)
        //            {
                      
        //                TempModel.Name = TempFullName;
        //                TempModel.UserName = TempUsername;
        //                chatViewModel.Add(TempModel);
        //            }

        //            TraderProfile tp = TraderProfile.GetByUserId(item.ProTraderUserId);
        //            TempModel.ImageUrl = tp.PortraitPath;
        //        }
        //    }

          
        //    return PartialView("_GetSubscribers",chatViewModel);
        //}


        public ActionResult RetrieveOldMessages(string username)
        {
            string myid = User.Identity.GetUserId();
            AspNetUsers friend = AspNetUsers.GetUserByUserName(username);
            string friendId = friend.Id;



            ApplicationDbContext context = new ApplicationDbContext();
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            var s = UserManager.GetRoles(myid);

            Guid? svid;

            if (s[0].ToString() == "ProTrader")
            {
                svid = SubscriptionView.GetSubscriptionId(friend.Id, myid);
            }
            else
            {
                svid = SubscriptionView.GetSubscriptionId(myid, friend.Id);
            }

            TraderProfile tp = TraderProfile.GetByUserId(friendId);
            string hisname = tp.FirstName + " " + tp.LastName;

            //string ssvid = svid.ToString();
            ChatRecordCollection crc = ChatRecord.RetrieveMessages(svid);
            List<PrivateChatViewModel> pcvLst = new List<PrivateChatViewModel>();
            foreach (var item in crc)
            {
                PrivateChatViewModel pcv = new PrivateChatViewModel();

                pcv.Message = item.Message;
                pcv.Time = string.Format("{0:hh\\:mm\\:ss}", item.SenderTime);
             
                //pcv.Date = item.SenderDate.ToString();
                if (item.SenderID == myid)
                {
                    pcv.MessageStatus = "sent";
                    pcv.Name = "You";

                }
                else
                {
                    pcv.MessageStatus = "recive";
                    pcv.Name = hisname;

                }
                pcvLst.Add(pcv);
            }
            return PartialView("_RetrieveOldMessages", pcvLst);
        }

        public ActionResult Notifications()
        {
            string myid = User.Identity.GetUserId();

            ChatRecordCollection crc = new ChatRecordCollection();
            crc = ChatRecord.UnreadMessages(myid);
            //foreach (var item in collection)
            //{

            //}

            return View();
        }

    }
}