﻿using System.Threading.Tasks;
using System.Web.Mvc;
using CoinTradePros.Messaging.Telegram;
using CoinTradePros.Web.Models;

namespace CoinTradePros.Web.Controllers
{
    public partial class TelegramController : Controller
    {
        // GET: Telegram



        [HttpPost]
        public virtual async Task<ActionResult> SendCode()
        {
            string number = Request["PhoneNumber"];
            TelegramNotifier notifier = new TelegramNotifier();
            string hash = await notifier.SendCode(number);
            return RedirectToAction("VerifyCode", new { PhoneNumber = number });
        }

        public virtual ActionResult VerifyCode(string phoneNumber)
        {
            ViewBag.PhoneNumber = phoneNumber;
            return View();
        }

        [HttpPost]
        public virtual async Task<ActionResult> VerifyCode(NumberVerifyModel model)
        {
            string number = model.PhoneNumber;
            TelegramNotifier notifier = new TelegramNotifier();
            //await notifier.VerifyCode(model.);
            return View();

        }
    }
}