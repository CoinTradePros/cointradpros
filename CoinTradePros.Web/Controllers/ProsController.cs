﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.Caching;
using System.Web.Mvc;
using CoinTradePros.BusinessObjects;
using CoinTradePros.BusinessObjects.Calc;
using CoinTradePros.Web.Helpers;
using CoinTradePros.Web.Models;
using Microsoft.Ajax.Utilities;

namespace CoinTradePros.Web.Controllers
{
    public partial class ProsController : Controller
    {
        // GET: Pros
        public virtual ActionResult Index()
        {
            ProTraderGroupModel model = new ProTraderGroupModel();
            ICollection<ProTraderModel> models = new List<ProTraderModel>();
            // TODO:  Write a view or something to filter and get all this data in one shot...
            ProTraderProfileCollection coll = new ProTraderProfileCollection();

            ObjectCache cache = MemoryCache.Default;
            ICollection<ProTraderModel> sortedModels;
            if (cache["coaches"] != null)
            {
                sortedModels = (ICollection<ProTraderModel>)cache["coaches"];
                model.ProTraders = sortedModels;
                if (User.Identity.IsAuthenticated)
                {
                    var user = User.Identity;
                    ViewBag.Name = user.Name;

                }
                return View(model);
            }
            coll.LoadAll();
            foreach (var profile in coll)
            {
                PackageCollection packages = profile.GetPackages();
                if (packages.Count > 0)
                {
                    //  For now, just grab anyone who has a package in the profile.
                    AspNetUsers user = profile.GetUserAccount();
                    TraderProfile trader = profile.GetTraderProfile();
                    ICollection<ITradeItem> trades = profile.GetTrades().Cast<ITradeItem>().ToList();
                    TradeCalculator calc = new TradeCalculator(trades);

                    ProTraderModel ptm = new ProTraderModel();
                    ptm.AvatarUrl = IdentityHelper.GetTraderAvatarUrl(trader);
                    ptm.ProTraderProfileId = profile.Id ?? Guid.Empty;

                    ptm.FullName = $"{trader.FirstName} {trader.LastName}";
                    ptm.Username = user.UserName;
                    if (!string.IsNullOrEmpty(profile.Description) && profile.Description.Length > 200)
                    {
                        ptm.ProfileDescription = profile.Description.Substring(0, 197) + "...";
                    }
                    else
                    {
                        ptm.ProfileDescription = profile.Description;
                    }
                    ptm.ProfileTagline = profile.TagLine;
                    ptm.Packages = ModelHelper.FillPackages(packages);
                    ptm.ProfitAmount = calc.GetTotalNetProfit();
                    ptm.ProfitPercent = calc.GetTotalProfitPercent();
                    ptm.TradeCount = trades.Count;

                    models.Add(ptm);
                }
            }
            sortedModels = models.OrderByDescending(o => o.ProfitAmount).ToList();
            cache["coaches"] = sortedModels;
            model.ProTraders = sortedModels;
            if (User.Identity.IsAuthenticated)
            {
                var user = User.Identity;
                ViewBag.Name = user.Name;
                
            }
            return View(model);
        }

        [Authorize(Roles = "Admin")]
        public virtual ActionResult ShowPros()
        {
            ProTraderGroupModel model = new ProTraderGroupModel();
            ICollection<ProTraderModel> models = new List<ProTraderModel>();
            // TODO:  Write a view or something to filter and get all this data in one shot...
            ProTraderProfileCollection coll = new ProTraderProfileCollection();
            coll.LoadAll();
            foreach (var profile in coll)
            {
                PackageCollection packages = profile.GetPackages();
                //  For now, just grab anyone who has a package in the profile.
                AspNetUsers user = profile.GetUserAccount();
                TraderProfile trader = profile.GetTraderProfile();
                ICollection<ITradeItem> trades = profile.GetTrades().Cast<ITradeItem>().ToList();
                TradeCalculator calc = new TradeCalculator(trades);

                ProTraderModel ptm = new ProTraderModel();
                ptm.AvatarUrl = IdentityHelper.GetTraderAvatarUrl(trader);
                ptm.ProTraderProfileId = profile.Id ?? Guid.Empty;

                ptm.FullName = $"{trader.FirstName} {trader.LastName}";
                ptm.Username = user.UserName;
                if (!string.IsNullOrEmpty(profile.Description) && profile.Description.Length > 200)
                {
                    ptm.ProfileDescription = profile.Description.Substring(0, 197) + "...";
                }
                else
                {
                    ptm.ProfileDescription = profile.Description;
                }
                ptm.ProfileTagline = profile.TagLine;
                ptm.VideoEmbed = profile.VideoEmbed;
                ptm.Packages = ModelHelper.FillPackages(packages);
                ptm.ProfitAmount = calc.GetTotalNetProfit();
                ptm.ProfitPercent = calc.GetTotalProfitPercent();
                ptm.TradeCount = trades.Count;

                models.Add(ptm);
            }
            if (User.Identity.IsAuthenticated)
            {
                var user = User.Identity;
                ViewBag.Name = user.Name;

            }
            model.ProTraders = models;
            return View(model);
        }

        [Authorize(Roles = "Admin")]
        public virtual ActionResult Create()
        {

            return View();

        }

        [Authorize(Roles = "Admin")]
        public virtual ActionResult Create(RegisterViewModel model)
        {
            return View();
        }


        public virtual ActionResult Edit(Guid proTraderProfileId)
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public virtual ActionResult Edit(SalesPageSettingsModel model)
        {
            return View(model);
        }


        public virtual ActionResult Details(Guid id)
        {
            ProTraderProfile profile = new ProTraderProfile();
            profile.LoadByPrimaryKey(id);

            return View(profile);
        }
    }
}