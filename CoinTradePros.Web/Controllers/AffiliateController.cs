﻿using System.Web.Mvc;

namespace CoinTradePros.Web.Controllers
{
    public partial class AffiliateController : Controller
    {
        // GET: Affiliate
        public virtual ActionResult Index()
        {
            return View();
        }

        // GET: Affiliate/Details/5
        public virtual ActionResult Details(int id)
        {
            return RedirectToAction("Index");
        }

        // GET: Affiliate/Create
        public virtual ActionResult Create()
        {
            return RedirectToAction("Index");
        }

        // POST: Affiliate/Create
        [HttpPost]
        public virtual ActionResult Create(FormCollection collection)
        {
            return RedirectToAction("Index");
        }

        // GET: Affiliate/Edit/5
        public virtual ActionResult Edit(int id)
        {
            return RedirectToAction("Index");
        }

        // POST: Affiliate/Edit/5
        [HttpPost]
        public virtual ActionResult Edit(int id, FormCollection collection)
        {
           
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                // return View();
            }
            return RedirectToAction("Index");
        }

        // GET: Affiliate/Delete/5
        public virtual ActionResult Delete(int id)
        {
            return RedirectToAction("Index");
        }

        // POST: Affiliate/Delete/5
        [HttpPost]
        public virtual ActionResult Delete(int id, FormCollection collection)
        {
            return RedirectToAction("Index");
        }
    }
}
