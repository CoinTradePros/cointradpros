﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CoinTradePros.BusinessObjects;
using CoinTradePros.BusinessObjects.Calc;
using CoinTradePros.Web.Helpers;
using CoinTradePros.Web.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Tiraggo.Interfaces;

namespace CoinTradePros.Web.Controllers
{
    public partial class ProController : Controller
    {
        public virtual ActionResult Updated(string username)
        {
            return Index(username);
        }


        // GET: Pro

        public virtual ActionResult Index(string username)
        {
            
            string defaultpage = ConfigurationManager.AppSettings["DefaultSalesPage"];
            if (string.IsNullOrEmpty(username))
            {
                username = defaultpage;
            }
            if (User.Identity.IsAuthenticated)
            {
                var user1 = User.Identity;
                ViewBag.Name = user1.Name;

            }
            ApplicationUser user = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>().FindByName(username);

            Guid userId;
            if (user == null || !Guid.TryParse(user.Id, out userId))
            {
                
                return RedirectToAction("Index", "Pro", new { username = defaultpage });
            }

            TraderProfile traderProfile = TraderProfile.GetTraderProfileFromUserId(userId);
            ProTraderProfile proTraderProfile = ProTraderProfile.GetFromUserId(userId);
            if (User.IsInRole("ProTrader") && proTraderProfile.Id == null)
            {
                return RedirectToAction("Index", "Pro", new { username = defaultpage });
            }

            ICollection<Package> packages = Package.GetProTraderPackagesByUserId(userId);
            ICollection<Feature> allFeatures = Feature.GetProTraderFeatures(proTraderProfile.Id ?? Guid.Empty);

            FeatureTypeCollection featureTypes = new FeatureTypeCollection();
            featureTypes.LoadAll(tgSqlAccessType.DynamicSQL);



            ICollection<ITradeItem> trades = proTraderProfile.GetTrades().Cast<ITradeItem>().ToList();
            TradeCalculator calc = new TradeCalculator(trades);
            SalesPageModel model = new SalesPageModel();
            model.ProfitPercent = calc.GetTotalProfitPercent();
            model.ProfitAmount = calc.GetTotalNetProfit();
            model.TradeCount = trades.Count;
            model.Username = username;
            model.Tagline = proTraderProfile.TagLine;
            model.Description = proTraderProfile.Description;
            model.FirstName = traderProfile.FirstName;
            model.LastName = traderProfile.LastName;
            model.IsProTraderViewing = userId == proTraderProfile.Id;
            model.VideoUrl = proTraderProfile.VideoEmbed;
            model.AvatarUrl = IdentityHelper.GetAvatarUrlFromTraderId(traderProfile.Id ?? Guid.Empty);

            model.ProfileDetails = ModelHelper.FillProfileDetails(traderProfile.Id ?? Guid.Empty);

            ICollection<FeatureModel> allFeatureModels = new List<FeatureModel>();
            ICollection<PackageModel> packageModels = new List<PackageModel>();
            if (Session["AlternateCurrency"] != null)
            {
                // convert all the package prices into the selected currency
                model.AlternateCurrency = (string)Session["AlternateCurrency"];
                model.UseAlternateCurrency = true;
            }
            else
            {
                model.UseAlternateCurrency = false;
            }

            foreach (Feature feature in allFeatures)
            {
                FeatureModel m = new FeatureModel();
                m.ProTraderProfileId = feature.ProTraderProfileId;
                m.Description = feature.Description;
                m.FeatureId = feature.Id;
                m.FeatureTypeId = feature.FeatureTypeId;
                if (feature.FeatureTypeId.HasValue)
                {
                    FeatureType ft = featureTypes.FindByPrimaryKey(feature.FeatureTypeId.Value);
                    FeatureTypeModel fmodel = new FeatureTypeModel();
                    fmodel.Description = ft.Description;
                    fmodel.DefaultIconClass = ft.DefaultIconClass;
                    fmodel.FeatureProcessor = ft.FeatureProcessor;
                    fmodel.FeatureTypeId = ft.Id ?? Guid.Empty;
                    fmodel.Name = ft.Name;
                    fmodel.DefaultIconClass = ft.DefaultIconClass;
                    fmodel.RequiresProcessing = ft.RequiresProcessing ?? false;
                    m.SelectedFeatureType = fmodel;
                }
                m.Name = feature.Name;
                m.FeatureIconUrl = feature.IconPath;
                m.SortOrder = feature.SortOrder;
                allFeatureModels.Add(m);
            }
            foreach (var package in packages)
            {
                PackageModel p = ModelHelper.FillPackage(package);
                p.UseAlternateCurrency = model.UseAlternateCurrency;
                p.AlternateCurrency = model.AlternateCurrency;
                p.Username = username;
                packageModels.Add(p);
            }

            model.Questions = ModelHelper.FillQuestions(proTraderProfile.Id);
            model.Packages = packageModels;
            model.AllFeatures = allFeatureModels;
            if (model.UseAlternateCurrency)
            {
                model.PaymentCurrencies = ModelHelper.GetPaymentCoins(model.AlternateCurrency);
            }
            else
            {
                model.PaymentCurrencies = ModelHelper.GetPaymentCoins(string.Empty);
            }
            

            return View(model);
        }
    }
}