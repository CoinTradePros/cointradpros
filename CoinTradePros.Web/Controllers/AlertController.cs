﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.Mvc;
using CoinTradePros.Adapters;
using CoinTradePros.Adapters.Factory;
using CoinTradePros.BusinessObjects;
using CoinTradePros.Interfaces.Messaging;
using CoinTradePros.Interfaces.Messaging.Events;
using CoinTradePros.Messaging.Factory;
using CoinTradePros.Web.Helpers;
using CoinTradePros.Web.Models;
using Microsoft.AspNet.Identity;
using Tiraggo.Interfaces;
using CoinTradePros.Messaging.Twilio;
using CoinTradePros.Web.Hubs;
using Microsoft.AspNet.SignalR;
using System.Web.Security;

namespace CoinTradePros.Web.Controllers
{
    public partial class AlertController : AsyncController
    {
        [ValidateAntiForgeryToken]
        [HttpPost]
        public void SendManualAlertToPhoneAndChat(SubscriberAlertModel sam, Message message, ICollection<Guid> PackageIdList)
        {
            //bool includeChart = Request["IncludeChart"] != null && Request["IncludeChart"] == "on";
            //bool uploadChart = Request["UploadChart"] != null && Request["UploadChart"] == "on";

            Guid MyId = IdentityHelper.GetCurrentUserId(User);
            Guid MyProTraderProfileId = IdentityHelper.GetCurrentUserProTraderId(User);
            TradeView tv = new TradeView();

            if (!(sam.TradeView.TradeId.HasValue))
            {

                Trade trade = new Trade();
                trade.Id = Guid.NewGuid();
                trade.PositionSize = sam.TradeView.PositionSize;
                trade.ProTraderProfileId = MyProTraderProfileId;
                trade.AssetId = sam.TradeView.AssetKey;
                trade.Comments = sam.TradeView.Comments;
                trade.DenominationId = sam.TradeView.DemoninationKey;
                trade.EntryDate = sam.TradeView.EntryDate;
                trade.EntryPrice = sam.TradeView.EntryPrice;
                trade.ExchangeId = sam.TradeView.ExchangeId;
                //trade.ExitDate = model.ExitDate;
                //trade.ExitPrice = model.ExitPrice;


                Guid mytraderid = IdentityHelper.GetCurrentUserTraderId(User);
                trade.TraderId = mytraderid;
                UploadHelper uploadHelper = new UploadHelper(mytraderid, Server.MapPath("~/"));
                if (sam.ChartImage != null)
                {
                    trade.ChartImagePath = uploadHelper.GetFinalItemRelativePath(sam.ChartImage);
                }
                trade.Save(Tiraggo.Interfaces.tgSqlAccessType.DynamicSQL);
                tv = TradeView.LoadByTradeId(trade.Id.HasValue ? trade.Id.Value : new Guid());
            }
            else
            {
                tv = TradeView.LoadByTradeId(sam.TradeView.TradeId.HasValue ? sam.TradeView.TradeId.Value : new Guid());
            }

            string username = tv.TraderUserName;
            //FIX UNIT AND UNIT PRICE ISSUE
            //string unit = tv;
            //string unit_price = "";
            string trade_time = string.Format("{0:MM/dd/yyyy hh:mm tt PST}", tv.EntryDate);
            string comments = tv.Comments;
            string msg = "CoinTradePros Subscriber Alert from " + username + " at " + trade_time + " " + comments;

            //string packagename = Request["AlertGroup"];
            //ICollection<Guid> PackageIdList = AlertHelper.GetPackageIds(packagename);

            SubscriptionViewCollection svc = new SubscriptionViewCollection();
            foreach (Guid PackageId in PackageIdList)
            {
                SubscriptionViewCollection tempsvc = new SubscriptionViewCollection();
                tempsvc = SubscriptionView.GetSubscribersByPackage(PackageId);
                foreach (SubscriptionView item in tempsvc)
                {
                    svc.Add(item);
                }
            }



            foreach (var item in svc)
            {
                if (item.SubscriberUserId != null && item.SubscriberUserId != MyId.ToString())
                {
                    AspNetUsers ToUser = new AspNetUsers();
                    ToUser = AspNetUsers.GetUserByUserId(item.SubscriberUserId.ToString());

                    //SEND ALERT TO PHONE-START
                    if ((!string.IsNullOrEmpty(ToUser.PhoneNumber)) && (ToUser.PhoneNumberConfirmed == true))
                    {
                        TwilioNotificationService tc = new TwilioNotificationService();
                        tc.SendSmsNotification(ToUser.PhoneNumber, msg, string.Empty);
                    }
                    //SEND ALERT TO PHONE-END


                    //SEND ALERT TO CHAT-START
                    Guid? Subscriptionid;
                    Subscriptionid = SubscriptionView.GetSubscriptionId(ToUser.Id, MyId.ToString());

                    string MessageText = "ALERT: " + msg;
                    ChatRecord.AddMessage(MyId.ToString(), ToUser.Id, MessageText, false, Subscriptionid);

                    var hubContext = GlobalHost.ConnectionManager.GetHubContext<ChatHub>();

                    ConnectionTableCollection connectionlist = ConnectionTable.GetConnectionId(ToUser.Id);
                    if (connectionlist != null)
                    {
                        foreach (var conid in connectionlist)
                        {

                            TraderProfile temptp = TraderProfile.GetByUserId(MyId.ToString());
                            TraderProfile tp = TraderProfile.GetByUserId(ToUser.Id);
                            string tofullname = tp.FirstName + " " + tp.LastName;
                            var abcContext = GlobalHost.ConnectionManager.GetHubContext<ChatHub>();
                            string x = conid.ConnectionID.ToString();
                            hubContext.Clients.Client(x).appendmsg(MessageText, "received", tofullname, username, temptp.PortraitPath, message.SendDate);
                            //hubContext.Clients.Client(x).check();

                        }
                    }
                    //SEND ALERT TO CHAT-END
                }
            }

        }


        public void SendAlertAsync(AlertModel alertModel)
        {
            AsyncManager.OutstandingOperations.Increment();

            Message message = new Message();
            message.Id = Guid.NewGuid();
            message.DataSourceId = alertModel.DataSourceId;
            message.SendDate = DateTimeOffset.UtcNow;
            message.SenderId = alertModel.SenderId;
            message.MessageType = alertModel.MessageType.ToString();
            message.Save(tgSqlAccessType.DynamicSQL);

            ISupplyMessageData dataItem = (ISupplyMessageData)TempData["TradeView"];
            IDictionary<string, string> items = (IDictionary<string, string>)TempData["MessageFields"];

            IProvideMessagingSource alertSource =
                MessagingSourceProviderFactory.GetInstance(dataItem,
                    MessagingHelper.GetTransmitterTypes(alertModel.AlertChannels), MessagingType.TradeAlert,
                    AlertHelper.GetPackageIds(alertModel.AlertGroups));

            IProcessMessages processor = MessageProcessorFactory.GetInstance();
            processor.MessageProcessingCompleted += MessageProcessingComplete;
            foreach (var field in items)
            {
                processor.AddMessageField(field.Key, field.Value);
            }
            processor.ProcessMessageAsync(alertSource.GetMessagingSource());
        }

        [System.Web.Mvc.Authorize(Roles = "ProTrader")]
        public ActionResult SendAlertCompleted(Guid messageId)
        {
            AlertDetailModel model = new AlertDetailModel();
            Message message = new Message();

            if (message.LoadByPrimaryKey(messageId))
            {
                model.MessageId = messageId;
                model.ProcessingTime = message.GetProcessingSpan();
            }

            return View("AlertDetails", model);
        }

        [HttpPost]
        [System.Web.Mvc.Authorize(Roles = "ProTrader")]
        public void SendManualAlertAsync(SubscriberAlertModel sam)
        {
            bool includeChart = Request["IncludeChart"] != null && Request["IncludeChart"] == "on";
            bool uploadChart = Request["UploadChart"] != null && Request["UploadChart"] == "on";

            AsyncManager.OutstandingOperations.Increment();
            string packages = Request["AlertGroup"];
            string messagingGroups = Request["MessagingGroup"];
            Message message = new Message();
            message.Id = Guid.NewGuid();
            message.DataSourceId = null;
            message.MessageType = MessagingType.ManualAlert.ToString();
            message.SendDate = DateTimeOffset.UtcNow;
            message.SenderId = User.Identity.GetUserId();

            if (sam.TradeView.TradeId.HasValue)
            {
                TradeView tv = new TradeView();
                tv = TradeView.LoadByTradeId(sam.TradeView.TradeId.HasValue ? sam.TradeView.TradeId.Value : new Guid());
                string trade_time = string.Format("{0:MM/dd/yyyy hh:mm tt PST}", tv.EntryDate);

                message.SubmittedText = "CoinTradePros Subscriber Alert from " + tv.TraderUserName + " at " + trade_time + " " + tv.Comments;
            }
            else
            {
                //Review it for new trade
                message.SubmittedText = sam.SendAlertModel.AlertBody;

                if (sam.SendAlertModel.IncludeChart && !string.IsNullOrEmpty(Request["ImageUrl"]))
                {
                    Guid proTraderProfileId = IdentityHelper.GetCurrentUserProTraderId(User);
                    UploadHelper helper = new UploadHelper(proTraderProfileId, Server.MapPath("~/"));
                    string imagePath = helper.SaveFromImagrUrl(message.Id ?? Guid.Empty, Request["ImageUrl"]);
                    message.SubmittedImagePath = imagePath;
                }
                else if (uploadChart && sam.ChartImage != null)
                {
                    Guid proTraderProfileId = IdentityHelper.GetCurrentUserProTraderId(User);
                    UploadHelper helper = new UploadHelper(proTraderProfileId, Server.MapPath("~/"));
                    //string fileName = System.IO.Path.GetFileName(alertModel.ChartImage.FileName);
                    //alertModel.ChartImage.SaveAs(Server.MapPath("~/Uploads/") + fileName);
                    //message.SubmittedImagePath = Server.MapPath("~/Uploads/") + fileName;
                    message.SubmittedImagePath = helper.GetFinalItemRelativePath(sam.ChartImage);
                }
                else { };

            }
            message.Save(tgSqlAccessType.DynamicSQL);

            ICollection<Guid> PackageIdList = AlertHelper.GetPackageIds(packages);

            IProvideMessagingSource alertSource =
                MessagingSourceProviderFactory.GetInstance(message, MessagingHelper.GetTransmitterTypes(messagingGroups), MessagingType.ManualAlert, PackageIdList);

            IProcessMessages processor = MessageProcessorFactory.GetInstance();
            processor.MessageProcessingCompleted += MessageProcessingComplete;

            string moreUrl = $"{Url.Action("Details", "Alert", new { alertid = message.Id })}";
            string chartUrl = $"{message.SubmittedImagePath}";

            //--FUNCTION TO SEND MESSAGE TO CHAT AND PHONE
            SendManualAlertToPhoneAndChat(sam, message, PackageIdList);

            processor.AddMessageField("MoreInfoUrl", moreUrl);
            processor.AddMessageField("ChartImageUrl", chartUrl);
            processor.ProcessMessageAsync(alertSource.GetMessagingSource());
        }

        [System.Web.Mvc.Authorize(Roles = "ProTrader")]
        public ActionResult SendManualAlertCompleted(Guid messageId)
        {
            AlertDetailModel model = new AlertDetailModel();
            Message message = new Message();
            if (message.LoadByPrimaryKey(messageId))
            {
                model.MessageId = messageId;
                model.ProcessingTime = message.GetProcessingSpan();
            }
            return View("AlertDetails", model);
        }

        private void MessageProcessingComplete(object sender, MessageProcessingCompleteArgs e)
        {
            Message message = new Message();
            if (message.LoadByPrimaryKey(e.MessageId))
            {
                message.CompletedDate = DateTimeOffset.UtcNow;
                message.ErrorCount = e.Results.GetErrors().Count;
                message.Save(tgSqlAccessType.DynamicSQL);
            }
            AsyncManager.Parameters.Add("messageId", e.MessageId);
            AsyncManager.OutstandingOperations.Decrement();
        }

        [System.Web.Mvc.Authorize(Roles = "ProTrader")]
        public virtual ActionResult Details(Guid messageId)
        {
            return View();
        }
    }
}