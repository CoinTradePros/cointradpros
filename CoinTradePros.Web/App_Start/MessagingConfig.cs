﻿using System.Threading.Tasks;
using CoinTradePros.Messaging.Discord;

namespace CoinTradePros.Web
{
	public class MessagingConfig
	{
		// For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
		public static void InitMessaging()
		{
			// Do any messaging related startup tasks.
            DiscordNotifier.Init();
		}
	}
}
