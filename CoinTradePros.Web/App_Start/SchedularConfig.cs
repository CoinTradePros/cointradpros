﻿using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CoinTradePros.Payments;
using System.Collections.Specialized;
using System.Threading.Tasks;
using System.Configuration;

namespace CoinTradePros.Web
{
    public class SchedularConfig
    {
        public static void Start()
        {
            RunSchedular().GetAwaiter().GetResult();
        }

        private static async Task RunSchedular()
        {
            try
            {
                // Grab the Scheduler instance from the Factory
                NameValueCollection props = new NameValueCollection
                {
                    { "quartz.serializer.type", "binary" }
                };
                StdSchedulerFactory factory = new StdSchedulerFactory(props);
                IScheduler scheduler = await factory.GetScheduler();

                // and start it off
                await scheduler.Start();

                // define the job and tie it to our HelloJob class
                IJobDetail job = JobBuilder.Create<PaymentDueNotifier>()
                    .WithIdentity("jobpaymentnotifier", "jobpaymentgroup")
                    .Build();

                // Trigger the job to run now, and then repeat every 10 seconds
                ITrigger trigger = TriggerBuilder.Create()
                        .WithIdentity("paymentnotifytrigger", "paymentnotifygroup")
                        .WithSchedule(CronScheduleBuilder.DailyAtHourAndMinute(int.Parse(ConfigurationManager.AppSettings["PaymentNotifyTime"]), 0))
                        .ForJob(job)
                        .Build();
          
                // Tell quartz to schedule the job using our trigger
                await scheduler.ScheduleJob(job, trigger);

                // some sleep to show what's happening
                // await Task.Delay(TimeSpan.FromSeconds(60));

                // and last shut down the scheduler when you are ready to close your program
                await scheduler.Shutdown();
            }
            catch (SchedulerException se)
            {

                Console.WriteLine(se);
            }
        }

    }
}