﻿using System.Web.Optimization;

namespace CoinTradePros.Web
{
	public class BundleConfig
	{
		// For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
		public static void RegisterBundles(BundleCollection bundles)
		{
			bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
						"~/Scripts/jquery-3.2.1.min.js",
                        //"~/Scripts/jquery.signalR-2.2.2.min.js",
                        
                        "~/Scripts/moment-with-locales.min.js",
                        "~/Scripts/ie10-viewport-bug-workaround.js",
                        "~/Scripts/tempusdominus-bootstrap-4.min.js",
			            "~/Scripts/jquery.signalR-2.2.2.min.js",
                        "~/Scripts/knockout-3.4.2.min.js",
                        "~/Scripts/html2canvas.min.js",
                        "~/Scripts/history.js/history.js",
                        "~/Scripts/jquery.showLoading.min.js",
                        "~/Scripts/_customsite.min.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
						"~/Scripts/modernizr-*"
                        ));

			bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
			          "~/Scripts/popper.min.js",
                      "~/Scripts/toolkit.min.js"
                      /*
			          "~/Scripts/util.min.js",
                      "~/Scripts/affix.min.js",
                      "~/Scripts/alert.min.js",
					  "~/Scripts/button.min.js",
                      "~/Scripts/carousel.min.js",
					  "~/Scripts/collapse.min.js",
					  "~/Scripts/dropdown.min.js",
					  "~/Scripts/image-grid.min.js",
                      "~/Scripts/modal.min.js",
                      "~/Scripts/popover.min.js",
                      "~/Scripts/tab.min.js",
                      "~/Scripts/tooltip.min.js",
		              "~/Scripts/zoom.min.js"
                      */
                      ));

			bundles.Add(new StyleBundle("~/Content/css").Include(
					  "~/Content/Styles/bootstrap.css",
					  "~/Content/Styles/toolkit.css",
					  "~/Content/Styles/font-awesome.css",
                      "~/Content/Styles/site.css",
                      "~/Content/Styles/cryptocoins.css",
                      "~/Content/Styles/cryptocoins-colors.css"
                      ));
		}
	}
}
