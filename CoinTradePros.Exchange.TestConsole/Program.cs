﻿using System;
using System.Collections.Generic;
using CoinTradePros.Exchange.Bittrex;
using CoinTradePros.Interfaces.Exchange;
using System.IO;

namespace CoinTradePros.Exchange.TestConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            IProvideExchangeData tradeProvider = new BittrexTradeProvider();
            /*IExchangeRequest request = new BaseExchangeRequest();
            request.ApiKey = "d17fb9ed3a9a41e69e3b28ee311fbdf2";
            request.ApiSecret = "ec397c95220a496b9e3e92e7079fa1c6";

            ICollection<IExchangeOrder> orders = tradeProvider.GetOrders(request);

            foreach (IExchangeOrder order in orders)
            {
                Console.WriteLine($"Order Data: {order.OrderId}");
                foreach (IOrderSegment segment in order.Segments)
                {
                    Console.WriteLine($"\tOrder Segment:");
                    Console.WriteLine($"\t\tCondition: {segment.Condition}");
                    Console.WriteLine($"\t\tCondition Target: {segment.ConditionTarget}");
                    Console.WriteLine($"\t\tExchange: {segment.Exchange}");
                    Console.WriteLine($"\t\tLimit: {segment.Limit:F8}");
                    Console.WriteLine($"\t\tOrder Type: {segment.OrderType}");
                    Console.WriteLine($"\t\tPrice: {segment.Price}");
                    Console.WriteLine($"\t\tPrice per Unit: {segment.PricePerUnit}");
                    Console.WriteLine($"\t\tQuantity: {segment.Quantity}");
                    Console.WriteLine($"\t\tQuantity Remaining: {segment.QuantityRemaining:F8}");
                    Console.WriteLine($"\t\tTimestamp: {segment.TimeStamp}");
                }
            }*/

            FileStream csvFile = File.Open(@"C:\bittrex.csv", FileMode.Open);
            ICollection<IExchangeOrder> orders = tradeProvider.GetOrders(csvFile);
            csvFile.Close();

            foreach (IExchangeOrder order in orders)
            {
                Console.WriteLine($"\tOrder Data:");
                Console.WriteLine($"\t\tClosed Date: {order.CloseDate.ToString()}");
                Console.WriteLine($"\t\tOpened Date: {order.OpenDate.ToString()}");
                Console.WriteLine($"\t\tMarket: {order.Market}");
                Console.WriteLine($"\t\tType: {order.OrderType}");
                Console.WriteLine($"\t\tBid / Ask: {order.BidAsk}");
                Console.WriteLine($"\t\tUnits Filled: {order.UnitsFilled}");
                Console.WriteLine($"\t\tUnits Total: {order.UnitsFilled}");
                Console.WriteLine($"\t\tActual Rate: {order.ActualRate}");
                Console.WriteLine($"\t\tCost / Proceeds: {order.CostProceeds}");
            }
        }
    }
}
