﻿using System.Collections.Generic;

namespace CoinTradePros.Exchange.Kraken
{
    public class ApiResult
    {
        public Dictionary<string, OrderItem> trades { get; set; }
        public int count { get; set; }
    }
}
