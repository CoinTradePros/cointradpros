﻿using System;
using System.Collections.Generic;
using CoinTradePros.Interfaces.Exchange;

namespace CoinTradePros.Exchange.Kraken
{
    public class OrderItem
    {
        public string ordertxid { get; set; }
        public string pair { get; set; }
        public double time { get; set; }
        public string type { get; set; }
        public string ordertype { get; set; }
        public decimal price { get; set; }
        public decimal cost { get; set; }
        public decimal fee { get; set; }
        public decimal vol { get; set; }
        public decimal margin { get; set; }
        public string misc { get; set; }
        public string closing { get; set; }
        public string count { get; set; }
    }
}
