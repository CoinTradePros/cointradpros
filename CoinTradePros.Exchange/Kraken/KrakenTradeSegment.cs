﻿using System;
using CoinTradePros.Interfaces.Exchange;

namespace CoinTradePros.Exchange.Kraken
{
    public class KrakenTradeSegment : IOrderSegment
    {
        public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }

        public KrakenTradeSegment(OrderItem item)
        {
            TimeStamp = UnixTimeStampToDateTime(item.time);
            OrderId = item.ordertxid;
            Exchange = item.pair;
            OrderType = item.ordertype;
            Commission = item.fee;
            Price = item.cost;
            PricePerUnit = item.price;
            Quantity = item.vol;
        }
        public KrakenTradeSegment()
        {
        }

        private string _orderUuid;
        public IExchangeOrder ExchangeOrder { get; set; }

        public string OrderUuid
        {
            get { return _orderUuid; }
            set { _orderUuid = value; }
        }

        public object OrderId
        {
            get { return _orderUuid; }
            set { _orderUuid = value.ToString(); }
        }
        public string Exchange { get; set; }
        public DateTime TimeStamp { get; set; }
        public string OrderType { get; set; }
        public decimal Limit { get; set; }
        public decimal Quantity { get; set; }
        public decimal QuantityRemaining { get; set; }
        public decimal Commission { get; set; }
        public decimal Price { get; set; }
        public decimal PricePerUnit { get; set; }
        public bool IsConditional { get; set; }
        public string Condition { get; set; }
        public string ConditionTarget { get; set; }
        public bool ImmediateOrCancel { get; set; }
    }
}
