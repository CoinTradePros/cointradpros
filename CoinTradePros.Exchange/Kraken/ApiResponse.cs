﻿using System.Collections.Generic;

namespace CoinTradePros.Exchange.Kraken
{
    class ApiResponse
    {
        public List<string> error { get; set; }
        public ApiResult result { get; set; }
    }
}
