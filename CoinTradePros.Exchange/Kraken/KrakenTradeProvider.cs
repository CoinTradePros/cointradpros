﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using CoinTradePros.Interfaces.Exchange;
using Newtonsoft.Json;
using CoinTradePros.Interfaces.Exchange.DataImport;
using System.Linq;

namespace CoinTradePros.Exchange.Kraken
{
    public class KrakenTradeProvider : IProvideExchangeData
    {
        const string ApiBaseUrl = "https://api.kraken.com";
        const string ApiVersion = "0";
        const string ApiMethod = "TradesHistory";
        private byte[] sha256_hash(String value)
        {
            using (SHA256 hash = SHA256Managed.Create())
            {
                Encoding enc = Encoding.UTF8;

                Byte[] result = hash.ComputeHash(enc.GetBytes(value));

                return result;
            }
        }

        private byte[] getHash(byte[] keyByte, byte[] messageBytes)
        {
            using (var hmacsha512 = new HMACSHA512(keyByte))
            {

                Byte[] result = hmacsha512.ComputeHash(messageBytes);

                return result;

            }
        }
        
        public ExchangeApiRequestType GetApiAuthType()
        {
            return ExchangeApiRequestType.KeyPair;
        }

        public string GetExchangeName()
        {
            return "Kraken";
        }

        public ICollection<IExchangeOrder> GetOrders(IExchangeRequest tradeRequest)
        {
            string path = string.Format("/{0}/private/{1}", ApiVersion, ApiMethod);
            string address = ApiBaseUrl + path;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(address);

            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";

            request.Headers.Add("API-Key", tradeRequest.ApiKey);

            DateTime now = DateTime.UtcNow;
            now = now.AddHours(8);
            var nonce = now.Ticks;

            byte[] base64DecodedSecred = Convert.FromBase64String(tradeRequest.ApiSecret);

            var props = string.Format("&ofs={0}&type={1}&trades={2}", "", "all", false);
            props = "nonce=" + nonce + props;
            var np = nonce + Convert.ToChar(0) + props;

            var pathBytes = Encoding.UTF8.GetBytes(path);
            var hash256Bytes = sha256_hash(np);
            var z = new byte[pathBytes.Count() + hash256Bytes.Count()];
            pathBytes.CopyTo(z, 0);
            hash256Bytes.CopyTo(z, pathBytes.Count());

            var signature = getHash(base64DecodedSecred, z);

            request.Headers.Add("API-Sign", Convert.ToBase64String(signature));

            if (props != null)
            {
                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(props);
                }
            }

            IDictionary<object, IExchangeOrder> orderTable = new Dictionary<object, IExchangeOrder>();
            using (var response = (HttpWebResponse)request.GetResponse())
            {
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    Stream responseStream = response.GetResponseStream();

                    if (responseStream == null)
                    {
                        return orderTable.Values;
                    }
                    using (var sr = new StreamReader(responseStream))
                    {
                        var content = sr.ReadToEnd();
                        ApiResponse resp = JsonConvert.DeserializeObject<ApiResponse>(content);

                        if (resp.error.Count == 0)
                        {
                            foreach (var orderItem in resp.result.trades)
                            {
                                OrderItem item = orderItem.Value;

                                if (!orderTable.ContainsKey(item.ordertxid))
                                {
                                    IExchangeOrder order = new KrakenOrder();
                                    order.OrderId = item.ordertxid;
                                    order.Segments.Add(new KrakenTradeSegment(item));
                                    orderTable.Add(item.ordertxid, order);
                                }
                                else
                                {
                                    IExchangeOrder order = orderTable[item.ordertxid];
                                    order.Segments.Add(new KrakenTradeSegment(item));
                                }
                            }
                            return orderTable.Values;
                        }
                        return null;
                    }
                }
                throw new Exception("Error - StatusCode=" + response.StatusCode + " Call Details=" + address);
            }
        }
        public ICollection<IExchangeOrder> GetOrders(Stream incomingData)
        {
            IDictionary<object, IExchangeOrder> orderTable = new Dictionary<object, IExchangeOrder>();

            using (CsvFileReader reader = new CsvFileReader(incomingData))
            {
                CsvRow row = new CsvRow();
                while (reader.ReadRow(row))
                {
                    KrakenTradeSegment item = new KrakenTradeSegment();

                    int index = 0;
                    foreach (string s in row)
                    {
                        if (s.Contains("txid"))
                        {
                            break;
                        }
                        else
                        {
                            if (index == 1)
                            {
                                item.OrderId = s;
                            }
                            if (index == 2)
                            {
                                item.Exchange = s;
                            }
                            if (index == 3)
                            {
                                item.TimeStamp = DateTime.Parse(s);
                            }
                            if (index == 5)
                            {
                                item.OrderType = s;
                            }
                            if (index == 6)
                            {
                                item.PricePerUnit = decimal.Parse(s);
                            }
                            if (index == 7)
                            {
                                item.Price = decimal.Parse(s);
                            }
                            if (index == 8)
                            {
                                item.Commission = decimal.Parse(s);
                            }
                            if (index == 9)
                            {
                                item.Quantity = decimal.Parse(s);
                            }
                        }
                        index++;
                    }

                    if(item.OrderId != null)
                    {
                        if (!orderTable.ContainsKey(item.OrderId))
                        {
                            IExchangeOrder order = new KrakenOrder();
                            order.OrderId = item.OrderId;
                            order.Segments.Add(item);
                            orderTable.Add(item.OrderId, order);
                        }
                        else
                        {
                            IExchangeOrder order = orderTable[item.OrderId];
                            order.Segments.Add(item);
                        }
                    }
                }
            }

            return orderTable.Values;
        }
    }
}
