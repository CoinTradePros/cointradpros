﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoinTradePros.Exchange.Bitfinex;
using CoinTradePros.Exchange.Bittrex;
using CoinTradePros.Interfaces.Exchange;
using CoinTradePros.Exchange.Poloniex;

namespace CoinTradePros.Exchange
{
  public  static class ExchangeProviderFactory
    {
        public static IProvideExchangeData GetExchangeProvider(ExchangeType type)
        {
            switch (type)
            {
                case ExchangeType.Bitfinex:
                {
                    return new BitfinexTradeProvider();
                }
                case ExchangeType.Bittrex:
                {
                    return new BittrexTradeProvider();
                }
                case ExchangeType.Poloniex:
                    {
                        return new PoloniexTradeProvider();
                    }
                default:
                        throw new NotImplementedException($"Exchange type {type} not supported.");
            }
        }
    }
}
