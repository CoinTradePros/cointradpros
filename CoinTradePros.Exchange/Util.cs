﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoinTradePros.Exchange
{
    class Util
    {
        public static double UnixNowUTC()
        {
            long unixTimeStamp;
            DateTime now = DateTime.Now.ToUniversalTime();
            DateTime unixEpoch = new DateTime(1970, 1, 1);
            unixTimeStamp = (long)((now.Subtract(unixEpoch)).TotalMilliseconds * 1000000D);
            return unixTimeStamp;
        }
        public static double UnixPoloniexUTC()
        {
            long unixTimeStamp;
            DateTime now = DateTime.UtcNow;
            DateTime unixEpoch = new DateTime(1970, 1, 1);
            unixTimeStamp = (long)((now.Subtract(unixEpoch)).TotalMilliseconds) * 10;
            return unixTimeStamp;
        }
        public static double UnixTimeStampUTC(DateTime dt)
        {
            double unixTimeStamp;
            dt = dt.ToUniversalTime();
            DateTime unixEpoch = new DateTime(1970, 1, 1);
            unixTimeStamp = (double)(dt.Subtract(unixEpoch)).TotalSeconds;
            return unixTimeStamp;
        }
        public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }
    }
}
