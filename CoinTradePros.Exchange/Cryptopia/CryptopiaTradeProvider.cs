﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using CoinTradePros.Interfaces.Exchange;
using Newtonsoft.Json;
using CoinTradePros.Interfaces.Exchange.DataImport;
using System.Net.Http;
using System.Net.Http.Headers;

namespace CoinTradePros.Exchange.Cryptopia
{
    public class CryptopiaTradeProvider : IProvideExchangeData
    {
        private const string ApiCallTemplate = "https://www.cryptopia.co.nz/api/{0}";
        private const string ApiMethod = "GetTradeHistory";

        public ICollection<IExchangeOrder> GetOrders(IExchangeRequest tradeRequest)
        {
            HttpClient _client = HttpClientFactory.Create(new AuthDelegatingHandler(
                tradeRequest.ApiKey, tradeRequest.ApiSecret));

            var url = string.Format(ApiCallTemplate, ApiMethod);

            var respTask = _client.PostAsJsonAsync<ApiRequest>(url, new ApiRequest());
            respTask.Wait();

            var resultTask = respTask.Result.Content.ReadAsAsync<ApiResponse>();
            resultTask.Wait();

            IDictionary<object, IExchangeOrder> orderTable = new Dictionary<object, IExchangeOrder>();
            foreach (var orderItem in resultTask.Result.Data)
            {
                if (!orderTable.ContainsKey(orderItem.TradeId))
                {
                    IExchangeOrder order = new CryptopiaOrder();
                    order.OrderId = orderItem.TradeId;
                    order.Segments.Add(new CryptopiaTradeSegement(orderItem));
                    orderTable.Add(order.OrderId, order);
                }
                else
                {
                    IExchangeOrder order = orderTable[orderItem.TradeId];
                    order.Segments.Add(new CryptopiaTradeSegement(orderItem));
                }
            }

            return orderTable.Values;
        }

        public string GetExchangeName()
        {
            return "Cryptopia";
        }

        public ExchangeApiRequestType GetApiAuthType()
        {
            return ExchangeApiRequestType.KeyPair;
        }

        public ICollection<IExchangeOrder> GetOrders(Stream incomingData)
        {
            return null;
        }
    }
}
