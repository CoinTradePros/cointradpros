﻿using System;
using CoinTradePros.Interfaces.Exchange;

namespace CoinTradePros.Exchange.Cryptopia
{
    public class CryptopiaTradeSegement : IOrderSegment
    {
        public CryptopiaTradeSegement(OrderItem item)
        {
            OrderId = item.TradeId;
            TradePairId = item.TradePairId;
            Exchange = item.Market;
            OrderType = item.Type;
            Rate = item.Rate;
            Quantity = item.Amount;
            Price = item.Total;
            Commission = item.Fee;
            TimeStamp = item.TimeStamp;
        }

        private string _orderUuid;
        public IExchangeOrder ExchangeOrder { get; set; }

        public string OrderUuid
        {
            get { return _orderUuid; }
            set { _orderUuid = value; }
        }

        public object OrderId {
            get { return _orderUuid; }
            set { _orderUuid = value.ToString(); }
        }
        public string Exchange { get; set; }
        public DateTime TimeStamp { get; set; }
        public string OrderType { get; set; }
        public decimal Limit { get; set; }
        public decimal Quantity { get; set; }
        public decimal QuantityRemaining { get; set; }
        public decimal Commission { get; set; }
        public string CommissionCurrency { get; set; }
        public decimal Price { get; set; }
        public decimal PricePerUnit { get; set; }
        public bool IsConditional { get; set; }
        public string Condition { get; set; }
        public string ConditionTarget { get; set; }
        public bool ImmediateOrCancel { get; set; }
        public int TradePairId { get; set; }
        public decimal Rate { get; set; }
    }
}
