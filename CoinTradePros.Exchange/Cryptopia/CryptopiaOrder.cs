﻿using System;
using System.Collections.Generic;
using CoinTradePros.Interfaces.Exchange;

namespace CoinTradePros.Exchange.Cryptopia
{
    public class CryptopiaOrder : IExchangeOrder
    {
        public CryptopiaOrder()
        {
            Segments = new List<IOrderSegment>();
        }

        public string OrderUuid { get; set; }
        public string Exchange { get; set; }
        public DateTime OpenDate { get; set; }

        public DateTime CloseDate { get; set; }

        public ExchangeOrderType OrderType { get; set; }

        public string Market { get; set; }

        public decimal BidAsk { get; set; }

        public decimal UnitsFilled { get; set; }

        public decimal UnitsTotal { get; set; }

        public decimal ActualRate { get; set; }

        public decimal CostProceeds { get; set; }

        public ICollection<IOrderSegment> Segments { get; set; }

        public object OrderId
        {
            get { return OrderUuid; }

            set { OrderUuid = value?.ToString() ?? string.Empty;
            }
        }
    }
}
