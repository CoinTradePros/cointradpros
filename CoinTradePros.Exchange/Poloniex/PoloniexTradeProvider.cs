﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using CoinTradePros.Interfaces.Exchange;
using Newtonsoft.Json;
using CoinTradePros.Interfaces.Exchange.DataImport;

namespace CoinTradePros.Exchange.Poloniex
{
    public  class PoloniexTradeProvider : IProvideExchangeData
    {
        private const string ApiCallTemplate = "https://poloniex.com/{0}";
        private const string ApiMethod = "tradingApi";        

        public  ICollection<IExchangeOrder> GetOrders(IExchangeRequest tradeRequest)
        {
            var nonce = DateTime.Now.Ticks;

            var uri = string.Format(ApiCallTemplate, ApiMethod);
            var request = WebRequest.Create(uri);
            request.ContentType = "application/x-www-form-urlencoded";
            request.Method = "POST";

            request.Headers.Add("Key", tradeRequest.ApiKey);

            var uTime = Util.UnixPoloniexUTC();
            string myParam = "command=returnTradeHistory&currencyPair=all&start=1501545600&nonce=" + uTime;
            var sign = genHMAC(myParam, tradeRequest.ApiSecret);
            request.Headers.Add("Sign", sign);

            byte[] bytes = Encoding.UTF8.GetBytes(myParam);
            request.ContentLength = bytes.Length;
            Stream requestStream = request.GetRequestStream();
            requestStream.Write(bytes, 0, bytes.Length);

            IDictionary<object, IExchangeOrder> orderTable = new Dictionary<object, IExchangeOrder>();
            using (var response = (HttpWebResponse)request.GetResponse())
            {
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    Stream responseStream = response.GetResponseStream();

                    if (responseStream == null)
                    {
                        return orderTable.Values;
                    }
                    using (var sr = new StreamReader(responseStream))
                    {
                        var content = sr.ReadToEnd();
                        var contents = JsonConvert.DeserializeObject<IDictionary<string, IList<OrderItem>>>(content);

                        foreach (string exchange in contents.Keys)
                        {
                            foreach (var orderItem in contents[exchange])
                            {
                                if (!orderTable.ContainsKey(orderItem.globalTradeID))
                                {
                                    IExchangeOrder order = new PoloniexOrder();
                                    order.OrderId = orderItem.globalTradeID;
                                    order.Segments.Add(new PoloniexTradeSegement(orderItem, exchange));
                                    orderTable.Add(order.OrderId, order);
                                }
                                else
                                {
                                    IExchangeOrder order = orderTable[orderItem.globalTradeID];
                                    order.Segments.Add(new PoloniexTradeSegement(orderItem, exchange));
                                }
                            }
                        }

                        return orderTable.Values;
                    }
                }
                throw new Exception("Error - StatusCode=" + response.StatusCode + " Call Details=" + uri);
            }
        }

        private string genHMAC(string message, string apisecret)
        {
            var hmac = new HMACSHA512(Encoding.ASCII.GetBytes(apisecret));
            var messagebyte = Encoding.ASCII.GetBytes(message);
            var hashmessage = hmac.ComputeHash(messagebyte);
            var sign = BitConverter.ToString(hashmessage).Replace("-", "").ToLower();
            return sign;
        }

        public string GetExchangeName()
        {
            return "Poloniex";
        }

        public ExchangeApiRequestType GetApiAuthType()
        {
            return ExchangeApiRequestType.KeyPair;
        }

        public ICollection<IExchangeOrder> GetOrders(Stream incomingData)
        {
            return null;
        }
    }
}
