﻿using System;
using CoinTradePros.Interfaces.Exchange;
using Newtonsoft.Json;

namespace CoinTradePros.Exchange.Poloniex
{
    public class OrderItem
    {
        public string globalTradeID { get; set; }
        public string tradeID { get; set; }
        public DateTime date { get; set; }
        public decimal rate { get; set; }
        public decimal amount { get; set; }
        public decimal total { get; set; }
        public decimal fee { get; set; }
        public string orderNumber { get; set; }
        public string type { get; set; }
        public string category { get; set; }
    }
}
