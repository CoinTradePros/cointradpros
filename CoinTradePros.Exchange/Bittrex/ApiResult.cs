﻿using System.Collections.Generic;

namespace CoinTradePros.Exchange.Bittrex
{
    class ApiResult
    {
        public bool success { get; set; }
        public string message { get; set; }
        public List<BittrexTradeSegment> Result { get; set; }
    }
}
