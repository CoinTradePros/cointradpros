﻿using System;
using CoinTradePros.Interfaces.Exchange;

namespace CoinTradePros.Exchange.Bitfinex
{
    public class BitfinexTradeSegment : IOrderSegment
    {
        public BitfinexTradeSegment(OrderItem item)
        {
            TimeStamp = Util.UnixTimeStampToDateTime(item.Timestamp);
            OrderId = item.Tid;
            Exchange = item.Exchange;
            OrderType = item.Type;
            Commission = item.FeeAmount;
            CommissionCurrency = item.FeeCurrency;
            Price = item.Price;
            Quantity = item.Amount;
        }
        public BitfinexTradeSegment()
        {
        }    
        private string _orderUuid;
        public IExchangeOrder ExchangeOrder { get; set; }

        public string OrderUuid
        {
            get { return _orderUuid; }
            set { _orderUuid = value; }
        }

        public object OrderId {
            get { return _orderUuid; }
            set { _orderUuid = value.ToString(); }
        }
        public string Exchange { get; set; }
        public DateTime TimeStamp { get; set; }
        public string OrderType { get; set; }
        public decimal Limit { get; set; }
        public decimal Quantity { get; set; }
        public decimal QuantityRemaining { get; set; }
        public decimal Commission { get; set; }
        public string CommissionCurrency { get; set; }
        public decimal Price { get; set; }
        public decimal PricePerUnit { get; set; }
        public bool IsConditional { get; set; }
        public string Condition { get; set; }
        public string ConditionTarget { get; set; }
        public bool ImmediateOrCancel { get; set; }
    }
}
