﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using CoinTradePros.Interfaces.Exchange;
using Newtonsoft.Json;
using CoinTradePros.Interfaces.Exchange.DataImport;
using RestSharp;

namespace CoinTradePros.Exchange.Bitfinex
{
    enum myPairTypeEnum
    {
        btcusd = 0,
        ltcusd = 1,
        ltcbtc = 2,
        ethusd = 3,
        ethbtc = 4,
        etcbtc = 5,
        etcusd = 6,
        rrtusd = 7,
        rrtbtc = 8,
        zecusd = 9,
        zecbtc = 10,
        xmrusd = 11,
        xmrbtc = 12,
        dshusd = 13,
        dshbtc = 14,
        bccbtc = 15,
        bcubtc = 16,
        bccusd = 17,
        bcuusd = 18,
        xrpusd = 19,
        xrpbtc = 20,
        iotusd = 21,
        iotbtc = 22,
        ioteth = 23,
        eosusd = 24,
        eosbtc = 25,
        eoseth = 26,
        sanusd = 27,
        sanbtc = 28,
        saneth = 29,
        omgusd = 30,
        omgbtc = 31,
        omgeth = 32,
        bchusd = 33,
        bchbtc = 34,
        bcheth = 35,
        neousd = 36,
        neobtc = 37,
        neoeth = 38,
        etpusd = 39,
        etpbtc = 40,
        etpeth = 41,
        qtmusd = 42,
        qtmbtc = 43,
        qtmeth = 44,
        bt1usd = 45,
        bt2usd = 46,
        bt1btc = 47,
        bt2btc = 48,
        avtusd = 49,
        avtbtc = 50,
        avteth = 51
    }

    public class BitfinexTradeProvider : IProvideExchangeData
    {
        private const string ApiCallTemplate = "https://api.bitfinex.com/{0}/{1}";
        private const string ApiVersion = "v1";
        private const string ApiMethod = "mytrades";

        private const string ApiBfxKey = "X-BFX-APIKEY";
        private const string ApiBfxPayload = "X-BFX-PAYLOAD";
        private const string ApiBfxSig = "X-BFX-SIGNATURE";

        public ICollection<IExchangeOrder> GetOrders(IExchangeRequest tradeRequest)
        {
            var symbols = Enum.GetValues(typeof(myPairTypeEnum));

            IDictionary<object, IExchangeOrder> orderTable = new Dictionary<object, IExchangeOrder>();

            using (var rateGate = new RateGate(1, TimeSpan.FromSeconds(1.5)))
            {
                foreach (var symbol in symbols)
                {
                    rateGate.WaitToProceed();

                    var obj = new ApiRequest();
                    obj.Request = '/' + ApiVersion + '/' + ApiMethod;
                    obj.Nonce = Util.UnixNowUTC().ToString();
                    obj.Symbol = symbol.ToString();
                    obj.Timestamp = Util.UnixTimeStampUTC(new DateTime(2010, 1, 1)).ToString();
                    obj.Limit = 1000;

                    var client = new RestClient();
                    var url = string.Format(ApiCallTemplate, ApiVersion, ApiMethod);
                    client.BaseUrl = new Uri(url);

                    var jsonObj = JsonConvert.SerializeObject(obj);
                    var payload = Convert.ToBase64String(Encoding.UTF8.GetBytes(jsonObj));
                    var request = new RestRequest();
                    request.Method = Method.POST;
                    request.AddHeader(ApiBfxKey, tradeRequest.ApiKey);
                    request.AddHeader(ApiBfxPayload, payload);
                    request.AddHeader(ApiBfxSig, GetHexHashSignature(tradeRequest.ApiSecret, payload));

                    var response = client.Execute(request);

                    var contents = JsonConvert.DeserializeObject<IList<OrderItem>>(response.Content);

                    foreach (var orderItem in contents)
                    {
                        orderItem.Exchange = obj.Symbol;
                        if (!orderTable.ContainsKey(orderItem.OrderId))
                        {
                            IExchangeOrder order = new BitfinexOrder();
                            order.OrderId = orderItem.OrderId;
                            order.Segments.Add(new BitfinexTradeSegment(orderItem));
                            orderTable.Add(order.OrderId, order);
                        }
                        else
                        {
                            IExchangeOrder order = orderTable[orderItem.OrderId];
                            order.Segments.Add(new BitfinexTradeSegment(orderItem));
                        }
                    }
                }
            }

            return orderTable.Values;
        }
        private string GetHexHashSignature(string apisecret, string payload)
        {
            HMACSHA384 hmac = new HMACSHA384(Encoding.UTF8.GetBytes(apisecret));
            byte[] hash = hmac.ComputeHash(Encoding.UTF8.GetBytes(payload));
            return BitConverter.ToString(hash).Replace("-", "").ToLower();
        }

        public string GetExchangeName()
        {
            return "Bitfinex";
        }

        public ExchangeApiRequestType GetApiAuthType()
        {
            return ExchangeApiRequestType.KeyPair;
        }

        public ICollection<IExchangeOrder> GetOrders(Stream incomingData)
        {
            List<IExchangeOrder> orderList = new List<IExchangeOrder>();

            using (CsvFileReader reader = new CsvFileReader(incomingData))
            {
                CsvRow row = new CsvRow();
                while (reader.ReadRow(row))
                {
                    IExchangeOrder order = new BitfinexOrder();
                    BitfinexTradeSegment item = new BitfinexTradeSegment();

                    int index = 0;
                    foreach (string s in row)
                    {
                        if (s.Contains("#"))
                        {
                            break;
                        }
                        else
                        {
                            if (index == 0)
                            {
                                item.OrderId = s;
                            }
                            if (index == 1)
                            {
                                item.Exchange = s;
                            }
                            if (index == 2)
                            {
                                item.Quantity = decimal.Parse(s);
                            }
                            if (index == 3)
                            {
                                item.Price = decimal.Parse(s);
                            }
                            if (index == 4)
                            {
                                item.TimeStamp = DateTime.Parse(s);
                            }
                        }
                        index++;
                    }

                    order.Segments.Add(item);
                    orderList.Add(order);
                }
            }

            return orderList;
        }
    }
}
