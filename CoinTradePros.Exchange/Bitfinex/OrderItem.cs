﻿using System;
using CoinTradePros.Interfaces.Exchange;
using Newtonsoft.Json;

namespace CoinTradePros.Exchange.Bitfinex
{
    public class OrderItem
    {
        [JsonProperty("price")]
        public decimal Price { get; set; }

        [JsonProperty("amount")]
        public decimal Amount { get; set; }

        [JsonProperty("timestamp")]
        public double Timestamp { get; set; }

        [JsonProperty("exchange")]
        public string Exchange { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("fee_currency")]
        public string FeeCurrency { get; set; }

        [JsonProperty("fee_amount")]
        public decimal FeeAmount { get; set; }

        [JsonProperty("tid")]
        public string Tid { get; set; }

        [JsonProperty("order_id")]
        public string OrderId { get; set; }
    }
}
