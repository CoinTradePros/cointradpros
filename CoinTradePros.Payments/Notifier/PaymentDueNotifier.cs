﻿using CoinTradePros.BusinessObjects;
using CoinTradePros.Messaging.Email;
using Quartz;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tiraggo.Interfaces;

namespace CoinTradePros.Payments
{
    public class PaymentDueNotifier : IJob
    {
        public async Task Execute(IJobExecutionContext context)
        {
              
            var paymentNotifyDay = int.Parse(ConfigurationManager.AppSettings["PaymentNotifyDay"]);
            var invoiceList = CryptoInvoice.GetAllInvoices()
                              .Where(d=>d.DueDate < DateTime.Now.AddDays(paymentNotifyDay) 
                              && d.PaymentStatus == 0 && d.NotificationCount == 0);
          
            var cryptoInvoice = new CryptoInvoice();
            foreach (var invoice in invoiceList)
            {
                await SendNotification(invoice);
                cryptoInvoice.UpdateNotificationCount(1, invoice.Id.Value);
            }
        }

        private async Task SendNotification(CryptoInvoice invoice)
        {
            var emailService = new EmailNotificationService();
            var userId = GetSubscriptionView(invoice.SubscriptionId).SubscriberUserId;
            if (userId == null)
            {
                return;
            }
            var userEmail = AspNetUsers.GetUserByUserId(userId).Email;

            var messageBody = $"Subscription number:{invoice.SubscriptionId}" +
                                $"<br/> Current bill total: {invoice.PaymentAmount} <br/> Required payment date: {invoice.DueDate}";
            await emailService.SendMessageAsync(messageBody, "Your Subscription bill is now available", true, userEmail);

        }

        private SubscriptionView GetSubscriptionView(Guid? subscriptionId)
        {
            var sub = new SubscriptionView();
            var sq = new SubscriptionViewQuery("s");
            sq.Select().Where(sq.SubscriptionId == subscriptionId.Value);
            sub.Load(sq);
            return sub;
        }
    }
}
