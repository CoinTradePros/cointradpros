﻿namespace CoinTradePros.Payments
{
    public class CoinPaymentsTxRequest
    {
        public decimal Amount { get; set; }

        public string Currency1 { get; set; }

        public string Currency2 { get; set; }
        
        public string BuyerEmail { get; set; }

        public string BuyerName { get; set; }

        public string ItemName { get; set; }

        public string ItemNumber { get; set; }

        public string IpnUrl { get; set; }

        public string Custom { get; set; }
    }
}