﻿using System;
using System.Runtime.Remoting.Messaging;
using CoinTradePros.Helpers;

namespace CoinTradePros.Payments
{
    public class CurrencyConverter
    {
        private CoinPaymentsAsset _baseCurrency;
        private CoinPaymentsAsset _secondaryCurrency;
        private readonly CoinPaymentsHelper _helper;

        public CurrencyConverter()
        {
            _helper = new CoinPaymentsHelper();
        }

        public void SetBaseCurrency(string currencyCode)
        {
            var coin = _helper.GetCoin(currencyCode);
            if (coin == null)
            {
                throw new InvalidOperationException($"No currency was found for code: {currencyCode}");
            }
            _baseCurrency = coin;
        }

        public void SetBaseCurrency(CoinPaymentsAsset baseCurrency)
        {
            _baseCurrency = baseCurrency;
        }

        public void SetSecondaryCurrency(string currencyCode)
        {
            CoinPaymentsAsset coin = _helper.GetCoin(currencyCode);
            if (coin == null)
            {
                throw new InvalidOperationException($"No currency was found for code: {currencyCode}");
            }
            _secondaryCurrency = coin;
        }

        public void SetSecondaryCurrency(CoinPaymentsAsset secondaryCurrency)
        {
            _secondaryCurrency = secondaryCurrency;
        }

        

        public decimal Convert()
        {
            if (_baseCurrency == null)
            {
                throw new InvalidOperationException("The base currency must be set before calling this method.");
            }
            if (_secondaryCurrency == null)
            {
                throw new InvalidOperationException("The secondary currency must be set before calling this method.");
            }
            return Convert(_baseCurrency, _secondaryCurrency);
        }

        public decimal Convert(CoinPaymentsAsset secondCurrency)
        {
            if (_baseCurrency == null)
            {
                throw new InvalidOperationException("The base currency must be set before calling this method.");
            }
            return Convert(_baseCurrency, secondCurrency);
        }

        public decimal Convert(string currency1Code, string currency2Code)
        {
            CoinPaymentsAsset coin1 = _helper.GetCoin(currency1Code);
            CoinPaymentsAsset coin2 = _helper.GetCoin(currency2Code);


            return Convert(coin1, coin2);
        }

        public decimal Convert(CoinPaymentsAsset currency1, CoinPaymentsAsset currency2)
        {
            /* 
             * example:
             * BTC to USD
             * BTC/USD = USD price for BTC
             * 
             */
            if (currency1 == null)
            {
                throw new ArgumentNullException(nameof(currency1));
            }
            if (currency2 == null)
            {
                throw new ArgumentNullException(nameof(currency2));
            }
            if (currency2.RateBtc == 0)
            {
                return 0;
            }
            return currency1.RateBtc / currency2.RateBtc;
        }

        public decimal GetUsdBtcPrice()
        {
            return Convert("BTC", "USD");
        }

        public decimal GetUsdLtcPrice()
        {
            return Convert("LTC", "USD");
        }

        public decimal GetUsdEthPrice()
        {
            return Convert("ETH", "USD");
        }
        
    }
}