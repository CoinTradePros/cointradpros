﻿using System.Collections.Generic;

namespace CoinTradePros.Payments
{
    public class CoinPaymentsTransaction
    {
        public decimal Amount { get; set; }

        public string Address { get; set; }

        public string TxId { get; set; }

        public int ConfirmsNeeded { get; set; }

        public int Timeout { get; set; }

        public string StatusUrl { get; set; }

        public string QrCodeUrl { get; set; }

        public void Fill(Dictionary<string, object> data)
        {
            decimal d;
            int e;
            foreach (var prop in data)
            {
                if (prop.Key == "amount")
                {
                    if (decimal.TryParse(prop.Value.ToString(), out d))
                    {
                        Amount = d;
                    }
                }
                if (prop.Key == "address")
                {
                    Address = prop.Value.ToString();
                }
                if (prop.Key == "txn_id")
                {
                    TxId = prop.Value.ToString();
                }
                if (prop.Key == "confirms_needed")
                {
                    if (int.TryParse(prop.Value.ToString(), out e))
                    {
                        ConfirmsNeeded = e;
                    }
                }
                if (prop.Key == "timeout")
                {
                    if (int.TryParse(prop.Value.ToString(), out e))
                    {
                        Timeout = e;
                    }
                }
                if (prop.Key == "status_url")
                {
                    StatusUrl = prop.Value.ToString();
                }
                if (prop.Key == "qrcode_url")
                {
                    QrCodeUrl = prop.Value.ToString();
                }
            }
        }
    }
}