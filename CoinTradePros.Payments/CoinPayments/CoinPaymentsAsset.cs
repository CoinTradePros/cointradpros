﻿using System.Collections.Generic;

namespace CoinTradePros.Payments
{
    public class CoinPaymentsAsset
    {
        public string Code { get; set; }
        public bool IsFiat { get; set; }
        public decimal RateBtc { get; set; }

        public double LastUpdate { get; set; }

        public decimal TxFee { get; set; }
        
        public string Name { get; set; }

        public int Confirms { get; set; }

        public bool CanConvert { get; set; }

        public void Fill(KeyValuePair<string, object> values)
        {
            Code = values.Key;
            decimal d;
            int e;
            foreach (var prop in (Dictionary<string, object>)values.Value)
            {
                if (prop.Key == "is_fiat")
                {
                    IsFiat = prop.Value.ToString() == "1";
                }
                if (prop.Key == "last_update")
                {

                }
                if (prop.Key == "rate_btc")
                {
                    if (decimal.TryParse(prop.Value.ToString(), out d))
                    {
                        RateBtc = d;
                    }
                }
                if (prop.Key == "tx_fee")
                {
                    if (decimal.TryParse(prop.Value.ToString(), out d))
                    {
                        TxFee = d;
                    }
                }
                if (prop.Key == "name")
                {
                    Name = prop.Value.ToString();
                }
                if (prop.Key == "confirms")
                {
                    if (int.TryParse(prop.Value.ToString(), out e))
                    {
                        Confirms = e;
                    }
                }
                if (prop.Key == "can_convert")
                {
                    CanConvert = prop.Value.ToString() == "1";
                }
            }
        }
    }
}