﻿using System;
using System.Configuration;
using CoinTradePros.BusinessObjects;
using Stripe;

namespace CoinTradePros.Payments.Stripe
{
    public class StripeHelper
    {

        private readonly string _interval;

        public StripeHelper(string interval)
        {
            _interval = interval;
        }

        public bool NeedsStripPlan()
        {
            return false;
        }

        public string CheckCustomer(string email, string stripeToken)
        {
            Subscription subscription = new Subscription();
            StripeCustomerService customerService = new StripeCustomerService(GetApiKey());
            StripeCustomer customer;
            if (subscription.LoadByEmail(email))
            {
                customer = customerService.Get(subscription.StripeCustomerId);
            }
            else
            {
                // create the new customer record on the Stripe service
                customer = CreateCustomer(email, stripeToken);
            }
            return customer.Id;
        }

        public string SubscribeCustomer(string customerId, string planId)
        {
            StripeSubscriptionService service = new StripeSubscriptionService(GetApiKey());
            var subscriptionOptions = new StripeSubscriptionCreateOptions();
            subscriptionOptions.CustomerId = customerId;
            subscriptionOptions.PlanId = planId;
            StripeSubscription subscription = service.Create(customerId, subscriptionOptions);
            return subscription.Id;
        }

        public string CreateCharge(decimal paymentAmount, string customerId, string description)
        {
            var charges = new StripeChargeService();

            var charge = charges.Create(new StripeChargeCreateOptions
            {
                Amount = (int)paymentAmount * 100,
                Description = description,
                Currency = "usd",
                CustomerId = customerId
            });
            return charge.Id;
        }

        public string CheckPlan(Package package)
        {
            if (package == null)
            {
                throw new ArgumentNullException(nameof(package));
            }
            if (package.Id == null)
            {
                throw new ArgumentException("The package must have an id to create a Stripe plan");
            }
            if (!package.IsRecurringInterval(_interval))
            {
                // the package/interval does not need a stripe plan since it is a one time payment.
                throw new InvalidOperationException($"The package {package.Id} is not recurring for {_interval}, so does not need a Stripe plan.  Use Package.IsRecurringInterval to check.");
            }
            PackagePlanAdapter adapter = new PackagePlanAdapter();
            string planName = adapter.GetPlanName(package, _interval);
            StripePlanService service = new StripePlanService(GetApiKey());
            StripePlan plan = null;
            try
            {
                plan = service.Get(planName);
            }
            catch (StripeException ex)
            {
                string str = ex.Message;
                if (!str.ToLower().Contains("no such plan"))
                {
                    throw;
                }
            }
            if (plan == null)
            {
                PlanOptions options = adapter.GetPlanOptions(package, _interval);
                plan = CreatePlan(options);
            }
            return plan.Id;
        }

        private StripeCustomer CreateCustomer(string email, string stripeToken)
        {
            var customers = new StripeCustomerService();
            var cust = customers.Create(new StripeCustomerCreateOptions
            {
                Email = email,
                SourceToken = stripeToken
            });
            return cust;
        }

        private StripePlan CreatePlan(PlanOptions options)
        {
            StripePlanService service = new StripePlanService(GetApiKey());

            var planOptions = new StripePlanCreateOptions()
            {
                Id = options.Id,
                Name = options.Name,
                Amount = options.Amount,
                Currency = options.Currency,
                Interval = options.Interval,

            };
            planOptions.IntervalCount = options.IntervalCount;
            planOptions.StatementDescriptor = options.StatementDescriptor;
            
            StripePlan newplan = service.Create(planOptions);
            return newplan;
        }

        private string GetApiKey()
        {
            return ConfigurationManager.AppSettings["StripeSecretKey"];
        }
    }
}